<?php

namespace Teamcrop\Rest;

class ProductInventoryAlert extends Base
{
    public static $serviceurl = '/v1/productinventoryalerts';

    const TYPE_CRITICAL = 1;
    const TYPE_WARNING = 3;
    const TYPE_NOTICE = 5;

    public $cid = 0;
    public $pid = 0;
    public $poid = 0;
    public $swid = 0;
    public $pmid = 0;
    public $id = 0;
    public $instock = 0;
    public $alerttype = 0;
    public $isold = 0;
    public $datecreated = 0;
    public $datemodified = 0;

    public function __construct($id = 0, $loadFromCache = false, $asyncName = '')
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id, $loadFromCache);
            } else {
                $this->getData($id, $loadFromCache);
            }
        }
    }

    public function addData(&$error = array())
    {
        $data = array(
            'creator_id' => $this->uid,
            'company_id' => $this->cid,
            'product_id' => $this->pid,
            'product_variant_id' => $this->poid,
            'warehouse_id' => $this->swid,
            'product_inventory_monitor_id' => $this->pmid,
            'instock' => $this->instock,
            'alert_type' => $this->alerttype,
            'is_old' => $this->isold
        );

        $this->id = self::doAdd($data, '', '', $error);

        return $this->id;
    }

    public function updateData(&$error = array())
    {
        $data = array(
            'creator_id' => $this->uid,
            'company_id' => $this->cid,
            'product_id' => $this->pid,
            'product_variant_id' => $this->poid,
            'warehouse_id' => $this->swid,
            'product_inventory_monitor_id' => $this->pmid,
            'instock' => $this->instock,
            'alert_type' => $this->alerttype,
            'is_old' => $this->isold
        );

        $url = self::$serviceurl . '/' . $this->id;
        return self::doUpdate($data, $url, '', $error);
    }

    public function delete(&$error = array())
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }

    /**
     * @param $formData
     * @param string $sortby
     * @param string $sorttype
     * @param string $limitString
     * @param bool|false $countOnly
     * @return array
     */
    public static function getProductInventoryAlerts(
        $formData,
        $sortby = '',
        $sorttype = '',
        $limitString = '',
        $countOnly = false)
    {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }
    }

    public function getJsonData()
    {
        $data = array(
            'company_id' => (int)$this->cid,
            'product_id' => (int)$this->pid,
            'product_variant_id' => (int)$this->poid,
            'warehouse_id' => (int)$this->swid,
            'product_inventory_monitor_id' => (int)$this->pmid,
            'id' => (int)$this->id,
            'instock' => (int)$this->instock,
            'alert_type' => (int)$this->alerttype,
            'is_old' => (int)$this->isold,
            'date_created' => (int)$this->datecreated,
            'date_modified' => (int)$this->datemodified
        );

        return $data;
    }

    public static function getAlerttypeList()
    {
        $output = array();

        $output[self::TYPE_CRITICAL] = ' Critical';
        $output[self::TYPE_WARNING] = 'Warning';
        $output[self::TYPE_NOTICE] = 'Notice';


        return $output;
    }

    public static function cacheKey($departmentid)
    {
        return 'tc_productinventoryalert_' . $departmentid;
    }

    public function getDataByArrayCache($row)
    {
        $this->cid = (int)$row['company_id'];
        $this->pid = (int)$row['product_id'];
        $this->poid = (int)$row['product_variant_id'];
        $this->swid = (int)$row['warehouse_id'];
        $this->pmid = (int)$row['product_inventory_monitor_id'];
        $this->id = (int)$row['id'];
        $this->instock = (int)$row['instock'];
        $this->alerttype = (int)$row['alert_type'];
        $this->isold = (int)$row['is_old'];
        $this->datecreated = (int)$row['date_created'];
        $this->datemodified = (int)$row['date_modified'];
    }
}
