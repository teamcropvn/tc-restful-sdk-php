<?php

namespace Teamcrop\Rest;

class Activity extends Base
{
    public function __construct($id = 0, $loadFromCache = false)
    {
        if ($id > 0) {
            if ($loadFromCache) {

            } else {
                $this->getData($id);
            }
        }

        parent::__construct();
    }

    public function addData()
    {

    }

    public function updateData()
    {

    }

    public function getData($id)
    {

    }

    public function delete()
    {

    }

    private static function countList($where, $bindParams)
    {

    }

    private static function getList($where, $order, $limit, $bindParams)
    {

    }

    public static function getUsers($formData, $sortby, $sorttype, $limit = '', $countOnly = false)
    {

    }

    public function getJsonData()
    {

    }
}