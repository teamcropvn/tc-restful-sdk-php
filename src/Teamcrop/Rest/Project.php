<?php

namespace Teamcrop\Rest;

class Project extends Base
{
    public static $serviceurl = '/v1/projects';

    const STATUS_ENABLE = 1;
    const STATUS_WORKING = 3;
    const STATUS_COMPLETED = 5;
    const STATUS_DISABLE = 7;

    const DROPBOX_PERMISSION_READ = 1;
    const DROPBOX_PERMISSION_READWRITE = 3;

    const GOOGLEDRIVE_PERMISSION_READ = 1;
    const GOOGLEDRIVE_PERMISSION_READWRITE = 3;

    public $cid = 0;
    public $uid = 0;
    public $id = 0;
    public $name = '';
    public $description = '';
    public $setting = array();
    public $clientcompany = 0;
    public $counttodostory = 0;
    public $countdoingstory = 0;
    public $countdonestory = 0;
    public $countstory = 0;
    public $counttodobug = 0;
    public $countdoingbug = 0;
    public $countdonebug = 0;
    public $countbug = 0;
    public $storyprefix = '';
    public $latestiteration = 0;
    public $lateststory = 0;
    public $latestbug = 0;
    public $latestactivity = 0;
    public $rgbcolor = '';
    public $status = 0;
    public $wikiid = 0;
    public $pinlist = array();
    public $rolepermission = array();
    public $isscrum = 0;
    public $isdeleted = 0;
    public $ipaddress = 0;
    public $dropboxaccesstoken = '';
    public $dropboxaccesspermission = 0;
    public $dropboxaccessrootpath = '';
    public $googledriveaccesstoken = '';
    public $googledriveaccesspermission = 0;
    public $googledriveaccessrootpath = '';
    public $datecreated = 0;
    public $datemodified = 0;
    public $datedeleted = 0;
    public $datestarted = 0;
    public $datecompletedplan = 0;
    public $datecompleted = 0;

    public $product_owner = array();
    public $development_team = array();
    public $scrum_master = array();

    public function __construct($id = 0, $loadFromCache = false)
    {
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData(&$error = array())
    {
        $data = array(
            'creator_id' => $this->uid,
            'company_id' => $this->cid,
            'is_scrum' => $this->isscrum,
            'name' => $this->name,
            'description' => $this->description,
            'color' => $this->color,
            'status' => $this->status,
            'date_started' => $this->datestarted,
            'date_completed_plan' => $this->datecompletedplan,
            'product_owner_add_story' => in_array(ProjectRole::ROLE_PRODUCT_OWNER, $this->rolepermission['story']),
            'development_team_add_story' => in_array(ProjectRole::ROLE_DEVELOPMENT_TEAM, $this->rolepermission['story']),
            'scrum_master_add_story' => in_array(ProjectRole::ROLE_SCRUMMASTER, $this->rolepermission['story']),
            'setting' => $this->setting,
            'product_owner' => $this->product_owner,
            'development_team' => $this->development_team,
            'scrum_master' => $this->scrum_master,
        );

        $this->id = self::doAdd($data, '', '', $error);

        return $this->id;
    }

    public function updateData(&$error = array())
    {
        $data = array(
            'creator_id' => $this->uid,
            'company_id' => $this->cid,
            'is_scrum' => $this->isscrum,
            'name' => $this->name,
            'description' => $this->description,
            'color' => $this->color,
            'status' => $this->status,
            'date_started' => $this->datestarted,
            'date_completed_plan' => $this->datecompletedplan,
            'product_owner_add_story' => in_array(ProjectRole::ROLE_PRODUCT_OWNER, $this->rolepermission['story']),
            'development_team_add_story' => in_array(ProjectRole::ROLE_DEVELOPMENT_TEAM, $this->rolepermission['story']),
            'scrum_master_add_story' => in_array(ProjectRole::ROLE_SCRUMMASTER, $this->rolepermission['story']),
            'setting' => $this->setting,
            'product_owner' => $this->product_owner,
            'development_team' => $this->development_team,
            'scrum_master' => $this->scrum_master,
        );

        $url = self::$serviceurl . '/' . $this->id;
        return self::doUpdate($data, $url, '', $error);
    }


    public function delete(&$error = array())
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }

    public static function getProjects($formData, $sortby = '', $sorttype = '', $limitString = '', $countOnly = false)
    {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
        	return self::getItems($formData, $sortby, $sorttype, $limitString);
        }
    }

    public function getJsonData()
    {
        $data = array(
            'id' => (int) $this->id,
            'creator_id' => (int) $this->uid,
            'company_id' => (int) $this->cid,
            'name' => (string) $this->name,
            'description' => (string) $this->description,
            'count_bug' => (int) $this->countbug,
            'count_story' => (int) $this->countstory,
            'count_activity' => (int) $activityCount,
            'complete_percent' => (int) $completepercent,
            'color' => (string) $this->rgbcolor,
            'count_member' => count($members),
            'members' => $members,
            'product_owner' => $productOwnerIdList,
            'development_team' => $developmentTeamIdList,
            'scrum_master' => $scrumMasterIdList,
            'story_prefix' => (string) $this->storyprefix,
            'is_scrum' => (int) $this->isscrum,
            'is_disabled' => $this->checkStatusName('disable') ? 1 : 0,
            'is_completed' => $this->checkStatusName('completed') ? 1 : 0,
            'wiki_id' => (int) $this->wikiid,
            'status' => (int) $this->status,
            'duration' => (int) $duration,
            'setting' => $this->setting,
            'role_per_mission' => $this->getRolePermission(),
            'date_created' => (int) $this->datecreated,
            'date_modified' => (int) $this->datemodified,
            'date_started' => (int) $this->datestarted,
            'date_completed_plan' => (int) $this->datecompletedplan
        );

        return $data;
    }

    public function getDataByJson($row)
    {
        $this->id = (int)$row['id'];
        $this->uid = (int)$row['creator_id'];
        $this->cid = (int)$row['company_id'];
        $this->name = (string)$row['name'];
        $this->description = (string)$row['description'];
        $this->countbug = (int)$row['count_bug'];
        $this->countstory = (int)$row['count_story'];
        $this->countactivity = (int)$row['count_activity'];
        $this->completepercent = (int)$row['complete_percent'];
        $this->rgbcolor = (string)$row['color'];
        $this->countmember = (int)$row['count_member'];
        $this->members = $row['members'];
        $this->productowner = $row['product_owner'];
        $this->developmentteam = $row['development_team'];
        $this->scrummaster = $row['scrum_master'];
        $this->storyprefix = (string)$row['story_prefix'];
        $this->isscrum = (int)$row['is_scrum'];
        $this->isdisabled = (int)$row['is_disabled'];
        $this->iscompleted = (int)$row['is_completed'];
        $this->status = (int)$row['status'];
        $this->wikiid = (int)$row['wiki_id'];
        $this->duration = (int)$row['duration'];
        $this->setting = $row['setting'];
        $this->rolepermission = $row['role_per_mission'];
        $this->datecreated = (int)$row['date_created'];
        $this->datemodified = (int)$row['date_modified'];
        $this->datestarted = (int)$row['date_started'];
        $this->datecompletedplan = (int)$row['date_completed_plan'];
    }
}
