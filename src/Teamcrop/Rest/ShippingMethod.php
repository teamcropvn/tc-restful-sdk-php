<?php

namespace Teamcrop\Rest;

class ShippingMethod extends Base
{
    public static $serviceurl = '/v1/shippingmethods';

    const APITYPE_EMPTY = 0;
    const APITYPE_GIAOHANGNHANH = 1;
    const APITYPE_SHIPCHUNG = 3;
    const APITYPE_PROSHIP = 5;
    const APITYPE_VIETTELPOST = 7;
    const APITYPE_TRUNGTHANH = 9;
    const APITYPE_VNPOST = 11;

    const APISETTING_SANDBOX_YES= 1;
    const APISETTING_SANDBOX_NO = 0;

    const APISETTING_SERVICE_SLOW= 1;
    const APISETTING_SERVICE_FAST = 2;

    const APISETTING_COD_YES = 1;
    const APISETTING_COD_NO = 2;

    const APISETTING_PROTECTED_YES = 1;
    const APISETTING_PROTECTED_NO = 2;

    const APISETTING_CHECKING_YES = 1;
    const APISETTING_CHECKING_NO = 2;

    const APISETTING_FRAGILE_YES = 1;
    const APISETTING_FRAGILE_NO = 2;

    const APISETTING_PAYMENT_ME = 1;
    const APISETTING_PAYMENT_BUYER = 2;

    const APISETTING_SHIPPING_METHOD_SAVING = 1;
    const APISETTING_SHIPPING_METHOD_HYPERSPEED = 2;
    const APISETTING_SHIPPING_METHOD_QUICK_DELIVER = 3;
    const APISETTING_SHIPPING_METHOD_BY_DAY = 4;
    const APISETTING_SHIPPING_METHOD_NIGHT_SUPERFAST = 5;

    const APISETTING_ADDITION_SERVICE_INSURRANCE = 1;
    const APISETTING_ADDITION_SERVICE_COLLECT = 2;
    const APISETTING_ADDITION_SERVICE_TIMER = 3;

    const APISETTING_SHIPMENT_TYPE_COLLECT = 3;
    const APISETTING_SHIPMENT_TYPE_NO_COLLECT = 1;

    const APISETTING_SERVICE_CODE_V60 = 'V60';
    const APISETTING_SERVICE_CODE_VBK = 'VBK';
    const APISETTING_SERVICE_CODE_VCN = 'VCN';

    const APISETTING_PRODUCT_TYPE_CODE_TH = 'TH';
    const APISETTING_PRODUCT_TYPE_CODE_HH = 'HH';

    const APISETTING_PAYMENT_TYPE_LAST_MONTH = 1;
    const APISETTING_PAYMENT_TYPE_CASH = 2;
    const APISETTING_PAYMENT_TYPE_PAYEE = 3;

    const APISETTING_SERVICE_EMS = 1;
    const APISETTING_SERVICE_PARCEL = 2;
    const APISETTING_SERVICE_MAILERS = 3;

    const STATUS_ENABLE = 1;
    const STATUS_DISABLE = 0;

    public $cid = 0;
    public $uid = 0;
    public $id = 0;
    public $name = '';
    public $discription = '';
    public $status = 0;
    public $displayorder = 0;
    public $apitype = 0;
    public $apisetting = array();
    public $datecreated = 0;
    public $datemodified = 0;

    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData(&$error = array())
    {
        $data = array(
            'creator_id' => $this->uid,
            'company_id' => $this->cid,
            'name' => $this->name,
            'display_order' => $this->displayorder,
            'api_type' => $this->apitype,
            'discription' => $this->discription
        );

        $this->id = self::doAdd($data, '', '', $error);

        return $this->id;
    }

    public function updateData(&$error = array())
    {
        $data = array(
            'creator_id' => $this->uid,
            'company_id' => $this->cid,
            'name' => $this->name,
            'display_order' => $this->displayorder,
            'api_type' => $this->apitype,
            'api_setting' => $this->apisetting,
            'status' => $this->status,
            'discription' => $this->discription
        );

        $url = self::$serviceurl . '/' . $this->id;
        return self::doUpdate($data, $url, '', $error);
    }

    public function delete(&$error = array())
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }

    public static function shippingmethods(
        $formData,
        $sortby = '',
        $sorttype = '',
        $limitString = '',
        $countOnly = false
    )
    {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }
    }

    public function getJsonData()
    {
        $data = array(
            'company_id' => $this->cid,
            'creator_id' => $this->uid,
            'id' => $this->id,
            'name' => $this->name,
            'discription' => $this->discription,
            'status' => $this->status,
            'display_order' => $this->displayorder,
            'api_type' => $this->apitype,
            'api_setting' => $this->apisetting,
            'date_created' => $this->datecreated,
            'date_modified' => $this->datemodified
        );

        return $data;
    }

    public function getDataByJson($jsonData)
    {
        $this->cid = (int)$jsonData['company_id'];
        $this->uid = (int)$jsonData['creator_id'];
        $this->id = (int)$jsonData['id'];
        $this->name = (string)$jsonData['name'];
        $this->discription = (string)$jsonData['discription'];
        $this->status = (int)$jsonData['status'];
        $this->displayorder = (int)$jsonData['display_order'];
        $this->apitype = (int)$jsonData['api_type'];
        $this->apisetting = $jsonData['api_setting'];
        $this->datecreated = (int)$jsonData['date_created'];
        $this->datemodified = (int)$jsonData['date_modified'];

    }
}
