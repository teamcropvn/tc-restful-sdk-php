<?php

namespace Teamcrop\Rest;

class CustomerSegmentDetail extends Base
{
    public static $serviceurl = '/v1/customersegmentdetails';

    const RULE_ID = 1;
    const RULE_GENDER = 3;
    const RULE_TYPE = 5;
    const RULE_REGION = 7;
    const RULE_SUBREGION = 9;
    const RULE_CODE = 11;
    const RULE_BIRTHDAY = 13;
    const RULE_PHONE = 15;
    const RULE_EMAIL = 17;
    const RULE_TAG = 19;
    const RULE_STATUS = 21;
    const RULE_ORIGIN = 23;
    const RULE_COUNTORDER = 25;
    const RULE_COUNTSKU = 27;
    const RULE_DATECREATED = 29;
    const RULE_DATEFIRSTORDERED = 31;
    const RULE_DATELASTORDERED = 33;

    const VALUETYPE_NUMBER = 1;
    const VALUETYPE_EXACTSTRING = 3;
    const VALUETYPE_CONTAINSTRING = 5;
    const VALUETYPE_REGEXSTRING = 7;

    public $cid = 0;
    public $uid = 0;
    public $ccsid = 0;
    public $id = 0;
    public $rule = 0;
    public $valuetype = 0;
    public $value = '';
    public $valueend = '';
    public $ipaddress = 0;
    public $datecreated = 0;
    public $datemodified = 0;

    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData(&$error = array())
    {
        $data = array(
            'creator_id' => $this->uid,
            'company_id' => $this->cid,
            'customer_segment_id' => $this->ccsid,
            'rule' => $this->rule,
            'value_type' => $this->valuetype,
            'value' => $this->value,
            'value_end' => $this->valueend
        );

        $this->id = self::doAdd($data, '', '', $error);

        return $this->id;
    }

    public function updateData(&$error = array())
    {
        $data = array(
            'creator_id' => $this->uid,
            'company_id' => $this->cid,
            'customer_segment_id' => $this->ccsid,
            'rule' => $this->rule,
            'value_type' => $this->valuetype,
            'value' => $this->value,
            'value_end' => $this->valueend
        );

        $url = self::$serviceurl . '/' . $this->id;
        return self::doUpdate($data, $url, '', $error);
    }

    public function delete(&$error = array())
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }

    public static function getCustomerSegmentDetails(
        $formData,
        $sortby = '',
        $sorttype = '',
        $limitString = '',
        $countOnly = false
    ) {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }

    }

    public function getFieldName()
    {
        $fields = array(
            self::RULE_ID => 'cc.cc_id',
            self::RULE_GENDER => 'cc.cc_gender',
            self::RULE_TYPE => 'cc.cct_id',
            self::RULE_REGION => 'cc.cc_region',
            self::RULE_SUBREGION => 'cc.cc_subregion',
            self::RULE_CODE => 'cc.cc_code',
            self::RULE_BIRTHDAY => 'cc.cc_birthday',
            self::RULE_PHONE => 'cc.cc_phone',
            self::RULE_EMAIL => 'cc.cc_email',
            // self::RULE_TAG => 'cc.cc_tag',
            self::RULE_STATUS => 'cc.cc_status',
            self::RULE_ORIGIN => 'cc.cc_origin',
            self::RULE_COUNTORDER => 'cc.cc_countorder',
            self::RULE_COUNTSKU => 'cc.cc_countsku',
            self::RULE_DATECREATED => 'cc.cc_datecreated',
            self::RULE_DATEFIRSTORDERED => 'cc.cc_datefirstordered',
            self::RULE_DATELASTORDERED => 'cc.cc_datelastordered',
        );

        return $fields[$this->rule];
    }

    public function getJsonData($full = true)
    {
        if ($full) {
            $data = array(
                'company_id' => (int)$this->cid,
                'creator_id' => (int)$this->uid,
                'customer_segment_id' => (int)$this->ccsid,
                'id' => (int)$this->id,
                'rule' => (int)$this->rule,
                'value_type' => (int)$this->valuetype,
                'value' => (string)$this->value,
                'value_end' => (string)$this->valueend,
                'ip_address' => (string)$this->ipaddress,
                'date_created' => (int)$this->datecreated,
                'date_modified' => (int)$this->datemodified
            );
        } else {
            $data = array(
                'id' => (int)$this->id,
                'rule' => (int)$this->rule,
                'value_type' => (int)$this->valuetype,
                'value' => (string)$this->value,
                'value_end' => (string)$this->valueend
            );
        }

        return $data;
    }

    public function getDataByJson($jsonData)
    {
        $this->id = (int)$jsonData['id'];
        $this->cid = (int)$jsonData['company_id'];
        $this->uid = (int)$jsonData['creator_id'];
        $this->ccsid = (int)$jsonData['customer_segment_id'];
        $this->rule = (int)$jsonData['rule'];
        $this->valuetype = (int)$jsonData['value_type'];
        $this->value = (string)$jsonData['value'];
        $this->valueend = (string)$jsonData['value_end'];
        $this->ipaddress = (int)$jsonData['ip_address'];
        $this->datecreated = (int)$jsonData['date_created'];
        $this->datemodifed = (int)$jsonData['date_modified'];
    }
}
