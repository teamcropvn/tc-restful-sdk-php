<?php

namespace Teamcrop\Rest;


/**
 * LocationRole Class
 *
 * File contains the class used for LocationRole Model
 *
 * @category Litpi
 * @package Model
 * @author Vo Duy Tuan <tuanmaster2012@gmail.com>
 * @copyright Copyright (c) 2013 - Litpi Framework (http://www.litpi.com)
 */
class StoreMember extends Base
{
    public static $serviceurl = '/v1/stores';
    const RANK_NORMAL = 0;
    const RANK_POS = 5;
    const RANK_MANAGER_LEVEL_ONE = 10;
    const RANK_MANAGER_LEVEL_TWO = 20;

    const STATUS_ENABLE = 1;
    const STATUS_DISABLE = 2;

    const IS_DELETE = 1;

    public $cid = 0;
    public $uid = 0;
    public $slid = 0;
    public $ssid = 0;
    public $id = 0;
    public $position = '';
    public $rank = 0;
    public $status = 0;
    public $isdeleted = 0;
    public $isdeletedby = 0;
    public $datecreated = 0;
    public $datemodified = 0;
    public $datedeleted = 0;

    public function __construct($id = 0)
    {
        parent::__construct();

        if ($id > 0) {
            $this->getData($id);
        }
    }

    public static function getMemberList($formData, $sortby = '', $sorttype = '', $limitString = '', $countOnly = false)
    {
        $url = self::$serviceurl . "/member";
        if ($countOnly) {
            return self::countItems($formData, $limitString, $url);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString, $url);
        }
    }
    public function delete(&$error = array())
    {

    }
    public function addData(&$error = array())
    {

    }

    public function updateData(&$error = array())
    {

    }

    public function getJsonData()
    {
        $data = array(
            $jsonData['company_id'] = (int)$this->cid,
            $jsonData['creator_id'] = (int)$this->uid,
            $jsonData['store_id'] = (int)$this->slid,
            $jsonData['id'] = (int)$this->id,
            $jsonData['position'] = (string)$this->position,
            $jsonData['rank'] = (string)$this->rank,
            $jsonData['status'] = (string)$this->status,
            $jsonData['is_deleted'] = (int)$this->isdeleted,
            $jsonData['address'] = (string)$this->isdeletedby,
            $jsonData['date_deleted'] = (int)$this->datedeleted,

        );

        return $data;
    }

    public function getDataByJson($jsonData)
    {
        $this->cid = (int)$jsonData['company_id'];
        $this->uid = (int)$jsonData['creator_id'];
        $this->slid = (int)$jsonData['store_id'];
        $this->id = (int)$jsonData['id'];
        $this->position = (string)$jsonData['position'];
        $this->rank = (string)$jsonData['rank'];
        $this->status = (string)$jsonData['status'];
        $this->isdeleted = (int)$jsonData['is_deleted'];
        $this->isdeletedby = (string)$jsonData['address'];
        $this->datedeleted = (int)$jsonData['date_deleted'];
    }
}
