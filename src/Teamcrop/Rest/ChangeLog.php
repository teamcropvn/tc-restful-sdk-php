<?php

namespace Teamcrop\Rest;

class ChangeLog extends Base
{
    public static $serviceurl = '/v1/changelogs';

    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 3;

    const FLAG_NONE = 1;
    const FLAG_NEW = 3;
    const FLAG_IMPORTANT = 5;

    const POPUP_TYPE_NONE = 1;
    const POPUP_TYPE_CONTENT = 3;
    const POPUP_TYPE_IMAGE = 5;
    const POPUP_TYPE_VIDEO = 7;

    public $creator_id;
    public $id;
    public $title;
    public $content;
    public $flag = 0;
    public $popuptype = 0;
    public $popupmediaurl = '';
    public $version = '1.0.0';
    public $url = '';
    public $status;
    public $date_created;
    public $date_modified;

    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            $this->getData($id, $loadFromCache);
        }
    }




    public function addData(&$error = array())
    {
        $data = array(
            'creator_id' => $this->creator_id,
            'title' => $this->title,
            'content' => $this->content,
            'status' => $this->status,
            'flag' => $this->flag,
            'popup_type' => $this->popuptype,
            'popup_media_url' => $this->popupmediaurl,
            'version' => $this->version,
            'url' => $this->url,
            'date_created' => $this->date_created,
            'date_modified' => $this->date_modified
        );

        $this->id = self::doAdd($data, '', '', $error);

        return $this->id;
    }

    public function updateData(&$error = array())
    {
        $data = array(
            'creator_id' => $this->creator_id,
            'title' => $this->title,
            'content' => $this->content,
            'flag' => $this->flag,
            'popup_type' => $this->popuptype,
            'popup_media_url' => $this->popupmediaurl,
            'version' => $this->version,
            'url' => $this->url,
            'status' => $this->status,
            'date_created' => $this->date_created,
            'date_modified' => $this->date_modified
        );
        $url = self::$serviceurl . '/' . $this->id;
        return self::doUpdate($data, $url, '', $error);
    }

    public function delete(&$error = array())
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }

    public static function getChangeLogs($formData, $sortby = '', $sorttype = '', $limitString = '', $countOnly = false)
    {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }
    }

    public function getDataByJson($row)
    {
        $this->creator_id = (int)$row['creator_id'];
        $this->id = (int)$row['id'];
        $this->title = (string)$row['title'];
        $this->content = (string)$row['content'];
        $this->flag = (int)$row['flag'];
        $this->popuptype = (int)$row['popup_type'];
        $this->popupmediaurl = (string)$row['popup_media_url'];
        $this->version = (string)$row['version'];
        $this->url = (string)$row['url'];
        $this->status = (int)$row['status'];
        $this->date_created = (int)$row['date_created'];
        $this->date_modified = (int)$row['date_modified'];
    }

    public function getJsonData()
    {
        $data = array(
            'creator_id' => (int)$this->creator_id,
            'id' => (int)$this->id,
            'title' => (string)$this->title,
            'content' => (string)$this->content,
            'flag' => (int)$this->flag,
            'popup_type' => (int)$this->popuptype,
            'popup_media_url' => (string)$this->popupmediaurl,
            'version' => (string)$this->version,
            'url' => (string)$this->url,
            'status' => (int)$this->status,
            'date_created' => (int)$this->date_created,
            'date_modified' => (int)$this->date_modified
        );

        return $data;
    }
}
