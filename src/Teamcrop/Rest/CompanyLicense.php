<?php

namespace Teamcrop\Rest;

class CompanyLicense extends Base
{
    public static $serviceurl = '/v1/companylicenses';

    const STATUS_ENABLE = 1;
    const STATUS_DISABLED = 3;

    public $uid = 0;
    public $cid = 0;
    public $id = 0;
    public $detail = '';
    public $note = '';
    public $status = 0;
    public $datecreated = 0;
    public $datemodified = 0;
    public $licenseList = array();

    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData(&$error = array())
    {
        $data = array(
            'user_id' => $this->uid,
            'company_id' => $this->cid,
            'name' => $this->name,
            'license_list' => $this->licenseList,
            'detail' => $this->detail,
            'note' => $this->note,
            'status' => $this->status
        );

        $this->id = self::doAdd($data, '', '', $error);

        return $this->id;
    }

    public function updateData(&$error = array())
    {
        $data = array(
            'user_id' => $this->uid,
            'company_id' => $this->cid,
            'name' => $this->name,
            'license_list' => $this->licenseList,
            'detail' => $this->detail,
            'note' => $this->note,
            'status' => $this->status
        );

        $url = self::$serviceurl . '/' . $this->id;
        return self::doUpdate($data, $url, '', $error);
    }

    public function delete(&$error = array())
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }

    public static function getCompanyLicenses($formData, $sortby = '', $sorttype = '', $limitString = '')
    {
        return self::getItems($formData, $sortby, $sorttype, $limitString);
    }

    public static function licenseMapping()
    {
        $mapping = array(
            1 => 'app.activity.*',
            2 => 'app.poll.*',
            3 => 'app.news.*',
            4 => 'app.calendar.*',
            5 => 'app.channel.*',
            6 => 'app.subchannel.*',
            7 => 'app.message.*',
            8 => 'app.project.*',
            9 => 'app.workflow.*',
            10 => 'app.file.*',
            11 => 'app.dropbox.*',
            12 => 'app.googledrive.*',
            13 => 'app.worktime.*',
            14 => 'app.onlineworktracking.*',
            15 => 'app.chat.*',
            16 => 'app.api.*',
            82 => 'sale.setting.index',
            17 => 'sale.setting.permission',
            18 => 'sale.setting.inventory',
            19 => 'sale.setting.pos',
            20 => 'sale.setting.cms',
            21 => 'sale.product.*',
            22 => 'sale.combo.*',
            23 => 'sale.promotion.*',
            24 => 'sale.order.*',
            25 => 'sale.customer.*',
            26 => 'sale.salepipeline.*',
            27 => 'sale.callcenter.*',
            28 => 'sale.location.*',
            29 => 'sale.productinventory.*',
            30 => 'sale.cashflow.*',
            31 => 'sale.collection.*',
            32 => 'sale.cmsproduct.*',
            33 => 'sale.cmspage.*',
            34 => 'sale.cmscontactform.*',
            35 => 'sale.cmsbanner.*',
            36 => 'sale.cmsproductcomment.*',
            37 => 'sale.cmspagecomment.*',
            38 => 'sale.report.dashboard',
            39 => 'sale.report.sale',
            40 => 'sale.report.cashflow',
            41 => 'sale.report.inventory',
            42 => 'sale.report.customer',
            43 => 'sale.report.salepipeline',
            44 => 'sale.report.googleanalytic',
            45 => 'sale.productpriceplanning.*',
            46 => 'sale.productpriceunit.*',
            47 => 'sale.orderreturn.*',
            48 => 'sale.orderreturnrequest.*',
            49 => 'sale.workshow.*',
            50 => 'sale.shippinggateway.*',
            51 => 'sale.rewardbysale.*',
            52 => 'sale.rewardbysku.*',
            53 => 'sale.menu.product',
            54 => 'sale.menu.promotion',
            55 => 'sale.menu.order',
            56 => 'sale.menu.customer',
            57 => 'sale.menu.location',
            58 => 'sale.menu.cmspage',
            59 => 'sale.menu.cmscontactform',
            60 => 'sale.menu.cmscomment',
            61 => 'sale.menu.cmsbanner',
            62 => 'sale.menu.inventory',
            63 => 'sale.menu.report',
            64 => 'sale.menu.mobileapp',
            65 => 'sale.team.*',
            66 => 'sale.newsletter.*',
            67 => 'sale.cmsintegration.woocommerce',
            68 => 'sale.cmsintegration.magento',
            69 => 'sale.cmsintegration.prestashop',
            70 => 'sale.cmsintegration.haravan',
            71 => 'sale.cmsintegration.shopify',
            72 => 'sale.mobileapp.device',
            73 => 'sale.mobileapp.pushnotification',
            74 => 'sale.stocklink.*',
            75 => 'sale.locationchecklist.*',
            76 => 'sale.employeepoint.*',
            77 => 'sale.supportticket.*',
            78 => 'sale.membercard.*',
            79 => 'sale.callcenter.*',
            80 => 'pos.index.*',
            81 => 'other.sms.*',
            83 => 'sale.supplier.*',
            84 => 'sale.happycare.*'
        );


        return $mapping;
    }


    public function getDataByJson($jsonData)
    {
        $this->uid = (int)$jsonData['user_id'];
        $this->cid = (int)$jsonData['company_id'];
        $this->id = (int)$jsonData['id'];
        $this->note = (string)$jsonData['note'];
        $this->status = (int)$jsonData['status'];
        $this->datecreated = (int)$jsonData['date_created'];
        $this->datemodified = (int)$jsonData['date_modified'];
        $this->licenseList = array();
        $this->licenses = $jsonData['licenses'];

        if (is_array($jsonData['licenses'])) {
            $licenseMapping = self::licenseMapping();
            foreach ($jsonData['licenses'] as $licenseNumber) {
                if (isset($licenseMapping[$licenseNumber])) {
                    $this->licenseList[] = $licenseMapping[$licenseNumber];
                }
            }
        }

        $this->detail = implode(',', $this->licenseList);
    }
}