<?php

namespace Teamcrop\Rest;

class Outputtype extends Base
{
    public static $serviceurl = '/v1/outputtypes';

    const STATUS_ENABLE = 1;
    const STATUS_DISABLE = 2;

    const IS_YES = 1;
    const IS_NO = 0;

    public $cid = 0;
    public $uid = 0;
    public $id = 0;
    public $name = '';
    public $issale = 0;
    public $isreward = 0;
    public $status = 0;
    public $displayorder = 0;
    public $isdelete = 0;
    public $isdeleteby = 0;
    public $datecreated = 0;
    public $datemodifed = 0;
    public $datedeleted = 0;


    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData()
    {

    }

    public function updateData()
    {

    }


    public function delete()
    {

    }

    public static function getProducts($formData, $sortby = '', $sorttype = '', $limitString = '', $countOnly = false)
    {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }

    }

    public function getJsonData()
    {
        $data = array(
            'id' => (int)$this->id,
            'company_id' => (int)$this->cid,
            'creator_id' => (int)$this->uid,
            'name' => (string)$this->name,
            'is_sale' => (int)$this->issale,
            'is_reward' => (int)$this->isreward,
            'status' => (int)$this->status,
            'display_order' => (int)$this->displayorder,
            'is_deleted' => (int)$this->isdeleted,
            'is_deleteby' => (int)$this->isdeleteby,
            'date_created' => (int)$this->datecreated,
            'date_modified' => (int)$this->datemodified,
            'date_deleted' => (int)$this->datedeleted
        );

        return $data;
    }

    public function getDataByJson($jsonData)
    {
        $this->id = (int)$jsonData['id'];
        $this->cid = (int)$jsonData['company_id'];
        $this->uid = (int)$jsonData['creator_id'];
        $this->name = (string)$jsonData['name'];
        $this->issale = (int)$jsonData['is_sale'];
        $this->isreward = (int)$jsonData['is_reward'];
        $this->status = (int)$jsonData['status'];
        $this->displayorder = (int)$jsonData['display_order'];
        $this->isdelete = (int)$jsonData['is_deleted'];
        $this->isdeleteby = (int)$jsonData['is_deleteby'];
        $this->datecreated = (int)$jsonData['date_created'];
        $this->datemodifed = (int)$jsonData['date_modified'];
        $this->datedeleted = (int)$jsonData['date_deleted'];
    }
}
