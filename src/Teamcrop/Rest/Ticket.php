<?php

namespace Teamcrop\Rest;

class Ticket extends Base
{
    public static $serviceurl = '/v1/tickets';

    const CHANNEL_DIRECT = 1;
    const CHANNEL_PHONE = 3;
    const CHANNEL_EMAIL = 5;
    const CHANNEL_CALLCENTER = 7;
    const CHANNEL_SMS = 9;
    const CHANNEL_FACEBOOK = 11;
    const CHANNEL_CHAT = 13;
    const CHANNEL_API = 15;
    const CHANNEL_MAIL = 17;
    const CHANNEL_WEBSTE = 19;

    const RELATEDTYPE_NONE = 0;
    const RELATEDTYPE_PRODUCT = 1;
    const RELATEDTYPE_ORDER = 3;
    const RELATEDTYPE_TICKET = 5;

    const PRIORITY_LOW = 1;
    const PRIORITY_NORMAL = 3;
    const PRIORITY_HIGH = 5;
    const PRIORITY_URGENT = 7;

    const STATUS_NEW = 1;
    const STATUS_OPEN = 3;
    const STATUS_PENDING = 5;
    const STATUS_SOLVED = 7;
    const STATUS_CLOSED = 9;
    const STATUS_SPAM = 11;

    public $uid = 0;
    public $cid = 0;
    public $ccid = 0;
    public $id = 0;
    public $channel = 0;
    public $email = '';
    public $phone = '';
    public $fullname = '';
    public $assignee = '';
    public $subject = '';
    public $content = '';
    public $note = '';
    public $priority = 0;
    public $tag = '';
    public $status = '';
    public $ipaddress = 0;
    public $useragent = '';



    public function __construct($id = 0, $loadFromCache = false)
    {
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }


    public function addData(&$error = array())
    {

        $data = array(
            'creator_id' => $this->uid,
            'company_id' => $this->cid,
            'customer_id' => $this->ccid,
            'channel' => $this->channel,
            'email' => $this->email,
            'phone' => $this->phone,
            'fullname' => $this->fullname,
            'assignee' => $this->assignee,
            'subject' => $this->subject,
            'content' => $this->content,
            'note' => $this->note,
            'tag' => $this->tag,
            'priority' => $this->priority,
            'status' => $this->status,
            'ip_address' => $this->ipaddress,
            'user_agent' => $this->useragent
        );

        $this->id = self::doAdd($data, '', '', $error);

        return $this->id;
    }

    public function updateData(&$error = array())
    {

    }


    public function delete(&$error = array())
    {

    }

    public static function getTickets($formData, $sortby = '', $sorttype = '', $limitString = '', $countOnly = false)
    {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
        	return self::getItems($formData, $sortby, $sorttype, $limitString);
        }
    }

    public function getJsonData()
    {

    }
}
