<?php

namespace Teamcrop\Rest;

class PriceZoneStore extends Base
{
    public static $serviceurl = '/v1/pricezonestores';

    const IS_DELETE = 1;

    public $ssid = 0;
    public $spzid = 0;
    public $id = 0;
    public $isdeleted = 0;
    public $isdeleteby = 0;
    public $datecreated = 0;
    public $datemodifed = 0;
    public $datedeleted = 0;

    public $store_list = array();


    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData(&$error = array())
    {
        $data = array(
            'creator_id' => $this->uid,
            'company_id' => $this->cid,
            'store_id' => $this->ssid,
            'price_zone_id' => $this->spzid
        );

        $this->id = self::doAdd($data, '', '', $error);

        return $this->id;
    }

    public function updateData(&$error = array())
    {
        $data = array(
            'creator_id' => $this->uid,
            'company_id' => $this->cid,
            'store_id' => $this->ssid,
            'price_zone_id' => $this->spzid
        );

        $url = self::$serviceurl . '/' . $this->id;
        return self::doUpdate($data, $url, '', $error);
    }

    public function delete(&$error = array())
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }

    public static function getPricezoneStores(
        $formData,
        $sortby = '',
        $sorttype = '',
        $limitString = '',
        $countOnly = false
    ) {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }

    }

    public function getJsonData()
    {
        $data = array(
            'id' => (int)$this->id,
            'store_id' => (int)$this->ssid,
            'price_zone_id' => (int)$this->spzid,
            'is_deleted' => (int)$this->isdeleted,
            'is_delete_by' => (int)$this->isdeleteby,
            'date_created' => (int)$this->datecreated,
            'date_modified' => (int)$this->datemodified,
            'date_deleted' => (int)$this->datedeleted
        );

        return $data;
    }

    public function getDataByJson($jsonData)
    {
        $this->id = (int)$jsonData['id'];
        $this->ssid = (int)$jsonData['store_id'];
        $this->spzid = (int)$jsonData['price_zone_id'];
        $this->isdelete = (int)$jsonData['is_deleted'];
        $this->isdeleteby = (int)$jsonData['is_delete_by'];
        $this->datecreated = (int)$jsonData['date_created'];
        $this->datemodifed = (int)$jsonData['date_modified'];
        $this->datedeleted = (int)$jsonData['date_deleted'];
    }
}
