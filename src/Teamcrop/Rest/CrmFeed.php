<?php

namespace Teamcrop\Rest;

class CrmFeed extends Base
{
    public static $serviceurl = '/v1/crmfeeds';

    const CATEGORY_NOTE = 1;
    const CATEGORY_ORDER = 3;
    const CATEGORY_EMAIL = 5;
    const CATEGORY_SMS = 7;
    const CATEGORY_VOICE = 9;
    const CATEGORY_TICKET = 11;
    const CATEGORY_EVENT = 13;
    const CATEGORY_TASK = 15;
    const CATEGORY_CHANGE_INFO = 17;
    const CATEGORY_CALLCENTER = 19;
    const CATEGORY_UPGRADE = 21;

    const STATUS_ENABLE = 1;
    const STATUS_DISABLED = 3;

    public $cid = 0;
    public $uid = 0;
    public $ccid = 0;
    public $id = 0;
    public $category = 0;
    public $type = 0;
    public $objectid = 0;
    public $title = '';
    public $summary = '';
    public $moredata = '';
    public $customerstatus = 0;
    public $status = 0;
    public $ipaddress = '';
    public $datecreated = 0;
    public $datemodified = 0;

    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData(&$error = array())
    {
        $data = array(
            'creator_id' => $this->uid,
            'company_id' => $this->cid,
            'customer_id' => $this->ccid,
            'category' => $this->category,
            'type' => $this->type,
            'object_id' => $this->object_id,
            'title' => $this->title,
            'summary' => $this->summary,
            'more_data' => $this->moredata,
            'customer_status' => $this->customerstatus,
        );

        $this->id = self::doAdd($data, '', '', $error);

        return $this->id;
    }

    public function updateData(&$error = array())
    {
        $data = array(
            'creator_id' => $this->uid,
            'company_id' => $this->cid,
            'title' => $this->title,
            'summary' => $this->summary,
            'more_data' => $this->moredata,
        );

        $url = self::$serviceurl . '/' . $this->id;
        return self::doUpdate($data, $url, '', $error);
    }


    public function delete(&$error = array())
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }

    public static function getCrmFeeds($formData, $sortby = '', $sorttype = '', $limitString = '', $countOnly = false)
    {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }
    }

    /**
     * @return array
     */
    public function getJsonData()
    {
        $data = array(
            'creator_id' => (int)$this->uid,
            'company_id' => (int)$this->cid,
            'customer_id' => (int)$this->ccid,
            'id' => (int)$this->id,
            'category' => (int)$this->category,
            'type' => (int)$this->type,
            'object_id' => (int)$this->objectid,
            'title' => (string)$this->title,
            'summary' => (string)$this->summary,
            'status' => (int)$this->status,
            'customer_status' => (int)$this->customerstatus,
            'ip_address' => (string)$this->ipaddress,
            'date_created' => (int)$this->datecreated,
            'date_modified' => (int)$this->datemodified
        );

        return $data;
    }

    public static function cacheKey($notifyid)
    {
        return 'tc_crm_feed_' . $notifyid;
    }

    public function getDataByArrayCache($row)
    {

    }

    public function getDataByJson($jsonData)
    {
        $this->cid = (int)$jsonData['company_id'];
        $this->uid = (int)$jsonData['creator_id'];
        $this->ccid = (int)$jsonData['customer_id'];
        $this->id = (int)$jsonData['id'];
        $this->category = (string)$jsonData['category'];
        $this->type = (string)$jsonData['type'];
        $this->objectid = (int)$jsonData['object_id'];
        $this->title = (string)$jsonData['title'];
        $this->summary = (string)$jsonData['summary'];
        $this->moredata = (string)$jsonData['more_data'];
        $this->status = (int)$jsonData['status'];
        $this->customerstatus = (int)$jsonData['customer_status'];
        $this->ipaddress = (string)$jsonData['ip_address'];
        $this->datecreated = (int)$jsonData['date_created'];
        $this->datemodified = (int)$jsonData['date_modified'];
    }
}
