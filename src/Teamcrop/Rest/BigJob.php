<?php

namespace Teamcrop\Rest;

class BigJob extends Base
{
    public static $serviceurl = '/v1/bigjobs';

    const MODE_AUTO = 1;
    const MODE_MANUAL = 3;

    const SCOPE_USER = 1;
    const SCOPE_COMPANY = 3;
    const SCROPE_SYSTEM = 5;

    const STATUS_PENDDING = 1;
    const STATUS_WORKING = 3;
    const STATUS_SUCCESS = 5;
    const STATUS_ERROR = 7;
    const STATUS_CANCEL = 9;

    public $cid = 0;
    public $uid = 0;
    public $id = 0;
    public $type = "";
    public $mode = 0;
    public $scope = 0;
    public $title = '';
    public $counttotal = 0;
    public $countdone = 0;
    public $checkpoint = 0;
    public $metadata = '';
    public $duration = 0;
    public $errordata = '';
    public $ipaddress = 0;
    public $fileidlist = '';
    public $workingfileidlist = '';
    public $status = 0;
    public $datecreated = 0;
    public $datemodified = 0;
    public $dateended = 0;
    public $dateexpired = 0;

    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData(&$error = array())
    {
        $row = $this->getJsonData();
        $this->id = self::doAdd($row, '', '', $error);

        return $this->id;
    }

    public function updateData(&$error = array())
    {
        $row = $this->getJsonData();
        $url = self::$serviceurl . '/' . $this->id;
        return self::doUpdate($row, $url, '', $error);
    }

    public function delete(&$error = array())
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }

    public static function getGroups(
        $formData,
        $sortby = '',
        $sorttype = '',
        $limitString = '',
        $countOnly = false
    ) {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }
    }


    public function getDataByJson($row)
    {
        $this->cid = (int)$row['c_id'];
        $this->uid = (int)$row['u_id'];
        $this->id = (int)$row['bj_id'];
        $this->type = (string)$row['bj_type'];
        $this->mode = (int)$row['bj_mode'];
        $this->scope = (int)$row['bj_scope'];
        $this->title = (string)$row['bj_title'];
        $this->counttotal = (int)$row['bj_counttotal'];
        $this->countdone = (int)$row['bj_countdone'];
        $this->checkpoint = (int)$row['bj_checkpoint'];
        $this->metadata = (string)$row['bj_metadata'];
        $this->duration = (int)$row['bj_duration'];
        $this->errordata = (string)$row['bj_errordata'];
        $this->ipaddress = (string) long2ip($row['bj_ipaddress']);
        $this->fileidlist = (string)$row['bj_fileidlist'];
        $this->workingfileidlist = (string)$row['bj_workingfileidlist'];
        $this->status = (int)$row['bj_status'];
        $this->datecreated = (int)$row['bj_datecreated'];
        $this->datemodified = (int)$row['bj_datemodified'];
        $this->dateended = (int)$row['bj_dateended'];
        $this->dateexpired = (int)$row['bj_dateexpired'];
    }

    public function getJsonData()
    {
        $row['company_id']  = (int)$this->cid;
        $row['creator_id']  = (int)$this->uid;
        $row['id']  = (int)$this->id;
        $row['type']  = (string)$this->type;
        $row['mode']  = (int)$this->mode;
        $row['scope']  = (int)$this->scope;
        $row['title']  = (string)$this->title;
        $row['count_total']  = (int)$this->counttotal;
        $row['count_done']  = (int)$this->countdone;
        $row['check_point']  = (int)$this->checkpoint;
        $row['meta_data']  = (string)$this->metadata;
        $row['duration']  = (int)$this->duration;
        $row['error_data']  = (string)$this->errordata;
        $row['file_id_list']  = (string)$this->fileidlist;
        $row['working_file_id_list']  = (string)$this->workingfileidlist;
        $row['status']  = (int)$this->status;
        $row['date_created']  = (int)$this->datecreated;
        $row['date_modified']  = (int)$this->datemodified;
        $row['date_ended']  = (int)$this->dateended;
        $row['date_expired']  = (int)$this->dateexpired;

        return $row;
    }


}
