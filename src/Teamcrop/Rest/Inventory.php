<?php

namespace Teamcrop\Rest;

class Inventory extends Base
{
    public static $serviceurl = '/v1/inventories';

    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData(&$error = array())
    {

    }

    public function updateData(&$error = array())
    {

    }

    public function delete(&$error = array())
    {

    }

    public static function getProductInventory($formData)
    {
        return self::getRawItems($formData, self::$serviceurl .'/sku');
    }
}
