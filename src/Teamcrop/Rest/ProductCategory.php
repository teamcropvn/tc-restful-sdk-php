<?php

namespace Teamcrop\Rest;

class ProductCategory extends Base
{
    public static $serviceurl = '/v1/productcategorys';

    const STATUS_ENABLE = 1;
    const STATUS_DISABLE = 2;

    const STATUS_YES = 1;
    const STATUS_NO = 0;

    public $cid = 0;
    public $uid = 0;
    public $id = 0;
    public $image = '';
    public $resourceserver = 0;
    public $name = '';
    public $slug = '';
    public $summary = '';
    public $seotitle = '';
    public $seokeyword = '';
    public $seodescription = '';
    public $metarobot = '';
    public $titlecol1 = '';
    public $desccol1 = '';
    public $titlecol2 = '';
    public $desccol2 = '';
    public $titlecol3 = '';
    public $desccol3 = '';
    public $topseokeyword = '';
    public $parentid = 0;
    public $countitem = 0;
    public $displayorder = 0;
    public $status = 0;
    public $isdeleteby = 0;
    public $datecreated = 0;
    public $datemodified = 0;
    public $datedeleted = 0;
    public $appendtoproductname = 0;

    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData()
    {

    }

    public function updateData()
    {

    }


    public function delete()
    {

    }

    public static function getCategorys($formData, $sortby = '', $sorttype = '', $limitString = '', $countOnly = false)
    {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }

    }

    public function getDataByJson($jsonData)
    {
        $this->id = (int)$jsonData['id'];
        $this->cid = (int)$jsonData['company_id'];
        $this->uid = (int)$jsonData['creator_id'];
        $this->name = (string)$jsonData['name'];
        $this->summary = (string)$jsonData['summary'];
        $this->parentid = (int)$jsonData['parent_id'];
        $this->displayorder = (int)$jsonData['display_order'];
        $this->status = (int)$jsonData['status'];
        $this->datecreated= (int)$jsonData['date_created'];
    }
}
