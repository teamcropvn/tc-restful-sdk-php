<?php

namespace Teamcrop\Rest;

class ProductCategory extends Base
{
    public static $serviceurl = '/v1/productcategorys';

    const STATUS_ENABLE = 1;
    const STATUS_DISABLE = 2;

    const STATUS_YES = 1;
    const STATUS_NO = 0;

    public $cid = 0;
    public $uid = 0;
    public $id = 0;
    public $image = '';
    public $resourceserver = 0;
    public $name = '';
    public $slug = '';
    public $summary = '';
    public $seotitle = '';
    public $seokeyword = '';
    public $seodescription = '';
    public $metarobot = '';
    public $titlecol1 = '';
    public $desccol1 = '';
    public $titlecol2 = '';
    public $desccol2 = '';
    public $titlecol3 = '';
    public $desccol3 = '';
    public $topseokeyword = '';
    public $parentid = 0;
    public $level = 0;
    public $countitem = 0;
    public $displayorder = 0;
    public $status = 0;
    public $isdeleteby = 0;
    public $datecreated = 0;
    public $datemodified = 0;
    public $datedeleted = 0;
    public $appendtoproductname = 0;

    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData()
    {

    }

    public function updateData()
    {

    }


    public function delete()
    {

    }
    public static function getChildlist($formData, $id)
    {
        return self::getItems($formData, "", "", "", self::$serviceurl . "/childlist/" . $id);
    }

    public static function getParentlist($formData, $id)
    {
        return self::getItems($formData, "", "", "", self::$serviceurl . "/parentlist/" . $id);
    }

    public static function getCategorys($formData, $sortby = '', $sorttype = '', $limitString = '', $countOnly = false)
    {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }

    }

    public function getDataByJson($jsonData)
    {
        $this->id = (int)$jsonData['id'];
        $this->cid = (int)$jsonData['company_id'];
        $this->uid = (int)$jsonData['creator_id'];
        $this->name = (string)$jsonData['name'];
        $this->summary = (string)$jsonData['summary'];
        $this->parentid = (int)$jsonData['parent_id'];
        $this->level = (int)$jsonData['level'];
        $this->displayorder = (int)$jsonData['display_order'];
        $this->status = (int)$jsonData['status'];
        $this->datecreated= (int)$jsonData['date_created'];
    }

    public function getJsonData()
    {
        $row['id'] = $this->id ;
        $row['company_id'] = $this->cid ;
        $row['creator_id'] = $this->uid ;
        $row['name'] = $this->name ;
        $row['summary'] = $this->summary ;
        $row['parent_id'] = $this->parentid ;
        $row['level'] = $this->level ;
        $row['display_order'] = $this->displayorder ;
        $row['status'] = $this->status ;
        $row['date_created'] = $this->datecreated;


        return $row;

    }

    /**
     * Get all product categories
     *
     * @param $companyId
     * @param array $error
     * @return array categories
     */
    public static function getFullData($companyId, &$error)
    {
        $categories = array();

        $serviceurl = self::$serviceurl . '/fulldata?company_id=' . $companyId;

        try {
            //Do request and get response with submit data
            $response = self::doRequest('GET', $serviceurl, array(), false, true);

            //request success
            if ($response['status'] == '200') {
                if (is_array($response['data'])) {
                    $categories = $response['data'];
                }

            } else {
                $error = self::parsingErrorFromResponse($response);
            }
        } catch (\Exception $e) {
            $error[] = $e->getMessage();
        }

        return $categories;
    }
}
