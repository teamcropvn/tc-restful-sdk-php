<?php

namespace Teamcrop\Rest;

class RbacSubjectObject extends Base
{
    public static $serviceurl = '/v1/rbacsubjectobjects';

    public $cid = 0;
    public $uid = 0;
    public $rsid = 0;
    public $id = 0;
    public $idlist = array();
    public $creatorid = 0;
    public $ipaddress = 0;
    public $datecreated = 0;
    public $datemodified = 0;


    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData()
    {

    }

    public function updateData()
    {

    }


    public function delete()
    {

    }

    public static function getRbacSubjectObjects($formData, $sortby = '', $sorttype = '', $limitString = '')
    {
        return self::getItems($formData, $sortby, $sorttype, $limitString);
    }

    public function getDataByJson($jsonData)
    {
        $this->cid = $jsonData['company_id'];
        $this->uid = $jsonData['user_id'];
        $this->rsid = $jsonData['subject_id'];
        $this->id = $jsonData['id'];
        $this->idlist = $jsonData['id_list'];
        $this->creatorid = $jsonData['creator_id'];
        $this->ipaddress = $jsonData['ip_address'];
        $this->datecreated = $jsonData['date_created'];
        $this->datemodified = $jsonData['date_modified'];
    }
}