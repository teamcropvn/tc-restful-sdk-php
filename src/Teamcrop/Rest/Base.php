<?php

namespace Teamcrop\Rest;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Promise;
use Litpi\Cacher;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Exception\BadResponseException;

abstract class Base extends \stdClass
{
    /**
     * Base URL of all restful request
     * This variable can be overwrite by environment variable 'TEAMCROP_REST_BASEURL'
     */
    public static $baseurl = 'https://qly.vn';

    /**
     * This property will be set on each model to point to correct api point
     * Full request url will be combine by baseurl and serviceurl.
     * If service url is start with 'http', the full request url will be not combined with base url
     */
    public static $serviceurl = '';

    /**
     * Toggle the debug mode for request
     *
     * @var bool
     */
    public static $debug = false;

    /**
     * This property will be set on almost request for authorization (in request headers)
     * This variable can be overwrite by environment variable 'TEAMCROP_REST_AUTH_TOKEN'
     */
    public static $authorizationToken = '';

    /**
     * This property will be set on almost request for inter-service request (in request headers)
     * This variable SHOULD be set by environment variable 'TEAMCROP_ACCESS_TRUSTED_KEY'
     * @var string
     */
    public static $accessTrustedKey = '';
    /**
     * Request timeout for request to api service. in seconds
     */
    public static $requestTimeOut = 15;

    public static $client = null;

    /**
     * @var array Contains all the promises for async request
     */
    public static $promises = array();

    public function __construct($id = 0, $loadFromCache = false)
    {
        self::initClient();

        if ($id > 0) {
            $this->getData($id, $loadFromCache);
        }
    }

    public static function initClient()
    {
        //Init request client (in this case, GuzzleHttp)
        if (is_null(self::$client)) {
            self::$client = new Client(array('base_uri' => self::$baseurl));
        }
    }

    abstract public function addData();
    abstract public function updateData();
    abstract public function delete();


    public static function getAuthorizationToken()
    {
        return self::$authorizationToken;
    }

    public static function getAccessTrustedKey()
    {
        return self::$accessTrustedKey;
    }


    public static function doRequest(
        $method,
        $url = '',
        $headers = array(),
        $useJwt = true,
        $useAccessTrusted = true,
        $requestbody = array(),
        $asyncItemId = 0
    ) {
        //Get url base on parameter
        if ($url == '') {
            $url = self::$serviceurl;
        }

        if ($useJwt) {
            $headers['Authorization'] = self::getAuthorizationToken();
        }

        if ($useAccessTrusted) {
            $headers['AccessTrustedKey'] = self::getAccessTrustedKey();
        }

        //instance client object
        self::initClient();

        /** @var \GuzzleHttp\Client $client */
        $client = self::$client;

        $request = new Request($method, $url, $headers, $requestbody);

        if ($asyncItemId > 0) {

            $currentCalledClass = get_called_class();
            Base::$promises[$currentCalledClass][$asyncItemId] = $client->sendAsync($request, array(
                'timeout' => self::$requestTimeOut
            ));

            return array($currentCalledClass, $asyncItemId);
        } else {
            try {

                $response = $client->send($request, array(
                    'timeout' => self::$requestTimeOut
                ));

                return self::parsingResponse($response);

            } catch (BadResponseException $e) {
                return self::parsingResponse($e->getResponse());
            }
        }
    }

    public static function parsingResponse(ResponseInterface $response)
    {
        $responseData = array();

        $responseData['status'] = $response->getStatusCode(); // 200
        $responseData['contenttype'] = $response->getHeader('Content-Type');
        $bodystringdata = $response->getBody()->getContents();
        if (stripos($responseData['contenttype'][0], 'json') !== false) {
            $responseData['data'] = json_decode($bodystringdata, true);
        } else {
            $responseData['data'] = $bodystringdata;
        }

        return $responseData;
    }

    /**
     * Used to extract CURRENT_PAGE and RECORD_PER_PAGE from limit string
     * Ex: if $limitString is '100, 50', it will return array with
     * CURRENT_PAGE = 3 & RECORD_PER_PAGE = 50
     * @param $limitString
     * @return mixed ($currentPage, $recordPerPage)
     */
    public static function extractPageInfoFromLimit($limitString)
    {
        $currentPage = 0;
        $recordPerPage = 0;

        if (is_numeric($limitString)) {
            $currentPage = 1;
            $recordPerPage = (int)$limitString;

        } else {
            $parts = explode(',', $limitString);
            if (count($parts) == 2) {
                $recordPerPage = (int)trim($parts[1]);
                if ($recordPerPage > 0) {
                    $currentPage = (int)trim($parts[0]) / $recordPerPage + 1;
                }
            }
        }


        return array($currentPage, $recordPerPage);
    }

    /**
     * Request to Restful API to get Data
     * @param $id
     * @param bool $loadFromCache
     */
    public function getData($id, $loadFromCache = false)
    {
        if ($loadFromCache) {
			$myCacher = new Cacher(static::cacheKey($id));
            $row = $myCacher->get();
            if (!empty($row)) {
                $this->getDataByArrayCache($row);
            }
        }

        //Even if with load from cache, id is still zero, we get from remote data
        if ($this->id == 0) {
            //call to get response data
            $responseData = self::doRequest(
                'GET',
                trim(static::$serviceurl, '/') . '/' . $id
            );

            if (!empty($responseData['data'])) {
                $this->getDataByJson($responseData['data']);
            }
        }
    }


    /**
     * Default: Set object properties from array input. the key of array is the property name.
     * @param array $jsonData
     */
    public function getDataByJson($jsonData)
    {
        if (is_array($jsonData)) {
            foreach ($jsonData as $key => $value) {
                $this->$key = $value;
            }
        }
    }

    public function getDataByArrayCache($row)
    {

    }

    /**
     * Loop through all properties and return as array
     * @return array
     */
    public function getJsonData()
    {
        return get_object_vars($this);
    }

    /**
     * Base method for all derived class to get all records
     * @param $formData
     * @param string $limitString
     * @param string $serviceurl
     * @return integer
     */
    public static function countItems($formData, $limitString = '', $serviceurl = '')
    {
        //get default serviceurl from static inherit class
        if ($serviceurl == '') {
            $serviceurl = static::$serviceurl;
        }

        if ($limitString != '') {
            list($currentPage, $recordPerPage) = self::extractPageInfoFromLimit($limitString);
            if ($currentPage > 0 && $recordPerPage > 0) {
                $formData['page'] = $currentPage;
                $formData['limit'] = $recordPerPage;
            }
        }

        //important flag to get number only
        $formData['totalonly'] = 1;

        $serviceurl = $serviceurl . '?' . http_build_query($formData);
        $responseData = self::doRequest('GET', $serviceurl);

        return (int)$responseData['data']['total'];
    }

    /**
     * Base method for all derived class to get all records
     * @param $formData
     * @param $sortby
     * @param $sorttype
     * @param string $limitString
     * @param string $serviceurl
     * @return array
     */
    public static function getItems($formData, $sortby, $sorttype, $limitString = '', $serviceurl = '')
    {
        $items = array();

        if ($sortby != '') {
            $formData['sortby'] = $sortby;
        }

        if ($sorttype != '') {
            $formData['sorttype'] = $sorttype;
        }

        //get default serviceurl from static inherit class
        if ($serviceurl == '') {
            $serviceurl = static::$serviceurl;
        }

        if ($limitString != '') {
            list($currentPage, $recordPerPage) = self::extractPageInfoFromLimit($limitString);
            if ($currentPage > 0 && $recordPerPage > 0) {
                $formData['page'] = $currentPage;
                $formData['limit'] = $recordPerPage;
            }
        }

        $serviceurl = $serviceurl . '?' . http_build_query($formData);
        $responseData = self::doRequest('GET', $serviceurl);

        if (count($responseData['data']['items']) > 0) {
            foreach ($responseData['data']['items'] as $jsonData) {
                $myObject = new static();
                $myObject->getDataByJson($jsonData);
                $items[] = $myObject;
            }
        }

        return $items;
    }

	/**
     * Base method for all derived class to get all records
     * @param string $serviceurl
     * @return array
     */
    public static function getRawItems($formData = array(), $serviceurl = '')
    {
        if ($serviceurl == '') {
            $serviceurl = static::$serviceurl;
        }

        $serviceurl = $serviceurl . '?' . http_build_query($formData);
        $responseData = self::doRequest('GET', $serviceurl);

		$data  = array();
        if (!empty($responseData['data'])) {
            $data = $responseData['data'];
		}

        return $data;
    }

    /**
     * Get async data from list of item, and get ids from $selectedProperty
     * @param $items
     * @param $selectedProperty, if empty, the items will be id list
     * @return self[] $data
     */
    public static function getDataAsync($items, $selectedProperty)
    {
        $data = array();

        // If selected property is empty,
        //it;s mean items will be the id list
        if ($selectedProperty == '') {
            $ids = $items;
        } else {
            $ids = self::getPropValues($items, $selectedProperty);
        }

        if (is_array($ids)) {

            $currentCalledClass = get_called_class();

            $missCount = 0;
            $dataFromCache = $dataFromRemote = array();

            //Remove duplicate ids
            $ids = array_unique($ids);

            //Loop through all ids and register request async
            foreach ($ids as $id) {

                //Try to get from cache first ^^
                $myCacher = new Cacher(static::cacheKey($id));
                $row = $myCacher->get();


                if (!$row) {
                    //call to get promise async
                    self::doRequest(
                        'GET',
                        trim(static::$serviceurl, '/') . '/' . $id,
                        array(),
                        true,
                        true,
                        array(),
                        $id
                    );
                    $missCount++;
//                    echo 'MISS.';

                } else {
                    //Init data from cache
                    /** @var self $obj */
                    $obj = new $currentCalledClass();
                    $obj->getDataByArrayCache($row);
                    $dataFromCache[$id] = $obj;
//                    echo 'HIT.';
                }
            }

            //Start dispatch all async request to get response
            if ($missCount > 0) {
                $dataFromRemote = self::startDataAsync();
            }

            //array_merge do not preserve the numberic key, so, we use plus (union operator)
            $data = $dataFromRemote + $dataFromCache;
        }

        return $data;
    }

    /**
     * @return self[]
     */
    public static function startDataAsync()
    {
        $data = array();
        try {
            $currentCalledClass = get_called_class();
            $results = Promise\unwrap(Base::$promises[$currentCalledClass]);

            //Loop through all response
            /**
             * @var string $asyncName
             * @var ResponseInterface $response
             */
            foreach ($results as $asyncItemId => $response) {
                if ($response->getStatusCode() == 200) {
                    $response = self::parsingResponse($response);

                    $obj = new $currentCalledClass();
                    foreach ($response['data'] as $key => $value) {
                        $obj->$key = $value;
                    }
                    $data[$asyncItemId] = $obj;
                }
            }

        } catch (\Exception $e) {
            //die($e->getMessage());
        }

        return $data;
    }

    /**
     * Return array of value of one property of objectlist
     * @param self[] $objList
     * @param $property
     * @return array
     */
    public static function getPropValues($objList, $property)
    {
        $values = array();

        foreach ($objList as $myObj) {
            if (property_exists($myObj, $property)) {
                $values[] = $myObj->$property;
            }
        }

        return $values;
    }

    public function copy(\stdClass $object)
    {
        foreach (get_object_vars($object) as $key => $value) {
            $this->$key = $value;
        }
    }

    public static function cacheKey($id)
    {
        return $id;
    }
}

