<?php

namespace Teamcrop\Rest;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Promise;
use Litpi\Cacher;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

#http://docs.guzzlephp.org/en/latest/quickstart.html#exceptions
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Exception\TooManyRedirectsException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;


abstract class Base extends \stdClass
{
    /**
     * Base URL of all restful request
     * This variable can be overwrite by environment variable 'TEAMCROP_REST_BASEURL'
     */
    public static $baseurl = 'https://api.teamcrop.com';

    /**
     * This property will be set on each model to point to correct api point
     * Full request url will be combine by baseurl and serviceurl.
     * If service url is start with 'http', the full request url will be not combined with base url
     */
    public static $serviceurl = '';

    /**
     * Toggle the debug mode for request
     *
     * @var bool
     */
    public static $debug = false;

    /**
     * This property will be set on almost request for authorization (in request headers)
     * This variable can be overwrite by environment variable 'TEAMCROP_REST_AUTH_TOKEN'
     */
    public static $authorizationToken = '';

    /**
     * This property will be set on almost request for inter-service request (in request headers)
     * This variable SHOULD be set by environment variable 'TEAMCROP_ACCESS_TRUSTED_KEY'
     * @var string
     */
    public static $accessTrustedKey = '';
    /**
     * Request timeout for request to api service. in seconds
     */
    public static $requestTimeOut = 15;

    /**
     * Helper property for restore requesttimeout when using set temporary timeout
     */
    public static $requestTimeoutTmp = 0;

    /**
     * @var string Used to identifier current request in sub request, generated in bootstrap
     */
    public static $myChainIdentifier = '';

    /**
     * @var string Chain Identifier received from request
     */
    public static $requestChainIdentifier = '';

    public static $client = null;
    public static $sslVerify = true;

    /**
     * @var array Contains all the promises for async request
     */
    public static $promises = array();

    public function __construct($id = 0, $loadFromCache = false, $querydata = array())
    {
        self::initClient();

        if ($id > 0) {
            $this->getData($id, $loadFromCache, $querydata);
        }
    }

    public static function initClient()
    {
        //Init request client (in this case, GuzzleHttp)
        if (is_null(self::$client)) {
            self::$client = new Client(array('base_uri' => self::$baseurl, 'verify' => self::$sslVerify));
        }
    }

    abstract public function addData();
    abstract public function updateData();
    abstract public function delete();


    public static function getAuthorizationToken()
    {
        return self::$authorizationToken;
    }

    public static function getAccessTrustedKey()
    {
        return self::$accessTrustedKey;
    }


    /**
     * Let guzzle make request and parsing response
     *
     * @param $method
     * @param string $url
     * @param array $headers
     * @param bool $useJwt
     * @param bool $useAccessTrusted
     * @param null $requestbody
     * @param int $asyncItemId
     * @param bool $requestSuccess
     * @return array
     */
    public static function doRequest(
        $method,
        $url = '',
        $headers = array(),
        $useJwt = true,
        $useAccessTrusted = true,
        $requestbody = null,
        $asyncItemId = 0,
        &$requestSuccess = null
    ) {
        //Get url base on parameter
        if ($url == '') {
            $url = self::$serviceurl;
        }

        //append debug info from calling service
        if (defined('SERVICE_NAME')) {
            if (isset($_SERVER['REQUEST_URI'])) {
                //remove jwt
                $requestUri = preg_replace('/__jwtAuthorization=[a-z0-9_\.\-]+/ims', '{TOKEN}', $_SERVER['REQUEST_URI']);
                $requestUri = substr($requestUri, -512);
            } else {
                $requestUri = '_notset';
            }

            $debugParam = '';
            $debugParam .= '&__tcs=' . SERVICE_NAME;
            $debugParam .= '&__tcfromip=' . (isset($_SERVER['SERVER_ADDR']) ? $_SERVER['SERVER_ADDR'] : '_notsetip');
            $debugParam .= '&__tcfrom=' . urlencode($requestUri);
            $debugParam .= '&__tcchaindepth=' . count(explode(',', (string)self::$requestChainIdentifier));
            $debugParam .= '&__tcchain=' . (string)self::$requestChainIdentifier . ', ' . (string)self::$myChainIdentifier;

            if (strpos($url, '?') === false) {
                $url .= '?';
            }

            $url .= $debugParam;
        }

        if ($useJwt) {
            $jwt = self::getAuthorizationToken();
            if ($jwt != '') {
                $headers['Authorization'] = $jwt;
            }
        }

        if ($useAccessTrusted) {
            $headers['AccessTrustedKey'] = self::getAccessTrustedKey();
        }

        //Append chain-identifier for tracking
        if (self::$requestChainIdentifier == '') {
            $headers['Chain-Identifier'] = self::$myChainIdentifier;
        } else {
            $headers['Chain-Identifier'] = self::$requestChainIdentifier . ', ' . self::$myChainIdentifier;
        }

        $headers['User-Agent'] = 'Guzzle';

        //instance client object
        self::initClient();

        /** @var \GuzzleHttp\Client $client */
        $client = self::$client;

        $request = new Request($method, $url, $headers, $requestbody);

        $return = null;

        if ($asyncItemId > 0) {

            $currentCalledClass = get_called_class();
            Base::$promises[$currentCalledClass][$asyncItemId] = $client->sendAsync($request, array(
                'timeout' => self::$requestTimeOut
            ));

            $return = array($currentCalledClass, $asyncItemId);
        } else {

            $catchErrorRequest = $catchErrorResponse = null;

            try {
                $response = $client->send($request, array(
                    'http_errors' => true,
                    'timeout' => self::$requestTimeOut,
                    'debug' => self::$debug
                ));

                $return = self::parsingResponse($response);
                $requestSuccess = true;

            } catch (ServerException $e) {

                if ($e->hasResponse()) {
                    $response = $e->getResponse();
                    $return = self::parsingResponse($response);
                }

                self::logErrorRequest($e);
                $requestSuccess = false;

            } catch (ClientException $e) {

                if ($e->hasResponse()) {
                    $response = $e->getResponse();
                    $return = self::parsingResponse($response);
                }

                self::logErrorRequest($e);
                $requestSuccess = false;

            } catch (BadResponseException $e) {
                self::logErrorRequest($e);
                $requestSuccess = false;

            } catch (ConnectException $e) {
                self::logErrorRequest($e);
                $requestSuccess = false;

            } catch (TooManyRedirectsException $e) {
                self::logErrorRequest($e);
                $requestSuccess = false;

            } catch (RequestException $e) {
                self::logErrorRequest($e);
                $requestSuccess = false;

            } catch (\Exception $e) {


                //Totally fail, do something.
                $generalParams = array(
                    'exception_code' => $e->getCode(),
                    'exception_message' => $e->getMessage(),
                    'method' => $request->getMethod(),
                    'uri' => (string)($request->getUri()),
                    'request_body' => (string)($request->getBody())
                );

                if ($request->hasHeader('Chain-Identifier')) {
                    $generalParams['chain_identifier'] = $request->getHeaderLine('Chain-Identifier');
                }


                self::logErrorRequestSubmit($generalParams);
                $requestSuccess = false;
            }

            /////////////////////
            //Log error
            if (!is_null($catchErrorRequest)) {
                self::logErrorRequest($catchErrorRequest, $catchErrorResponse);
            }
        }

        return $return;
    }

    /**
     * General method to call GET request to internal/external
     * @param $url
     * @param array $headers
     * @param bool $useAccessTrusted
     * @return null
     */
    public static function doGet($url, $headers = array(), $useAccessTrusted = true)
    {
        $output = null;

        //do request and get response
        $responseData = self::doRequest('GET', $url, $headers, false, $useAccessTrusted, null, 0, $requesetSuccess);

        if ($requesetSuccess) {
            $output = $responseData['data'];
        }

        return $output;
    }

    /**
     * Log exception of guzzle
     * @param RequestException $e
     */
    public static function logErrorRequest(RequestException $e)
    {
        $request = $e->getRequest();
        $response = null;

        if ($e->hasResponse()) {
            $response = $e->getResponse();
        }

        $params = array();
        $params['exception_code'] = $e->getCode();
        $params['exception_message'] = $e->getMessage();


        $params['chain_identifier'] = '';
        if ($request->hasHeader('Chain-Identifier')) {
            $params['chain_identifier'] = $request->getHeaderLine('Chain-Identifier');
        }

        $params['method'] = $request->getMethod();
        $params['uri'] = (string)($request->getUri());
        $params['request_body'] = (string)$request->getBody();

        //get response info
        $params['http_status'] = 0;
        $params['response_body'] = '';
        if (!is_null($response)) {
            $params['http_status'] = $response->getStatusCode();
            $params['response_body'] = $response->getBody()->getContents();
        }

        self::logErrorRequestSubmit($params);
    }

    /**
     * Begin request
     * @param $params
     */
    public static function logErrorRequestSubmit($params)
    {
        global $registry;

        //This $request is not the request in exception
        //This is the request of caller (current process)
        /** @var ServerRequestInterface $request */
        global $request;

        if (!is_null($request) && method_exists($request, 'getUri') && method_exists($request, 'getMethod')) {
            $params['uri_caller'] = (string)($request->getUri());
            $params['method_caller'] = (string)($request->getMethod());
        } else {
            $params['uri_caller'] = '';
            $params['method_caller'] = '';
        }

        $params['date_created'] = time();
        $params['company_id'] = $registry->company->id;
        $params['creator_id'] = $registry->me->id;

        //not found company in request, we fallback to main request and extract company_id and creator_id
        if ($params['company_id'] == 0) {


            //In case of tc-website, the $request is not from slim request, but from litpi\request
            //So, we need to check it has method getPrasedBody before continue
            if (method_exists($request, 'getMethod') && method_exists($request, 'getParsedBody')) {
                $method = strtoupper($request->getMethod());

                $originalRequestData = $request->getParsedBody();
                $originalQueryData = $request->getQueryParams();

                if ($method == 'GET') {
                    if (array_key_exists('company_id', $originalQueryData)) {
                        $params['company_id'] = $originalQueryData['company_id'];
                    }

                    //creator_id or fallback to user_id
                    if (array_key_exists('creator_id', $originalQueryData)) {
                        $params['creator_id'] = $originalQueryData['creator_id'];
                    } elseif (array_key_exists('user_id', $originalQueryData)) {
                        $params['creator_id'] = $originalQueryData['user_id'];
                    }

                } else {
                    if (array_key_exists('company_id', $originalRequestData)) {
                        $params['company_id'] = $originalRequestData['company_id'];
                    }

                    //creator_id or fallback to user_id
                    if (array_key_exists('creator_id', $originalRequestData)) {
                        $params['creator_id'] = $originalRequestData['creator_id'];
                    } elseif (array_key_exists('user_id', $originalRequestData)) {
                        $params['creator_id'] = $originalRequestData['user_id'];
                    }
                }
            }

        }



        $url = self::$baseurl . '/v1/logresterrors';
        $headers = array(
            'AccessTrustedKey' => self::$accessTrustedKey
        );
        self::postFireAndForgot($url, $params, $headers);
    }

    public static function parsingResponse(ResponseInterface $response)
    {
        $responseData = array();

        $responseData['status'] = $response->getStatusCode(); // 200
        $responseData['contenttype'] = $response->getHeader('Content-Type');
        $bodystringdata = $response->getBody()->getContents();
        if (stripos($responseData['contenttype'][0], 'json') !== false) {
            $responseData['data'] = json_decode($bodystringdata, true);
        } else {
            $responseData['data'] = $bodystringdata;
        }

        return $responseData;
    }

    /**
     * Used to extract CURRENT_PAGE and RECORD_PER_PAGE from limit string
     * Ex: if $limitString is '100, 50', it will return array with
     * CURRENT_PAGE = 3 & RECORD_PER_PAGE = 50
     * @param $limitString
     * @return mixed ($currentPage, $recordPerPage)
     */
    public static function extractPageInfoFromLimit($limitString)
    {
        $currentPage = 0;
        $recordPerPage = 0;

        if (is_numeric($limitString)) {
            $currentPage = 1;
            $recordPerPage = (int)$limitString;

        } else {
            $parts = explode(',', $limitString);
            if (count($parts) == 2) {
                $recordPerPage = (int)trim($parts[1]);
                if ($recordPerPage > 0) {
                    $currentPage = (int)trim($parts[0]) / $recordPerPage + 1;
                }
            }
        }


        return array($currentPage, $recordPerPage);
    }

    /**
     * Request to Restful API to get Data
     * @param $id
     * @param bool $loadFromCache
     * @param array $querydata
     */
    public function getData($id, $loadFromCache = false, $querydata = array())
    {
        if ($loadFromCache) {
			$myCacher = new Cacher(static::cacheKey($id));
            $row = $myCacher->get();
            if (!empty($row)) {
                $this->getDataByArrayCache($row);
            }
        }

        //Even if with load from cache, id is still zero, we get from remote data
        if ($this->id == 0) {

            $url = trim(static::$serviceurl, '/') . '/' . $id;

            //call to get response data
            if (!empty($querydata)) {
                $url .= '?' . http_build_query($querydata);
            }

            $response = self::doRequest(
                'GET',
                $url,
                array(),
                false,
                true
            );

            //request success
            if ($response['status'] == '200') {

                if (!empty($response['data'])) {
                    $this->getDataByJson($response['data']);
                }

            }

        }
    }


    /**
     * Default: Set object properties from array input. the key of array is the property name.
     * @param array $jsonData
     */
    public function getDataByJson($jsonData)
    {
        if (is_array($jsonData)) {
            foreach ($jsonData as $key => $value) {
                $this->$key = $value;
            }
        }
    }

    public function getDataByArrayCache($row)
    {

    }

    /**
     * Loop through all properties and return as array
     * @return array
     */
    public function getJsonData()
    {
        return get_object_vars($this);
    }

    /**
     * Base method for all derived class to get all records
     * @param $formData
     * @param string $limitString
     * @param string $serviceurl
     * @param bool $requestSuccess whether doRequest success or not (throw exception when do request)
     * @return integer
     */
    public static function countItems($formData, $limitString = '', $serviceurl = '', &$requestSuccess = null)
    {
        //get default serviceurl from static inherit class
        if ($serviceurl == '') {
            $serviceurl = static::$serviceurl;
        }

        if ($limitString != '') {
            list($currentPage, $recordPerPage) = self::extractPageInfoFromLimit($limitString);
            if ($currentPage > 0 && $recordPerPage > 0) {
                $formData['page'] = $currentPage;
                $formData['limit'] = $recordPerPage;
            }
        }

        //important flag to get number only
        $formData['totalonly'] = 1;

        $serviceurl = $serviceurl . '?' . http_build_query($formData);
        $responseData = self::doRequest(
            'GET',
            $serviceurl,
            array(),
            true,
            true,
            null,
            0,
            $requestSuccess
        );

        return (int)$responseData['data']['total'];
    }

    /**
     * Base method for all derived class to get all records
     * @param $formData
     * @param $sortby
     * @param $sorttype
     * @param string $limitString
     * @param string $serviceurl
     * @param bool $requestSuccess whether doRequest success or not (throw exception when do request)
     * @return array
     */
    public static function getItems(
        $formData,
        $sortby,
        $sorttype,
        $limitString = '',
        $serviceurl = '',
        &$requestSuccess = null
    ) {
        $items = array();

        if ($sortby != '') {
            $formData['sort_by'] = $sortby;
        }

        if ($sorttype != '') {
            $formData['sort_type'] = $sorttype;
        }

        //get default serviceurl from static inherit class
        if ($serviceurl == '') {
            $serviceurl = static::$serviceurl;
        }

        if ($limitString != '') {
            list($currentPage, $recordPerPage) = self::extractPageInfoFromLimit($limitString);
            if ($currentPage > 0 && $recordPerPage > 0) {
                $formData['page'] = $currentPage;
                $formData['limit'] = $recordPerPage;
            }
        }

        $serviceurl = $serviceurl . '?' . http_build_query($formData);
        $responseData = self::doRequest(
            'GET',
            $serviceurl,
            array(),
            true,
            true,
            null,
            0,
            $requestSuccess
        );

        if (isset($responseData['data']['items']) && count($responseData['data']['items']) > 0) {
            foreach ($responseData['data']['items'] as $jsonData) {
                $myObject = new static();
                $myObject->getDataByJson($jsonData);
                $items[] = $myObject;
            }
        }

        return $items;
    }

	/**
     * Base method for all derived class to get all records
     * @param array $formData
     * @param string $serviceurl
     * @return array
     */
    public static function getRawItems($formData = array(), $serviceurl = '')
    {
        if ($serviceurl == '') {
            $serviceurl = static::$serviceurl;
        }

        $serviceurl = $serviceurl . '?' . http_build_query($formData);
        $responseData = self::doRequest('GET', $serviceurl);

		$data  = array();
        if (!empty($responseData['data'])) {
            $data = $responseData['data'];
		}

        return $data;
    }

    /**
     * Get async data from list of item, and get ids from $selectedProperty
     * @param $items
     * @param $selectedProperty, if empty, the items will be id list
     * @return self[] $data
     */
    public static function getDataAsync($items, $selectedProperty)
    {
        $data = array();

        // If selected property is empty,
        //it;s mean items will be the id list
        if ($selectedProperty == '') {
            $ids = $items;
        } else {
            $ids = self::getPropValues($items, $selectedProperty);
        }

        if (is_array($ids)) {

            $currentCalledClass = get_called_class();

            $missCount = 0;
            $dataFromCache = $dataFromRemote = array();

            //Remove duplicate ids
            $ids = array_unique($ids);

            //Loop through all ids and register request async
            foreach ($ids as $id) {

                //Try to get from cache first ^^
                $myCacher = new Cacher(static::cacheKey($id));
                $row = $myCacher->get();


                if (!$row) {
                    //call to get promise async
                    self::doRequest(
                        'GET',
                        trim(static::$serviceurl, '/') . '/' . $id,
                        array(),
                        true,
                        true,
                        array(),
                        $id
                    );
                    $missCount++;
//                    echo 'MISS.';

                } else {
                    //Init data from cache
                    /** @var self $obj */
                    $obj = new $currentCalledClass();
                    $obj->getDataByArrayCache($row);
                    $dataFromCache[$id] = $obj;
//                    echo 'HIT.';
                }
            }

            //Start dispatch all async request to get response
            if ($missCount > 0) {
                $dataFromRemote = self::startDataAsync();
            }

            //array_merge do not preserve the numberic key, so, we use plus (union operator)
            $data = $dataFromRemote + $dataFromCache;
        }

        return $data;
    }

    /**
     * @return self[]
     */
    public static function startDataAsync()
    {
        $data = array();
        try {
            $currentCalledClass = get_called_class();
            $results = Promise\unwrap(Base::$promises[$currentCalledClass]);

            //Loop through all response
            /**
             * @var string $asyncName
             * @var ResponseInterface $response
             */
            foreach ($results as $asyncItemId => $response) {
                if ($response->getStatusCode() == 200) {
                    $response = self::parsingResponse($response);

                    $obj = new $currentCalledClass();
                    foreach ($response['data'] as $key => $value) {
                        $obj->$key = $value;
                    }
                    $data[$asyncItemId] = $obj;
                }
            }

        } catch (\Exception $e) {
            //die($e->getMessage());
        }

        return $data;
    }

    /**
     * Return array of value of one property of objectlist
     * @param self[] $objList
     * @param $property
     * @return array
     */
    public static function getPropValues($objList, $property)
    {
        $values = array();

        foreach ($objList as $myObj) {
            if (property_exists($myObj, $property)) {
                $values[] = $myObj->$property;
            }
        }

        return $values;
    }

    public function copy(\stdClass $object)
    {
        foreach (get_object_vars($object) as $key => $value) {
            $this->$key = $value;
        }
    }

    public static function cacheKey($id)
    {
        return $id;
    }

    /**
     * Base function for add record via restful POST
     *
     * @param $jsonData
     * @param string $serviceurl
     * @param array $headers
     * @param array $error
     * @return int
     */
    public static function doAdd($jsonData, $serviceurl = '', $headers = array(), &$error = array())
    {
        //ID of successfully added record
        $objectId = 0;

        //set default serviceurl from current sdk serviceurl
        if ($serviceurl == '') {
            $serviceurl = static::$serviceurl;
        }

        //set default headers to json data
        if (empty($headers)) {
            $headers = array(
                'Content-type' => 'application/json'
            );
        }

        //Do request and get response with submit data
        $response = self::doRequest('POST', $serviceurl, $headers, true, true, json_encode($jsonData));

        //request success
        if ($response['status'] == '200' || $response['status'] == '201') {
            if (is_array($response['data']) && isset($response['data']['id'])) {
                $objectId = $response['data']['id'];
            } else {
                $error[] = 'error_id_not_found_in_response';
            }

        } else {
            $error = self::parsingErrorFromResponse($response);
        }


        return $objectId;
    }

    /**
     * Base function for update record via restful PUT. Return BOOL
     *
     * @param $jsonData
     * @param string $serviceurl
     * @param array $headers
     * @param array $error
     * @return boolean
     */
    public static function doUpdate($jsonData, $serviceurl = '', $headers = array(), &$error = array())
    {
        //ID of successfully updated record
        $objectId = 0;

        //set default serviceurl from current sdk serviceurl
        if ($serviceurl == '') {
            $serviceurl = static::$serviceurl;
        }

        //set default headers to json data
        if (empty($headers)) {
            $headers = array(
                'Content-type' => 'application/json'
            );
        }

        //Do request and get response with submit data
        $response = self::doRequest('PUT', $serviceurl, $headers, true, true, json_encode($jsonData));
        //request success
        if ($response['status'] == '200' || $response['status'] == '201') {
            if (is_array($response['data']) && isset($response['data']['id'])) {
                $objectId = $response['data']['id'];
            } else {
                $error[] = 'error_id_not_found_in_response';
            }

        } else {
            $error = self::parsingErrorFromResponse($response);
        }

        return $objectId > 0;
    }

    /**
     * Base function for remove record via restful PUT. Return BOOL
     *
     * @param string $serviceurl
     * @param array $headers
     * @param array $error
     * @return boolean
     */
    public static function doDelete($serviceurl = '', $headers = array(), &$error = array())
    {
        $result = false;

        //set default serviceurl from current sdk serviceurl
        if ($serviceurl == '') {
            $serviceurl = static::$serviceurl;
        }

        //Do request and get response with submit data
        $response = self::doRequest('DELETE', $serviceurl, $headers, true, true);

        //request success
        if ($response['status'] == '200' || $response['status'] == '204') {
            $result = true;

        } else {
            $error = self::parsingErrorFromResponse($response);
        }

        return $result;
    }

    /**
     * In most case in POST, PUT, DELETE, we need to parse error array from response
     *
     * @param $response
     * @return array
     */
    public static function parsingErrorFromResponse($response)
    {
        $error = array();

        //check valid parsing data
        if (is_array($response['data'])) {
            if (isset($response['data']['error'])) {
                if (is_array($response['data']['error'])) {
                    $error = $response['data']['error'];
                } else {
                    $error[] = $response['data']['error'];
                }
            } else {
                $error[] = 'error_untrack_remote_error';
            }
        } else {
            $error[] = $response['data'];
        }

        return $error;
    }

    /**
     * In some request, we need to customer timeout (make it longer)
     * Use this method to set timeout and with restoreTimeout() method to
     * restore to previous value
     * @param $timeout
     */
    public static function setTmpTimeout($timeout)
    {
        self::$requestTimeoutTmp = self::$requestTimeOut;
        self::$requestTimeOut = $timeout;
    }

    /**
     * Companion method with setTmpTimeout, to restore timeout to saved timeout
     */
    public static function restoreTimeout()
    {
        self::$requestTimeOut = self::$requestTimeoutTmp;
    }

    /**
     * Used to create POST request and immediately timeout to prevent delay
     * @param $url
     * @param array $params
     * @param array $headers
     */
    public static function postFireAndForgot($url, $params = array(), $headers = array())
    {
        // create POST string
        $postParams = array();
        foreach ($params as $key => &$val) {
            $postParams[] = $key . '=' . urlencode($val);
        }
        $postString = implode('&', $postParams);

        // get URL segments
        $parts = parse_url($url);

        // workout port and open socket
        $port = isset($parts['port']) ? $parts['port'] : 80;
        $fp = fsockopen($parts['host'], $port, $errno, $errstr, 30);

        // create output string
        $output  = "POST " . $parts['path'] . " HTTP/1.1\r\n";
        $output .= "Host: " . $parts['host'] . "\r\n";

        if (is_array($headers)) {
            foreach ($headers as $headername => $headervalue) {
                $output .= "$headername: $headervalue\r\n";
            }
        }

        $output .= "Content-Type: application/x-www-form-urlencoded\r\n";
        $output .= "Content-Length: " . strlen($postString) . "\r\n";
        $output .= "Connection: Close\r\n\r\n";
        $output .= isset($postString) ? $postString : '';

        // send output to $url handle
        fwrite($fp, $output);
        fclose($fp);
    }

}

