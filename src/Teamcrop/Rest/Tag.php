<?php

namespace Teamcrop\Rest;

class Tag extends Base
{
    public static $serviceurl = '/v1/tags';

    const STATUS_ENABLE = 1;
    const STATUS_DISABLED = 3;

    public $cid = 0;
    public $id = 0;
    public $text = '';
    public $plaintext = '';
    public $color = '';
    public $status = 0;
    public $datecreated = 0;
    public $datemodified = 0;

    public $uid = 0;
    public function __construct($id = 0, $loadFromCache = false)
    {
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                //$this->copy(self::cacheGet($id));
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData(&$error = array())
    {
        $data = array(
            "company_id" => $this->cid,
            "creator_id" => $this->uid,
            "plain_text" => $this->plaintext,
            "text" => $this->text,
            "color" => $this->color,
            "status" => $this->status
        );

        $this->id = self::doAdd($data, '', '', $error);

        return $this->id;
    }

    public function updateData(&$error = array())
    {
        $data = array(
            "company_id" => $this->cid,
            "creator_id" => $this->uid,
            "plain_text" => $this->plaintext,
            "text" => $this->text,
            "color" => $this->color,
            "status" => $this->status
        );

        $url = self::$serviceurl . '/' . $this->id;
        return self::doUpdate($data, $url, '', $error);
    }


    public function delete(&$error = array())
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }

    public static function getTags($formData, $sortby = '', $sorttype = '', $limitString = '', $countOnly = false)
    {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }

    }

    public function getDataByJson($jsonData)
    {
        $this->cid = $jsonData['company_id'];
        $this->id = $jsonData['id'];
        $this->text = $jsonData['text'];
        $this->plaintext = $jsonData['plain_text'];
        $this->color = $jsonData['color'];
        $this->status = $jsonData['status'];
        $this->datecreated = $jsonData['date_created'];
        $this->datemodified = $jsonData['date_modified'];

    }
}
