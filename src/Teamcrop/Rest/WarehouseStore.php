<?php

namespace Teamcrop\Rest;

class WarehouseStore extends Base
{
    public static $serviceurl = '/v1/warehouselocations';

    const TYPE_INTERNAL = 5;
    const TYPE_EXTERNAL = 10;

    const STATUS_ENABLE = 1;
    const STATUS_DISABLE = 2;

    const IS_DELETE = 1;

    public $cid = 0;
    public $uid = 0;
    public $swid = 0;
    public $slid = 0;
    public $id = 0;
    public $priority = 0;
    public $type = 0;
    public $note = '';
    public $status = 0;
    public $isdeleted = 0;
    public $isdeletedby = 0;
    public $datecreated = 0;
    public $datemodified = 0;
    public $datedeleted = 0;

    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData(&$error = array())
    {
//        $data = array();
//
//        $this->id = self::doAdd($data, '', '', $error);

        return $this->id;
    }

    public function updateData(&$error = array())
    {
//        $data = array( );
//
//        $url = self::$serviceurl . '/' . $this->id;
//        return self::doUpdate($data, $url, '', $error);
    }

    public function delete(&$error = array())
    {
//        $url = self::$serviceurl . '/' . $this->id;
//        return self::doDelete($url, '', $error);
    }

    public static function getWareHouseStores(
        $formData,
        $sortby = '',
        $sorttype = '',
        $limitString = '',
        $countOnly = false
    )
    {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }
    }


    public function getDataByJson($row)
    {
        $this->cid = (int)$row['company_id'];
        $this->uid = (int)$row['creator_id'];
        $this->swid = (int)$row['warehouse_id'];
        $this->slid = (int)$row['store_id'];
        $this->id = (int)$row['id'];
        $this->priority = (int)$row['priority'];
        $this->type = (int)$row['type'];
        $this->note = (string)$row['note'];
        $this->status = (int)$row['status'];
        $this->isdeleted = (int)$row['is_deleted'];
        $this->isdeletedby = (int)$row['is_deleted_by'];
        $this->datecreated = (int)$row['date_created'];
        $this->datemodified = (int)$row['date_modified'];
        $this->datedeleted = (int)$row['date_deleted'];
    }

    public function getJsonData()
    {
        $data = array(
            'company_id' => (int)$this->cid,
            'creator_id' => (int)$this->uid,
            'warehouse_id' => (int)$this->swid,
            'store_id' => (int)$this->slid,
            'id' => (int)$this->id,
            'priority' => (int)$this->priority,
            'type' => (int)$this->type,
            'note' => (string)$this->note,
            'status' => (int)$this->status,
            'is_deleted' => (int)$this->isdeleted,
            'is_deleted_by' => (int)$this->isdeletedby,
            'date_created' => (int)$this->datecreated,
            'date_modified' => (int)$this->datemodified,
            'date_deleted' => (int)$this->datedeleted
        );

        return $data;
    }
}
