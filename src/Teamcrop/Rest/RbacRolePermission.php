<?php

namespace Teamcrop\Rest;

class RbacRolePermission extends Base
{
    public static $serviceurl = '/v1/rbacrolepermissions';

    public $cid = 0;
    public $rrid = 0;
    public $rsid = 0;
    public $id = 0;
    public $creatorid = 0;
    public $ipaddress = 0;
    public $datecreated = 0;
    public $datemodified = 0;

    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData()
    {

    }

    public function updateData()
    {

    }


    public function delete()
    {

    }

    public static function getRbacRolePermissions($formData, $sortby = '', $sorttype = '', $limitString = '')
    {
        return self::getItems($formData, $sortby, $sorttype, $limitString);
    }

    public function getDataByJson($jsonData)
    {
        $this->cid = $jsonData['company_id'];
        $this->rrid = $jsonData['role_id'];
        $this->rsid = $jsonData['subject_id'];
        $this->id = $jsonData['id'];
        $this->creatorid = $jsonData['creator_id'];
        $this->ipaddress = $jsonData['ip_address'];
        $this->datecreated = $jsonData['date_created'];
        $this->datemodified = $jsonData['date_modified'];
    }
}