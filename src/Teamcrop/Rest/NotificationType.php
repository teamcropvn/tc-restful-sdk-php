<?php

namespace Teamcrop\Rest;


/**
 * CompanyModule Class
 *
 * File contains the class used for CompanyModule Model
 *
 * @category Litpi
 * @package Model
 * @author Vo Duy Tuan <tuanmaster2012@gmail.com>
 * @copyright Copyright (c) 2013 - Litpi Framework (http://www.litpi.com)
 */
class NotificationType
{

    public static function typeMapping()
    {
        //item in format
        //array(group-name-code, item-name-code, type-list)
        $mapping = array(
            1 => array(
                'general',
                'employee',
                array(
                    'employee.profile',
                    'employee.contractadd',
                    'employee.contractedit',
                    'employee.progressadd',
                    'employee.progressedit',
                ),
            ),

            11 => array(
                'teamwork',
                'mail',
                array(
                    'mail.add'
                )
            ),
            13 => array(
                'teamwork',
                'project',
                array(
                    'project.add',
                    'project.assign',
                    'projecttask.add',
                    'projecttask.assign',
                    'projecttask.updatestatus',
                    'projecttaskcomment.add',
                    'projectissue.add',
                    'projectissue.assign',
                    'projectissue.updatestatus',
                    'projectissuecomment.add',
                )
            ),

            21 => array(
                'hrm',
                'salary',
                array(
                    'salary.add',
                    'salary.edit'
                ),
            ),
            22 => array(
                'hrm',
                'checkin',
                array(
                    'worktime.edit',
                    'worktime.review',
                    'worktime.reviewcancel'
                )
            ),
            23 => array(
                'hrm',
                'employeepoint',
                array(
                    'employeepoint.add'
                ),
            ),
            25 => array(
                'hrm',
                'leave',
                array(
                    'leave.needreview',
                    'leave.acceptpartial',
                    'leave.accept',
                    'leave.reject'
                )
            ),
            // array('hrm', 'workscheduling', 1),

            31 => array(
                'sale',
                'order',
                array(
                    'order.add',
                    'order.edit',
                    'order.statuschange',
                    'order.productadd',
                    'order.productedit',
                    'order.productdelete',
                    'shipping.add',
                    'shipping.statuschange',
                    'shipping.delete',
                )
            ),
            // array('sale', 'pos', 0),
            // array('sale', 'combo', 1),
            // array('sale', 'promotion', 0),
            // array('sale', 'couponvoucher', 1),

            41 => array(
                'crm',
                'customer',
                array(
                    'customer.add',
                    'customer.edit',
                    'customer.assign',
                    'customer.noteadd',
                    'customer.taskadd',
                    'customer.eventadd',
                    'customer.smsadd',
                    'customer.emailadd'
                )
            ),
            // array('crm', 'newsletter', 1),
            43 => array(
                'crm',
                'ticket',
                array(
                    'ticket.add',
                    'ticket.assign',
                    'ticket.statuschange',
                    'ticket.reply'
                )
            ),
            // array('crm', 'sms', 2),
            // array('crm', 'livechat', 1),
            // array('crm', 'facebookinbox', 0),
            // array('crm', 'facebookcomment', 1),
            // array('crm', 'facebookinboxcampaign', 2),

            51 => array(
                'inventory',
                'inventorytrack',
                array(
                    'productreceipt.add',
                    'productreceipt.statuschange',
                    'productreceipt.productchange'
                )
            ),
            // array('inventory', 'inventorycheck', 0),
            // array('inventory', 'inventorytransfer', 0),
            // array('inventory', 'inventoryalert', 0),
            55 => array(
                'inventory',
                'purchaseorder',
                array(
                    'po.add',
                    'po.edit',
                    'po.statuschange',
                    'po.productadd',
                    'po.productedit',
                    'po.productdelete'
                )
            ),
            // array('inventory', 'productcost', 1),

            61 => array(
                'accounting',
                'cashflow',
                array(
                    'cashflow.add',
                    'cashflow.edit',
                    'cashflow.statuschange'
                )
            ),
        );

        return $mapping;
    }

    public static function textToId($moduleName)
    {
        $moduleId = 0;

        $mapping = self::typeMapping();
        foreach ($mapping as $id => $moduleInfo) {
            if ($moduleInfo[1] == $moduleName) {
                $moduleId = $id;
                break;
            }
        }

        return $moduleId;
    }
}
