<?php

namespace Teamcrop\Rest;

class ProductReceipt extends Base
{
    public static $serviceurl = '/v1/productreceipts';

    const STATUS_PENDING = 0;
    const STATUS_PROCESSING = 3;
    const STATUS_COMPLETED = 9;
    const STATUS_CANCELLED = 11;


    const DIRECTION_OUTPUT = 1;
    const DIRECTION_INPUT = 3;

    const TYPE_BUYING = 1;
    const TYPE_SELLING_ONE = 3;
    const TYPE_SELLING_MANY = 9;
    const TYPE_TRANSFERSTORE = 5;
    const TYPE_BALANCESTORE = 7;
    const TYPE_INPUTRECEIVE = 11;
    const TYPE_OUTPUTRECEIVE = 13;
    const TYPE_OUTPUTCORRUPT = 15;
    const TYPE_OUTPUTLOSS = 17;
    const TYPE_OTHER = 21;

    public $cid = 0;
    public $uid = 0;
    public $id = 0;
    public $identifier = '';
    public $fromid = 0;
    public $swid = 0;
    public $type = 0;
    public $direction = 0;
    public $saleorderid = '';
    public $purchaseorderid = 0;
    public $exchangeid = 0;
    public $detailhash = '';
    public $pricesubtotal = 0;
    public $priceshipping = 0;
    public $pricehandling = 0;
    public $pricediscount = 0;
    public $pricetax = 0;
    public $pricetotal = 0;
    public $customerid = 0;
    public $contactemail = '';
    public $contactfullname = '';
    public $contactphone = '';
    public $contactphone2 = '';
    public $contactaddress = '';
    public $contactregion = 0;
    public $contactcountry = 0;
    public $note = '';
    public $ispaid = 0;
    public $filepath = '';
    public $status = 0;
    public $isdeleted = 0;
    public $isdeletedby = 0;
    public $datecreated = 0;
    public $datemodified = 0;
    public $dateorderred = 0;
    public $dateconfirmed = 0;
    public $datepaid = 0;
    public $datedelivered = 0;
    public $datereceived = 0;
    public $datecancelled = 0;
    public $datedeleted = 0;


    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData(&$error = array())
    {
        $data = array(
            'creator_id' => $this->uid,
            'company_id' => $this->cid,
            'direction' => $this->direction,
            'warehouse_id' => $this->swid,
            'type' => $this->type,
            'file_path' => $this->filepath,
            'order_id' => $this->saleorderid,
            'purchase_order_id' => $this->purchaseorderid,
            'exchange_id' => (int)$this->exchangeid,
            'detail_hash' => (string)$this->detailhash,
            'price_shipping' => $this->priceshipping,
            'price_handling' => $this->pricehandling,
            'price_discount' => $this->pricediscount,
            'price_tax' => $this->pricetax,
            'customer_id' => $this->customerid,
            'contact_email' => $this->contactemail,
            'contact_fullname' => $this->contactfullname,
            'contact_phone' => $this->contactphone,
            'contact_phone2' => $this->contactphone2,
            'contact_address' => $this->contactaddress,
            'contact_region_id' => $this->contactregion,
            'contact_country' => $this->contactcountry,
            'note' => $this->note,
            'is_paid' => $this->ispaid,
            'status' => $this->status
        );

        $this->id = self::doAdd($data, '', '', $error);

        return $this->id;
    }

    public static function addListData($order_id, $warehouse_id, &$error = array())
    {
        $data["warehouse_id"] = $warehouse_id;
        $data["order_id"] = $order_id;

        return self::doAdd($data, self::$serviceurl . "/addlist", '', $error);
    }

    public function updateData(&$error = array())
    {
        $data = array(
            'creator_id' => $this->uid,
            'company_id' => $this->cid,
            'type' => $this->type,
            'file_path' => $this->filepath,
            'order_id' => $this->saleorderid,
            'purchase_order_id' => $this->purchaseorderid,
            'exchange_id' => (int)$this->exchangeid,
            'detail_hash' => (string)$this->detailhash,
            'contact_email' => $this->contactemail,
            'contact_fullname' => $this->contactfullname,
            'contact_phone' => $this->contactphone,
            'contact_phone2' => $this->contactphone2,
            'contact_address' => $this->contactaddress,
            'contact_region_id' => $this->contactregion,
            'contact_country' => $this->contactcountry,
            'note' => $this->note,
            'is_paid' => $this->ispaid,
            'status' => $this->status
        );

        $url = self::$serviceurl . '/' . $this->id;
        return self::doUpdate($data, $url, '', $error);
    }

    public function delete(&$error = array())
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }

    public static function getByIds($formData)
    {
        return self::getRawItems($formData, self::$serviceurl . '/ids');
    }

    public static function getProducts($formData, $sortby = '', $sorttype = '', $limitString = '', $countOnly = false)
    {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }

    }

    public function getJsonData()
    {
        $data = array(
            'id' => (string)$this->id,
            'company_id' => (int)$this->cid,
            'creator_id' => (int)$this->uid,
            'warehouse_id' => (int)$this->swid,
            //'order_invoice_id' => (string)$soid,
            'count_sku' => 0,
            'type' => (int)$this->type,
            'direction' => (int)$this->direction,
            'order_id' => (int)$this->saleorderid,
            'purchase_order_id' => (int)$this->purchaseorderid,
            'exchange_id' => (int)$this->exchangeid,
            'detail_hash' => (string)$this->detailhash,
            'price_subtotal' => (float)$this->pricesubtotal,
            'price_shipping' => (float)$this->priceshipping,
            'price_handling' => (float)$this->pricehandling,
            'price_discount' => (float)$this->pricediscount,
            'price_tax' => (float)$this->pricetax,
            'price_total' => (float)$this->pricetotal,
            //'count' => 0,
            'contact_email' => (string)$this->contactemail,
            'contact_fullname' => (string)$this->contactfullname,
            'contact_phone' => (string)$this->contactphone,
            'contact_phone2' => (string)$this->contactphone2,
            'contact_address' => (string)$this->contactaddress,
            'contact_region_id' => (int)$this->contactregion,
            'contact_country' => (int)$this->contactcountry,
            'note' => (string)$this->note,
            'is_paid' => (int)$this->ispaid,
            'file_path' => (string)$this->filepath,
            'status' => (int)$this->status,
            'is_deleted' => (int)$this->isdeleted,
            'is_deletedby' => (int)$this->isdeletedby,
            'date_created' => (int)$this->datecreated,
            'date_modified' => (int)$this->datemodified,
            'date_orderred' => (int)$this->dateorderred,
            'date_confirmed' => (int)$this->dateconfirmed,
            'date_paid' => (int)$this->datepaid,
            'date_delivered' => (int)$this->datedelivered,
            'date_received' => (int)$this->datereceived,
            'date_cancelled' => (int)$this->datecancelled,
            'date_deleted' => (int)$this->datedeleted,
            'identifier' => (string)$this->identifier
        );

        return $data;
    }

    public function getDataByJson($jsonData)
    {
        $this->id = (int)$jsonData['id'];
        $this->cid = (int)$jsonData['company_id'];
        $this->uid = (int)$jsonData['creator_id'];
        $this->swid = (int)$jsonData['warehouse_id'];
        $this->identifier = (string)$jsonData['identifier'];
        $this->type = (int)$jsonData['type'];
        $this->direction = (int)$jsonData['direction'];
        $this->saleorderid = (int)$jsonData['order_id'];
        $this->purchaseorderid = (int)$jsonData['purchase_order_id'];
        $this->exchangeid = (int)$jsonData['exchange_id'];
        $this->detailhash = (string)$jsonData['detail_hash'];
        $this->pricesubtotal = (float)$jsonData['price_subtotal'];
        $this->priceshipping = (float)$jsonData['price_shipping'];
        $this->pricehandling = (float)$jsonData['price_handling'];
        $this->pricediscount = (float)$jsonData['price_discount'];
        $this->pricetax = (float)$jsonData['price_tax'];
        $this->pricetotal = (float)$jsonData['price_total'];
        $this->customerid = (int)$jsonData['customer_id'];
        $this->contactemail = (string)$jsonData['contact_email'];
        $this->contactfullname = (string)$jsonData['contact_fullname'];
        $this->contactphone = (string)$jsonData['contact_phone'];
        $this->contactphone2 = (string)$jsonData['contact_phone2'];
        $this->contactaddress = (string)$jsonData['contact_address'];
        $this->contactregion = (int)$jsonData['contact_region_id'];
        $this->contactcountry = (int)$jsonData['contact_country'];
        $this->note = (string)$jsonData['note'];
        $this->ispaid = (int)$jsonData['is_paid'];
        $this->filepath = (string)$jsonData['file_path'];
        $this->status = (int)$jsonData['status'];
        $this->isdeleted = (int)$jsonData['is_deleted'];
        $this->isdeletedby = (int)$jsonData['is_deletedby'];
        $this->datecreated = (int)$jsonData['date_created'];
        $this->datemodified = (int)$jsonData['date_modified'];
        $this->dateorderred = (int)$jsonData['date_orderred'];
        $this->dateconfirmed = (int)$jsonData['date_confirmed'];
        $this->datepaid = (int)$jsonData['date_paid'];
        $this->datedelivered = (int)$jsonData['date_delivered'];
        $this->datereceived = (int)$jsonData['date_received'];
        $this->datecancelled = (int)$jsonData['date_cancelled'];
        $this->datedeleted = (int)$jsonData['date_deleted'];
    }
}
