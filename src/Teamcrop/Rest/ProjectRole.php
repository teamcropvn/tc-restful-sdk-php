<?php

namespace Teamcrop\Rest;

class ProjectRole extends Base
{
    public static $serviceurl = '/v1/projectroles';

    const ROLE_PRODUCT_OWNER = 1;
    const ROLE_DEVELOPMENT_TEAM = 3;
    const ROLE_SCRUMMASTER = 5;
    const ROLE_VIEWER = 7;

    public $members;

    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData()
    {

    }

    public function updateData()
    {

    }


    public function delete()
    {

    }

    public static function getProjectRoles(
        $formData,
        $sortby = '',
        $sorttype = '',
        $limitString = '',
        $countOnly = false
    ) {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }
    }

    public function getJsonData()
    {
        $data = array(
            'members' => $this->members
        );

        return $data;
    }

    public function getDataByJson($row)
    {
        $this->members = $row['members'];
    }
}
