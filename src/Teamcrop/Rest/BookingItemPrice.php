<?php

namespace Teamcrop\Rest;

class BookingItemPrice extends Base
{
    public static $serviceurl = '/v1/bookingitemprices';

    const TYPE_HOTEL = 1;
    const  TYPE_ACTIVITY = 3;
    const  TYPE_PEOPLE = 5;
    const  TYPE_EQUIPMENT = 7;

    const PRICEMODE_DAILY = 1;
    const PRICEMODE_HOURLY = 3;
    const PRICEMODE_ONETIME = 5;

    const  SLOTTIMETYPE_FIXED = 1;
    const SLOTTIMETYPE_CUSTOMIZE = 1;

    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 3;

    public $cid = 0;
    public $uid = 0;
    public $id = 0;
    public $name = '';
    public $type = 0;
    public $pricemode = 0;
    public $price = '';
    public $adjustment = '';
    public $slotperday = 0;
    public $slottimetype = 0;
    public $slottimedetail = '';
    public $displayorder = 0;
    public $status = 0;
    public $datecreated = 0;
    public $datemodified = 0;

    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData(&$error = array())
    {
        $row = $this->getJsonData();
        $this->id = self::doAdd($row, '', '', $error);

        return $this->id;
    }

    public function updateData(&$error = array())
    {
        $row = $this->getJsonData();
        $url = self::$serviceurl . '/' . $this->id;
        return self::doUpdate($row, $url, '', $error);
    }

    public function delete(&$error = array())
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }

    public static function getBookingPrices(
        $formData,
        $sortby = '',
        $sorttype = '',
        $limitString = '',
        $countOnly = false
    ) {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }
    }


    public function getDataByJson($row)
    {
        $this->cid = $row['company_id'];
        $this->uid = $row['creator_id'];
        $this->id = $row['id'];
        $this->name = $row['name'];
        $this->type = $row['type'];
        $this->pricemode = $row['price_mode'];
        $this->price = $row['price'];
        $this->adjustment = $row['adjustment'];
        $this->slotperday = $row['slot_per_day'];
        $this->slottimetype = $row['slot_time_type'];
        $this->slottimedetail = $row['slot_time_detail'];
        $this->displayorder = $row['display_order'];
        $this->status = $row['status'];
        $this->datecreated = $row['date_created'];
        $this->datemodified = $row['date_modified'];
    }

    public function getJsonData()
    {
        $row['company_id'] = $this->cid;
        $row['creator_id'] = $this->uid;
        $row['id'] = $this->id;
        $row['name'] = $this->name;
        $row['type'] = $this->type;
        $row['price_mode'] = $this->pricemode;
        $row['price'] = $this->price;
        $row['adjustment'] = $this->adjustment;
        $row['slot_per_day'] = $this->slotperday;
        $row['slot_time_type'] = $this->slottimetype;
        $row['slot_time_detail'] = $this->slottimedetail;
        $row['display_order'] = $this->displayorder;
        $row['status'] = $this->status;
        $row['date_created'] = $this->datecreated;
        $row['date_modified'] = $this->datemodified;

        return $row;
    }


}
