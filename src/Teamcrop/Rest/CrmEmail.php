<?php

namespace Teamcrop\Rest;

class CrmEmail extends Base
{
    public static $serviceurl = '/v1/crmemails';

    const STATUS_PENDING = 1;
    const STATUS_SUCCESS = 3;
    const STATUS_ERROR = 5;

    const TYPE_NORMAL = 1;
    const TYPE_ORDER = 3;

    public $cid = 0;
    public $uid = 0;
    public $ccid = 0;
    public $objectid = 0;
    public $id = 0;
    public $toname = '';
    public $toemail = '';
    public $subject = '';
    public $content = '';
    public $provider = '';
    public $fileidlist = '';
    public $isopenned = 0;
    public $campaignid = 0;
    public $subscriberlistid = '';
    public $trackingdetail = '';
    public $status = 0;
    public $type = 0;
    public $ipaddress = '';
    public $datecreated = 0;
    public $datemodified = 0;

    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData(&$error = array())
    {
        $data = array(
            'creator_id' => (int)$this->uid,
            'company_id' => (int)$this->cid,
            'customer_id' => (int)$this->ccid,
            'object_id' => (int)$this->objectid,
            'to_name' => (string)$this->toname,
            'to_email' => (string)$this->toemail,
            'subject' => (string)$this->subject,
            'content' => (string)$this->content,
            'provider' => (string)$this->provider,
            'file_id_list' => (string)$this->fileidlist,
            'is_openned' => (int)$this->isopenned,
            'campaign_id' => (int)$this->campaignid,
            'subscriber_list_id' => (string)$this->subscriberlistid,
            'tracking_detail' => (string)$this->trackingdetail,
            "status" => (int)$this->status,
            "type" => (int)$this->type
        );

        $this->id = self::doAdd($data, '', '', $error);

        return $this->id;
    }

    public function updateData(&$error = array())
    {
        $data = array(
            'creator_id' => (int)$this->uid,
            'company_id' => (int)$this->cid,
            'customer_id' => (int)$this->ccid,
            'object_id' => (int)$this->objectid,
            'id' => (int)$this->id,
            'to_name' => (string)$this->toname,
            'to_email' => (string)$this->toemail,
            'provider' => (string)$this->provider,
            'file_id_list' => (string)$this->fileidlist,
            'is_openned' => (int)$this->isopenned,
            'campaign_id' => (int)$this->campaignid,
            'subscriber_list_id' => (string)$this->subscriberlistid,
            'tracking_detail' => (string)$this->trackingdetail,
            "status" => (int)$this->status,
            "type" => (int)$this->type
        );

        $url = self::$serviceurl . '/' . $this->id;
        return self::doUpdate($data, $url, '', $error);
    }


    public function delete(&$error = array())
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }

    public static function getCrmEmails($formData, $sortby = '', $sorttype = '', $limitString = '', $countOnly = false)
    {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }
    }

    public static function cacheKey($notifyid)
    {
        return 'tc_crm_feed_' . $notifyid;
    }

    public function getDataByArrayCache($row)
    {

    }

    public function getDataByJson($jsonData)
    {
        $this->cid = (int)$jsonData['company_id'];
        $this->uid = (int)$jsonData['creator_id'];
        $this->ccid = (int)$jsonData['customer_id'];
        $this->objectid = (int)$jsonData['object_id'];
        $this->id = (int)$jsonData['id'];
        $this->toname = (string)$jsonData['to_name'];
        $this->toemail = (string)$jsonData['to_email'];
        $this->subject = (string)$jsonData['subject'];
        $this->content = (string)$jsonData['content'];
        $this->provider = (string)$jsonData['provider'];
        $this->fileidlist = (string)$jsonData['file_id_list'];
        $this->isopenned = (int)$jsonData['is_openned'];
        $this->campaignid = (int)$jsonData['campaign_id'];
        $this->subscriberlistid = (string)$jsonData['subscriber_list_id'];
        $this->trackingdetail = (string)$jsonData['tracking_detail'];
        $this->status = (int)$jsonData['status'];
        $this->type = (int)$jsonData['type'];
        $this->ipaddress = (string)$jsonData['ip_address'];
        $this->datecreated = (int)$jsonData['date_created'];
        $this->datemodified = (int)$jsonData['date_modified'];
    }
}
