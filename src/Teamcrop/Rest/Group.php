<?php

namespace Teamcrop\Rest;

class Group extends Base
{
    public static $serviceurl = '/v1/groups';

    const STATUS_ENABLE = 1;
    const STATUS_DISABLED = 3;

    public $cid = 0;
    public $uid = 0;
    public $id = 0;
    public $code = '';
    public $name = '';
    public $description = '';
    public $color = '';
    public $countuser = 0;
    public $useridlist = '';
    public $useridadmin = '';
    public $fileidlist = '';
    public $icon = '';
    public $status = 0;
    public $ipaddress = 0;
    public $datecreated = 0;
    public $datemodified = 0;

    public $details = array();

    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData(&$error = array())
    {
        $data = array(
            'creator_id' => (int)$this->uid,
            'company_id' => (int)$this->cid,
            'code' => (string)$this->code,
            'name' => (string)$this->name,
            'description' => (string)$this->description,
            'color' => (string)$this->color,
            'details' => $this->details
        );

        $this->id = self::doAdd($data, '', '', $error);

        return $this->id;
    }

    public function updateData(&$error = array())
    {
        $data = array(
            'creator_id' => (int)$this->uid,
            'company_id' => (int)$this->cid,
            'code' => (string)$this->code,
            'name' => (string)$this->name,
            'description' => (string)$this->description,
            'color' => (string)$this->color,
            'details' => $this->details,
            'status' => (int)$this->status
        );

        $url = self::$serviceurl . '/' . $this->id;
        return self::doUpdate($data, $url, '', $error);
    }

    public function delete(&$error = array())
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }

    public static function getGroups(
        $formData,
        $sortby = '',
        $sorttype = '',
        $limitString = '',
        $countOnly = false
    ) {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }

    }

    public function getJsonData()
    {
        $countCustomer = 0;

        $data = array(
            'creator_id' => (int)$this->uid,
            'company_id' => (int)$this->cid,
            'id' => (int)$this->id,
            'code' => (string)$this->code,
            'name' => (string)$this->name,
            'description' => (string)$this->description,
            'color' => (string)$this->color,
            'count_user' => (int)$this->countuser,
            'details' => $this->details,
            'user_id_list' => explode(',', $this->useridlist),
            'user_id_admin' => explode(',', $this->useridadmin),
            'file_id_list' => explode(',', $this->fileidlist),
            'icon' => (string)$this->icon,
            'status' => (int)$this->status,
            'ip_address' => (string)$this->ipaddress,
            'date_created' => (int)$this->datecreated,
            'date_modified' => (int)$this->datemodified
        );

        return $data;
    }

    public function getDataByJson($jsonData)
    {
        $this->id = (int)$jsonData['id'];
        $this->cid = (int)$jsonData['company_id'];
        $this->uid = (int)$jsonData['creator_id'];
        $this->code = (string)$jsonData['code'];
        $this->name = (string)$jsonData['name'];
        $this->color = (string)$jsonData['color'];
        $this->description = (string)$jsonData['description'];
        $this->countuser = (string)$jsonData['count_user'];
        $this->details = $jsonData['details'];
        $this->useridlist = implode(',', $jsonData['user_id_list']);
        $this->useridadmin = implode(',', $jsonData['user_id_admin']);
        $this->fileidlist = implode(',', $jsonData['file_id_list']);
        $this->icon = (string)$jsonData['icon'];
        $this->status = (int)$jsonData['status'];
        $this->ipaddress = (int)$jsonData['ip_address'];
        $this->datecreated = (int)$jsonData['date_created'];
        $this->datemodifed = (int)$jsonData['date_modified'];
    }
}
