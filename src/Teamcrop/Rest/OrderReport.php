<?php

namespace Teamcrop\Rest;

class OrderReport extends Base
{
    public static $serviceurl = '/v1/orderreports';



    public function __construct($id = 0, $loadFromCache = false)
    {
        parent::__construct();


    }

    public function addData()
    {

    }

    public function updateData()
    {

    }


    public function delete()
    {

    }

    public static function getOrderReports($formData, $countOnly = false)
    {
        if ($countOnly) {
            return self::getRawItems($formData, "/v1/orderreports/total");
        } else {
            return self::getRawItems($formData, "/v1/orderreports/sum");
        }
    }
}
