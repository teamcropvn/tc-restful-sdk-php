<?php

namespace Teamcrop\Rest;

class ProductSerial extends Base
{
    public static $serviceurl = '/v1/productserials';

    const STATUS_AVAILABLE = 1;
    const  STATUS_ONHOLD = 3;
    const  STATUS_OUTOFSTOCK = 5;

    const QUALITY_STATUS_NEW = 1;
    const  QUALITY_STATUS_SHOWROOM = 3;
    const  QUALITY_STATUS_REFURBISHED = 5;
    const  QUALITY_STATUS_OLD = 7;

    const  SOURCE_PRODUCTRECEIPT = 1;
    const  SOURCE_POSORDER = 3;
    const  SOURCE_INVENTORYCHECK = 5;
    const  SOURCE_API = 7;

    public $cid = 0;
    public $uid = 0;
    public $pid = 0;
    public $poid = 0;
    public $id = 0;
    public $serialnumber = '';
    public $cost = '';
    public $note = '';
    public $source = 0;
    public $sourceid = 0;
    public $quantity = 0;
    public $currentwarehouseid = 0;
    public $status = 0;
    public $qualitystatus = 0;
    public $ipaddress = 0;
    public $datecreated = 0;
    public $datemodified = 0;
    public $datemanufactured = 0;
    public $dateexpired = 0;

    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData(&$error = array(), $dataMore = array())
    {
        $row = $this->getJsonData();
        $row = array_merge($row, $dataMore);
        $this->id = self::doAdd($row, '', '', $error);

        return $this->id;
    }

    public static function addListData($data, &$error = array())
    {
        return self::doAdd($data, self::$serviceurl . "/list", '', $error);
    }

    public function updateData(&$error = array(), $dataMore = array())
    {
        $row = $this->getJsonData();
        $row = array_merge($row, $dataMore);
        $url = self::$serviceurl . '/' . $this->id;
        return self::doUpdate($row, $url, '', $error);
    }

    public function delete(&$error = array())
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }

    public static function getProductSerialNumbers(
        $formData,
        $sortby = '',
        $sorttype = '',
        $limitString = '',
        $countOnly = false
    ) {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }
    }


    public function getDataByJson($row)
    {
        $this->cid = (int)$row['company_id'];
        $this->uid = (int)$row['creator_id'];
        $this->pid = (int)$row['product_id'];
        $this->poid = (int)$row['variant_id'];
        $this->id = (int)$row['id'];
        $this->serialnumber = (string)$row['serial_number'];
        $this->cost = (string)$row['cost'];
        $this->note = (string)$row['note'];
        $this->source = (int)$row['source'];
        $this->sourceid = (int)$row['source_id'];
        $this->quantity = (int)$row['quantity'];
        $this->currentwarehouseid = (int)$row['current_warehouse_id'];
        $this->status = (int)$row['status'];
        $this->qualitystatus = (int)$row['quality_status'];
        $this->ipaddress = (string)$row['ip_address'];
        $this->datecreated = (int)$row['date_created'];
        $this->datemodified = (int)$row['date_modified'];
        $this->datemanufactured = (int)$row['date_manufactured'];
        $this->dateexpired = (int)$row['date_expired'];
    }

    public function getJsonData()
    {
        $row['company_id'] = (int)$this->cid;
        $row['creator_id'] = (int)$this->uid;
        $row['product_id'] = (int)$this->pid;
        $row['variant_id'] = (int)$this->poid;
        $row['id'] = (int)$this->id;
        $row['serial_number'] = (string)$this->serialnumber;
        $row['cost'] = (string)$this->cost;
        $row['note'] = (string)$this->note;
        $row['source'] = (int)$this->source;
        $row['source_id'] = (int)$this->sourceid;
        $row['quantity'] = (int)$this->quantity;
        $row['current_warehouse_id'] = (int)$this->currentwarehouseid;
        $row['status'] = (int)$this->status;
        $row['quality_status'] = (int)$this->qualitystatus;
        $row['ip_address'] = (string)$this->ipaddress;
        $row['date_created'] = (int)$this->datecreated;
        $row['date_modified'] = (int)$this->datemodified;
        $row['date_manufactured'] = (int)$this->datemanufactured;
        $row['date_expired'] = (int)$this->dateexpired;

        return $row;
    }


}
