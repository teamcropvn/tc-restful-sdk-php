<?php

namespace Teamcrop\Rest;

class CompanyResource extends Base
{
    public static $serviceurl = '/v1/companyresources';

    public $cid = 0;
    public $id = 0;
    public $jsversion = array();
    public $datecreated = 0;
    public $datemodified = 0;
    public $datelastaccessed = 0;
    public $hmac = '';

    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData(&$error = array())
    {
        $data = array(
            'company_id' => $this->cid,
        );

        $this->id = self::doAdd($data, '', '', $error);

        return $this->id;
    }

    public function updateData(&$error = array())
    {
        $data = array(
            'company_id' => $this->cid,
            'jsversion' => $this->jsversion,
        );

        $url = self::$serviceurl . '/' . $this->id;
        return self::doUpdate($data, $url, '', $error);
    }

    public function delete(&$error = array())
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }

    public static function getCompanyResources($formData, $sortby = '', $sorttype = '', $limitString = '', $countOnly = false)
    {
        return self::getItems($formData, $sortby, $sorttype, $limitString, $countOnly);
    }

    public function getDataByJson($jsonData)
    {
        $this->cid = (int)$jsonData['company_id'];
        $this->id = (int)$jsonData['id'];
        $this->jsversion = (array)$jsonData['jsversion'];
        $this->datecreated = (int)$jsonData['date_created'];
        $this->datemodified = (int)$jsonData['date_modified'];
        $this->datelastaccessed = (int)$jsonData['date_last_accessed'];
    }

    public static function verifyHmac($queryData)
    {
        $pass = false;

        $myCompany = new Company($queryData['cid'], true);

        if ($myCompany->id > 0) {
            $data = 'cid=' . $queryData['cid'] . '&rid=' . $queryData['rid'];

            //We do not need to urlencode because queryData['hmac'] already decode after coming to app
            $calculatedHmac = base64_encode(hash_hmac('sha256', $data, $myCompany->apikey, true));

            $pass = ($queryData['hmac'] != '' && $calculatedHmac === $queryData['hmac']);
        }

        return $pass;
    }

    public static function increaseVersion($companyid, $table, &$error = array())
    {
        $responseData = null;

        $data = array(
            'company_id' => $companyid,
            'table' => $table,
        );

        $serviceurl = self::$serviceurl . '/increaseversion';
        $headers = array(
            'Content-type' => 'application/json'
        );

        try {
            //Do request and get response with submit data
            $response = self::doRequest('PUT', $serviceurl, $headers, false, true, json_encode($data));

            //request success
            if ($response['status'] == '200' || $response['status'] == '201') {
                $responseData = $response['data'];
            } else {
                $error = self::parsingErrorFromResponse($response);
            }
        } catch (\Exception $e) {
            $error[] = $e->getMessage();
        }

        return $responseData;
    }
}
