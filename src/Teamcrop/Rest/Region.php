<?php

namespace Teamcrop\Rest;

class Region extends Base
{
    public static $serviceurl = '/v1/regions';

    public $id = 0;
    public $name = '';
    public $slug = '';
    public $country = '';
    public $displayorder = 0;
    public $parentid = 0;
    public $level = 0;
    public $lat = 0;
    public $lng = 0;

    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData()
    {

    }

    public function updateData()
    {

    }


    public function delete()
    {

    }

    public static function getRegions($formData, $sortby = '', $sorttype = '', $limitString = '', $countOnly = false)
    {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }
    }

    public function getJsonData()
    {
        $data = array(
            'id' => (int)$this->id,
            'name' => (string)$this->name,
            'slug' => (string)$this->slug,
            'country' => (string)$this->country,
            'display_order' => (int)$this->displayorder,
            'parent_id' => (int)$this->parentid,
            'level' => (int)$this->level,
            'lat' => (float)$this->lat,
            'lng' => (float)$this->lng
        );

        return $data;
    }

    public function getDataByJson($jsonData)
    {
        $this->id = (int)$jsonData['id'];
        $this->name = (int)$jsonData['name'];
        $this->slug = (int)$jsonData['slug'];
        $this->country = (int)$jsonData['country'];
        $this->displayorder = (int)$jsonData['display_order'];
        $this->parentid = (int)$jsonData['parent_id'];
        $this->level = (int)$jsonData['level'];
        $this->lat = (int)$jsonData['lat'];
        $this->lng = (int)$jsonData['lng'];
    }
}
