<?php

namespace Teamcrop\Rest;

class PromotionCouponUsage extends Base
{
    public static $serviceurl = '/v1/promotioncouponusages';

    const VALUETYPE_PERCENT = 1;
    const VALUETYPE_VALUE = 3;

    const TYPE_COUPON = 1;
    const TYPE_VOUCHER = 3;
    const TYPE_GIFTCARD = 5;

    const STATUS_ENABLE = 1;
    const STATUS_DISABLE = 3;
    const STATUS_ERROR = 5;


    public $cid = 0;
    public $uid = 0;
    public $sid = 0; // Store id
    public $soid = 0; // Order id
    public $ccid = 0; //Customer id
    public $id = 0;
    public $type = 0;
    public $code = '';
    public $ipaddress = 0;
    public $valuetype = 0;
    public $value = 0;
    public $status = 0;
    public $note = '';
    public $datecreated = 0;
    public $datemodified = 0;

    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData(&$error = array())
    {
        $data = array(
            'creator_id' => $this->uid,
            'company_id' => $this->cid,
            'title' => $this->title

        );

        //$this->id = self::doAdd($data, '', '', $error);

        return $this->id;
    }

    public function updateData(&$error = array())
    {
        // $data = array(
        //     'creator_id' => $this->uid,
        //     'company_id' => $this->cid
        // );
        //
        // $url = self::$serviceurl . '/' . $this->id;
        // return self::doUpdate($data, $url, '', $error);
    }

    public function delete(&$error = array())
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }

    public static function getPromotionCouponUsages($formData, $sortby = '', $sorttype = '', $limitString = '', $countOnly = false)
    {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }
    }

    public function getDataByJson($row)
    {
        $this->cid = (int)$row['company_id'];
        $this->uid = (int)$row['creator_id'];
        $this->sid = (int)$row['store_id'];
        $this->soid = (int)$row['order_id'];
        $this->ccid = (int)$row['customer_id'];
        $this->id = (int)$row['id'];
        $this->type = (int)$row['type'];
        $this->code = (string)$row['code'];
        $this->ipaddress = $row['ip_address'];
        $this->valuetype = (int)$row['value_type'];
        $this->value = (float)$row['value'];
        $this->status = (int)$row['status'];
        $this->note = (string)$row['note'];
        $this->datecreated = (int)$row['date_created'];
        $this->datemodified = (int)$row['date_modified'];
    }

    public function getJsonData()
    {
        $data = array(
            'company_id' => (int)$this->cid,
            'creator_id' => (int)$this->uid,
            'store_id' => (int)$this->sid,
            'order_id' => (int)$this->soid,
            'customer_id' => (int)$this->ccid,
            'id' => (int)$this->id,
            'type' => (int)$this->type,
            'code' => (string)$this->code,
            'ip_address' => $this->ipaddress,
            'value_type' => (int)$this->valuetype,
            'value' => (float)$this->value,
            'status' => (int)$this->status,
            'note' => (string)$this->note,
            'date_created' => (int)$this->datecreated,
            'date_modified' => (int)$this->datemodified
        );

        return $data;
    }
}
