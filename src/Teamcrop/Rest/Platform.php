<?php

namespace Teamcrop\Rest;

class Platform extends Base
{
    public static $serviceurl = '/v1/platforms';

    const STATUS_SETUP = 1;
    const STATUS_ENABLE = 3;
    const STATUS_DISABLE = 5;

    const SOURCE_SHOPIFY = 1;
    const SOURCE_HARAVAN = 3;
    const SOURCE_BIZWEB = 5;

    const CURRENCY_SYMBOL_POSITION_LEFT = 1;
    const CURRENCY_SYMBOL_POSITION_RIGHT = 3;

    const PRODUCT_SCOPE_READ = 1;
    const PRODUCT_SCOPE_WRITE = 3;
    const PRODUCT_SCOPE_ALL = 5;

    const ORDER_SCOPE_READ = 1;
    const ORDER_SCOPE_WRITE = 3;
    const ORDER_SCOPE_ALL = 5;

    const CUSTOMER_SCOPE_READ = 1;
    const CUSTOMER_SCOPE_WRITE = 3;
    const CUSTOMER_SCOPE_ALL = 5;

    public $uid = 0;
    public $id = 0;
    public $source = 0;
    public $sourceid = '';
    public $storename = '';
    public $platformdomain = '';
    public $domain = '';
    public $owner = '';
    public $email = '';
    public $phone = '';
    public $address = '';
    public $city = '';
    public $region = 0;
    public $zipcode = '';
    public $country = '';
    public $timezone = '';
    public $currency = '';
    public $currencysymbol = '';
    public $currencysymbolposition = 0;
    public $decimalseparator = '';
    public $thousandseparator = '';
    public $decimalplace = 0;
    public $language = '';
    public $planname = '';
    public $enableembed = 0;
    public $enablestandalone = 0;
    public $productscope = 0;
    public $producttotal = 0;
    public $productpullstarted = 0;
    public $productpullended = 0;
    public $productpullprogress = 0;
    public $productdatelastwebhook = 0;
    public $productenablepush = 0;
    public $orderscope = 0;
    public $ordertotal = 0;
    public $orderpullstarted = 0;
    public $orderpullended = 0;
    public $orderpullprogress = 0;
    public $orderdatelastwebhook = 0;
    public $orderenablepush = 0;
    public $customerscope = 0;
    public $customertotal = 0;
    public $customerpullstarted = 0;
    public $customerpullended = 0;
    public $customerpullprogress = 0;
    public $customerdatelastwebhook = 0;
    public $customerenablepush = 0;
    public $accesstoken = '';
    public $refreshtoken = '';
    public $setupstep = '';
    public $status = 0;
    public $ipaddress = 0;
    public $datecreated = 0;
    public $datemodified = 0;
    public $datesourcelastupdated = 0;
    public $dateaccesstokenpulled = 0;
    public $dateaccesstokenrefreshed = 0;
    public $dateaccesstokenexpired = 0;
    public $datelastaccessed = 0;
    public $datelastaccessedembed = 0;
    public $datelastaccessedstandalone = 0;

    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData(&$error = array())
    {
        $data = array(
            'creator_id' => $this->uid,
            'source' => $this->source,
            'source_id' => $this->sourceid,
            'store_name' => $this->storename,
            'platform_domain' => $this->platformdomain,
            'domain' => $this->domain,
            'owner' => $this->owner,
            'email' => $this->email,
            'phone' => $this->phone,
            'address' => $this->address,
            'city' => $this->city,
            'region_id' => $this->region,
            'zip_code' => $this->zipcode,
            'country' => $this->country,
            'timezone' => $this->timezone,
            'currency' => $this->currency,
            'currency_symbol' => $this->currencysymbol,
            'currency_symbol_position' => $this->currencysymbolposition,
            'decimal_separator' => $this->decimalseparator,
            'thousand_separator' => $this->thousandseparator,
            'decimal_place' => $this->decimalplace,
            'language' => $this->language,
            'planname' => $this->planname,
            'enable_embed' => $this->enableembed,
            'enable_standalone' => $this->enablestandalone,
            'product_scope' => $this->productscope,
            'product_total' => $this->producttotal,
            'product_pull_started' => $this->productpullstarted,
            'product_pull_ended' => $this->productpullended,
            'product_pull_progress' => $this->productpullprogress,
            'product_enable_push' => $this->productenablepush,
            'order_scope' => $this->orderscope,
            'order_total' => $this->ordertotal,
            'order_pull_started' => $this->orderpullstarted,
            'order_pull_ended' => $this->orderpullended,
            'order_pull_progress' => $this->orderpullprogress,
            'order_enable_push' => $this->orderenablepush,
            'customer_scope' => $this->customerscope,
            'customer_total' => $this->customertotal,
            'customer_pull_started' => $this->customerpullstarted,
            'customer_pull_ended' => $this->customerpullended,
            'customer_pull_progress' => $this->customerpullprogress,
            'customer_enable_push' => $this->customerenablepush,
            'access_token' => $this->accesstoken,
            'refresh_token' => $this->refreshtoken,
            'setup_step' => $this->setupstep
        );

        $this->id = self::doAdd($data, '', '', $error);

        return $this->id;
    }

    public function updateData(&$error = array())
    {
        $data = array(
            'creator_id' => $this->uid,
            'source' => $this->source,
            'source_id' => $this->sourceid,
            'store_name' => $this->storename,
            'platform_domain' => $this->platformdomain,
            'domain' => $this->domain,
            'owner' => $this->owner,
            'email' => $this->email,
            'phone' => $this->phone,
            'address' => $this->address,
            'city' => $this->city,
            'region_id' => $this->region,
            'zip_code' => $this->zipcode,
            'country' => $this->country,
            'timezone' => $this->timezone,
            'currency' => $this->currency,
            'currency_symbol' => $this->currencysymbol,
            'currency_symbol_position' => $this->currencysymbolposition,
            'decimal_separator' => $this->decimalseparator,
            'thousand_separator' => $this->thousandseparator,
            'decimal_place' => $this->decimalplace,
            'language' => $this->language,
            'planname' => $this->planname,
            'enable_embed' => $this->enableembed,
            'enable_standalone' => $this->enablestandalone,
            'product_scope' => $this->productscope,
            'product_total' => $this->producttotal,
            'product_pull_started' => $this->productpullstarted,
            'product_pull_ended' => $this->productpullended,
            'product_pull_progress' => $this->productpullprogress,
            'product_enable_push' => $this->productenablepush,
            'order_scope' => $this->orderscope,
            'order_total' => $this->ordertotal,
            'order_pull_started' => $this->orderpullstarted,
            'order_pull_ended' => $this->orderpullended,
            'order_pull_progress' => $this->orderpullprogress,
            'order_enable_push' => $this->orderenablepush,
            'customer_scope' => $this->customerscope,
            'customer_total' => $this->customertotal,
            'customer_pull_started' => $this->customerpullstarted,
            'customer_pull_ended' => $this->customerpullended,
            'customer_pull_progress' => $this->customerpullprogress,
            'customer_enable_push' => $this->customerenablepush,
            'access_token' => $this->accesstoken,
            'refresh_token' => $this->refreshtoken,
            'setup_step' => $this->setupstep,
            'status' => $this->status
        );

        $url = self::$serviceurl . '/' . $this->id;
        return self::doUpdate($data, $url, '', $error);
    }


    public function delete(&$error = array())
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }

    public static function getPlatforms($formData, $sortby = '', $sorttype = '', $limitString = '', $countOnly = false)
    {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }
    }

    public function getJsonData()
    {
        $data = array(
            'creator_id' => (int)$this->u_id,
            'id' => (int)$this->id,
            'source' => (int)$this->source,
            'source_id' => (string)$this->sourceid,
            'store_name' => (string)$this->storename,
            'platform_domain' => (string)$this->platformdomain,
            'domain' => (string)$this->domain,
            'owner' => (string)$this->owner,
            'email' => (string)$this->email,
            'phone' => (string)$this->phone,
            'address' => (string)$this->address,
            'city' => (string)$this->city,
            'region_id' => (int)$this->region,
            'zip_code' => (string)$this->zipcode,
            'country' => (string)$this->country,
            'timezone' => (string)$this->timezone,
            'currency' => (string)$this->currency,
            'currency_symbol' => (string)$this->currencysymbol,
            'currency_symbol_position' => (int)$this->currencysymbolposition,
            'decimal_separator' => (string)$this->decimalseparator,
            'thousand_separator' => (string)$this->thousandseparator,
            'decimal_place' => (int)$this->decimalplace,
            'language' => (string)$this->language,
            'planname' => (string)$this->planname,
            'enable_embed' => (int)$this->enableembed,
            'enable_standalone' => (int)$this->enablestandalone,
            'product_scope' => (int)$this->productscope,
            'product_total' => (int)$this->producttotal,
            'product_pull_started' => (int)$this->productpullstarted,
            'product_pull_ended' => (int)$this->productpullended,
            'product_pull_progress' => (int)$this->productpullprogress,
            'product_date_last_webhook' => (int)$this->productdatelastwebhook,
            'product_enable_push' => (int)$this->productenablepush,
            'order_scope' => (int)$this->orderscope,
            'order_total' => (int)$this->ordertotal,
            'order_pull_started' => (int)$this->orderpullstarted,
            'order_pull_ended' => (int)$this->orderpullended,
            'order_pull_progress' => (int)$this->orderpullprogress,
            'order_date_last_webhook' => (int)$this->orderdatelastwebhook,
            'order_enable_push' => (int)$this->orderenablepush,
            'customer_scope' => (int)$this->customerscope,
            'customer_total' => (int)$this->customertotal,
            'customer_pull_started' => (int)$this->customerpullstarted,
            'customer_pull_ended' => (int)$this->customerpullended,
            'customer_pull_progress' => (int)$this->customerpullprogress,
            'customer_date_last_webhook' => (int)$this->customerdatelastwebhook,
            'customer_enable_push' => (int)$this->customerenablepush,
            'access_token' => (string)$this->accesstoken,
            'refresh_token' => (string)$this->refreshtoken,
            'setup_step' => (string)$this->setupstep,
            'status' => (int)$this->status,
            'ip_address' => (int)$this->ipaddress,
            'date_created' => (int)$this->datecreated,
            'date_modified' => (int)$this->datemodified,
            'date_source_last_updated' => (int)$this->datesourcelastupdated,
            'date_access_token_pulled' => (int)$this->dateaccesstokenpulled,
            'date_access_token_refreshed' => (int)$this->dateaccesstokenrefreshed,
            'date_access_token_expired' => (int)$this->dateaccesstokenexpired,
            'date_last_accessed' => (int)$this->datelastaccessed,
            'date_last_accessed_embed' => (int)$this->datelastaccessedembed,
            'date_last_accessed_standalon' => (int)$this->datelastaccessedstandalon
        );
        return $data;
    }

    public static function cacheKey($id)
    {
        return 'tc_platform_' . $id;
    }

    public function getDataByArrayCache($row)
    {

    }

    public function getDataByJson($jsonData)
    {
        $this->uid = (int)$jsonData['creator_id'];
        $this->id = (int)$jsonData['id'];
        $this->source = (int)$jsonData['source'];
        $this->sourceid = (string)$jsonData['source_id'];
        $this->storename = (string)$jsonData['store_name'];
        $this->platformdomain = (string)$jsonData['platform_domain'];
        $this->domain = (string)$jsonData['domain'];
        $this->owner = (string)$jsonData['owner'];
        $this->email = (string)$jsonData['email'];
        $this->phone = (string)$jsonData['phone'];
        $this->address = (string)$jsonData['address'];
        $this->city = (string)$jsonData['city'];
        $this->region = (int)$jsonData['region_id'];
        $this->zipcode = (string)$jsonData['zip_code'];
        $this->country = (string)$jsonData['country'];
        $this->timezone = (string)$jsonData['timezone'];
        $this->currency = (string)$jsonData['currency'];
        $this->currencysymbol = (string)$jsonData['currency_symbol'];
        $this->currencysymbolposition = (int)$jsonData['currency_symbol_position'];
        $this->decimalseparator = (string)$jsonData['decimal_separator'];
        $this->thousandseparator = (string)$jsonData['thousand_separator'];
        $this->decimalplace = (int)$jsonData['decimal_place'];
        $this->language = (string)$jsonData['language'];
        $this->planname = (string)$jsonData['planname'];
        $this->enableembed = (int)$jsonData['enable_embed'];
        $this->enablestandalone = (int)$jsonData['enable_standalone'];
        $this->productscope = (int)$jsonData['product_scope'];
        $this->producttotal = (int)$jsonData['product_total'];
        $this->productpullstarted = (int)$jsonData['product_pull_started'];
        $this->productpullended = (int)$jsonData['product_pull_ended'];
        $this->productpullprogress = (int)$jsonData['product_pull_progress'];
        $this->productdatelastwebhook = (int)$jsonData['product_date_last_webhook'];
        $this->productenablepush = (int)$jsonData['product_enable_push'];
        $this->orderscope = (int)$jsonData['order_scope'];
        $this->ordertotal = (int)$jsonData['order_total'];
        $this->orderpullstarted = (int)$jsonData['order_pull_started'];
        $this->orderpullended = (int)$jsonData['order_pull_ended'];
        $this->orderpullprogress = (int)$jsonData['order_pull_progress'];
        $this->orderdatelastwebhook = (int)$jsonData['order_date_last_webhook'];
        $this->orderenablepush = (int)$jsonData['order_enable_push'];
        $this->customerscope = (int)$jsonData['customer_scope'];
        $this->customertotal = (int)$jsonData['customer_total'];
        $this->customerpullstarted = (int)$jsonData['customer_pull_started'];
        $this->customerpullended = (int)$jsonData['customer_pull_ended'];
        $this->customerpullprogress = (int)$jsonData['customer_pull_progress'];
        $this->customerdatelastwebhook = (int)$jsonData['customer_date_last_webhook'];
        $this->customerenablepush = (int)$jsonData['customer_enable_push'];
        $this->accesstoken = (string)$jsonData['access_token'];
        $this->refreshtoken = (string)$jsonData['refresh_token'];
        $this->setupstep = (string)$jsonData['setup_step'];
        $this->status = (int)$jsonData['status'];
        $this->ipaddress = (int)$jsonData['ip_address'];
        $this->datecreated = (int)$jsonData['date_created'];
        $this->datemodified = (int)$jsonData['date_modified'];
        $this->datesourcelastupdated = (int)$jsonData['date_source_last_updated'];
        $this->dateaccesstokenpulled = (int)$jsonData['date_access_token_pulled'];
        $this->dateaccesstokenrefreshed = (int)$jsonData['date_access_token_refreshed'];
        $this->dateaccesstokenexpired = (int)$jsonData['date_access_token_expired'];
        $this->datelastaccessed = (int)$jsonData['date_last_accessed'];
        $this->datelastaccessedembed = (int)$jsonData['date_last_accessed_embed'];
        $this->datelastaccessedstandalon = (int)$jsonData['date_last_accessed_standalon'];
    }
}
