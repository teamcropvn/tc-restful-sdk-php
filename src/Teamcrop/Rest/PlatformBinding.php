<?php

namespace Teamcrop\Rest;

class PlatformBinding extends Base
{
    public static $serviceurl = '/v1/platformbindings';

    public $uid = 0;
    public $cid = 0;
    public $slid = 0;
    public $pid = 0;
    public $id = 0;
    public $note = '';
    public $ipaddress = 0;
    public $datecreated = 0;
    public $datemodified = 0;

    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData(&$error = array())
    {
        $data = array(
            'note' => $this->note,
            'platform_id' => $this->pid,
            'store_id' => $this->slid,
            'company_id' => $this->cid,
            'creator_id' => $this->uid
        );

        $this->id = self::doAdd($data, '', '', $error);

        return $this->id;
    }

    public function updateData(&$error = array())
    {
        $data = array(
            'note' => $this->note,
            'platform_id' => $this->pid,
            'store_id' => $this->slid,
            'company_id' => $this->cid,
            'creator_id' => $this->uid
        );

        $url = self::$serviceurl . '/' . $this->id;
        return self::doUpdate($data, $url, '', $error);
    }


    public function delete(&$error = array())
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }

    public static function getPlatformBindings(
        $formData,
        $sortby = '',
        $sorttype = '',
        $limitString = '',
        $countOnly = false
    )
    {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }
    }

    public function getJsonData()
    {
        $data = array(
            'creator_id' => (int)$this->uid,
            'company_id' => (int)$this->cid,
            'store_id' => (int)$this->slid,
            'platform_id' => (int)$this->pid,
            'id' => (int)$this->id,
            'note' => (string)$this->note,
            'ip_address' => (int)$this->ipaddress,
            'date_created' => (int)$this->datecreated,
            'date_modified' => (int)$this->datemodified,

        );
        return $data;
    }

    public static function cacheKey($id)
    {
        return 'tc_platform_binding_' . $id;
    }

    public function getDataByArrayCache($row)
    {

    }

    public function getDataByJson($jsonData)
    {
        $this->uid = (int)$jsonData['creator_id'];
        $this->cid = (int)$jsonData['company_id'];
        $this->slid = (int)$jsonData['store_id'];
        $this->pid = (int)$jsonData['platform_id'];
        $this->id = (int)$jsonData['id'];
        $this->note = (string)$jsonData['note'];
        $this->ipaddress = (int)$jsonData['ip_address'];
        $this->datecreated = (int)$jsonData['date_created'];
        $this->datemodified = (int)$jsonData['date_modified'];
    }
}
