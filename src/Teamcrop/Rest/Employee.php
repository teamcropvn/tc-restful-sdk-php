<?php

namespace Teamcrop\Rest;

class Employee extends Base
{
    public static $serviceurl = '/v1/employees';

    const STATUS_UNACTIVATED = 0;
    const STATUS_ENABLE = 1;
    const STATUS_DISABLED = 3;
    const STATUS_DELETED = 5;

    public $uid = 0;
    public $cdid = 0;
    public $coid = 0;
    public $cid = 0;
    public $cpid = 0;
    public $id = 0;
    public $internalid = '';
    public $isexternalpartner = 0;
    public $fullname = '';
    public $jobtitle = '';
    public $jobdescription = '';
    public $email = '';
    public $phone = '';
    public $address = '';
    public $region = 0;
    public $creatorid = 0;
    public $managerid = 0;
    public $permission = 0;
    public $ipaddress = 0;
    public $status = 0;
    public $isdeleted = 0;
    public $datecreated = 0;
    public $datemodified = 0;
    public $datedeleted = 0;
    public $datelastloggedin = 0;
    public $datelastactivity = 0;
    public $dateactivated = 0;

    public $newnotification = 0;
    public $newmessage = 0;
    public $newchat = 0;
    public $init = 0;

    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData(&$error = array())
    {
        $data = array(
            'creator_id' => $this->creatorid,
            'company_id' => $this->cid,
            'user_id' => $this->uid,
            'department_id' => $this->cdid,
            'office_id' => $this->coid,
            'position_id' => $this->cpid,
            'internal_id' => $this->internalid,
            'is_external_partner' => $this->isexternalpartner,
            'full_name' => $this->fullname,
            'job_title' => $this->jobtitle,
            'job_description' => $this->jobdescription,
            'email' => $this->email,
            'phone' => $this->phone,
            'address' => $this->address,
            'region_id' => $this->region,
            'manager_id' => $this->managerid,
            'permission' => $this->permission,
            'status' => $this->status,
            'init' => $this->init
        );

        $this->id = self::doAdd($data, '', '', $error);

        return $this->id;
    }

    public function updateData(&$error = array())
    {
        $data = array(
            'creator_id' => $this->creatorid,
            'company_id' => $this->cid,
            'department_id' => $this->cdid,
            'office_id' => $this->coid,
            'position_id' => $this->cpid,
            'internal_id' => $this->internalid,
            'is_external_partner' => $this->isexternalpartner,
            'full_name' => $this->fullname,
            'job_title' => $this->jobtitle,
            'job_description' => $this->jobdescription,
            'email' => $this->email,
            'phone' => $this->phone,
            'address' => $this->address,
            'region_id' => $this->region,
            'manager_id' => $this->managerid,
            'permission' => $this->permission,
            'status' => $this->status
        );

        $url = self::$serviceurl . '/' . $this->id;
        return self::doUpdate($data, $url, '', $error);
    }

    public function delete(&$error = array())
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }

    public static function getEmployees($formData, $sortby = '', $sorttype = '', $limitString = '', $countOnly = false)
    {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }
    }

    public static function adminGetEmployees($formData, $sortby = '', $sorttype = '', $limitString = '', $countOnly = false)
    {
        $url = self::$serviceurl . '/admin';
        if ($countOnly) {
            return self::countItems($formData, $limitString, $url);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString, $url);
        }
    }

    public static function isEmployed($cid, $uid)
    {
        return self::countItems(array('company_id' => $cid, 'user_id' => $uid, 'status' => 1)) > 0;
    }

    public function getJsonData()
    {
        $myDepartment = new Department($this->cdid, true);
        $myOffice = new Office($this->coid, true);

        $data = array(
            'id' => (int)$this->id,
            'is_group_id' => (int)$this->isgroupid,
            'user_id' => (int)$this->uid,
            'department' => $myDepartment->getJsonData(),
            'office' => $myOffice->getJsonData(),
            'department_id' => (int)$this->cdid,
            'office_id' => (int)$this->coid,
            'company_id' => $this->cid,
            'position_id' => (int)$this->cpid,
            'internal_id' => (string)$this->internalid,
            'full_name' => (string)$this->fullname,
            'job_title' => (string)$this->jobtitle,
            'job_description' => (string)$this->jobdescription,
            'gender' => (int)$this->gender,
            'type' => (int)$this->type,
            'email' => (string)$this->email,
            'phone' => (string)$this->phone,
            'address' => (string)$this->address,
            'region_id' => (int)$this->region,
            'creator_id' => (int)$this->creatorid,
            'ip_address' => (string)$this->ipaddress,
            'ignore_ip_checkin' => (int)$this->ignoreipcheckin,
            'status' => (int)$this->status,
            'is_deleted' => (int)$this->isdeleted,
            'date_created' => (int)$this->datecreated,
            'date_modified' => (int)$this->datemodified,
            'date_deleted' => (int)$this->datedeleted
        );

        return $data;
    }

    public function getDataByJson($jsonData)
    {
        $this->uid = (int)$jsonData['user_id'];
        $this->cdid = (int)$jsonData['department']['id'];
        $this->coid = (int)$jsonData['office']['id'];
        $this->cid = (int)$jsonData['company_id'];
        $this->cpid = (int)$jsonData['position_id'];
        $this->id = (int)$jsonData['id'];
        $this->internalid = (string)$jsonData['internal_id'];
        $this->isexternalpartner = (int)$jsonData['is_external_partner'];
        $this->fullname = (string)$jsonData['full_name'];
        $this->jobtitle = (string)$jsonData['job_title'];
        $this->jobdescription = (string)$jsonData['job_description'];
        $this->email = (string)$jsonData['email'];
        $this->phone = (string)$jsonData['phone'];
        $this->address = (string)$jsonData['address'];
        $this->region = (int)$jsonData['region_id'];
        $this->creatorid = (int)$jsonData['creator_id'];
        $this->ipaddress = $jsonData['ip_address'];
        $this->status = (int)$jsonData['status'];
        $this->isdeleted = (int)$jsonData['is_deleted'];
        $this->datecreated = (int)$jsonData['date_created'];
        $this->datemodified = (int)$jsonData['date_modified'];
        $this->datedeleted = (int)$jsonData['date_deleted'];
        $this->dateactivated = (int)$jsonData['date_activated'];
        $this->newnotification = (int)$jsonData['new_notification'];
        $this->newmessage = (int)$jsonData['new_message'];
    }

    /**
     * Get only User ID list of all employees from company
     *
     * @param $companyId
     * @param array $error
     * @return array userid ist
     */
    public static function getUserIdList($companyId, &$error)
    {
        $userIdList = array();

        $serviceurl = self::$serviceurl . '/useridlist?company_id=' . $companyId;

        try {
            //Do request and get response with submit data
            $response = self::doRequest('GET', $serviceurl, array(), false, true);

            //request success
            if ($response['status'] == '200') {
                if (trim($response['data']['ids']) != '') {
                    $userIdList = explode(',', $response['data']['ids']);
                }

            } else {
                $error = self::parsingErrorFromResponse($response);
            }
        } catch (\Exception $e) {
            $error[] = $e->getMessage();
        }

        return $userIdList;
    }

    /**
     * Get User ID and fullname list of all employees from company
     *
     * @param $companyId
     * @param array $error
     * @return array userid ist
     */
    public static function getUserList($companyId, &$error)
    {
        $userList = array();

        $serviceurl = self::$serviceurl . '/userlist?company_id=' . $companyId;

        try {
            //Do request and get response with submit data
            $response = self::doRequest('GET', $serviceurl, array(), false, true);

            //request success
            if ($response['status'] == '200') {
                if (is_array($response['data']['items']) != '') {
                    $userList = $response['data']['items'];
                }

            } else {
                $error = self::parsingErrorFromResponse($response);
            }
        } catch (\Exception $e) {
            $error[] = $e->getMessage();
        }

        return $userList;
    }


    public static function generateLoginToken($jwt, $userId, $companyId, $employeeId)
    {
        $token = '';

        $data = array(
            'jwt' => $jwt,
            'user_id' => $userId,
            'company_id' => $companyId,
            'employee_id' => $employeeId
        );

        $serviceurl = self::$serviceurl . '/token';
        $headers = array(
            'Content-type' => 'application/json'
        );

        try {
            //Do request and get response with submit data
            $response = self::doRequest('POST', $serviceurl, $headers, false, true, json_encode($data));

            //request success
            if ($response['status'] == '200' || $response['status'] == '201') {
                $token = $response['data']['token'];
            } else {
                $error = self::parsingErrorFromResponse($response);
            }
        } catch (\Exception $e) {
            $error[] = $e->getMessage();
        }

        return $token;
    }

    public static function fetchLoginToken($employeetoken, &$error)
    {
        $employeeInfo = array();

        $serviceurl = self::$serviceurl . '/token?token=' . $employeetoken;

        try {
            //Do request and get response with submit data
            $response = self::doRequest('GET', $serviceurl, array(), false);

            //request success
            if ($response['status'] == '200') {
                $employeeInfo = $response['data']['employee'];
            } else {
                $error = self::parsingErrorFromResponse($response);
            }
        } catch (\Exception $e) {
            $error[] = $e->getMessage();
        }

        return $employeeInfo;
    }



    
}
