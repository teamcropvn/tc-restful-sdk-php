<?php

namespace Teamcrop\Rest;

class Employee extends Base
{
    public static $serviceurl = '/v1/employees';

    const STATUS_UNACTIVATED = 0;
    const STATUS_ENABLE = 1;
    const STATUS_DISABLED = 3;
    const STATUS_DELETED = 5;

    public $uid = 0;
    public $cdid = 0;
    public $coid = 0;
    public $cid = 0;
    public $cpid = 0;
    public $id = 0;
    public $internalid = '';
    public $isexternalpartner = 0;
    public $jobtitle = '';
    public $jobdescription = '';
    public $email = '';
    public $phone = '';
    public $address = '';
    public $region = 0;
    public $creatorid = 0;
    public $managerid = 0;
    public $permission = 0;
    public $ipaddress = 0;
    public $status = 0;
    public $isdeleted = 0;
    public $datecreated = 0;
    public $datemodified = 0;
    public $datedeleted = 0;
    public $datelastloggedin = 0;
    public $datelastactivity = 0;
    public $dateactivated = 0;

    public $newnotification = 0;
    public $newmessage = 0;
    public $newchat = 0;

    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData()
    {
        //Build request body
        $jsonRequest = array(
            'cid' => (int)$this->cid,
            'uid' => (int)$this->uid,
            'cdid' => (int)$this->cdid,
            'coid' => (int)$this->coid,
        );

        return (int)self::addItem(array());
    }

    public function updateData()
    {

    }


    public function delete()
    {

    }

    public static function getEmployees($formData, $sortby = '', $sorttype = '', $limitString = '', $countOnly = false)
    {
        if($countOnly) {
            return self::countItems($formData, $limitString);
        }
		else {
        	return self::getItems($formData, $sortby, $sorttype, $limitString);
		}
    }

    public static function isEmployed($cid, $uid)
    {
        return self::countItems(array('company_id' => $cid, 'user_id' => $uid, 'status' => 1)) > 0;
    }

    public function getDataByJson($jsonData)
    {
        $this->uid = (int)$jsonData['user']['id'];
        $this->cdid = (int)$jsonData['department']['id'];
        $this->coid = (int)$jsonData['office']['id'];
        $this->cid = (int)$jsonData['company_id'];
        $this->cpid = (int)$jsonData['position_id'];
        $this->id = (int)$jsonData['id'];
        $this->internalid = (string)$jsonData['internal_id'];
        $this->isexternalpartner = (int)$jsonData['is_external_partner'];
        $this->jobtitle = (string)$jsonData['job_title'];
        $this->jobdescription = (string)$jsonData['job_description'];
        $this->email = (string)$jsonData['email'];
        $this->phone = (string)$jsonData['phone'];
        $this->address = (string)$jsonData['address'];
        $this->region = (int)$jsonData['region_id'];
        $this->creatorid = (int)$jsonData['creator_id'];
        $this->ipaddress = $jsonData['ip_address'];
        $this->status = (int)$jsonData['status'];
        $this->isdeleted = (int)$jsonData['is_deleted'];
        $this->datecreated = (int)$jsonData['date_created'];
        $this->datemodified = (int)$jsonData['date_modified'];
        $this->datedeleted = (int)$jsonData['date_deleted'];
        $this->dateactivated = (int)$jsonData['date_activated'];
        $this->newnotification = (int)$jsonData['new_notification'];
        $this->newmessage = (int)$jsonData['new_message'];
    }
}
