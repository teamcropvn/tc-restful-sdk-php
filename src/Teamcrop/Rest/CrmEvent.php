<?php

namespace Teamcrop\Rest;

class CrmEvent extends Base
{
    public static $serviceurl = '/v1/crmevents';

    const STATUS_NEW = 1;
    const STATUS_COMPLETED = 3;
    const STATUS_CANCELLED = 3;

    public $cid = 0;
    public $uid = 0;
    public $ccid = 0;
    public $id = 0;
    public $name = '';
    public $description = '';
    public $location = '';
    public $inviteduserlist = '';
    public $randomcode = '';
    public $relatedeventid = 0;
    public $fileidlist = '';
    public $countremind = 0;
    public $status = 0;
    public $ipaddress = '';
    public $datecreated = 0;
    public $datemodified = 0;
    public $datestarted = 0;
    public $datefinished = 0;

    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData(&$error = array())
    {
        $data = array(
            'creator_id' => $this->uid,
            'company_id' => $this->cid,

        );

//        $this->id = self::doAdd($data, '', '', $error);
//
//        return $this->id;
    }

    public function updateData(&$error = array())
    {
        $data = array(
            'creator_id' => $this->uid,
            'company_id' => $this->cid,

        );

//        $url = self::$serviceurl . '/' . $this->id;
//        return self::doUpdate($data, $url, '', $error);
    }


    public function delete(&$error = array())
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }

    public static function getCrmEvents($formData, $sortby = '', $sorttype = '', $limitString = '', $countOnly = false)
    {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }
    }

    /**
     * @return array
     */
    public function getJsonData()
    {
        $data = array(
            'creator_id' => (int)$this->uid,
            'company_id' => (int)$this->cid,
            'customer_id' => (int)$this->ccid,
            'id' => (int)$this->id,
            'name' => (string)$this->name,
            'description' => (string)$this->description,
            'location' => (string)$this->location,
            'invited_user_list' => (string)$this->inviteduserlist,
            'random_code' => (string)$this->randomcode,
            'related_event_id' => (int)$this->relatedeventid,
            'file_id_list' => (string)$this->fileidlist,
            'count_remind' => (int)$this->countremind,
            "status" => (int)$this->status,
            "ip_address" => (string)$this->ipaddress,
            "date_created" => (int)$this->datecreated,
            "date_modified" => (int)$this->datemodified,
            "date_started" => (int)$this->datestarted,
            "date_finished" => (int)$this->datefinished
        );

        return $data;
    }

    public static function cacheKey($notifyid)
    {
        return 'tc_crm_event_' . $notifyid;
    }

    public function getDataByArrayCache($row)
    {

    }

    public function getDataByJson($row)
    {
        $this->cid = (int)$row['company_id'];
        $this->uid = (int)$row['creator_id'];
        $this->ccid = (int)$row['customer_id'];
        $this->id = (int)$row['id'];
        $this->name = (string)$row['name'];
        $this->description = (string)$row['description'];
        $this->location = (string)$row['location'];
        $this->inviteduserlist = (string)$row['invited_user_list'];
        $this->randomcode = (string)$row['random_code'];
        $this->relatedeventid = (int)$row['related_event_id'];
        $this->fileidlist = (string)$row['file_id_list'];
        $this->countremind = (int)$row['count_remind'];
        $this->status = (int)$row['status'];
        $this->ipaddress = long2ip($row['ip_address']);
        $this->datecreated = (int)$row['date_created'];
        $this->datemodified = (int)$row['date_modified'];
        $this->datestarted = (int)$row['date_started'];
        $this->datefinished = (int)$row['date_finished'];
    }
}
