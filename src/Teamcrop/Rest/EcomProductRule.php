<?php

namespace Teamcrop\Rest;

class EcomProductRule extends Base
{
    public static $serviceurl = '/v1/ecomproductrules';

    const RULE_ID = 1;
    const RULE_CODE = 3;
    const RULE_SKU = 5;
    const RULE_CATEGORY = 7;
    const RULE_PRICE = 9;
    const RULE_SUPPLIER = 11;
    const RULE_VENDOR = 13;
    const RULE_UNIT = 15;

    const VALUETYPE_NUMBER = 1;
    const VALUETYPE_EXACTSTRING = 3;
    const VALUETYPE_CONTAINSTRING = 5;
    const VALUETYPE_REGEXSTRING = 7;

    public $cid = 0;
    public $uid = 0;
    public $ewid = 0;
    public $id = 0;
    public $rule = 0;
    public $valuetype = 0;
    public $value = 0;
    public $ipaddress = '';
    public $datecreated = 0;
    public $datemodified = 0;

    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData(&$error = array())
    {
        $data = array(
            'company_id' => $this->cid,
            'creator_id' => $this->uid,
            'website_id' => $this->ewid,
            'rule' => $this->rule,
            'value_type' => $this->valuetype,
            'value' => $this->value
        );

        $this->id = self::doAdd($data, '', '', $error);

        return $this->id;
    }

    public function updateData(&$error = array())
    {
        $data = array(
            'rule' => $this->rule,
            'value_type' => $this->valuetype,
            'value' => $this->value
        );

        $url = self::$serviceurl . '/' . $this->id;
        return self::doUpdate($data, $url, '', $error);
    }


    public function delete(&$error = array())
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }

    public static function getEcomProductRules($formData, $sortby = '', $sorttype = '', $limitString = '', $countOnly = false)
    {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }

    }

    public function getDataByJson($jsonData)
    {
        $this->id = (int)$jsonData['id'];
        $this->cid = (int)$jsonData['company_id'];
        $this->uid = (int)$jsonData['creator_id'];
        $this->rule = (int)$jsonData['rule'];
        $this->valuetype = (int)$jsonData['value_type'];
        $this->value = (string)$jsonData['value'];
        $this->ipaddress = (string)$jsonData['ip_address'];
        $this->datecreated= (int)$jsonData['date_created'];
        $this->datemodified = (int)$jsonData['date_modified'];
    }
}
