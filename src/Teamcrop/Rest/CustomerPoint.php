<?php

namespace Teamcrop\Rest;

class CustomerPoint extends Base
{
    public static $serviceurl = '/v1/customerpoints';

    const STATUS_ENABLE = 1;
    const STATUS_DISABLED = 3;

    const MODE_AUTO = 1;
    const MODE_MANUAL = 3;

    const TYPE_ORDER = 1;
    const TYPE_BONUS = 3;
    const TYPE_CASHFLOW = 5;
    const TYPE_OTHER = 99;

    public $cid = 0;
    public $uid = 0;
    public $ccid = 0; // Customer ID
    public $cprid = 0; // Crm Point Rule ID
    public $id = 0;
    public $storeid = 0;
    public $type = 0;
    public $subtype = 0;
    public $mode = 0;
    public $objectid = 0;
    public $orderid = 0;
    public $orderamount = 0;
    public $value = 0; // Gia tri diem, khong co so le
    public $note = '';
    public $fileidlist = '';
    public $status = 0;
    public $datecreated = 0;
    public $datemodified = 0;
    
    public function __construct($id = 0, $loadFromCache = false)
    {
        if ($id > 0) {
            if ($loadFromCache) {

            } else {
                $this->getData($id);
            }
        }

        parent::__construct();
    }

    public function addData()
    {
        $data = array(
            'creator_id' => (int)$this->uid,
            'company_id' => (int)$this->cid,
            'customer_id' => (int)$this->ccid,
            'point_rule_id' => (int)$this->cprid,
            'id' => (int)$this->id,
            'store_id' => (int)$this->storeid,
            'type' => (int)$this->type,
            'sub_type' => (int)$this->subtype,
            'mode' => (int)$this->mode,
            'object_id' => (int)$this->objectid,
            'order_id' => (int)$this->orderid,
            'order_amount' => (int)$this->orderamount,
            'value' => (int)$this->value,
            'note' => (string)$this->note,
            'file_id_list' => (string)$this->fileidlist,
            'status' => (int)$this->status

        );

        $this->id = self::doAdd($data, '', '', $error);

        return $this->id;
    }

    public function updateData(&$error = array())
    {
        $data = array(
            'creator_id' => (int)$this->uid,
            'company_id' => (int)$this->cid,
            'customer_id' => (int)$this->ccid,
            'point_rule_id' => (int)$this->cprid,
            'id' => (int)$this->id,
            'store_id' => (int)$this->storeid,
            'type' => (int)$this->type,
            'sub_type' => (int)$this->subtype,
            'mode' => (int)$this->mode,
            'object_id' => (int)$this->objectid,
            'order_id' => (int)$this->orderid,
            'order_amount' => (int)$this->orderamount,
            'value' => (int)$this->value,
            'note' => (string)$this->note,
            'file_id_list' => (string)$this->fileidlist,
            'status' => (int)$this->status
        );

        $url = self::$serviceurl . '/' . $this->id;
        return self::doUpdate($data, $url, '', $error);
    }

    public function delete(&$error = array())
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }

    public static function getCustomerPoints($formData, $sortby, $sorttype, $limit = '', $countOnly = false)
    {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }
    }

    public function getDataByJson($row)
    {
        $this->cid = (int)$row['company_id'];
        $this->uid = (int)$row['creator_id'];
        $this->ccid = (int)$row['customer_id'];
        $this->cprid = (int)$row['point_rule_id'];
        $this->id = (int)$row['id'];
        $this->storeid = (int)$row['store_id'];
        $this->type = (int)$row['type'];
        $this->subtype = (int)$row['sub_type'];
        $this->mode = (int)$row['mode'];
        $this->objectid = (int)$row['object_id'];
        $this->orderid = (int)$row['order_id'];
        $this->orderamount = (int)$row['order_amount'];
        $this->value = (int)$row['value'];
        $this->note = (string)$row['note'];
        $this->fileidlist = (string)$row['file_id_list'];
        $this->status = (int)$row['status'];
        $this->datecreated = (int)$row['date_created'];
        $this->datemodified = (int)$row['date_modified'];
    }

    public function getJsonData()
    {
        $data = array(
            'creator_id' => (int)$this->uid,
            'company_id' => (int)$this->cid,
            'customer_id' => (int)$this->ccid,
            'point_rule_id' => (int)$this->cprid,
            'id' => (int)$this->id,
            'store_id' => (int)$this->storeid,
            'type' => (int)$this->type,
            'sub_type' => (int)$this->subtype,
            'mode' => (int)$this->mode,
            'object_id' => (int)$this->objectid,
            'order_id' => (int)$this->orderid,
            'order_amount' => (int)$this->orderamount,
            'value' => (int)$this->value,
            'note' => (string)$this->note,
            'file_id_list' => (string)$this->fileidlist,
            'status' => (int)$this->status,
            'date_created' => (int)$this->datecreated,
            'date_modified' => (int)$this->datemodified
        );

        return $data;
    }
}