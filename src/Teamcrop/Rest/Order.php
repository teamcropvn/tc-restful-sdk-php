<?php

namespace Teamcrop\Rest;

class Order extends Base
{
    public static $serviceurl = '/v1/orders';

    const DEVICE_DESKTOP = 1;
    const DEVICE_TABLET = 5;
    const DEVICE_MOBILE = 9;

    const DEVICEOS_ANDROID = 1;
    const DEVICEOS_IOS = 3;
    const DEVICEOS_WINDOWSPHONE = 5;
    const DEVICEOS_JAVA = 7;
    const DEVICEOS_BLACKBERRY = 9;
    const DEVICEOS_OTHER = 11;

    const PAYMENTSTATUS_PENDING = 1;
    const PAYMENTSTATUS_PAID = 3;
    const PAYMENTSTATUS_AUTHORIZED = 5;
    const PAYMENTSTATUS_REFUNDED = 7;
    const PAYMENTSTATUS_PARTIALLYREFUND = 9;
    const PAYMENTSTATUS_VOID = 0;

    const STATUS_VERIFY_PENDING = 1;
    const STATUS_VERIFIED = 3;
    const STATUS_PICKUP_PENDING = 5;
    const STATUS_PICKEDUP = 7;
    const STATUS_ABANDONED = 11;
    const STATUS_OPEN = 13;
    const STATUS_PROCESSING = 15;
    const STATUS_SHIPPING = 17;
    const STATUS_SHIPPED = 18;
    const STATUS_COMPLETE = 19;
    const STATUS_CANCEL_BYCHANGE = 20;
    const STATUS_CANCEL = 21;
    const STATUS_CANCEL_BYGIVEBACK = 22;
    const STATUS_RETURNING = 23;
    const STATUS_DELAY = 24;


    const COMPLETE_NOTFULFILL = 23;
    const COMPLETE_PARTIALLFULFILL = 25;
    const COMPLETE_FULFILL = 27;

    const REASON_CHANGE = 29;
    const REASON_FRAUDULENT = 31;
    const REASON_UNAVAILABLE = 33;
    const REASON_OTHER = 35;

    const PAYMENT_METHOD_PAYPAL = 37;
    const PAYMENT_METHOD_INTERNETBANKING = 39;
    const PAYMENT_METHOD_INTERNATIONAL = 40;
    const PAYMENT_METHOD_CASH = 41;
    const PAYMENT_METHOD_COD = 42;

    const SHIPPING_PENDING = 43;
    const SHIPPING_ONGOING = 45;
    const SHIPPING_COMPLETED = 47;

    const SOURCE_INTERNAL = 0;
    const SOURCE_WORDPRESS = 3;
    const SOURCE_MAGENTO = 5;
    const SOURCE_PRESTASHOP = 7;
    const SOURCE_OPENCART = 9;
    const SOURCE_OSCOMMERCE = 11;
    const SOURCE_OTHER = 13;
    const SOURCE_WOOECOMMERCE = 15;
    const SOURCE_MOBILEAPP = 17;

    const SOURCE_BIZWEB = 19;
    const SOURCE_HARAVAN = 21;
    const SOURCE_SHOPIFY = 23;

    const ORDERTYPE_INHOUSE = 1;
    const ORDERTYPE_TAKEAWAY = 3;
    const ORDERTYPE_DELIVERY = 5;

    public $cid = 0;
    public $uid = 0;
    public $ctmid = 0;
    public $customerpoint = 0;
    public $slid = 0;
    public $ssid = 0;
    public $ssnid = 0;
    public $id = 0;
    public $localposidentifier = 0;
    public $ordercode = '';
    public $invoiceid = '';
    public $invoiceidsimple = '';
    public $weekday = -1;
    public $dayhour = -1;
    public $device = 0;
    public $deviceos = 0;
    public $deviceosversion = '';
    public $source = 0;
    public $sourceupdatedtime = 0;
    public $sourceid = '';
    public $workshowgroupid = 0;
    public $workshowid = 0;
    public $taxratio = 0;
    public $pricesell = ''; /* tong detail */
    public $priceshipping = '';
    public $pricehandling = ''; /* price chinh tay */
    public $pricetax = '';
    public $pricediscount = '';
    public $pricefinal = 0; /* price final da tru discount va cong tax */
    public $pricedeposit = 0;
    public $pricedebt = 0;
    public $promotionid = 0;
    public $promotiondetail = "";
    public $promotionvoucher = 0;
    public $promotiongiftcard = 0;
    public $contactemail = '';
    public $customercode = '';
    public $billinggender = 0;
    public $billingfullname = '';
    public $billingphone = '';
    public $billingaddress = '';
    public $billingcompany = '';
    public $billingregionid = 0;
    public $billingsubregionid = 0;
    public $billingsubsubregionid = 0;
    public $billingcountry = '';
    public $billingzipcode = 0;
    public $shippingfullname = '';
    public $shippingphone = '';
    public $shippingaddress = '';
    public $shippingcompany = '';
    public $shippingcountry = '';
    public $shippingzipcode = 0;
    public $shippingregionid = 0;
    public $shippingsubregionid = 0;
    public $shippingsubsubregionid = 0;
    public $shippinglat = 0;
    public $shippinglng = 0;
    public $shippingservice = 0;
    public $shippingtrackingcode = '';
    public $relatedorderid = 0;
    public $productreceipt = '';
    public $cashflowid = '';
    public $outputtype = 0;
    public $spzid = 0;
    public $quantity = 0;
    public $deliverystatus = 0;
    public $note = '';
    public $paymentstatus = 0;
    public $paymentmethod = 0;
    public $codamount = 0;
    public $randomcode = '';
    public $iscustomerinvoice = 0;
    public $customerinvoicedetail = '';
    public $isgift = 0;
    public $ispaid = 0;
    public $isdelete = 0;
    public $isdeletedby = 0;
    public $ordertype = 0;
    public $completed = 0;
    public $status = 0;
    public $cancelreason = '';
    public $isreturn = 0;
    public $hasreturn = 0;
    public $customstatus = 0;
    public $origin = 0;
    public $originid = "";
    public $notifylist = array();
    public $notifyhidelist = array();
    public $employeeid = 0;
    public $employeepoint = 0;
    public $ipaddress = 0;
    public $datecreated = 0;
    public $datemodified = 0;
    public $datecompleted = 0;
    public $datearrived;
    public $dateeta = 0;
    public $duration = 0;
    public $weight = 0;

    public $ssname = '';    //store name
    public $ssnname = '';   //store node name
    public $statusname = '';    //status name


    //dynamic
    public $promotionprice = 0;
    public $promotionname = '';
    public $promotiontypeparent = 0;
    public $promotiontypechildren = 0;
    public $hasgift = 0;
    public $giftname = '';
    public $giftdatecreated = 0;
    public $giftdatemodified = 0;
    public $couponid = 0;
    public $couponcode = 0;
    public $warehouseid = 0;
    public $issubscription = 0;
    public $subscriptionreceipt = 0;
    public $subscriptioncashflow = 0;
    public $tag = '';
    public $details = array();
    public function __construct($id = 0, $loadFromCache = false)
    {
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                //$this->copy(self::cacheGet($id));
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function updateCustomer($cc_id, $so_id)
    {
        $data = array(
            "customer_id"=> $cc_id,
            "order_id"=>$so_id
        );
        $url = self::$serviceurl . "/updatecustomer";
        $this->id = self::doAdd($data, $url, '', $error);

        return $this->id;
    }


    public function addData(&$error = array())
    {
        $data = array(
            "invoice_id" => $this->invoiceid,
            "origin" => $this->origin,
            "origin_id" => $this->originid,
            "warehouse_id" => $this->warehouseid,
            "is_subscription" => $this->issubscription,
            "subscription_receipt" => $this->subscriptionreceipt,
            "subscription_cashflow" => $this->subscriptioncashflow,
            "contact_email" => $this->contactemail,
            "shipping_full_name" => $this->shippingfullname,
            "shipping_phone" => $this->shippingphone,
            "shipping_address" => $this->shippingaddress,
            "shipping_company" => $this->shippingcompany,
            "shipping_region_id" => $this->shippingregionid,
            "shipping_sub_region_id" => $this->shippingsubregionid,
            "shipping_sub_sub_region_id" => $this->shippingsubsubregionid,
            "shipping_country" => $this->shippingcountry,
            "shipping_zipcode" => $this->shippingzipcode,
            "billing_zipcode" => $this->billingzipcode,
            "billing_full_name" => $this->billingfullname,
            "billing_phone" => $this->billingphone,
            "billing_address" => $this->billingaddress,
            "billing_company" => $this->billingcompany,
            "billing_country" => $this->billingcountry,
            "billing_region_id" => $this->billingregionid,
            "custom_status" => $this->customstatus,
            "customer_code" => $this->customercode,
            "source" => $this->source,
            "date_arrived" => $this->datearrived,
            "source_id" => $this->sourceid,
            "billing_sub_region_id" => $this->billingsubregionid,
            "billing_sub_sub_region_id" => $this->billingsubsubregionid,
            "note" => $this->note,
            "status" => $this->status,
            "payment_status" => $this->paymentstatus,
            "payment_method" => $this->paymentmethod,
            "store_id" => $this->slid,
            "price_zone" => $this->spzid,
            "out_put_type" => $this->outputtype,
            "company_id" => $this->cid,
            "creator_id" => $this->uid,
            "price_discount" => $this->pricediscount,
            "price_tax" => $this->pricetax,
            "price_sell" => $this->pricesell,
            "price_final" => $this->pricefinal,
            "tax_ratio" => $this->taxratio,
            "price_shipping" => $this->priceshipping,
            "price_deposit" => $this->pricedeposit,
            "cod_amount" => $this->codamount,
            "customer_id" => $this->ctmid,
            "employee_id" => $this->employeeid,
            "tag" => $this->tag,
            "date_created" => $this->datecreated,
            "device_os" => $this->deviceos,
            "shipping_service" => $this->shippingservice,
            "shipping_tracking_code" => $this->shippingtrackingcode,
            "delivery_status" => $this->deliverystatus,
            "device_os_version" => $this->deviceosversion,
            "promotion_price" => $this->promotionprice,
            "promotion_id" => $this->promotionid,
            "promotion_name" => $this->promotionname,
            "promotion_type_parent" => $this->promotiontypeparent,
            "promotion_type_children" => $this->promotiontypechildren,
            "has_gift" => $this->hasgift,
            "gift_name" => $this->giftname,
            "gift_date_created" => $this->giftdatecreated,
            "gift_date_modified" => $this->giftdatemodified,
            "coupon_id" => $this->couponid,
            "cashflow_id" => $this->cashflowid,
            "product_receipt" => $this->productreceipt,
            "cancel_reason" => $this->cancelreason,
            "coupon_code" => $this->couponcode,
            "details" => $this->details,
            "is_return" => $this->isreturn,
            "has_return" => $this->hasreturn,
            "related_order_id" => $this->relatedorderid,

        );

        $this->id = self::doAdd($data, '', '', $error);

        return $this->id;
    }

    public function updateData(&$error = array())
    {
        $data = array(
            "invoice_id" => $this->invoiceid,
            "origin" => $this->origin,
            "origin_id" => $this->originid,
            "contact_email" => $this->contactemail,
            "shipping_full_name" => $this->shippingfullname,
            "shipping_phone" => $this->shippingphone,
            "shipping_address" => $this->shippingaddress,
            "shipping_company" => $this->shippingcompany,
            "shipping_region_id" => $this->shippingregionid,
            "shipping_sub_region_id" => $this->shippingsubregionid,
            "shipping_sub_sub_region_id" => $this->shippingsubsubregionid,
            "shipping_country" => $this->shippingcountry,
            "shipping_zipcode" => $this->shippingzipcode,
            "billing_zipcode" => $this->billingzipcode,
            "billing_full_name" => $this->billingfullname,
            "billing_phone" => $this->billingphone,
            "billing_address" => $this->billingaddress,
            "billing_company" => $this->billingcompany,
            "billing_country" => $this->billingcountry,
            "billing_region_id" => $this->billingregionid,
            "custom_status" => $this->customstatus,
            "source" => $this->source,
            "price_sell" => $this->pricesell,
            "price_final" => $this->pricefinal,
            "source_id" => $this->sourceid,
            "billing_sub_region_id" => $this->billingsubregionid,
            "billing_sub_sub_region_id" => $this->billingsubsubregionid,
            "note" => $this->note,
            "status" => $this->status,
            "date_arrived" => $this->datearrived,
            "payment_status" => $this->paymentstatus,
            "payment_method" => $this->paymentmethod,
            "store_id" => $this->slid,
            "price_zone" => $this->spzid,
            "out_put_type" => $this->outputtype,
            "company_id" => $this->cid,
            "creator_id" => $this->uid,
            "price_discount" => $this->pricediscount,
            "customer_code" => $this->customercode,
            "price_tax" => $this->pricetax,
            "tax_ratio" => $this->taxratio,
            "price_shipping" => $this->priceshipping,
            "price_deposit" => $this->pricedeposit,
            "cod_amount" => $this->codamount,
            "customer_id" => $this->ctmid,
            "employee_id" => $this->employeeid,
            "tag" => $this->tag,
            "date_created" => $this->datecreated,
            "device_os" => $this->deviceos,
            "shipping_service" => $this->shippingservice,
            "shipping_tracking_code" => $this->shippingtrackingcode,
            "delivery_status" => $this->deliverystatus,
            "device_os_version" => $this->deviceosversion,
            "promotion_price" => $this->promotionprice,
            "promotion_id" => $this->promotionid,
            "promotion_name" => $this->promotionname,
            "promotion_type_parent" => $this->promotiontypeparent,
            "promotion_type_children" => $this->promotiontypechildren,
            "has_gift" => $this->hasgift,
            "gift_name" => $this->giftname,
            "gift_date_created" => $this->giftdatecreated,
            "gift_date_modified" => $this->giftdatemodified,
            "coupon_id" => $this->couponid,
            "cashflow_id" => $this->cashflowid,
            "product_receipt" => $this->productreceipt,
            "cancel_reason" => $this->cancelreason,
            "coupon_code" => $this->couponcode,
            "is_return" => $this->isreturn,
            "has_return" => $this->hasreturn,
            "related_order_id" => $this->relatedorderid,
        );

        $url = self::$serviceurl . '/' . $this->id;
        return self::doUpdate($data, $url, '', $error);
    }


    public function delete(&$error = array())
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }

    public static function updatecashflow($cashflowid, $orderId, $priceDeposit, $action, &$error = array())
    {
        $arrData["cashflow_id"] = $cashflowid;
        $arrData["action"] = $action;
        $arrData["order_id"] = $orderId;
        $arrData["price_deposit"] = $priceDeposit;
        $url = self::$serviceurl . '/updatecashflow';
        return self::doAdd($arrData, $url, '', $error);
    }

    public function updateOneColumn($column, $order_id, $creator_id, &$error = array())
    {
        $arrData["column"] = $column;
        $arrData["creator_id"] = $creator_id;
        $arrData["order_id"] = $order_id;
        $url = self::$serviceurl . '/updateonecolumn';
        return self::doAdd($arrData, $url, '', $error);
    }

    public static function getOrders($formData, $sortby = '', $sorttype = '', $limitString = '', $countOnly = false)
    {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }
    }

    public static function getIds($ids, $cid)
    {
        $url = self::$serviceurl . '/ids';
        return self::getItems(array("ids" => $ids, "company_id" => $cid), "", "", "", $url);
    }

    public static function getSummaryByCustomer($customerID)
    {
        $url = self::$serviceurl . '/summarybycustomer?customer_id=' . $customerID;
        $response = self::doGet(
            $url,
            array()
        );
        $result = array();
        if (is_array($response)) {
            foreach ($response['items'] as $data) {
                $result[$data['customer_id']] = $data;
            }
        }

        return $result;
    }

    public static function getStatusList()
    {
        $output = array();

        $output[self::STATUS_OPEN] = 'open';
        $output[self::STATUS_PROCESSING] = 'processing';
        $output[self::STATUS_SHIPPING] = 'shipping';
        $output[self::STATUS_SHIPPED] = 'shipped';
        $output[self::STATUS_COMPLETE] = 'completed';
        $output[self::STATUS_CANCEL] = 'cancel';

        return $output;
    }





    public static function bigcommerceOrderSync($formData, &$error = array())
    {
        $url = "/v1/bigcommercesyncorders/pull";
        return self::doAdd($formData, $url, array(), $error);
    }

    public function getDataByJson($jsonData)
    {
        $this->cid = (int)$jsonData['company_id'];
        $this->uid = (int)$jsonData['creator_id'];
        $this->ctmid = (int)$jsonData['customer_id'];
        $this->customercode = (string)$jsonData['customer_code'];
        $this->customerpoint = (int)$jsonData['customer_point'];
        $this->slid = (int)$jsonData['store_id'];
        $this->outputtype = (int)$jsonData['out_put_type'];
        $this->spzid = (int)$jsonData['price_zone_id'];
        $this->id = (int)$jsonData['id'];
        $this->tag = (string)$jsonData["tag"];
        $this->localposidentifier = (int)$jsonData['local_pos_identifier'];
        $this->ordercode = (string)$jsonData['order_code'];
        $this->invoiceid = (string)$jsonData['invoice_id'];
        $this->invoiceidsimple = (string)$jsonData['invoiceid_simple'];
        $this->weekday = (int)$jsonData['week_day'];
        $this->dayhour = (int)$jsonData['day_hour'];
        $this->device = (int)$jsonData['device'];
        $this->deviceos = (int)$jsonData['device_os'];
        $this->deviceosversion = (string)$jsonData['device_os_version'];
        $this->source = (string)$jsonData['source'];
        $this->sourceupdatedtime = (string)$jsonData['source_updated_time'];
        $this->sourceid = (string)$jsonData['source_id'];
        $this->workshowgroupid = (int)$jsonData['work_show_group_id'];
        $this->workshowid = (int)$jsonData['work_show_id'];
        $this->pricesell = (float)$jsonData['price_sell'];
        $this->priceshipping = (float)$jsonData['price_shipping'];
        $this->pricehandling = (float)$jsonData['price_handling'];
        $this->pricetax = (float)$jsonData['price_tax'];
        $this->taxratio = (int)$jsonData['tax_ratio'];
        $this->pricediscount = (float)$jsonData['price_discount'];
        $this->pricefinal = (float)$jsonData['price_final'];
        $this->pricedeposit = (float)$jsonData['price_deposit'];
        $this->pricedebt = (float)$jsonData['price_debt'];
        $this->promotionid = (int)$jsonData['promotion_id'];
        $this->promotiondetail = (string)$jsonData['promotion_detail'];
        $this->promotionvoucher = (int)$jsonData['promotion_voucher'];
        $this->promotiongiftcard = (int)$jsonData['promotion_gift_card'];
        $this->contactemail = (string)$jsonData['contact_email'];
        $this->billinggender = (int)$jsonData['billing_gender'];
        $this->billingfullname = (string)$jsonData['billing_full_name'];
        $this->billingphone = (string)$jsonData['billing_phone'];
        $this->billingaddress = (string)$jsonData['billing_address'];
        $this->billingcompany = (string)$jsonData['billing_company'];
        $this->billingsubsubregionid = (int)$jsonData['billing_sub_sub_region_id'];
        $this->billingsubregionid = (int)$jsonData['billing_sub_region_id'];
        $this->billingregionid = (int)$jsonData['billing_region_id'];
        $this->billingcountry = (string)$jsonData['billing_country'];
        $this->billingzipcode = (int)$jsonData['billing_zipcode'];
        $this->shippingfullname = (string)$jsonData['shipping_full_name'];
        $this->shippingphone = (string)$jsonData['shipping_phone'];
        $this->shippingaddress = (string)$jsonData['shipping_address'];
        $this->shippingcompany = (string)$jsonData['shipping_company'];
        $this->shippingcountry = (string)$jsonData['shipping_country'];
        $this->shippingzipcode = (int)$jsonData['shipping_zipcode'];
        $this->shippingsubsubregionid = (int)$jsonData['shipping_sub_sub_region_id'];
        $this->shippingsubregionid = (int)$jsonData['shipping_sub_region_id'];
        $this->shippingregionid = (int)$jsonData['shipping_region_id'];
        $this->shippinglat = (string)$jsonData['shipping_lat'];
        $this->shippinglng = (string)$jsonData['shipping_lng'];
        $this->shippingservice = (int)$jsonData['shipping_service'];
        $this->shippingtrackingcode = (string)$jsonData['shipping_tracking_code'];
        $this->relatedorderid = (int)$jsonData['related_order_id'];
        $this->productreceipt = (string)$jsonData['product_receipt'];
        $this->cashflowid = (string)$jsonData['cashflow_id'];
        $this->quantity = (float)$jsonData['quantity'];
        $this->deliverystatus = (int)$jsonData['delivery_status'];
        $this->note = (string)$jsonData['note'];
        $this->paymentstatus = (int)$jsonData['payment_status'];
        $this->paymentmethod = (int)$jsonData['payment_method'];
        $this->codamount = (float)$jsonData['cod_amount'];
        $this->randomcode = (string)$jsonData['random_code'];
        $this->isdelete = (int)$jsonData['is_delete'];
        $this->isdeletedby = (int)$jsonData['is_deletedby'];
        $this->ordertype = (int)$jsonData['order_type'];
        $this->completed = (int)$jsonData['completed'];
        $this->status = (int)$jsonData['status'];
        $this->cancelreason = (int)$jsonData['cancel_reason'];
        $this->isreturn = (int)$jsonData['is_return'];
        $this->hasreturn = (int)$jsonData['has_return'];
        $this->customstatus = (int)$jsonData["custom_status"];
        $this->origin = (int)$jsonData["origin"];
        $this->originid = (string)$jsonData["origin_id"];
        $this->notifylist = $jsonData["notify_list"];
        $this->notifyhidelist = $jsonData["notify_hide_list"];
        $this->employeeid = (int)$jsonData['employee_id'];
        $this->employeepoint = (int)$jsonData['employee_point'];
        $this->ipaddress = (string)$jsonData['ip_address'];
        $this->datecreated = (int)$jsonData['date_created'];
        $this->datemodified = (int)$jsonData['date_modified'];
        $this->datecompleted = (int)$jsonData['date_completed'];
        $this->datearrived  = (int)$jsonData["date_arrived"];
        $this->dateeta = (int)$jsonData['date_eta'];
        $this->duration = (int)$jsonData['duration'];
        $this->weight = (int)$jsonData['weight'];
    }

    public function getJsonData($viewCost = false)
    {
        $row['company_id'] = (int)$this->cid;
        $row['creator_id'] = (int)$this->uid;
        $row['customer_id'] = (int)$this->ctmid;
        $row['customer_point'] = (int)$this->customerpoint;
        $row['customer_code'] = (string)$this->customercode;
        $row['store_id'] = (int)$this->slid;
        $row['id'] = (int)$this->id;
        $row['local_pos_identifier'] = (int)$this->localposidentifier;
        $row['order_code'] = (string)$this->ordercode;
        $row['invoice_id'] = (string)$this->invoiceid;
        $row['invoiceid_simple'] = (string)$this->invoiceidsimple;
        $row['week_day'] = (int)$this->weekday;
        $row['day_hour'] = (int)$this->dayhour;
        $row['device'] = (int)$this->device;
        $row['device_os'] = (int)$this->deviceos;
        $row['device_os_version'] = (string)$this->deviceosversion;
        $row['source'] = (string)$this->source;
        $row['source_updated_time'] = (string)$this->sourceupdatedtime;
        $row['source_id'] = (string)$this->sourceid;
        $row['work_show_group_id'] = (int)$this->workshowgroupid;
        $row['work_show_id'] = (int)$this->workshowid;
        $row['price_cost'] = (float)$this->pricecost;
        $row['price_sell'] = (float)$this->pricesell;
        $row['price_shipping'] = (float)$this->priceshipping;
        $row['price_handling'] = (float)$this->pricehandling;
        $row['price_tax'] = (float)$this->pricetax;
        $row['tax_ratio'] = (int)$this->taxratio;
        $row['price_discount'] = (float)$this->pricediscount;
        $row['price_final'] = (float)$this->pricefinal;
        $row['price_deposit'] = (float)$this->pricedeposit;
        $row['price_debt'] = (float)$this->pricedebt;
        $row['promotion_id'] = (int)$this->promotionid;
        $row['promotion_voucher'] = (int)$this->promotionvoucher;
        $row['promotion_gift_card'] = (int)$this->promotiongiftcard;
        $row['contact_email'] = (string)$this->contactemail;
        $row['billing_gender'] = (int)$this->billinggender;
        $row['billing_full_name'] = (string)$this->billingfullname;
        $row['billing_phone'] = (string)$this->billingphone;
        $row['billing_address'] = (string)$this->billingaddress;
        $row['billing_company'] = (string)$this->billingcompany;
        $row['billing_sub_sub_region_id'] = (int)$this->billingsubsubregionid;
        $row['billing_sub_region_id'] = (int)$this->billingsubregionid;
        $row['billing_region_id'] = (int)$this->billingregionid;
        $row['billing_country'] = (string)$this->billingcountry;
        $row['billing_zipcode'] = (int)$this->billingzipcode;
        $row['shipping_full_name'] = (string)$this->shippingfullname;
        $row['shipping_phone'] = (string)$this->shippingphone;
        $row['shipping_address'] = (string)$this->shippingaddress;
        $row['shipping_company'] = (string)$this->shippingcompany;
        $row['shipping_country'] = (string)$this->shippingcountry;
        $row['shipping_zipcode'] = (int)$this->shippingzipcode;
        $row['shipping_sub_sub_region_id'] = (int)$this->shippingsubsubregionid;
        $row['shipping_sub_region_id'] = (int)$this->shippingsubregionid;
        $row['shipping_region_id'] = (int)$this->shippingregionid;
        $row['shipping_lat'] = (string)$this->shippinglat;
        $row['shipping_lng'] = (string)$this->shippinglng;
        $row['shipping_service'] = (int)$this->shippingservice;
        $row['shipping_tracking_code'] = (string)$this->shippingtrackingcode;
        $row['related_order_id'] = (int)$this->relatedorderid;
        $row['product_receipt'] = (string)$this->productreceipt;
        $row['cashflow_id'] = (string)$this->cashflowid;
        $row['quantity'] = (float)$this->quantity;
        $row['delivery_status'] = (int)$this->deliverystatus;
        $row['note'] = (string)$this->note;
        $row['payment_status'] = (int)$this->paymentstatus;
        $row['payment_method'] = (int)$this->paymentmethod;
        $row['cod_amount'] = (float)$this->codamount;
        $row['random_code'] = (string)$this->randomcode;
        $row['is_delete'] = (int)$this->isdelete;
        $row['is_deletedby'] = (int)$this->isdeletedby;
        $row['order_type'] = (int)$this->ordertype;
        $row['completed'] = (int)$this->completed;
        $row['status'] = (int)$this->status;
        $row['cancel_reason'] = (int)$this->cancelreason;
        $row['is_return'] = (int)$this->isreturn;
        $row['has_return'] = (int)$this->hasreturn;
        $row["custom_status"] = (int)$this->customstatus;
        $row["tag"] = (string)$this->tag;
        $row["origin"] = (int)$this->origin;
        $row["origin_id"] = (string)$this->originid;
        $row['notify_list'] = (string)$this->notifylist;
        $row['notify_hide_list'] = (string)$this->notifyhidelist;
        $row['employee_id'] = (int)$this->employeeid;
        $row['employee_point'] = (int)$this->employeepoint;
        $row['ip_address'] = (string)long2ip($this->ipaddress);
        $row['date_arrived'] = (int)$this->datearrived;
        $row['date_created'] = (int)$this->datecreated;
        $row['date_modified'] = (int)$this->datemodified;
        $row['date_completed'] = (int)$this->datecompleted;
        $row['date_eta'] = (int)$this->dateeta;
        $row['duration'] = (int)$this->duration;
        $row['weight'] = (int)$this->weight;


        if (!$viewCost) {
            unset($row["price_cost"]);
        }

        return $row;
    }
    /**
     * Use this function to remove hidelist from all notification list
     */
    public function getEnableNotifyList()
    {
        $enableList = array();

        foreach ($this->notifylist as $userId) {
            if (!in_array($userId, $this->notifyhidelist)) {
                $enableList[] = $userId;
            }
        }

        return $enableList;
    }

    public static function getPaymentMethods()
    {
        return array(
            self::PAYMENT_METHOD_PAYPAL => 'Paypal',
            self::PAYMENT_METHOD_INTERNETBANKING => 'Thanh toán trực tiếp bằng thẻ nội địa',
            self::PAYMENT_METHOD_INTERNATIONAL => 'Credit / Debit card',
            self::PAYMENT_METHOD_CASH => 'Tiền mặt (CASH)',
            self::PAYMENT_METHOD_COD => 'Giao hàng thu tiền (COD)'
        );
    }
}
