<?php

namespace Teamcrop\Rest;

class Order extends Base
{
    public static $serviceurl = '/v1/orders';

    const DEVICE_DESKTOP = 1;
    const DEVICE_TABLET = 5;
    const DEVICE_MOBILE = 9;

    const DEVICEOS_ANDROID = 1;
    const DEVICEOS_IOS = 3;
    const DEVICEOS_WINDOWSPHONE = 5;
    const DEVICEOS_JAVA = 7;
    const DEVICEOS_BLACKBERRY = 9;
    const DEVICEOS_OTHER = 11;

    const PAYMENTSTATUS_PENDING = 1;
    const PAYMENTSTATUS_PAID = 3;
    const PAYMENTSTATUS_AUTHORIZED = 5;
    const PAYMENTSTATUS_REFUNDED = 7;
    const PAYMENTSTATUS_PARTIALLYREFUND = 9;
    const PAYMENTSTATUS_VOID = 0;

    const STATUS_VERIFY_PENDING = 1;
    const STATUS_VERIFIED = 3;
    const STATUS_PICKUP_PENDING = 5;
    const STATUS_PICKEDUP = 7;
    const STATUS_ABANDONED = 11;
    const STATUS_OPEN = 13;
    const STATUS_PROCESSING = 15;
    const STATUS_SHIPPING = 17;
    const STATUS_SHIPPED = 18;
    const STATUS_COMPLETE = 19;
    const STATUS_CANCEL_BYCHANGE = 20;
    const STATUS_CANCEL = 21;
    const STATUS_CANCEL_BYGIVEBACK = 22;
    const STATUS_RETURNING = 23;
    const STATUS_DELAY = 24;


    const COMPLETE_NOTFULFILL = 23;
    const COMPLETE_PARTIALLFULFILL = 25;
    const COMPLETE_FULFILL = 27;

    const REASON_CHANGE = 29;
    const REASON_FRAUDULENT = 31;
    const REASON_UNAVAILABLE = 33;
    const REASON_OTHER = 35;

    const PAYMENT_METHOD_PAYPAL = 37;
    const PAYMENT_METHOD_INTERNETBANKING = 39;
    const PAYMENT_METHOD_INTERNATIONAL = 40;
    const PAYMENT_METHOD_CASH = 41;
    const PAYMENT_METHOD_COD = 42;

    const SHIPPING_PENDING = 43;
    const SHIPPING_ONGOING = 45;
    const SHIPPING_COMPLETED = 47;

    const SOURCE_INTERNAL = 0;
    const SOURCE_WORDPRESS = 3;
    const SOURCE_MAGENTO = 5;
    const SOURCE_PRESTASHOP = 7;
    const SOURCE_OPENCART = 9;
    const SOURCE_OSCOMMERCE = 11;
    const SOURCE_OTHER = 13;
    const SOURCE_WOOECOMMERCE = 15;
    const SOURCE_MOBILEAPP = 17;


    const ORDERTYPE_INHOUSE = 1;
    const ORDERTYPE_TAKEAWAY = 3;
    const ORDERTYPE_DELIVERY = 5;

    public $cid = 0;
    public $uid = 0;
    public $ctmid = 0;
    public $customerpoint = 0;
    public $slid = 0;
    public $ssid = 0;
    public $ssnid = 0;
    public $id = 0;
    public $localposidentifier = 0;
    public $ordercode = '';
    public $invoiceid = '';
    public $invoiceidsimple = '';
    public $weekday = -1;
    public $dayhour = -1;
    public $device = 0;
    public $deviceos = 0;
    public $deviceosversion = '';
    public $source = 0;
    public $sourceupdatedtime = 0;
    public $sourceid = '';
    public $workshowgroupid = 0;
    public $workshowid = 0;
    public $pricesell = ''; /* tong detail */
    public $priceshipping = '';
    public $pricehandling = ''; /* price chinh tay */
    public $pricetax = '';
    public $pricediscount = '';
    public $pricefinal = 0; /* price final da tru discount va cong tax */
    public $pricedeposit = 0;
    public $pricedebt = 0;
    public $promotionid = 0;
    public $promotiondetail = "";
    public $promotionvoucher = 0;
    public $promotiongiftcard = 0;
    public $contactemail = '';
    public $billinggender = 0;
    public $billingfullname = '';
    public $billingphone = '';
    public $billingaddress = '';
    public $billingcompany = '';
    public $billingregionid = 0;
    public $billingsubregionid = 0;
    public $billingsubsubregionid = 0;
    public $billingcountry = '';
    public $billingzipcode = 0;
    public $shippingfullname = '';
    public $shippingphone = '';
    public $shippingaddress = '';
    public $shippingcompany = '';
    public $shippingcountry = '';
    public $shippingzipcode = 0;
    public $shippingregionid = 0;
    public $shippingsubregionid = 0;
    public $shippingsubsubregionid = 0;
    public $shippinglat = 0;
    public $shippinglng = 0;
    public $shippingservice = 0;
    public $shippingtrackingcode = '';
    public $relatedorderid = 0;
    public $productreceipt = 0;
    public $cashflowid = '';
    public $outputtype = 0;
    public $spzid = 0;
    public $quantity = 0;
    public $deliverystatus = 0;
    public $note = '';
    public $paymentstatus = 0;
    public $paymentmethod = 0;
    public $codamount = 0;
    public $randomcode = '';
    public $iscustomerinvoice = 0;
    public $customerinvoicedetail = '';
    public $isgift = 0;
    public $ispaid = 0;
    public $isdelete = 0;
    public $isdeletedby = 0;
    public $ordertype = 0;
    public $completed = 0;
    public $status = 0;
    public $cancelreason = '';
    public $isreturn = 0;
    public $hasreturn = 0;
    public $customstatus = 0;
    public $origin = 0;
    public $originid = "";
    public $employeeid = 0;
    public $employeepoint = 0;
    public $ipaddress = 0;
    public $datecreated = 0;
    public $datemodified = 0;
    public $datecompleted = 0;
    public $dateeta = 0;
    public $duration = 0;

    public $ssname = '';    //store name
    public $ssnname = '';   //store node name
    public $statusname = '';    //status name

    public function __construct($id = 0, $loadFromCache = false)
    {
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                //$this->copy(self::cacheGet($id));
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData()
    {

    }

    public function updateData()
    {

    }


    public function delete()
    {

    }

    public static function getHistorys($formData, $sortby = '', $sorttype = '', $limitString = '', $countOnly = false)
    {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }
    }
    public function getDataByJson($jsonData)
    {
        $this->cid = (int)$jsonData['company_id'];
        $this->uid = (int)$jsonData['creator_id'];
        $this->ctmid = (int)$jsonData['customer_id'];
        $this->customerpoint = (int)$jsonData['customer_point'];
        $this->slid = (int)$jsonData['store'];
        $this->outputtype = (int)$jsonData['out_put_type'];
        $this->spzid = (int)$jsonData['price_zone_id'];
        $this->id = (int)$jsonData['order_id'];
        $this->localposidentifier = (int)$jsonData['local_pos_identifier'];
        $this->ordercode = (string)$jsonData['order_code'];
        $this->invoiceid = (string)$jsonData['invoice_id'];
        $this->invoiceidsimple = (string)$jsonData['invoiceid_simple'];
        $this->weekday = (int)$jsonData['week_day'];
        $this->dayhour = (int)$jsonData['day_hour'];
        $this->device = (int)$jsonData['device'];
        $this->deviceos = (int)$jsonData['device_os'];
        $this->deviceosversion = (string)$jsonData['device_os_version'];
        $this->source = (string)$jsonData['source'];
        $this->sourceupdatedtime = (string)$jsonData['source_updated_time'];
        $this->sourceid = (string)$jsonData['source_id'];
        $this->workshowgroupid = (int)$jsonData['work_show_group_id'];
        $this->workshowid = (int)$jsonData['work_show_id'];
        $this->pricesell = (int)$jsonData['price_sell'];
        $this->priceshipping = (int)$jsonData['price_shipping'];
        $this->pricehandling = (int)$jsonData['price_handling'];
        $this->pricetax = (int)$jsonData['price_tax'];
        $this->pricediscount = (int)$jsonData['price_discount'];
        $this->pricefinal = (int)$jsonData['price_final'];
        $this->pricedeposit = (int)$jsonData['price_deposit'];
        $this->pricedebt = (int)$jsonData['price_debt'];
        $this->promotionid = (int)$jsonData['promotion_id'];
        $this->promotiondetail = (string)$jsonData['promotion_detail'];
        $this->promotionvoucher = (int)$jsonData['promotion_voucher'];
        $this->promotiongiftcard = (int)$jsonData['promotion_gift_card'];
        $this->contactemail = (string)$jsonData['contact_email'];
        $this->billinggender = (int)$jsonData['billing_gender'];
        $this->billingfullname = (string)$jsonData['billing_full_name'];
        $this->billingphone = (string)$jsonData['billing_phone'];
        $this->billingaddress = (string)$jsonData['billing_address'];
        $this->billingcompany = (string)$jsonData['billing_company'];
        $this->billingsubsubregionid = (int)$jsonData['billing_sub_sub_region_id'];
        $this->billingsubregionid = (int)$jsonData['billing_sub_region_id'];
        $this->billingregionid = (int)$jsonData['billing_region_id'];
        $this->billingcountry = (string)$jsonData['billing_country'];
        $this->billingzipcode = (int)$jsonData['billing_zipcode'];
        $this->shippingfullname = (string)$jsonData['shipping_full_name'];
        $this->shippingphone = (string)$jsonData['shipping_phone'];
        $this->shippingaddress = (string)$jsonData['shipping_address'];
        $this->shippingcompany = (string)$jsonData['shipping_company'];
        $this->shippingcountry = (string)$jsonData['shipping_country'];
        $this->shippingzipcode = (int)$jsonData['shipping_zipcode'];
        $this->shippingsubsubregionid = (int)$jsonData['shipping_sub_sub_region_id'];
        $this->shippingsubregionid = (int)$jsonData['shipping_sub_region_id'];
        $this->shippingregionid = (int)$jsonData['shipping_region_id'];
        $this->shippinglat = (string)$jsonData['shipping_lat'];
        $this->shippinglng = (string)$jsonData['shipping_lng'];
        $this->shippingservice = (int)$jsonData['shipping_service'];
        $this->shippingtrackingcode = (string)$jsonData['shipping_tracking_code'];
        $this->relatedorderid = (int)$jsonData['related_order_id'];
        $this->productreceipt = (int)$jsonData['product_receipt'];
        $this->cashflowid = (string)$jsonData['cashflow_id'];
        $this->quantity = (int)$jsonData['quantity'];
        $this->deliverystatus = (int)$jsonData['delivery_status'];
        $this->note = (string)$jsonData['note'];
        $this->paymentstatus = (int)$jsonData['payment_status'];
        $this->paymentmethod = (int)$jsonData['payment_method'];
        $this->codamount = (float)$jsonData['cod_amount'];
        $this->randomcode = (string)$jsonData['random_code'];
        $this->isdelete = (int)$jsonData['is_delete'];
        $this->isdeletedby = (int)$jsonData['is_deletedby'];
        $this->ordertype = (int)$jsonData['order_type'];
        $this->completed = (int)$jsonData['completed'];
        $this->status = (int)$jsonData['status'];
        $this->cancelreason = (int)$jsonData['cancel_reason'];
        $this->isreturn = (int)$jsonData['is_return'];
        $this->hasreturn = (int)$jsonData['has_return'];
        $this->customstatus = (int)$jsonData["custom_status"];
        $this->origin = (int)$jsonData["origin"];
        $this->originid = (string)$jsonData["origin_id"];
        $this->employeeid = (int)$jsonData['employee_id'];
        $this->employeepoint = (int)$jsonData['employee_point'];
        $this->ipaddress = (string)$jsonData['ip_address'];
        $this->datecreated = (int)$jsonData['date_created'];
        $this->datemodified = (int)$jsonData['date_modified'];
        $this->datecompleted = (int)$jsonData['date_completed'];
        $this->dateeta = (int)$jsonData['date_eta'];
        $this->duration = (int)$jsonData['duration'];

    }
}
