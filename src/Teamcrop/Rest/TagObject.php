<?php

namespace Teamcrop\Rest;

class TagObject extends Base
{
    public static $serviceurl = '/v1/tagobjects';

    const TYPE_ORDER = 1;
    const TYPE_PURCHASEORDER = 2;
    const TYPE_CUSTOMER = 3;
    const TYPE_PRODUCT = 5;
    const TYPE_CMSPRODUCT = 7;
    const TYPE_CMSNEWS = 9;
    const TYPE_MESSAGE = 11;
    const TYPE_PROJECTSTORY = 13;
    const TYPE_PROJECTISSUE = 15;
    const TYPE_FILE = 17;
    const TYPE_TICKET = 19;

    public $cid = 0;
    public $tid = 0;
    public $uid = 0;
    public $id = 0;
    public $text = '';
    public $type = 0;
    public $objectid = 0;
    public $displayorder = 0;
    public $datecreated = 0;
    public $datemodified = 0;

    public function __construct($id = 0, $loadFromCache = false)
    {
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                //$this->copy(self::cacheGet($id));
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData(&$error = array())
    {
        $data = array(
            "company_id" => $this->cid,
            "creator_id" => $this->uid,
            "tag_id" => $this->tid,
            "text" => $this->text,
            "type" => $this->type,
            "object_id" => $this->objectid,
            "display_order" => $this->displayorder,
        );

        $this->id = self::doAdd($data, '', '', $error);

        return $this->id;
    }

    public function updateData(&$error = array())
    {
        $data = array(
            "company_id" => $this->cid,
            "creator_id" => $this->uid,
            "tag_id" => $this->tid,
            "text" => $this->text,
            "type" => $this->type,
            "object_id" => $this->objectid,
            "display_order" => $this->displayorder,
        );
        $url = self::$serviceurl . '/' . $this->id;
        return self::doUpdate($data, $url, '', $error);
    }


    public function delete(&$error = array())
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }

    public static function getObjects($formData, $sortby = '', $sorttype = '', $limitString = '', $countOnly = false)
    {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }

    }

    public function getDataByJson($jsonData)
    {
        $this->cid = $jsonData['company_id'];
        $this->uid = $jsonData['creator_id'];
        $this->tid = $jsonData['tag_id'];
        $this->id = $jsonData['id'];
        $this->text = $jsonData['text'];
        $this->type = $jsonData['type'];
        $this->objectid = $jsonData['object_id'];
        $this->displayorder = $jsonData['display_order'];
        $this->datecreated = $jsonData['date_created'];
        $this->datemodified = $jsonData['date_modified'];

    }
}
