<?php
/**
 * Created by PhpStorm.
 * User: nhq
 * Date: 7/7/17
 * Time: 11:17 AM
 */

namespace Teamcrop\Rest;

class Shipper extends Base
{
    public static $serviceurl = '/v1/shippers';
    public $creator_id = 0;
    public $company_id = 0;
    public $id = 0;
    public $user_name = "";
    public $password = "";
    public $vehicle_id = "";
    public $full_name = "";
    public $gender = 0;
    public $email = "";
    public $phone = "";
    public $address = "";
    public $current_lat = 0;
    public $current_lng = 0;
    public $status = 0;
    public $online_status = 0;
    public $avatar_file_id = 0;
    public $date_created = 0;
    public $date_modified = 0;
    public $date_last_online = 0;
    public $access_token = "";
    public $device_id = "";
    public $device_brand = "";
    public $device_model = "";
    public $push_tracker_id = "";
    public $date_last_login = 0;

    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData(&$error = array())
    {
        $row = $this->getJsonData();
        $this->id = self::doAdd($row, '', '', $error);

        return $this->id;
    }

    public function updateData(&$error = array())
    {
        $row = $this->getJsonData();
        $url = self::$serviceurl . '/' . $this->id;
        return self::doUpdate($row, $url, '', $error);
    }

    public function delete(&$error = array())
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }

    public static function getShippers(
        $formData,
        $sortby = '',
        $sorttype = '',
        $limitString = '',
        $countOnly = false
    ) {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }
    }


    public function getDataByJson($row)
    {
        $this->creator_id = (int)$row['creator_id'];
        $this->company_id = (int)$row['company_id'];
        $this->id = (int)$row['id'];
        $this->user_name = (string)$row['user_name'];
        $this->vehicle_id = (string)$row['vehicle_id'];
        $this->full_name = (string)$row['full_name'];
        $this->gender = (int)$row['gender'];
        $this->email = (string)$row['email'];
        $this->phone = (string)$row['phone'];
        $this->address = (string)$row['address'];
        $this->current_lat = (float)$row['current_lat'];
        $this->current_lng = (float)$row['current_lng'];
        $this->status = (int)$row['status'];
        $this->online_status = (int)$row['online_status'];
        $this->avatar_file_id = (int)$row['avatar_file_id'];
        $this->date_created = (int)$row['date_created'];
        $this->date_modified = (int)$row['date_modified'];
        $this->date_last_online = (int)$row['date_last_online'];
        $this->access_token = (string)$row['access_token'];
        $this->device_id = (string)$row['device_id'];
        $this->device_brand = (string)$row['device_brand'];
        $this->device_model = (string)$row['device_model'];
        $this->push_tracker_id = (string)$row['push_tracker_id'];
        $this->date_last_login = (int)$row['date_last_login'];
    }

    public function getJsonData()
    {
        $data = array(
            'creator_id' => (int)$this->creator_id,
            'company_id' => (int)$this->company_id,
            'id' => (int)$this->id,
            'user_name' => (string)$this->user_name,
            // 'password' => (string)$this->password,
            'vehicle_id' => (string)$this->vehicle_id,
            'full_name' => (string)$this->full_name,
            'gender' => (int)$this->gender,
            'email' => (string)$this->email,
            'phone' => (string)$this->phone,
            'address' => (string)$this->address,
            'current_lat' => (float)$this->current_lat,
            'current_lng' => (float)$this->current_lng,
            'status' => (int)$this->status,
            'online_status' => (int)$this->online_status,
            'avatar_file_id' => (int)$this->avatar_file_id,
            'date_created' => (int)$this->date_created,
            'date_modified' => (int)$this->date_modified,
            'date_last_online' => (int)$this->date_last_online,
            'access_token' => (string)$this->access_token,
            'device_id' => (string)$this->device_id,
            'device_brand' => (string)$this->device_brand,
            'device_model' => (string)$this->device_model,
            'push_tracker_id' => (string)$this->push_tracker_id,
            'date_last_login' => (int)$this->date_last_login
        );



        return $data;
    }
}