<?php

namespace Teamcrop\Rest;

class OrderDetail extends Base
{
    public static $serviceurl = '/v1/orderdetails';

    const STATUS_PENDING = 1;
    const STATUS_PROCESSING = 3;
    const STATUS_SHIPPING = 5;
    const STATUS_COMPLETE = 7;
    const STATUS_CANCEL = 0;


    public $soid = 0;
    public $cid = 0;
    public $poid = 0;
    public $posku = 0;
    public $pid = 0;
    public $id = 0;
    public $isgiftproduct = 0;
    public $itemserialnumber = "";
    public $iteminventorytype = 0;
    public $itemname = '';
    public $itemunit = '';
    public $quantity = 0;
    public $itemunitpriceoriginal = 0;
    public $itemunitprice = 0;
    public $promotion = '';
    public $subtotal = '';
    public $tax = 0;
    public $pricehandling = 0;
    public $itemoption = '';
    public $status = 0;
    public $displayorder = 0;
    public $employeeid = 0;
    public $originid = '';
    public $datecreated = 0;
    public $datemodified = 0;

    /*dynamic*/
    public $sodatecreated = 0;
    public $invoiceid = "";
    public $sostatus = 0;
    public $sorandomcode = "";
    public $priceshipping = 0;

    public $promotionprice = 0;
    public $promotionid = 0;
    public $promotionname = '';
    public $promotiontypeparent = 0;
    public $promotiontypechildren = 0;
    public $hasgift = 0;
    public $giftname = '';
    public $giftdatecreated = 0;
    public $giftdatemodified = 0;
    public $couponid = 0;
    public $couponcode = 0;

    public $pos = false;

    public function __construct($id = 0, $loadFromCache = false)
    {
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                //$this->copy(self::cacheGet($id));
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData(&$error = array())
    {
        $data = array(
            "pos" => $this->pos,
            "order_id" => $this->soid,
            "finvoiceid" => $this->invoiceid,
            "company_id" => $this->cid,
            "creator_id" => $this->employeeid,
            "origin_id" => $this->originid,
            "product_id" => $this->pid,
            "item_unit" => $this->itemunit,
            "variant_id" => $this->poid,
            "sku" => $this->posku,
            'item_serial_number' => $this->itemserialnumber,
            'item_inventory_type' => $this->iteminventorytype,
            "title" => $this->itemname,
            "option" => $this->itemoption,
            'quantity' => $this->quantity,
            'item_unit_price' => $this->itemunitprice,
            "promotion_price" => $this->promotionprice,
            "promotion_id" => $this->promotionid,
            "promotion_name" => $this->promotionname,
            "promotion_type_parent" => $this->promotiontypeparent,
            "promotion_type_children" => $this->promotiontypechildren,
            "has_gift" => $this->hasgift,
            "gift_name" => $this->giftname,
            "gift_date_created" => $this->giftdatecreated,
            "gift_date_modified" => $this->giftdatemodified,
            "coupon_id" => $this->couponid,
            "coupon_code" => $this->couponcode
        );

        $this->id = self::doAdd($data, '', '', $error);

        return $this->id;
    }

    public function updateData(&$error = array())
    {
        $data = array(
            "pos" => $this->pos,
            "order_id" => $this->soid,
            "finvoiceid" => $this->invoiceid,
            "company_id" => $this->cid,
            "creator_id" => $this->employeeid,
            "origin_id" => $this->originid,
            "product_id" => $this->pid,
            "item_unit" => $this->itemunit,
            "variant_id" => $this->poid,
            "sku" => $this->posku,
            "title" => $this->itemname,
            "option" => $this->itemoption,
            'quantity' => $this->quantity,
            'item_serial_number' => $this->itemserialnumber,
            'item_inventory_type' => $this->iteminventorytype,
            'item_unit_price' => $this->itemunitprice,
            "promotion_price" => $this->promotionprice,
            "promotion_id" => $this->promotionid,
            "promotion_name" => $this->promotionname,
            "promotion_type_parent" => $this->promotiontypeparent,
            "promotion_type_children" => $this->promotiontypechildren,
            "has_gift" => $this->hasgift,
            "gift_name" => $this->giftname,
            "gift_date_created" => $this->giftdatecreated,
            "gift_date_modified" => $this->giftdatemodified,
            "coupon_id" => $this->couponid,
            "coupon_code" => $this->couponcode
        );

        $url = self::$serviceurl . '/' . $this->id;
        return self::doUpdate($data, $url, '', $error);
    }


    public function delete(&$error = array())
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }

    public static function getWeight($formData, $id)
    {
        $url = self::$serviceurl . "/weight/" . $id;
        return self::getRawItems($formData, $url);
    }

    public static function getDetail($formData, $sortby = '', $sorttype = '', $limitString = '', $countOnly = false)
    {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }
    }

    public function getDataByJson($jsonData)
    {
        $this->cid = (int)$jsonData['company_id'];
        $this->employeeid = (int)$jsonData['creator_id'];
        $this->poid = (int)$jsonData['variant_id'];
        $this->originid = (int)$jsonData['origin_id'];
        $this->posku = (string)$jsonData['sku'];
        $this->pid = (int)$jsonData['product_id'];
        $this->id = (int)$jsonData['id'];
        $this->itemserialnumber = (string)$jsonData['item_serial_number'];
        $this->iteminventorytype = (int)$jsonData['item_inventory_type'];
        $this->isgiftproduct = (int)$jsonData['is_gift_product'];
        $this->itemname = (string)$jsonData['item_name'];
        $this->itemunit = (string)$jsonData['item_unit'];
        $this->quantity = (float)$jsonData['quantity'];
        $this->itemunitpriceoriginal = (float)$jsonData['item_unit_price_original'];
        $this->itemunitprice = (float)$jsonData['item_unit_price'];
        $this->promotion = $jsonData['promotion'];
        $this->tax = (int)$jsonData['tax'];
        $this->soid = (int)$jsonData['order_id'];
        $this->subtotal = (float)$jsonData['sub_total'];
        $this->itemoption = (string)$jsonData['item_option'];
        $this->status = (int)$jsonData['status'];
        $this->displayorder = (int)$jsonData['display_order'];
        $this->originid = (string)$jsonData['origin_id'];
        $this->datecreated = (int)$jsonData['date_created'];
        $this->datemodified = (int)$jsonData['date_modified'];
        $this->sodatecreated = (int)$jsonData['date_created'];
        $this->invoiceid = (string)$jsonData['invoice_id'];
    }
}
