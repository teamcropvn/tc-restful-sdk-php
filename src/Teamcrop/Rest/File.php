<?php

namespace Teamcrop\Rest;

class File extends Base
{
    public static $serviceurl = 'http://api.teamcrop.com/v1/files';

    const OBJECTTYPE_FILE = 10;
    const OBJECTTYPE_PROJECT = 20;
    const OBJECTTYPE_PROJECT_ISSUE = 21;
    const OBJECTTYPE_PROJECT_ISSUE_COMMENT = 22;
    const OBJECTTYPE_PROJECT_STORY = 23;
    const OBJECTTYPE_PROJECT_STORY_COMMENT = 24;


    const OBJECTTYPE_ACTIVITY = 30;
    const OBJECTTYPE_MESSAGE = 40;
    const OBJECTTYPE_CHAT = 50;
    const OBJECTTYPE_CALENDAR_EVENT = 60;

    const OBJECTTYPE_DISCUSSION = 70;
    const OBJECTTYPE_DISCUSSION_COMMENT = 71;

    const OBJECTTYPE_FILEREVISION = 80;

    const OBJECTTYPE_NEWSCOMMENT = 81;
    const OBJECTTYPE_NEWS = 82;

    const OBJECTTYPE_WORKGROUP = 90;
    const OBJECTTYPE_CRM_CONTRACTSTAGE = 100;
    const OBJECTTYPE_FLOWACTION = 110;
    const OBJECTTYPE_HAPPYCARE = 120;

    const STATUS_ENABLE = 1;
    const STATUS_DISABLED = 3;
    const STATUS_PENDING = 5;

    const PREVIEWCONVERTSTATUS_NOTAVAILABLE = 0;
    const PREVIEWCONVERTSTATUS_PENDING = 1;
    const PREVIEWCONVERTSTATUS_CONVERTING = 3;
    const PREVIEWCONVERTSTATUS_CONVERTSUCCESS = 5;
    const PREVIEWCONVERTSTATUS_CONVERTFAIL = 7;
    const PREVIEWCONVERTSTATUS_ERROR = 9;

    public $cid = 0;
    public $uid = 0;
    public $did = 0;
    public $id = 0;
    public $objecttype = 0;
    public $objectid = 0;
    public $md5hash = '';
    public $fulldirectorypath = '';
    public $filepath = '';
    public $filepreview = '';
    public $filepreviewconvertstatus = 0;
    public $filepreviewconvertoutput = '';
    public $thumbnail = '';
    public $resourceserver = 0;
    public $randomcode = '';
    public $password = '';
    public $title = '';
    public $description = '';
    public $extension = '';
    public $sizeinbyte = 0;
    public $revision = 0;
    public $revisionversion = '';
    public $revisionoriginalid = 0;
    public $filelist = '';
    public $source = 0;
    public $countview = 0;
    public $countdownload = 0;
    public $countlike = '';
    public $countcomment = 0;
    public $countmarker = 0;
    public $countrevision = 0;
    public $permission = 0;
    public $deletereason = 0;
    public $status = 0;
    public $isdeleted = 0;
    public $isdirectory = 0;
    public $ipaddress = 0;
    public $datecreated = 0;
    public $datemodified = 0;
    public $datedeleted = 0;
    public $datelastview = 0;
    public $datelastdownload = 0;

    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData()
    {
        return true;
    }

    public function updateData()
    {
        return true;
    }


    public function delete()
    {
        return true;
    }

    public static function getFiles($formData, $sortby = '', $sorttype = '', $limitString = '', $countOnly = false)
    {
        return self::getItems($formData, $sortby, $sorttype, $limitString, $countOnly);
    }

    public function checkStatusName($name)
    {
        return true;
    }

    public function getDisplayIcon()
    {
        return '';
    }

    public function checkObjectTypeName($type)
    {
        return true;
    }
}
