<?php

namespace Teamcrop\Rest;

class File extends Base
{
    public static $serviceurl = '/v1/files';

    const OBJECTTYPE_FILE = 10;
    const OBJECTTYPE_PROJECT = 20;
    const OBJECTTYPE_PROJECT_ISSUE = 21;
    const OBJECTTYPE_PROJECT_ISSUE_COMMENT = 22;
    const OBJECTTYPE_PROJECT_STORY = 23;
    const OBJECTTYPE_PROJECT_STORY_COMMENT = 24;


    const OBJECTTYPE_ACTIVITY = 30;
    const OBJECTTYPE_MESSAGE = 40;
    const OBJECTTYPE_CHAT = 50;
    const OBJECTTYPE_CALENDAR_EVENT = 60;

    const OBJECTTYPE_DISCUSSION = 70;
    const OBJECTTYPE_DISCUSSION_COMMENT = 71;

    const OBJECTTYPE_FILEREVISION = 80;

    const OBJECTTYPE_NEWSCOMMENT = 81;
    const OBJECTTYPE_NEWS = 82;

    const OBJECTTYPE_WORKGROUP = 90;
    const OBJECTTYPE_CRM_CONTRACTSTAGE = 100;
    const OBJECTTYPE_FLOWACTION = 110;
    const OBJECTTYPE_HAPPYCARE = 120;

    const STATUS_ENABLE = 1;
    const STATUS_DISABLED = 3;
    const STATUS_PENDING = 5;

    const PREVIEWCONVERTSTATUS_NOTAVAILABLE = 0;
    const PREVIEWCONVERTSTATUS_PENDING = 1;
    const PREVIEWCONVERTSTATUS_CONVERTING = 3;
    const PREVIEWCONVERTSTATUS_CONVERTSUCCESS = 5;
    const PREVIEWCONVERTSTATUS_CONVERTFAIL = 7;
    const PREVIEWCONVERTSTATUS_ERROR = 9;

    public $cid = 0;
    public $uid = 0;
    public $did = 0;
    public $id = 0;
    public $objecttype = 0;
    public $objectid = 0;
    public $md5hash = '';
    public $fulldirectorypath = '';
    public $filepath = '';
    public $filepreview = '';
    public $filepreviewconvertstatus = 0;
    public $filepreviewconvertoutput = '';
    public $thumbnail = '';
    public $width = 0;
    public $height = 0;
    public $resourceserver = 0;
    public $randomcode = '';
    public $password = '';
    public $title = '';
    public $description = '';
    public $extension = '';
    public $sizeinbyte = 0;
    public $revision = 0;
    public $revisionversion = '';
    public $revisionoriginalid = 0;
    public $filelist = '';
    public $source = 0;
    public $countview = 0;
    public $countdownload = 0;
    public $countlike = '';
    public $countcomment = 0;
    public $countmarker = 0;
    public $countrevision = 0;
    public $permission = 0;
    public $deletereason = 0;
    public $status = 0;
    public $isdeleted = 0;
    public $isdirectory = 0;
    public $ipaddress = 0;
    public $datecreated = 0;
    public $datemodified = 0;
    public $datedeleted = 0;
    public $datelastview = 0;
    public $datelastdownload = 0;

    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData()
    {
        return true;
    }

    public function updateData()
    {
        return true;
    }

    public static function checkFileOwner($newFile, $oldFile, $uid)
    {
        $arrFileError = array();
        $arrNew = array();
        $arrOld = array();
        if (!is_array($newFile)) {
            $arrNew = explode(",", $newFile);
        }

        if (!is_array($oldFile)) {
            $arrOld = explode(",", $oldFile);
        }

        $arrNew = array_filter($arrNew);
        $arrOld = array_filter($arrOld);

        if (!empty($arrNew)) {
            $arrCheck = array_diff($arrNew, $arrOld);
            if (!empty($arrCheck)) {
                foreach ($arrCheck as $item) {
                    $objFile = new self((int)$item);
                    if ($objFile->id <= 0 || $objFile->uid != (int)$uid) {
                        $arrFileError[] = $item;
                    }
                }
            }
        }

        return $arrFileError;
    }

    public function delete(&$error = array())
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }

    public static function getFiles($formData, $sortby = '', $sorttype = '', $limitString = '', $countOnly = false)
    {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }
    }

    public function getJsonData()
    {
        $parentDirectories = array();

        $data = array(
            'is_directory' => (int)$this->isdirectory,
            'company_id' => (int)$this->cid,
            'creator_id' => (int)$this->uid,
            'id' => (int)$this->id,
            'parent_id' => (int)$this->did,
            'object_type' => $this->objecttype,
            'object_id' => $this->objectid,
            'md5_hash' => $this->md5hash,
            'file_path' => $this->filepath,
            'full_directory_path' => $this->fulldirectorypath,
            'file_preview' => $this->filepreview,
            'file_preview_convert_status' => $this->filepreviewconvertstatus,
            'thumbnail' => $this->thumbnail,
            'width' => $this->width,
            'height' => $this->height,
            'randomcode' => $this->randomcode,
            'title' => $this->title,
            'description' => $this->description,
            'extension' => $this->extension,
            'size_in_byte' => $this->sizeinbyte,
            'revision' => $this->revision,
            'source' => $this->source,
            'count_view' => $this->countview,
            'count_download' => $this->countdownload,
            'count_comment' => $this->countcomment,
            'status' => $this->status,
            'ip_address' => $this->ipaddress,
            'date_created' => $this->datecreated,
            'date_modified' => $this->datemodified,
            'parents' => $parentDirectories
        );

        return $data;
    }

    public function getDataByJson($jsonData)
    {
        $this->isdirectory = (int)$jsonData['is_directory'];
        $this->cid = (int)$jsonData['company_id'];
        $this->uid = (int)$jsonData['creator_id'];
        $this->id = (int)$jsonData['id'];
        $this->did = (int)$jsonData['parent_id'];
        $this->objecttype = (int)$jsonData['object_type'];
        $this->objectid = (int)$jsonData['object_id'];
        $this->md5hash = (string)$jsonData['md5_hash'];
        $this->filepath = (string)$jsonData['file_path'];
        $this->fulldirectorypath = (string)$jsonData['full_directory_path'];
        $this->filepreview = (string)$jsonData['file_preview'];
        $this->filepreviewconvertstatus = (string)$jsonData['file_preview_convert_status'];
        $this->thumbnail = (string)$jsonData['thumbnail'];
        $this->width = (int)$jsonData['width'];
        $this->height = (int)$jsonData['height'];
        $this->randomcode = (string)$jsonData['randomcode'];
        $this->title = (string)$jsonData['title'];
        $this->description = (string)$jsonData['description'];
        $this->extension = (string)$jsonData['extension'];
        $this->sizeinbyte = (int)$jsonData['size_in_byte'];
        $this->revision = (int)$jsonData['revision'];
        $this->source = (string)$jsonData['source'];
        $this->countview = (int)$jsonData['count_view'];
        $this->countdownload = (int)$jsonData['count_download'];
        $this->countcomment = (int)$jsonData['count_comment'];
        $this->status = (int)$jsonData['status'];
        $this->ipaddress = (string)$jsonData['ip_address'];
        $this->datecreated = (int)$jsonData['date_created'];
        $this->datepushed = (int)$jsonData['date_pushed'];
    }

    public function isImage()
    {
        return in_array($this->extension, array('jpg', 'png', 'jpeg'));
    }

    /**
     * Convert HTML to PDF
     *
     * @param $content
     * @param $error
     * @return null
     */
    public static function html2pdfConverter($content, &$error)
    {
        $responseData = null;

        $data = array(
            'html' => $content,
        );

        $serviceurl = self::$serviceurl . '/html2pdf';
        $headers = array(
            'Content-type' => 'application/json'
        );

        try {
            //Do request and get response with submit data
            $response = self::doRequest('POST', $serviceurl, $headers, false, true, json_encode($data));

            //request success
            if ($response['status'] == '200' || $response['status'] == '201') {
                $responseData = $response['data'];
            } else {
                $error = self::parsingErrorFromResponse($response);
            }
        } catch (\Exception $e) {
            $error[] = $e->getMessage();
        }

        return $responseData;
    }
}
