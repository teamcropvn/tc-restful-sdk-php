<?php

namespace Teamcrop\Rest;

class OrderShippingmethod extends Base
{
    public static $serviceurl = '/v1/relordershippingmethods';

    const STATUS_NEW = 1;
    const STATUS_PENDING = 3;
    const STATUS_PACKAGING = 5;
    const STATUS_WAITFORPICKING = 7;
    const STATUS_PICKED = 9;
    const STATUS_PROCESSING = 11;
    const STATUS_SHIPPING = 13;
    const STATUS_SHIPPED = 15;
    const STATUS_WAITFORPAYMENT = 17;
    const STATUS_RETURNING = 19;
    const STATUS_SUCCESS = 21;
    const STATUS_FAIL = 23;
    const STATUS_CANCEL = 25;

    public $cid = 0;
    public $uid = 0;
    public $soid = 0;
    public $ssmid = 0;
    public $transactionid = '';
    public $id = 0;
    public $value = '';
    public $scrostrackingcode = '';
    public $scrostime = 0;
    public $note = '';
    public $status = 0;
    public $datecreated = 0;
    public $datemodified = 0;

    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData(&$error = array())
    {
        $data = array(
            'creator_id' => $this->uid,
            'company_id' => $this->cid,
            'order_id' => $this->soid,
            'shipping_method_id' => $this->ssmid,
            'transaction_id' => $this->transactionid,
            'value' => $this->value,
            'scros_tracking_code' => $this->scrostrackingcode,
            'scros_time' => $this->scrostime,
            'note' => $this->note,
            'status' => $this->status
        );

        $this->id = self::doAdd($data, '', '', $error);

        return $this->id;
    }

    public function updateData(&$error = array())
    {
        $data = array(
            'creator_id' => $this->uid,
            'company_id' => $this->cid,
            'order_id' => $this->soid,
            'shipping_method_id' => $this->ssmid,
            'transaction_id' => $this->transactionid,
            'value' => $this->value,
            'scros_tracking_code' => $this->scrostrackingcode,
            'scros_time' => $this->scrostime,
            'note' => $this->note,
            'status' => $this->status
        );

        $url = self::$serviceurl . '/' . $this->id;
        return self::doUpdate($data, $url, '', $error);
    }

    public function delete(&$error = array())
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }

    public static function getStatusList()
    {
        $output = array();

        $output[self::STATUS_NEW] = 'shippingstatusNew';
        $output[self::STATUS_PENDING] = 'shippingstatusPending';
        $output[self::STATUS_PACKAGING] = 'shippingstatusPackaging';
        $output[self::STATUS_WAITFORPICKING] = 'shippingstatusWaitForPicking';
        $output[self::STATUS_PICKED] = 'shippingstatusPicked';
        $output[self::STATUS_PROCESSING] = 'shippingstatusProcessing';
        $output[self::STATUS_SHIPPING] = 'shippingstatusShipping';
        $output[self::STATUS_SHIPPED] = 'shippingstatusShipped';
        $output[self::STATUS_WAITFORPAYMENT] = 'shippingstatusWaitForPayment';
        $output[self::STATUS_RETURNING] = 'shippingstatusReturning';
        $output[self::STATUS_SUCCESS] = 'shippingstatusSuccess';
        $output[self::STATUS_FAIL] = 'shippingstatusFail';
        $output[self::STATUS_CANCEL] = 'shippingstatusCancel';

        return $output;
    }

    public static function getOrderShippingmethods(
        $formData,
        $sortby = '',
        $sorttype = '',
        $limitString = '',
        $countOnly = false
    )
    {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }
    }

    public function getJsonData()
    {
        $data = array(
            'company_id' => $this->cid,
            'creator_id' => $this->uid,
            'order_id' => (int)$this->soid,
            'shipping_method_id' => (int)$this->ssmid,
            'transaction_id' => (string)$this->transactionid,
            'id' => (int)$this->id,
            'value' => (string)$this->value,
            'scros_tracking_code' => (string)$this->scrostrackingcode,
            'scros_time' => (int)$this->scrostime,
            'note' => (string)$this->note,
            'status' => (int)$this->status,
            'date_created' => (int)$this->datecreated,
            'date_modified' => (int)$this->datemodified
        );

        return $data;
    }

    public function getDataByJson($row)
    {
        $this->cid = (int)$row['company_id'];
        $this->uid = (int)$row['creator_id'];
        $this->soid = (int)$row['order_id'];
        $this->ssmid = (int)$row['shipping_method_id'];
        $this->transactionid = (string)$row['transaction_id'];
        $this->id = (int)$row['id'];
        $this->value = (string)$row['value'];
        $this->scrostrackingcode = (string)$row['scros_tracking_code'];
        $this->scrostime = (int)$row['scros_time'];
        $this->note = (string)$row['note'];
        $this->status = (int)$row['status'];
        $this->datecreated = (int)$row['date_created'];
        $this->datemodified = (int)$row['date_modified'];

    }
}
