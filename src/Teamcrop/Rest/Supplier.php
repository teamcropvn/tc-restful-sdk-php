<?php

namespace Teamcrop\Rest;

class Supplier extends Base
{
    public static $serviceurl = '/v1/suppliers';

    const PRODUCTTYPE_GOODS = 1;
    const PRODUCTTYPE_SERVICE = 3;

    const STATUS_ENABLE = 1;
    const STATUS_DISABLE = 3;

    public $cid = 0;
    public $uid = 0;
    public $id = 0;
    public $name = '';
    public $summary = '';
    public $note = '';
    public $companyname = '';
    public $code = '';
    public $email = '';
    public $phone = '';
    public $fax = '';
    public $website = '';
    public $taxnumber = '';
    public $producttype = 0;
    public $salefullname = '';
    public $saleemail = '';
    public $salephone = '';
    public $address = '';
    public $region = 0;
    public $subregion = 0;
    public $filepath = '';
    public $point = 0;
    public $allowdebt = 0;
    public $allowdebtday = 0;
    public $accessusername = '';
    public $accesspassword = '';
    public $parentid = 0;
    public $managerid = 0;
    public $status = 0;
    public $datecreated = 0;
    public $datemodified = 0;

    // Fields bo sung
    public $company_info = '';

    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData(&$error = array())
    {
        $data = array(
            'creator_id' => $this->uid,
            'company_id' => $this->cid,
            'name' => $this->name,
            'summary' => $this->summary,
            'company_name' => $this->companyname,
            'code' => $this->code,
            'email' => $this->email,
            'phone' => $this->phone,
            'fax' => $this->fax,
            'website' => $this->website,
            'tax_number' => $this->taxnumber,
            'product_type' => $this->producttype,
            'sale_full_name' => $this->salefullname,
            'sale_email' => $this->saleemail,
            'sale_phone' => $this->salephone,
            'address' => $this->address,
            'region_id' => $this->region,
            'sub_region_id' => $this->subregion,
            'file_path' => $this->filepath,
            'point' => $this->point,
            'allow_debt' => $this->allowdebt,
            'allowde_btday' => $this->allowdebtday,
            'access_username' => $this->accessusername,
            'access_password' => $this->accesspassword,
            'parent_id' => $this->parentid,
            'manager_id' => $this->managerid
        );

        $this->id = self::doAdd($data, '', '', $error);

        return $this->id;
    }

    public function updateData(&$error = array())
    {
        $data = array(
            'creator_id' => $this->uid,
            'company_id' => $this->cid,
            'name' => $this->name,
            'summary' => $this->summary,
            'company_name' => $this->companyname,
            'code' => $this->code,
            'email' => $this->email,
            'phone' => $this->phone,
            'fax' => $this->fax,
            'website' => $this->website,
            'tax_number' => $this->taxnumber,
            'product_type' => $this->producttype,
            'sale_full_name' => $this->salefullname,
            'sale_email' => $this->saleemail,
            'sale_phone' => $this->salephone,
            'address' => $this->address,
            'region_id' => $this->region,
            'sub_region_id' => $this->subregion,
            'file_path' => $this->filepath,
            'point' => $this->point,
            'allow_debt' => $this->allowdebt,
            'allowde_btday' => $this->allowdebtday,
            'access_username' => $this->accessusername,
            'access_password' => $this->accesspassword,
            'parent_id' => $this->parentid,
            'manager_id' => $this->managerid
        );

        $url = self::$serviceurl . '/' . $this->id;
        return self::doUpdate($data, $url, '', $error);
    }


    public function delete(&$error = array())
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }

    public static function getSuppliers($formData, $sortby = '', $sorttype = '', $limitString = '', $countOnly = false)
    {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }
    }

    public function getJsonData($getfullinfo = false)
    {
        $data = array(
            'id' => (int)$this->id,
            'name' => (string)$this->name,
            "code" => (string)$this->code,
            "company_info" => (string)$this->company_info,
            "company_name" => (string)$this->companyname,
            "summary" => (string)$this->summary,
            "note" => (string)$this->note
        );

        if ($getfullinfo) {

            $data = array_merge(
                $data,
                array(
                    "company_id" => (int)$this->cid,
                    "creator_id" => (int)$this->uid,
                    "email" => (string)$this->email,
                    "phone" => (string)$this->phone,
                    "fax" => (string)$this->fax,
                    "website" => (string)$this->website,
                    "product_type" => (int)$this->producttype,
                    "sale_fullname" => (string)$this->salefullname,
                    "sale_email" => (string)$this->saleemail,
                    "sale_phone" => (string)$this->salephone,
                    "address" => (string)$this->address,
                    "region_id" => (int)$this->region,
                    "sub_region_id" => (int)$this->subregion,
                    "file_path" => (string)$this->filepath,
                    "point" => (float)$this->point,
                    "allow_debt" => (int)$this->allowdebt,
                    "allow_debt_day" => (int)$this->allowdebtday,
                    "access_username" => (string)$this->accessusername,
                    "access_password" => (string)$this->accesspassword,
                    "parent_id" => (int)$this->parentid,
                    "manager_id" => (int)$this->managerid,
                    "status" => (int)$this->status,
                    "date_created" => (int)$this->datecreated,
                    "date_modified" => (int)$this->datemodified
                )
            );
        }

        return $data;
    }

    public static function cacheKey($departmentid)
    {
        return 'tc_supplier_' . $departmentid;
    }

    public function getDataByArrayCache($row)
    {

    }

    public function getDataByJson($jsonData)
    {
        $this->cid = (int)$jsonData['company_id'];
        $this->uid = (int)$jsonData['creator_id'];
        $this->id = (int)$jsonData['id'];
        $this->name = (string)$jsonData['name'];
        $this->summary = (string)$jsonData['summary'];
        $this->note = (string)$jsonData['note'];
        $this->companyname = (string)$jsonData['company_name'];
        $this->code = (string)$jsonData['code'];
        $this->email = (string)$jsonData['email'];
        $this->phone = (string)$jsonData['phone'];
        $this->fax = (string)$jsonData['fax'];
        $this->website = (string)$jsonData['website'];
        $this->taxnumber = (string)$jsonData['tax_number'];
        $this->producttype = (int)$jsonData['product_type'];
        $this->salefullname = (string)$jsonData['sale_fullname'];
        $this->saleemail = (string)$jsonData['sale_email'];
        $this->salephone = (string)$jsonData['sale_phone'];
        $this->address = (string)$jsonData['address'];
        $this->region = (int)$jsonData['region_id'];
        $this->subregion = (int)$jsonData['sub_region_id'];
        $this->filepath = (string)$jsonData['file_path'];
        $this->point = (float)$jsonData['point'];
        $this->allowdebt = (int)$jsonData['allow_debt'];
        $this->allowdebtday = (int)$jsonData['allow_debt_day'];
        $this->accessusername = (string)$jsonData['access_username'];
        $this->accesspassword = (string)$jsonData['access_password'];
        $this->parentid = (int)$jsonData['parent_id'];
        $this->managerid = (int)$jsonData['manager_id'];
        $this->status = (int)$jsonData['status'];
        $this->datecreated = (int)$jsonData['date_created'];
        $this->datemodified = (int)$jsonData['date_modified'];
    }
}
