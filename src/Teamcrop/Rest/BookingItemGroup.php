<?php

namespace Teamcrop\Rest;

class BookingItemGroup extends Base
{
    public static $serviceurl = '/v1/bookingitemgroups';

    const TYPE_HOTEL = 1;
    const TYPE_ACTIVITY = 3;
    const TYPE_PEOPLE = 5;
    const TYPE_EQUIPMENT = 7;

    public $cid = 0;
    public $uid = 0;
    public $id = 0;
    public $name = '';
    public $code = '';
    public $description = '';
    public $type = 0;
    public $parentid = 0;
    public $displayorder = 0;
    public $datecreated = 0;
    public $datemodified = 0;

    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData(&$error = array())
    {
        $row = $this->getJsonData();
        $this->id = self::doAdd($row, '', '', $error);

        return $this->id;
    }

    public function updateData(&$error = array())
    {
        $row = $this->getJsonData();
        $url = self::$serviceurl . '/' . $this->id;
        return self::doUpdate($row, $url, '', $error);
    }

    public function delete(&$error = array())
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }

    public static function getBookingItemGroups(
        $formData,
        $sortby = '',
        $sorttype = '',
        $limitString = '',
        $countOnly = false
    ) {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }
    }


    public function getDataByJson($row)
    {
        $this->cid = (int)$row['company_id'];
        $this->uid = (int)$row['creator_id'];
        $this->id = (int)$row['id'];
        $this->name = (string)$row['name'];
        $this->code = (string)$row['code'];
        $this->description = (string)$row['description'];
        $this->type = (int)$row['type'];
        $this->parentid = (int)$row['parent_id'];
        $this->displayorder = (int)$row['display_order'];
        $this->datecreated = (int)$row['date_created'];
        $this->datemodified = (int)$row['date_modified'];
    }

    public function getJsonData()
    {
        $row['company_id'] = (int)$this->cid;
        $row['creator_id'] = (int)$this->uid;
        $row['id'] = (int)$this->id;
        $row['name'] = (string)$this->name;
        $row['code'] = (string)$this->code;
        $row['description'] = (string)$this->description;
        $row['type'] = (int)$this->type;
        $row['parent_id'] = (int)$this->parentid;
        $row['display_order'] = (int)$this->displayorder;
        $row['date_created'] = (int)$this->datecreated;
        $row['date_modified'] = (int)$this->datemodified;

        return $row;
    }


}
