<?php

namespace Teamcrop\Rest;

class EcomPlatformProduct extends Base
{
    public static $serviceurl = '/v1/ecomplatformproducts';

    const SYNCSTATUS_NEW = 1;
    const SYNCSTATUS_PROCESSING = 3;
    const SYNCSTATUS_COMPLETED = 5;
    const SYNCSTATUS_ERROR = 7;

    const STATUS_ENABLE = 1;
    const STATUS_DISABLED = 3;

    public $cid = 0;
    public $uid = 0;
    public $spid = 0;
    public $sepid = 0;
    public $id = 0;
    public $identifier = '';
    public $code = '';
    public $syncstatus = 0;
    public $syncoutput = '';
    public $syncduration = 0;
    public $status = 0;
    public $datecreated = 0;
    public $datemodified = 0;
    public $datelastsynced = 0;

    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData(&$error = array())
    {
        $data = array(
            'creator_id' => $this->uid,
            'company_id' => $this->cid,
            'product_id' => $this->spid,
            'platform_id' => $this->sepid,
            'identifier' => $this->identifier,
            'code' => $this->code,
            'sync_status' => $this->syncstatus,
            'sync_output' => $this->syncoutput,
            'sync_duration' => $this->syncduration,
            'status' => $this->status

        );

        $this->id = self::doAdd($data, '', '', $error);

        return $this->id;
    }

    public function updateData(&$error = array())
    {
        $data = array(
            'creator_id' => $this->uid,
            'company_id' => $this->cid,
            'product_id' => $this->spid,
            'platform_id' => $this->sepid,
            'identifier' => $this->identifier,
            'code' => $this->code,
            'sync_status' => $this->syncstatus,
            'sync_output' => $this->syncoutput,
            'sync_duration' => $this->syncduration,
            'status' => $this->status
        );

        $url = self::$serviceurl . '/' . $this->id;
        return self::doUpdate($data, $url, '', $error);
    }

    public function delete(&$error = array())
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }

    public static function getProducts($formData)
    {
        $url = self::$serviceurl . "/product";
        return self::getRawItems($formData, $url);
    }

    public static function getEcomplatformProduct(
        $formData,
        $sortby = '',
        $sorttype = '',
        $limitString = '',
        $countOnly = false
    )
    {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }
    }


    public function getDataByJson($row)
    {
        $this->cid = (int)$row['company_id'];
        $this->uid = (int)$row['creator_id'];
        $this->spid = (int)$row['product_id'];
        $this->sepid = (int)$row['platform_id'];
        $this->id = (int)$row['id'];
        $this->identifier = (string)$row['identifier'];
        $this->code = (string)$row['code'];
        $this->syncstatus = (int)$row['sync_status'];
        $this->syncoutput = (string)$row['sync_output'];
        $this->syncduration = (int)$row['sync_duration'];
        $this->status = (int)$row['status'];
        $this->datecreated = (int)$row['date_created'];
        $this->datemodified = (int)$row['date_modified'];
        $this->datelastsynced = (int)$row['date_last_synced'];
    }

    public function getJsonData()
    {
        $data = array(
            'creator_id' => (int)$this->uid,
            'company_id' => (int)$this->cid,
            'product_id' => (int)$this->spid,
            'platform_id' => (int)$this->sepid,
            'id' => (int)$this->id,
            'identifier' => (string)$this->identifier,
            "code" => (string)$this->code,
            "sync_status" => (int)$this->syncstatus,
            "sync_output" => (string)$this->syncoutput,
            "sync_duration" => (int)$this->syncduration,
            "status" => (int)$this->status,
            "date_created" => (int)$this->datecreated,
            "date_modified" => (int)$this->datemodified,
            "date_last_synced" => (int)$this->datelastsynced,
        );

        return $data;
    }
}
