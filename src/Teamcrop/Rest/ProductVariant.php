<?php

namespace Teamcrop\Rest;

class ProductVariant extends Base
{
    public static $serviceurl = '/v1/productvariants';

    const STATUS_ENABLE = 1;
    const STATUS_DISABLE = 2;

    public $cid = 0;
    public $uid = 0;
    public $pid = 0;
    public $id = 0;
    public $stock = 0;
    public $cost = '';
    public $price = '';
    public $pricecompare = '';
    public $weight = 0;
    public $sku = '';
    public $source = '';
    public $sourceid = '';
    public $barcode = '';
    public $istax = 0;
    public $isshipping = 0;
    public $istrackinventory = 0;
    public $allowordernolonger = 0;
    public $displayorder = 0;
    public $height = 0;
    public $width = 0;
    public $length = 0;
    public $status = 0;
    public $ipaddress = 0;
    public $datecreated = 0;
    public $datemodified = 0;
    public $optiongroupvaluelist = null;
    public $name = '';
    public $title = "";
    public $color = "";
    public $size = "";
    public $material = "";
    public $style = "";
    public $product = array();

    const SOURCE_INTERNAL = 0;
    const SOURCE_WORDPRESS = 3;
    const SOURCE_MAGENTO = 5;
    const SOURCE_PRESTASHOP = 7;
    const SOURCE_OPENCART = 9;
    const SOURCE_OSCOMMERCE = 11;
    const SOURCE_OTHER = 13;
    const SOURCE_WOOECOMMERCE = 15;

    const SOURCE_BIZWEB = 17;
    const SOURCE_HARAVAN = 19;
    const SOURCE_SHOPIFY = 21;
    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData(&$error = array())
    {
        $data = array(
            "company_id" => $this->cid,
            "creator_id" => $this->uid,
            "product_id" => $this->pid,
            "sku" => $this->sku,
            "source_id" => $this->sourceid,
            "source" => $this->source,
            "title" => $this->title,
            "color" => $this->color,
            "style" => $this->style,
            "material" => $this->material,
            "size" => $this->size,
            "cost" => $this->cost,
            "price" => $this->price,
            "weight" => $this->weight,
        );

        $this->id = self::doAdd($data, '', '', $error);

        return $this->id;
    }

    public function updateData(&$error = array())
    {
        $data = array(
            "company_id" => $this->cid,
            "creator_id" => $this->uid,
            "product_id" => $this->pid,
            "sku" => $this->sku,
            "source_id" => $this->sourceid,
            "source" => $this->source,
            "title" => $this->title,
            "color" => $this->color,
            "style" => $this->style,
            "material" => $this->material,
            "size" => $this->size,
            "weight" => $this->weight,
            "price" => $this->price,
            "cost" => $this->cost,
            'status' => $this->status
        );

        $url = self::$serviceurl . '/' . $this->id;
        return self::doUpdate($data, $url, '', $error);
    }


    public function delete(&$error = array())
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }

    public static function getVariants($formData, $sortby = '', $sorttype = '', $limitString = '', $countOnly = false)
    {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }
    }

    public static function getVariantIds($formData)
    {
        return self::getRawItems($formData, self::$serviceurl . "/ids");

    }

    public function getJsonData($getproduct = false)
    {
        $data['company_id'] = (int)$this->cid;
        $data['creator_id'] = (int)$this->uid;
        $data['id'] = (int)$this->id;
        $data['product_id'] = (int)$this->pid;
        $data['sku'] = (string)$this->sku;
        $data['source'] = (string)$this->source;
        $data['sourceid'] = (string)$this->sourceid;
        $data['display_order'] = (int)$this->displayorder;
        $data['title'] = (string)$this->title;
        $data['color'] = (string)$this->color;
        $data['size'] = (string)$this->size;
        $data['material'] = (string)$this->material;
        $data['name'] = (string)$this->name;
        $data['style'] = (string)$this->style;
        $data['weight'] = (float)$this->weight;
        $data['status'] = (int)$this->status;
        $data['price'] = (float)$this->price;
        $data['cost'] = (float)$this->cost;
        $data['date_created'] = $this->datecreated;
        $data['date_modified'] = $this->datemodified;
        if ($getproduct) {
            $data['product'] = $this->product;
        }

        return $data;
    }
    public function getDataByJson($jsonData)
    {
        $this->cid = (int)$jsonData['company_id'];
        $this->uid = (int)$jsonData['creator_id'];
        $this->pid = (int)$jsonData['product_id'];
        $this->id = (int)$jsonData['id'];
        $this->stock = (int)$jsonData['stock'];
        $this->price = (float)$jsonData['price'];
        $this->cost = (float)$jsonData['cost'];
        $this->pricecompare = (float)$jsonData['price_compare'];
        $this->weight = (float)$jsonData['weight'];
        $this->sku = (string)$jsonData['sku'];
        $this->source = (string)$jsonData['source'];
        $this->sourceid = (string)$jsonData['sourceid'];
        $this->barcode = (string)$jsonData['barcode'];
        $this->istax = (int)$jsonData['is_tax'];
        $this->isshipping = (int)$jsonData['is_shipping'];
        $this->istrackinventory = (int)$jsonData['is_track_inventory'];
        $this->displayorder = (int)$jsonData['display_order'];
        $this->height = (float)$jsonData['height'];
        $this->width = (float)$jsonData['width'];
        $this->length = (float)$jsonData['length'];
        $this->status = (int)$jsonData['status'];
        $this->price = (float)$jsonData['price'];
        $this->ipaddress = (string)long2ip($jsonData['ip_address']);
        $this->datecreated = (int)$jsonData['date_created'];
        $this->datemodified = (int)$jsonData['date_modified'];
        $this->product = $jsonData["product"];
        $this->title  = (string)$jsonData['title'];
        $this->color  = (string)$jsonData['color'];
        $this->size  = (string)$jsonData['size'];
        $this->material  = (string)$jsonData['material'];
        $this->name  = (string)$jsonData['name'];
        $this->style  = (string)$jsonData['style'];
        $this->weight  = (float)$jsonData['weight'];
    }
}
