<?php

namespace Teamcrop\Rest;

class BillingMembership extends Base
{
    public static $serviceurl = '/v1/billingmemberships';
    const STATUS_ENABLE = 1;
    const STATUS_DISABLE = 3;

    public $cid = 0;
    public $uid = 0;
    public $id = 0;
    public $chargecycle = 0;
    public $discounttype = 0;
    public $discountvalueperstore = '';
    public $prepaidmonth = 0;
    public $contactcompany = '';
    public $contactemail = '';
    public $contactemailmore = '';
    public $contactphone = '';
    public $contactphonemore = '';
    public $contactfullname = '';
    public $contactaddress = '';
    public $invoiceinfo = '';
    public $note = '';
    public $price = '';
    public $allow_toggle_feature = 0;
    public $referrerfullname = '';
    public $referreremail = '';
    public $referrerphone = '';
    public $referreruserid = 0;
    public $referrercommissionpercent = '';
    public $status = 0;
    public $datecreated = 0;
    public $datemodified = 0;
    public $dateexpired = 0;
    public $dateexpiredoriginal = 0;

    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData(&$error = array())
    {
        $row['company_id'] = (int)$this->cid;
        $row['creator_id'] = (int)$this->uid;
        $row['charge_cycle'] = (int)$this->chargecycle;
        $row['discount_type'] = (int)$this->discounttype;
        $row['discount_value_per_store'] = (string)$this->discountvalueperstore;
        $row['pre_paid_month'] = (int)$this->prepaidmonth;
        $row['contact_company'] = (string)$this->contactcompany;
        $row['contact_email'] = (string)$this->contactemail;
        $row['contact_emailmore'] = (string)$this->contactemailmore;
        $row['contact_phone'] = (string)$this->contactphone;
        $row['contact_phonemore'] = (string)$this->contactphonemore;
        $row['contact_fullname'] = (string)$this->contactfullname;
        $row['contact_address'] = (string)$this->contactaddress;
        $row['invoice_info'] = (string)$this->invoiceinfo;
        $row['note'] = (string)$this->note;
        $row['price']  = $this->price;
        $row['allow_toggle_feature']  = (string)$this->allow_toggle_feature;
        $row['referrer_fullname'] = (string)$this->referrerfullname;
        $row['referrer_email'] = (string)$this->referreremail;
        $row['referrer_phone'] = (string)$this->referrerphone;
        $row['referrer_userid'] = (int)$this->referreruserid;
        $row['referrer_commission_percent'] = (string)$this->referrercommissionpercent;
        $row['status'] = (int)$this->status;
        $row['date_expired'] = (int)$this->dateexpired;
        $row['date_expired_original'] = (int)$this->dateexpiredoriginal;


        $this->id = self::doAdd($row, '', '', $error);

        return $this->id;
    }

    public function updateData(&$error = array())
    {
        $row['company_id'] = (int)$this->cid;
        $row['creator_id'] = (int)$this->uid;
        $row['id'] = (int)$this->id;
        $row['charge_cycle'] = (int)$this->chargecycle;
        $row['discount_type'] = (int)$this->discounttype;
        $row['discount_value_per_store'] = (string)$this->discountvalueperstore;
        $row['pre_paid_month'] = (int)$this->prepaidmonth;
        $row['contact_company'] = (string)$this->contactcompany;
        $row['contact_email'] = (string)$this->contactemail;
        $row['contact_emailmore'] = (string)$this->contactemailmore;
        $row['contact_phone'] = (string)$this->contactphone;
        $row['contact_phonemore'] = (string)$this->contactphonemore;
        $row['contact_fullname'] = (string)$this->contactfullname;
        $row['contact_address'] = (string)$this->contactaddress;
        $row['invoice_info'] = (string)$this->invoiceinfo;
        $row['note'] = (string)$this->note;
        $row['price']  = $this->price;
        $row['allow_toggle_feature']  = (string)$this->allow_toggle_feature;
        $row['referrer_fullname'] = (string)$this->referrerfullname;
        $row['referrer_email'] = (string)$this->referreremail;
        $row['referrer_phone'] = (string)$this->referrerphone;
        $row['referrer_userid'] = (int)$this->referreruserid;
        $row['referrer_commission_percent'] = (string)$this->referrercommissionpercent;
        $row['status'] = (int)$this->status;
        $row['date_expired'] = (int)$this->dateexpired;
        $row['date_expired_original'] = (int)$this->dateexpiredoriginal;


        $url = self::$serviceurl . '/' . $this->id;
        return self::doUpdate($row, $url, '', $error);
    }

    public function delete(&$error = array())
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }

    public static function getGroups(
        $formData,
        $sortby = '',
        $sorttype = '',
        $limitString = '',
        $countOnly = false
    ) {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }
    }


    public function getDataByJson($row)
    {
        $this->cid = (int)$row['company_id'];
        $this->uid = (int)$row['creator_id'];
        $this->id = (int)$row['id'];
        $this->chargecycle = (int)$row['charge_cycle'];
        $this->discounttype = (int)$row['discount_type'];
        $this->discountvalueperstore = (string)$row['discount_value_per_store'];
        $this->prepaidmonth = (int)$row['pre_paid_month'];
        $this->contactcompany = (string)$row['contact_company'];
        $this->contactemail = (string)$row['contact_email'];
        $this->contactemailmore = (string)$row['contact_emailmore'];
        $this->contactphone = (string)$row['contact_phone'];
        $this->contactphonemore = (string)$row['contact_phonemore'];
        $this->contactfullname = (string)$row['contact_fullname'];
        $this->contactaddress = (string)$row['contact_address'];
        $this->invoiceinfo = (string)$row['invoice_info'];
        $this->note = (string)$row['note'];
        $this->price = json_decode($row['bm_price'], true);
        $this->allowtogglefeature = (int)$row['allow_toggle_feature'];
        $this->referrerfullname = (string)$row['referrer_fullname'];
        $this->referreremail = (string)$row['referrer_email'];
        $this->referrerphone = (string)$row['referrer_phone'];
        $this->referreruserid = (int)$row['referrer_userid'];
        $this->referrercommissionpercent = (string)$row['referrer_commission_percent'];
        $this->status = (int)$row['status'];
        $this->datecreated = (int)$row['date_created'];
        $this->datemodified = (int)$row['date_modified'];
        $this->dateexpired = (int)$row['date_expired'];
        $this->dateexpiredoriginal = (int)$row['date_expired_original'];
    }

    public function getJsonData()
    {
        $row['company_id'] = (int)$this->cid;
        $row['creator_id'] = (int)$this->uid;
        $row['id'] = (int)$this->id;
        $row['charge_cycle'] = (int)$this->chargecycle;
        $row['discount_type'] = (int)$this->discounttype;
        $row['discount_value_per_store'] = (string)$this->discountvalueperstore;
        $row['pre_paid_month'] = (int)$this->prepaidmonth;
        $row['contact_company'] = (string)$this->contactcompany;
        $row['contact_email'] = (string)$this->contactemail;
        $row['contact_emailmore'] = (string)$this->contactemailmore;
        $row['contact_phone'] = (string)$this->contactphone;
        $row['contact_phonemore'] = (string)$this->contactphonemore;
        $row['contact_fullname'] = (string)$this->contactfullname;
        $row['contact_address'] = (string)$this->contactaddress;
        $row['invoice_info'] = (string)$this->invoiceinfo;
        $row['note'] = (string)$this->note;
        $row['price']  = $this->price;
        $row['allow_toggle_feature']  = (string)$this->allowtogglefeature;
        $row['referrer_fullname'] = (string)$this->referrerfullname;
        $row['referrer_email'] = (string)$this->referreremail;
        $row['referrer_phone'] = (string)$this->referrerphone;
        $row['referrer_userid'] = (int)$this->referreruserid;
        $row['referrer_commission_percent'] = (string)$this->referrercommissionpercent;
        $row['status'] = (int)$this->status;
        $row['date_created'] = (int)$this->datecreated;
        $row['date_modified'] = (int)$this->datemodified;
        $row['date_expired'] = (int)$this->dateexpired;
        $row['date_expired_original'] = (int)$this->dateexpiredoriginal;


        return $row;
    }


}
