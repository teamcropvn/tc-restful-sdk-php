<?php

namespace Teamcrop\Rest;

class ProductSerialHistory extends Base
{
    public static $serviceurl = '/v1/productserialhistorys';

    const TYPE_ADD = 1;
    const  TYPE_BUY = 3;
    const  TYPE_IMPORT = 5;
    const  TYPE_EXPORT = 7;
    const  TYPE_SELL = 9;
    const  TYPE_WARRANTY_RECEIVE = 11;
    const  TYPE_REPAIR = 13;
    const  TYPE_WARRANTY_RETURN = 15;

    public $cid = 0;
    public $uid = 0;
    public $pid = 0;
    public $poid = 0;
    public $serialnumber = '';
    public $id = 0;
    public $type = 0;
    public $objectid = 0;
    public $subobjectid = 0;
    public $note = '';
    public $ipaddress = 0;
    public $datecreated = 0;
    public $datemodified = 0;

    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData(&$error = array())
    {
        $row = $this->getJsonData();
        $this->id = self::doAdd($row, '', '', $error);

        return $this->id;
    }

    public static function addListData($data, &$error = array())
    {
        return self::doAdd($data, self::$serviceurl . "/list", '', $error);
    }

    public function updateData(&$error = array())
    {
        $row = $this->getJsonData();
        $url = self::$serviceurl . '/' . $this->id;
        return self::doUpdate($row, $url, '', $error);
    }

    public function delete(&$error = array())
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }

    public static function getProductSerialNumbers(
        $formData,
        $sortby = '',
        $sorttype = '',
        $limitString = '',
        $countOnly = false
    ) {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }
    }


    public function getDataByJson($row)
    {
        $this->cid = (int)$row['company_id'];
        $this->uid = (int)$row['creator_id'];
        $this->pid = (int)$row['product_id'];
        $this->poid = (int)$row['variant_id'];
        $this->serialnumber = (string)$row['serial_number'];
        $this->id = (int)$row['id'];
        $this->type = (int)$row['type'];
        $this->objectid = (int)$row['object_id'];
        $this->subobjectid = (int)$row['sub_object_id'];
        $this->note = (string)$row['note'];
        $this->datecreated = (int)$row['date_created'];
        $this->datemodified = (int)$row['date_modified'];
    }

    public function getJsonData()
    {
        $row['company_id'] = (int)$this->cid;
        $row['creator_id'] = (int)$this->uid;
        $row['product_id'] = (int)$this->pid;
        $row['variant_id'] = (int)$this->poid;
        $row['serial_number'] = (string)$this->serialnumber;
        $row['id'] = (int)$this->id;
        $row['type'] = (int)$this->type;
        $row['object_id'] = (int)$this->objectid;
        $row['sub_object_id'] = (int)$this->subobjectid;
        $row['note'] = (string)$this->note;
        $row['date_created'] = (int)$this->datecreated;
        $row['date_modified'] = (int)$this->datemodified;

        return $row;
    }
}
