<?php

namespace Teamcrop\Rest;

class Booking extends Base
{
    public static $serviceurl = '/v1/bookings';

    const SOURCE_WEB = 1;
    const SOURCE_APP = 3;
    const SOURCE_PARTNER = 5;
    const SOURCE_CMS = 7;
    const SOURCE_API = 9;

    const PAYMENTSTATUS_PENDING = 1;
    const PAYMENTSTATUS_PAYING = 3;
    const PAYMENTSTATUS_PAID = 5;

    const STATUS_NEW = 1;
    const STATUS_PENDING = 3;
    const STATUS_BOOKED = 5;
    const STATUS_CUSTOMERCANCELLED = 7;
    const STATUS_HOST = 9;
    const STATUS_NOSHOW = 11;
    const STATUS_ERROR = 13;

    public $cid = 0;
    public $uid = 0;
    public $ccid = 0;
    public $soid = 0;
    public $biid = 0;
    public $bipid = 0;
    public $id = 0;
    public $code = '';
    public $externalcode = '';
    public $source = 0;
    public $partnerid = 0;
    public $pricesell = '';
    public $priceshipping = '';
    public $pricediscount = '';
    public $pricefinal = '';
    public $paymentstatus = 0;
    public $contactemail = '';
    public $contactphone = '';
    public $duration = 0;
    public $planduration = 0;
    public $status = 0;
    public $note = '';
    public $tag = '';
    public $fileidlist = '';
    public $datecreated = 0;
    public $datemodified = 0;
    public $datecheckedin = 0;
    public $datecheckedout = 0;
    public $dateplancheckedin = 0;
    public $dateplancheckedout = 0;
    public $datepaid = 0;

    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData(&$error = array())
    {
        $row = $this->getJsonData();
        $this->id = self::doAdd($row, '', '', $error);

        return $this->id;
    }

    public function updateData(&$error = array())
    {
        $row = $this->getJsonData();
        $url = self::$serviceurl . '/' . $this->id;
        return self::doUpdate($row, $url, '', $error);
    }

    public function delete(&$error = array())
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }

    public static function getBookings(
        $formData,
        $sortby = '',
        $sorttype = '',
        $limitString = '',
        $countOnly = false
    ) {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }
    }


    public function getDataByJson($row)
    {
        $this->cid = $row['company_id'];
        $this->uid = $row['creator_id'];
        $this->ccid = $row['customer_id'];
        $this->soid = $row['order_id'];
        $this->biid = $row['item_id'];
        $this->bipid = $row['item_price_id'];
        $this->id = $row['id'];
        $this->code = $row['code'];
        $this->externalcode = $row['external_code'];
        $this->source = $row['source'];
        $this->partnerid = $row['partner_id'];
        $this->pricesell = $row['price_sell'];
        $this->priceshipping = $row['price_shipping'];
        $this->pricediscount = $row['price_discount'];
        $this->pricefinal = $row['price_final'];
        $this->paymentstatus = $row['payment_status'];
        $this->contactemail = $row['contact_email'];
        $this->contactphone = $row['contact_phone'];
        $this->duration = $row['duration'];
        $this->planduration = $row['plan_duration'];
        $this->status = $row['status'];
        $this->note = $row['note'];
        $this->tag = $row['tag'];
        $this->fileidlist = $row['file_id_list'];
        $this->datecreated = $row['date_created'];
        $this->datemodified = $row['date_modified'];
        $this->datecheckedin = $row['date_checkedin'];
        $this->datecheckedout = $row['date_checkedout'];
        $this->dateplancheckedin = $row['date_plan_checkedin'];
        $this->dateplancheckedout = $row['date_plan_checkedout'];
        $this->datepaid = $row['datepaid'];
    }

    public function getJsonData()
    {
        $row['company_id'] = $this->cid;
        $row['creator_id'] = $this->uid;
        $row['customer_id'] = $this->ccid;
        $row['order_id'] = $this->soid;
        $row['item_id'] = $this->biid;
        $row['item_price_id'] = $this->bipid;
        $row['id'] = $this->id;
        $row['code'] = $this->code;
        $row['external_code'] = $this->externalcode;
        $row['source'] = $this->source;
        $row['partner_id'] = $this->partnerid;
        $row['price_sell'] = $this->pricesell;
        $row['price_shipping'] = $this->priceshipping;
        $row['price_discount'] = $this->pricediscount;
        $row['price_final'] = $this->pricefinal;
        $row['payment_status'] = $this->paymentstatus;
        $row['contact_email'] = $this->contactemail;
        $row['contact_phone'] = $this->contactphone;
        $row['duration'] = $this->duration;
        $row['plan_duration'] = $this->planduration;
        $row['status'] = $this->status;
        $row['note'] = $this->note;
        $row['tag'] = $this->tag;
        $row['file_id_list'] = $this->fileidlist;
        $row['date_created'] = $this->datecreated;
        $row['date_modified'] = $this->datemodified;
        $row['date_checkedin'] = $this->datecheckedin;
        $row['date_checkedout'] = $this->datecheckedout;
        $row['date_plan_checkedin'] = $this->dateplancheckedin;
        $row['date_plan_checkedout'] = $this->dateplancheckedout;
        $row['datepaid'] = $this->datepaid;

        return $row;
    }


}
