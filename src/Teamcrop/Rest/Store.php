<?php

namespace Teamcrop\Rest;

class Store extends Base
{
    public static $serviceurl = '/v1/stores';


    const TYPE_RESTAURANT = 5;
    const TYPE_SHOPPING = 10;

    const STATUS_ENABLE = 1;
    const STATUS_DISABLE = 2;

    const IS_DELETE = 1;

    public $cid = 0;
    public $uid = 0;
    public $coid = 0;
    public $id = 0;
    public $businesstype = 0;
    public $name = '';
    public $code = '';
    public $description = '';
    public $region = 0;
    public $address = '';
    public $lat = 0;
    public $lng = 0;
    public $status = 0;
    public $isdelete = 0;
    public $isdeleteby = 0;
    public $ipaddress = 0;
    public $datecreated = 0;
    public $datemodified = 0;
    public $datedeleted = 0;

    public function __construct($id = 0, $loadFromCache = false)
    {
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                //$this->copy(self::cacheGet($id));
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData()
    {

    }

    public function updateData()
    {

    }


    public function delete()
    {

    }

    public static function getStores($formData, $sortby = '', $sorttype = '', $limitString = '', $countOnly = false)
    {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
        	return self::getItems($formData, $sortby, $sorttype, $limitString);
        }
    }

    public function getJsonData()
    {
        $data = array(
            'id' => (int)$this->id,
            'creator_id' => (int)$this->uid,
            'company_id' => (int)$this->cid,
            'office_id' => (int)$this->coid,
            'business_type' => (int)$this->businesstype,
            'name' => (string)$this->name,
            'code' => (string)$this->code,
            'description' => (string)$this->description,
            'region_id' => (int)$this->region,
            'address' => (string)$this->address,
            'lat' => (float)$this->lat,
            'lng' => (float)$this->lng,
            'is_order_hide_store' => true,
            'is_order_hide_store_node' => true,
            'status' => (int)$this->status,
            'is_delete' => (int)$this->isdelete,
            'is_delete_by' => (int)$this->isdeleteby,
            'ip_address' => (string)$this->ipaddress,
            'date_created' => (int)$this->datecreated,
            'date_modified' => (int)$this->datemodified,
            'date_deleted' => (int)$this->datedeleted
        );

        return $data;
    }

    public function getDataByJson($jsonData)
    {
        $this->id = (int)$jsonData['id'];
        $this->uid = (int)$jsonData['creator_id'];
        $this->cid = (int)$jsonData['company_id'];
        $this->coid = (int)$jsonData['office_id'];
        $this->businesstype = (int)$jsonData['business_type'];
        $this->name = (string)$jsonData['name'];
        $this->code = (string)$jsonData['code'];
        $this->description = (string)$jsonData['description'];
        $this->region = (int)$jsonData['region_id'];
        $this->address = (string)$jsonData['address'];
        $this->lat = (float)$jsonData['lat'];
        $this->lng = (float)$jsonData['lng'];
        $this->status = (int)$jsonData['status'];
        $this->isdelete = (int)$jsonData['is_delete'];
        $this->isdeleteby = (int)$jsonData['is_delete_by'];
        $this->ipaddress = (int)$jsonData['ip_address'];
        $this->datecreated = (int)$jsonData['date_created'];
        $this->datemodified = (int)$jsonData['date_modified'];
        $this->datedeleted = (int)$jsonData['ate_deleted'];
    }
}
