<?php

namespace Teamcrop\Rest;

class NotifySetting extends Base
{
    public static $serviceurl = '/v1/notifysettings';

    public $cid = 0;
    public $uid = 0;
    public $id = 0;
    public $ignoretype = array();
    public $ignorechannel = array();
    public $datecreated = 0;
    public $datemodified = 0;

    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData(&$error = array())
    {
        $data = array(
            'creator_id' => $this->uid,
            'company_id' => $this->cid,
            'ignore_type' => $this->ignoretype,
            'ignore_channel' => $this->ignorechannel
        );

        $this->id = self::doAdd($data, '', '', $error);

        return $this->id;
    }

    public function updateData(&$error = array())
    {
        $data = array(
            'creator_id' => $this->uid,
            'company_id' => $this->cid,
            'ignore_type' => $this->ignoretype,
            'ignore_channel' => $this->ignorechannel
        );

        $url = self::$serviceurl . '/' . $this->id;
        return self::doUpdate($data, $url, '', $error);
    }


    public function delete(&$error = array())
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }

    public static function getNotifieSettings($formData, $sortby = '', $sorttype = '', $limitString = '', $countOnly = false)
    {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }
    }

    public static function getUserSetting($companyId, $userId)
    {
        $formData['company_id'] = $companyId;
        $formData['creator_id'] = $userId;

        $url = self::$serviceurl . '/getusersetting' . '?' . http_build_query($formData);

        return self::doRequest('GET', $serviceurl);
    }

    /**
     * @return array
     */
    public function getJsonData()
    {
        $data = array(
            'creator_id' => (int)$this->uid,
            'company_id' => (int)$this->cid,
            'id' => (int)$this->id,
            'ignore_type' => $this->ignoretype,
            'ignore_channel' => $this->ignorechannel,
            'date_created' => (int)$this->datecreated,
            'date_modified' => (int)$this->datemodified
        );

        return $data;
    }

    public static function cacheKey($settingid)
    {
        return 'tc_notifysetting_' . $settingid;
    }

    public function getDataByArrayCache($row)
    {

    }

    public function getDataByJson($jsonData)
    {
        $this->cid = (int)$jsonData['company_id'];
        $this->uid = (int)$jsonData['creator_id'];
        $this->id = (int)$jsonData['id'];
        $this->ignoretype = $jsonData['ignore_type'];
        $this->ignorechannel = $jsonData['ignore_channel'];
        $this->datecreated = (int)$jsonData['date_created'];
        $this->datemodified = (int)$jsonData['date_modified'];
    }
}
