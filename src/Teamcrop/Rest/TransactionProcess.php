<?php

namespace Teamcrop\Rest;

class TransactionProcess extends Base
{
    public static $serviceurl = '/v1/transactionprocesses';

    const STATUS_PENDING = 0;
    const STATUS_RUNNING = 1;
    const STATUS_SUCCESS = 3;
    const STATUS_FAIL = 5;

    public $cid = 0;
    public $uid = 0;
    public $tid = 0;
    public $id = 0;
    public $type = '';
    public $duration = 0;
    public $objectid = 0;
    public $commitdetailjson = array();
    public $rollbackdetailjson = array();
    public $status = 0;
    public $ipaddress = '';
    public $datecreated = 0;
    public $datemodified = 0;
    public $datecommitted = 0;
    public $daterollbacked = 0;

    public function __construct($id = 0, $loadFromCache = false)
    {
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData(&$error = array())
    {
        $data = array(
            'company_id' => $this->cid,
            'creator_id' => $this->uid,
            'transaction_id' => $this->tid,
            'type' => $this->type,
            'duration' => $this->duration,
            'object_id' => $this->objectid,
            'commit_detail' => $this->commitdetailjson,
            'rollback_detail' => $this->rollbackdetailjson,
            'status' => $this->status,
            'ip_address' => $this->ipaddress,
        );

        $this->id = self::doAdd($data, '', '', $error);

        return $this->id;
    }

    public function updateData(&$error = array())
    {
        $data = array(
            'company_id' => $this->cid,
            'creator_id' => $this->uid,
            'duration' => $this->duration,
            'type' => $this->type,
            'object_id' => $this->objectid,
            'commit_detail' => $this->commitdetailjson,
            'rollback_detail' => $this->rollbackdetailjson,
            'status' => $this->status,
            'date_committed' => $this->datecommitted,
            'date_rollbacked' => $this->daterollbacked,
        );

        $url = self::$serviceurl . '/' . $this->id;
        return self::doUpdate($data, $url, '', $error);
    }


    public function delete(&$error = array())
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }

    public static function getTransactionProcesses($formData, $sortby = '', $sorttype = '', $limitString = '', $countOnly = false)
    {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }
    }

    /**
     * @return array
     */
    public function getJsonData()
    {
        $data = array(
            'creator_id' => (int)$this->uid,
            'company_id' => (int)$this->cid,
            'transaction_id' => (int)$this->tid,
            'id' => (int)$this->id,
            'type' => (string)$this->type,
            "duration" => (int)$this->duration,
            "object_id" => (int)$this->objectid,
            "commit_detail" => $this->commitdetail,
            "rollback_detail" => $this->rollbackdetail,
            "status" => (int)$this->status,
            "ip_address" => (string)$this->ipaddress,
            "date_created" => (int)$this->datecreated,
            "date_modified" => (int)$this->datemodified,
            "date_committed" => (int)$this->datecommitted,
            "date_rollbacked" => (int)$this->daterollbacked,
        );

        return $data;
    }


    public function getDataByJson($jsonData)
    {
        $this->cid = (int)$jsonData['company_id'];
        $this->uid = (int)$jsonData['creator_id'];
        $this->tid = (int)$jsonData['transaction_id'];
        $this->id = (int)$jsonData['id'];
        $this->type = (string)$jsonData['type'];
        $this->duration = (int)$jsonData['duration'];
        $this->objectid = (int)$jsonData['object_id'];
        $this->commitdetailjson = $jsonData['commit_detail'];
        $this->rollbackdetailjson = $jsonData['rollback_detail'];
        $this->status = (int)$jsonData['status'];
        $this->ipaddress = (int)$jsonData['ip_address'];
        $this->datecreated = (int)$jsonData['date_created'];
        $this->datemodified = (int)$jsonData['date_modified'];
        $this->datecommitted = (int)$jsonData['date_committed'];
        $this->daterollbacked = (int)$jsonData['date_rollbacked'];
    }


}
