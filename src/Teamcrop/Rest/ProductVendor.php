<?php

namespace Teamcrop\Rest;

class ProductVendor extends Base
{
    public static $serviceurl = '/v1/productvendors';
    const TYPE_VENDOR = 5;
    const TYPE_SUBVENDOR = 10;
    const TYPE_OTHER = 15;

    const STATUS_ENABLE = 1;
    const STATUS_DISABLE = 2;

    const IS_DELETE = 1;

    public $cid = 0;
    public $uid = 0;
    public $id = 0;
    public $code = '';
    public $name = '';
    public $slug = '';
    public $hash = '';
    public $image = '';
    public $resourceserver = 0;
    public $description = '';
    public $insurance = '';
    public $seotitle = '';
    public $seodescription = '';
    public $seokeyword = '';
    public $countproduct = 0;
    public $type = 0;
    public $isdeleted = 0;
    public $isdeletedby = 0;
    public $displayorder = 0;
    public $status = 0;
    public $datecreated = 0;
    public $datemodified = 0;
    public $datedeleted = 0;

    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData(&$error = array())
    {
        $data = array(
            "company_id" => $this->cid,
            "creator_id" => $this->uid,
            'name' => $this->name,
            'code' => $this->code,
            'description' => $this->description,
            'type' => $this->type
        );

        $this->id = self::doAdd($data, '', '', $error);

        return $this->id;
    }

    public function updateData(&$error = array())
    {
        $data = array(
            "company_id" => $this->cid,
            "creator_id" => $this->uid,
            'name' => $this->name,
            'code' => $this->code,
            'description' => $this->description,
            'type' => $this->type,
            'status' => $this->status
        );

        $url = self::$serviceurl . '/' . $this->id;
        return self::doUpdate($data, $url, '', $error);
    }


    public function delete(&$error = array())
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }

    public static function getProducts($formData, $sortby = '', $sorttype = '', $limitString = '', $countOnly = false)
    {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }

    }

    public function getDataByJson($jsonData)
    {
        $this->id = (int)$jsonData['id'];
        $this->cid = (int)$jsonData['company_id'];
        $this->uid = (int)$jsonData['createtor_id'];
        $this->code = (string)$jsonData['code'];
        $this->name = (string)$jsonData['name'];
        $this->description = (string)$jsonData['description'];
        $this->type = (int)$jsonData['type'];
        $this->displayorder = (int)$jsonData['display_order'];
        $this->status = (int)$jsonData['status'];
        $this->datecreated= (int)$jsonData['date_created'];
        $this->datemodified = (int)$jsonData['date_modified'];
        $this->datedeleted = (int)$jsonData['date_deleted'];
    }
}
