<?php

namespace Teamcrop\Rest;

class Warehouse extends Base
{
    public static $serviceurl = '/v1/warehouses';

    const STATUS_ENABLE = 1;
    const STATUS_DISABLE = 2;

    const IS_DELETE = 1;

    public $cid = 0;
    public $uid = 0;
    public $id = 0;
    public $code = '';
    public $name = '';
    public $description = '';
    public $address = '';
    public $lat = 0;
    public $long = 0;
    public $countroom = 0;
    public $countblock = 0;
    public $countbox = 0;
    public $area = 0;
    public $displayorder = 0;
    public $status = 0;
    public $isdeleted = 0;
    public $isdeletedby = 0;
    public $datecreated = 0;
    public $datemodified = 0;
    public $datedeleted = 0;
    public $datelastinput = 0;
    public $datelastoutput = 0;

    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData(&$error = array())
    {
        $data = array(
            'name' => $this->name,
            'code' => $this->code,
            'company_id' => $this->cid,
            'creator_id' => $this->uid,
            'address' => $this->address,
            'display_order' => $this->displayorder,
            'status' => $this->status
        );

        $this->id = self::doAdd($data, '', '', $error);

        return $this->id;
    }

    public function updateData(&$error = array())
    {
        $data = array(
            'name' => $this->name,
            'code' => $this->code,
            'company_id' => $this->cid,
            'creator_id' => $this->uid,
            'address' => $this->address,
            'display_order' => $this->displayorder,
            'status' => $this->status
        );

        $url = self::$serviceurl . '/' . $this->id;
        return self::doUpdate($data, $url, '', $error);
    }

    public function delete(&$error = array())
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }

    public static function getWarehouses($formData, $sortby = '', $sorttype = '', $limitString = '', $countOnly = false)
    {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }
    }

    public function getJsonData($getfullinfo = false)
    {
        $data = array(
            'id' => (int)$this->id,
            'code' => (string)$this->code,
            'name' => (string)$this->name,
            'description' => (string)$this->description,
            'address' => (string)$this->address,
            'lat' => (float)$this->lat,
            'long' => (float)$this->long,
            'status' => (int)$this->status
        );

        if ($getfullinfo) {
            $data = array_merge(
                $data,
                array(
                    'creator_id' => (int)$this->uid,
                    'company_id' => (int)$this->cid,
                    'display_order' => (int)$this->displayorder,
                    'is_deleted' => (int)$this->isdeleted,
                    'date_created' => (int)$this->datecreated,
                    'date_modified' => (int)$this->datemodified
                )
            );
        }

        return $data;
    }

    public static function cacheKey($id)
    {
        return 'tc_warehouse_' . $id;
    }

    public function getDataByArrayCache($row)
    {

    }

    public function getDataByJson($jsonData)
    {
        $this->cid = (int)$jsonData['company_id'];
        $this->uid = (int)$jsonData['creator_id'];
        $this->id = (int)$jsonData['id'];
        $this->code = (string)$jsonData['code'];
        $this->name = (string)$jsonData['name'];
        $this->description = (string)$jsonData['description'];
        $this->address = (string)$jsonData['address'];
        $this->lat = (float)$jsonData['lat'];
        $this->long = (float)$jsonData['long'];
        $this->countroom = (int)$jsonData['count_room'];
        $this->countblock = (int)$jsonData['count_block'];
        $this->countbox = (int)$jsonData['count_box'];
        $this->area = (float)$jsonData['area'];
        $this->displayorder = (int)$jsonData['display_order'];
        $this->status = (int)$jsonData['status'];
        $this->isdeleted = (int)$jsonData['is_deleted'];
        $this->isdeletedby = (int)$jsonData['deleted_by'];
        $this->datecreated = (int)$jsonData['date_created'];
        $this->datemodified = (int)$jsonData['date_modified'];
        $this->datedeleted = (int)$jsonData['date_deleted'];
        $this->datelastinput = (int)$jsonData['date_last_input'];
        $this->datelastoutput = (int)$jsonData['date_last_output'];
    }
}
