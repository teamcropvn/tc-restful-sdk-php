<?php

namespace Teamcrop\Rest;

class Warehouse extends Base
{
    public static $serviceurl = '/v1/warehouses';

    const STATUS_ENABLE = 1;
    const STATUS_DISABLE = 2;

    const IS_DELETE = 1;

    public $cid = 0;
    public $uid = 0;
    public $id = 0;
    public $code = '';
    public $name = '';
    public $description = '';
    public $address = '';
    public $lat = 0;
    public $long = 0;
    public $countroom = 0;
    public $countblock = 0;
    public $countbox = 0;
    public $area = 0;
    public $displayorder = 0;
    public $status = 0;
    public $isdeleted = 0;
    public $isdeletedby = 0;
    public $datecreated = 0;
    public $datemodified = 0;
    public $datedeleted = 0;
    public $datelastinput = 0;
    public $datelastoutput = 0;

    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData()
    {

    }

    public function updateData()
    {

    }


    public function delete()
    {

    }

    public static function getWarehouses($formData, $sortby = '', $sorttype = '', $limitString = '', $countOnly = false)
    {
        if($countOnly) {
            return self::countItems($formData, $limitString);
        }
		else {
        	return self::getItems($formData, $sortby, $sorttype, $limitString);
		}
    }
}
