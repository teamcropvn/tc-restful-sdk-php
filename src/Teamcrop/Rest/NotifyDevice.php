<?php

namespace Teamcrop\Rest;

class NotifyDevice extends Base
{
    public static $serviceurl = '/v1/notifydevices';

    const PLATFORM_ANDROID = 1;
    const PLATFORM_IOS = 3;
    const PLATFORM_WINDOWSPHONE = 5;
    const PLATFORM_BLACKBERRY = 7;
    const PLATFORM_OTHER = 9;
    const STATUS_ENABLE = 1;
    const STATUS_DISABLED = 3;

    public $cid = 0;
    public $uid = 0;
    public $id = 0;
    public $app = 'teamcrop';
    public $deviceid = '';
    public $pushtrackerid = '';
    public $platform = 0;
    public $screenwidth = 0;
    public $screenheight = 0;
    public $name = '';
    public $brand = '';
    public $os = '';
    public $status = 0;
    public $ipaddress = '';
    public $datecreated = 0;
    public $datemodified = 0;
    public $datelastpushed = 0;
    public $datelastaccessed = 0;

    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData(&$error = array())
    {
        $data = array(
            'company_id' => $this->cid,
            'creator_id' => $this->uid,
            'app' => $this->app,
            'device_id' => $this->deviceid,
            'push_tracker_id' => $this->pushtrackerid,
            'platform' => $this->platform,
            'screen_width' => $this->screenwidth,
            'screen_height' => $this->screenheight,
            'name' => $this->name,
            'brand' => $this->brand,
            'os' => $this->os,
        );

        $this->id = self::doAdd($data, '', '', $error);

        return $this->id;
    }

    public function updateData(&$error = array())
    {
        $data = array(
            'company_id' => $this->cid,
            'creator_id' => $this->uid,
            'app' => $this->app,
            'device_id' => $this->deviceid,
            'push_tracker_id' => $this->pushtrackerid,
            'platform' => $this->platform,
            'screen_width' => $this->screenwidth,
            'screen_height' => $this->screenheight,
            'name' => $this->name,
            'brand' => $this->brand,
            'os' => $this->os,
        );

        $url = self::$serviceurl . '/' . $this->id;
        return self::doUpdate($data, $url, '', $error);
    }


    public function delete(&$error = array())
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }

    public static function getNotifyDevices($formData, $sortby = '', $sorttype = '', $limitString = '', $countOnly = false)
    {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }
    }

    /**
     * @return array
     */
    public function getJsonData()
    {
        $data = array(
            'creator_id' => (int)$this->uid,
            'company_id' => (int)$this->cid,
            'id' => (int)$this->id,
            'app' => (string)$this->app,
            'device_id' => (string)$this->deviceid,
            'push_tracker_id' => (string)$this->pushtrackerid,
            'platform' => (int)$this->platform,
            'screen_width' => (int)$this->screenwidth,
            'screen_height' => (int)$this->screenheight,
            'name' => (string)$this->name,
            'brand' => (string)$this->brand,
            'os' => (string)$this->os,
            'status' => (int)$this->status,
            'ip_address' => (string)$this->ipaddress,
            'date_created' => (int)$this->datecreated,
            'date_modified' => (int)$this->datemodified,
            'date_last_pushed' => (int)$this->datelastpushed,
            'date_last_accessed' => (int)$this->datelastaccessed,
        );

        return $data;
    }

    // public static function getLastActiveDevice($deviceId, $app)
    // {
    //     $myNotifyDevice = new self();

    //     //deviceid is required and must not be empty
    //     if (strlen($deviceId) > 0) {
    //         $lastRecords = self::getNotifyDevices(
    //             array(
    //                 'device_id' => $deviceId,
    //                 'status' => self::STATUS_ENABLED,
    //                 'app' => $app
    //             ),
    //             'id',
    //             'DESC',
    //             1
    //         );

    //         if (!empty($lastRecords)) {
    //             $myNotifyDevice = $lastRecords[0];
    //         }
    //     }

    //     return $myNotifyDevice;
    // }

    public static function cacheKey($deviceId)
    {
        return 'tc_notify_device_' . $deviceId;
    }

    public function getDataByArrayCache($row)
    {

    }

    public function getDataByJson($jsonData)
    {
        $this->cid = (int)$jsonData['company_id'];
        $this->uid = (int)$jsonData['creator_id'];
        $this->id = (int)$jsonData['id'];
        $this->app = (string)$jsonData['app'];
        $this->deviceid = (string)$jsonData['device_id'];
        $this->pushtrackerid = (string)$jsonData['push_tracker_id'];
        $this->platform = (int)$jsonData['platform'];
        $this->screenwidth = (int)$jsonData['screen_width'];
        $this->screenheight = (int)$jsonData['screen_height'];
        $this->name = (string)$jsonData['name'];
        $this->brand = (string)$jsonData['brand'];
        $this->os = (string)$jsonData['os'];
        $this->status = (int)$jsonData['status'];
        $this->ipaddress = (string)$jsonData['ip_address'];
        $this->datecreated = (int)$jsonData['date_created'];
        $this->datemodified = (int)$jsonData['date_modified'];
        $this->datelastpushed = (int)$jsonData['date_last_pushed'];
        $this->datelastaccessed = (int)$jsonData['date_last_accessed'];
    }
}
