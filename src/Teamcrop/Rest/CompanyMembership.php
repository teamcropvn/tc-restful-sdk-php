<?php

namespace Teamcrop\Rest;

class CompanyMembership extends Base
{
    public static $serviceurl = '/v1/companymemberships';

    const PACKAGE_DEMO = 0;
    const PACKAGE_SILVER = 1;
    const PACKAGE_GOLD = 2;
    const PACKAGE_DIAMOND = 3;

    const STATUS_ENABLE = 1;
    const STATUS_DISABLE = 2;

    public $uid = 0;
    public $cid = 0;
    public $id = 0;
    public $package = 0;
    public $numlocation = 0;
    public $numinvitecode = '';
    public $sourceinvitecode = '';
    public $setting = array();
    public $printorder = '';
    public $printcashflowin = '';
    public $printcashflowout = '';
    public $printproductreceiptin = '';
    public $printproductreceiptout = '';
    public $status = 0;
    public $ipaddress = 0;
    public $datecreated = 0;
    public $datemodified = 0;
    public $datereportstarted = 0;
    public $dateexpired = 0;

    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData(&$error = array())
    {
        $data = array(
            'creator_id' => $this->uid,
            'company_id' => $this->cid,


        );

        $this->id = self::doAdd($data, '', '', $error);

        return $this->id;
    }

    public function updateData(&$error = array())
    {
        $data = array(

        );

        $url = self::$serviceurl . '/' . $this->id;
        return self::doUpdate($data, $url, '', $error);
    }

    public function delete(&$error = array())
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }

    public static function getMemberships(
        $formData,
        $sortby = '',
        $sorttype = '',
        $limitString = '',
        $countOnly = false
    )
    {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }
    }

    public static function isEmployed($cid, $uid)
    {
        return self::countItems(array('company_id' => $cid, 'user_id' => $uid, 'status' => 1)) > 0;
    }

    public function getDataByJson($row)
    {
        $this->uid = (int)$row['user_id'];
        $this->cid = (int)$row['company_id'];
        $this->id = (int)$row['id'];
        $this->package = (int)$row['package'];
        $this->numlocation = (int)$row['num_location'];
        $this->numinvitecode = (string)$row['num_invite_code'];
        $this->sourceinvitecode = (string)$row['num_invite_code'];
        $this->setting = $row['setting'];
        $this->printorder = (string)$row['print_order'];
        $this->printcashflowin = (string)$row['print_cashflow_in'];
        $this->printcashflowout = (string)$row['print_cashflow_out'];
        $this->printproductreceiptin = (string)$row['print_product_receipt_in'];
        $this->printproductreceiptout = (string)$row['print_product_receipt_out'];
        $this->status = (int)$row['status'];
        $this->ipaddress = (string)long2ip($row['ip_address']);
        $this->datecreated = (int)$row['date_created'];
        $this->datemodified = (int)$row['date_modified'];
        $this->datereportstarted = (int)$row['date_report_started'];
        $this->dateexpired = (int)$row['date_expired'];
    }

    public function getJsonData() {
        return array(
            'user_id' => (int)$this->uid,
            'company_id' => (int)$this->cid,
            'id' => (int)$this->id,
            'package' => (int)$this->package,
            'num_location' => (int)$this->numlocation,
            'num_invite_code' => (string)$this->numinvitecode,
            'source_invite_code' => (string)$this->sourceinvitecode,
            'setting' => json_decode($this->setting),
            'print_order' => (string)$this->printorder,
            'print_cashflow_in' => (string)$this->printcashflowin,
            'print_cashflow_out' => (string)$this->printcashflowout,
            'print_product_receipt_in' => (string)$this->printproductreceiptin,
            'print_product_receipt_out' => (string)$this->printproductreceiptout,
            'status' => (int)$this->status,
            'ip_address' => (int)$this->ipaddress,
            'date_created' => (int)$this->datecreated,
            'date_modified' => (int)$this->datemodified,
            'date_report_started' => (int)$this->datereportstarted,
            'date_expired' => (int)$this->dateexpired
        );
    }
}
