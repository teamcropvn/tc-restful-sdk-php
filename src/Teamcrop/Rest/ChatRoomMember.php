<?php

namespace Teamcrop\Rest;

use Litpi\Cacher;

class ChatRoomMember extends Base
{
    public static $serviceurl = '/v1/chatroommembers';

    const SOURCE_INTERNAL = 0;
    const SOURCE_FACEBOOK_PAGE = 1;
    const SOURCE_FACEBOOK_COMMENT = 2;
    const SOURCE_TEAMCROP_LIVECHAT = 3;

    const FROM_WEBSITE = 1;
    const FROM_APP = 3;

    const STATUS_ENABLE = 1;
    const STATUS_DISABLED = 3;

    public $uid = 0;
    public $cid = 0;
    public $crid = 0;
    public $id = 0;
    public $source = 0;
    public $sourceid = '';
    public $identifier = '';
    public $fullname = '';
    public $email = '';
    public $phone = '';
    public $address = '';
    public $creatorid = 0;
    public $customerid = 0;
    public $isteacher = 0;
    public $duration = 0;
    public $durationinminute = 0;
    public $money = 0;
    public $currency = 0;
    public $counttext = 0;
    public $countvoice = 0;
    public $countvideo = 0;
    public $countscreensharing = 0;
    public $lastviewid = 0;
    public $latestchatdataid = 0;
    public $countunread = 0;
    public $useragent = '';
    public $from = 0;
    public $device = '';
    public $os = '';
    public $screenwidth = 0;
    public $screenheight = 0;
    public $status = 0;
    public $ipaddress = 0;
    public $datecreated = 0;
    public $datemodified = 0;
    public $datejoined = 0;
    public $dateexited = 0;

    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData(&$error = array())
    {
        return $this->id;
    }

    public function updateData(&$error = array())
    {
        return false;
    }


    public function delete(&$error = array())
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }

    public static function getChatRoomMembers($formData, $sortby = '', $sorttype = '', $limitString = '', $countOnly = false)
    {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }
    }

    /**
     * @return array
     */
    public function getJsonData()
    {
        $data = array(
            'user_id' => $this->uid,
            'company_id' => $this->cid,
            'chatroom_id' => $this->crid,
            'id' => $this->id,
            'source' => $this->source,
            'source_id' => $this->sourceid,
            'identifier' => $this->identifier,
            'fullname' => $this->fullname,
            'email' => $this->email,
            'phone' => $this->phone,
            'address' => $this->address,
            'creator_id' => $this->creatorid,
            'customer_id' => $this->customerid,
            'last_view_id' => $this->lastviewid,
            'latest_chat_data_id' => $this->latestchatdataid,
            'count_unread' => $this->countunread,
            'user_agent' => $this->useragent,
            'from' => $this->from,
            'device' => $this->device,
            'os' => $this->os,
            'screen_width' => $this->screenwidth,
            'screen_height' => $this->screenheight,
            'status' => $this->status,
            'ip_address' => $this->ipaddress,
            'date_created' => $this->datecreated,
            'date_modified' => $this->datemodified,
        );

        return $data;
    }

    public static function cacheKey($chatroomid)
    {
        return 'tc_chat_room_member_' . $chatroomid;
    }

    public function getDataByArrayCache($row)
    {

    }

    public function getDataByJson($jsonData)
    {
        $this->uid = (int)$jsonData['user_id'];
        $this->cid = (int)$jsonData['company_id'];
        $this->crid = (int)$jsonData['chatroom_id'];
        $this->id = (int)$jsonData['id'];
        $this->source = (int)$jsonData['source'];
        $this->sourceid = (string)$jsonData['source_id'];
        $this->identifier = (string)$jsonData['identifier'];
        $this->fullname = (string)$jsonData['fullname'];
        $this->email = (string)$jsonData['email'];
        $this->phone = (string)$jsonData['phone'];
        $this->address = (string)$jsonData['address'];
        $this->creatorid = (int)$jsonData['creator_id'];
        $this->customerid = (int)$jsonData['customer_id'];
        $this->lastviewid = (int)$jsonData['last_view_id'];
        $this->latestchatdataid = (int)$jsonData['latest_chat_data_id'];
        $this->countunread = (int)$jsonData['count_unread'];
        $this->useragent = (string)$jsonData['user_agent'];
        $this->from = (int)$jsonData['from'];
        $this->device = (string)$jsonData['device'];
        $this->os = (string)$jsonData['os'];
        $this->screenwidth = (int)$jsonData['screen_width'];
        $this->screenheight = (int)$jsonData['screen_height'];
        $this->status = (int)$jsonData['status'];
        $this->ipaddress = (string) $jsonData['ip_address'];
        $this->datecreated = (int)$jsonData['date_created'];
        $this->datemodified = (int)$jsonData['date_modified'];
    }

    public static function findUserBySourceId($companyId, $sourceId, $conversationId)
    {
        $info = array(
            'id' => 0,
            'name' => '',
            'debug' => ''
        );

        //search in cache first

        $myCacher = new Cacher(self::getCacheKeyBySourceId($companyId, $sourceId, $conversationId));
        $row = $myCacher->get();
        if (empty($row)) {
            //MISS
            //doRequest to get info
            $members = self::getChatRoomMembers(
                array(
                    'company_id' => $companyId,
                    'source_id' => $sourceId,
                    'conversation_id' => $conversationId
                ),
                'id',
                'ASC',
                1
            );
            if (!empty($members)) {
                $info['id'] = $members[0]->uid;
                $info['name'] = $members[0]->fullname;

                //store
                $myCacher->set(json_encode($info));
            }

            $info['debug'] = 'MISS';
        } else {
            //HIT
            $info = json_decode($row, true);
            $info['debug'] = 'HIT';
        }

        return $info;
    }

    public static function getCacheKeyBySourceId($companyId, $sourceId, $conversationId)
    {
        return 'tc_chatroommember_' . $companyId . '_' . $sourceId . '_' . $conversationId;
    }

}
