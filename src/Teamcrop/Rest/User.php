<?php

namespace Teamcrop\Rest;

class User extends Base
{
    public static $serviceurl = '/v1/users';

    const GROUPID_GUEST = 0;
    const GROUPID_ADMIN = 1;
    const GROUPID_MODERATOR = 2;
    const GROUPID_DEVELOPER = 3;
    const GROUPID_EMPLOYEE = 5;
    const GROUPID_PARTNER = 10;
    const GROUPID_DEPARTMENT = 15;
    const GROUPID_GROUP = 16;
    const GROUPID_MEMBER = 20;
    const GROUPID_MEMBERBANNED = 25;

    const OAUTH_PARTNER_EMPTY = 0;
    const OAUTH_PARTNER_FACEBOOK = 1;
    const OAUTH_PARTNER_YAHOO = 2;
    const OAUTH_PARTNER_GOOGLE = 3;

    const GENDER_UNKNOWN = 0;
    const GENDER_MALE = 1;
    const GENDER_FEMALE = 2;

    public $id = 0;
    public $screenname = '';
    public $fullname = '';
    public $avatar = '';
    public $avatarCurrent = '';
    public $groupid = 0;
    public $gender = 'unknown';
    public $region = 0;

    public $view = 0;
    public $datelastaction = 0;
    public $email = '';
    public $password = '';
    public $birthday = '0000-00-00';
    public $phone = '';
    public $address = '';
    public $city = '';
    public $country = 'VN';
    public $website = '';
    public $bio = '';
    public $activatedcode = '';
    public $datecreated = 0;
    public $datemodified = 0;
    public $datelastlogin = 0;
    public $parentid = 0;
    public $fromcompany = 0;
    public $skype = '';

    public $oauthPartner = 0;
    public $oauthUid = 0;

    public $ipaddress = '';    //dia chi IP register user
    public $newnotification = 0;
    public $newmessage = 0;

    public $newpass = '';
    public $sessionid = '';
    public $userpath = '';

    // dynamic
    public $internalid = "";
    public $department = "";

    public function addData(&$error = array())
    {
        $data = array(
            'email' => $this->email,
            'password' => $this->password,
            'fullname' => $this->fullname,
            'phone' => $this->phone,
            'address' => $this->address,
            'website' => $this->website,
            'bio' => $this->bio,
            'group_id' => $this->groupid,
            'from_company' => $this->fromcompany,
            'region_id' => $this->region
        );

        $this->id = self::doAdd($data, '', '', $error);

        return $this->id;
    }

    public function updateData(&$error = array())
    {
        $data = array(
            'email' => $this->email,
            'password' => $this->password,
            'fullname' => $this->fullname,
            'phone' => $this->phone,
            'address' => $this->address,
            'website' => $this->website,
            'bio' => $this->bio,
            'group_id' => $this->groupid,
            'region_id' => $this->region
        );

        $url = self::$serviceurl . '/' . $this->id;
        return self::doUpdate($data, $url, '', $error);
    }

    public function delete(&$error = array())
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }

    public static function getUsers($formData, $sortby, $sorttype, $limitString = '', $countOnly = false)
    {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }
    }

    public static function getListUserFromIds($formData, $sortby, $sorttype, $limitString = '')
    {
        return self::getItems($formData, $sortby, $sorttype, $limitString, self::$serviceurl . '/ids');
    }

    /**
     * Used to login to the system from platform access (bizweb, haravan, shopify...)
     *
     * @param $email
     * @param $screenname
     * @param $companyid
     * @param $error
     * @return null
     */
    public static function loginPlatform($email, $screenname, $companyid, &$error)
    {
        $responseData = null;

        $data = array(
            'email' => $email,
            'password' => '',
            'screenname' => $screenname,
            'platformcompany' => $companyid
        );

        $serviceurl = self::$serviceurl . '/login';
        $headers = array(
            'Content-type' => 'application/json'
        );

        try {
            //Do request and get response with submit data
            $response = self::doRequest('POST', $serviceurl, $headers, false, true, json_encode($data));

            //request success
            if ($response['status'] == '200' || $response['status'] == '201') {
                $responseData = $response['data'];
            } else {
                $error = self::parsingErrorFromResponse($response);
            }
        } catch (\Exception $e) {
            $error[] = $e->getMessage();
        }

        return $responseData;
    }

    public static function loginUser($email, $screenname, $password, &$error)
    {
        $responseData = null;

        $data = array(
            'email' => $email,
            'password' => $password,
            'screenname' => $screenname,
        );

        $serviceurl = self::$serviceurl . '/login';
        $headers = array(
            'Content-type' => 'application/json'
        );

        try {
            //Do request and get response with submit data
            $response = self::doRequest('POST', $serviceurl, $headers, false, true, json_encode($data));

            //request success
            if ($response['status'] == '200' || $response['status'] == '201') {
                $responseData = $response['data'];
            } else {
                $error = self::parsingErrorFromResponse($response);
            }
        } catch (\Exception $e) {
            $error[] = $e->getMessage();
        }

        return $responseData;
    }
    /**
     * Used to login to the system from platform access (bizweb, haravan, shopify...)
     *
     * @param $email
     * @param $screenname
     * @param $companyid
     * @param $error
     * @return null
     */
    public static function checkUser($email, $password)
    {
        $valid = false;

        $data = array(
            'email' => $email,
            'password' => $password
        );

        $serviceurl = self::$serviceurl . '/login';
        $headers = array(
            'Content-type' => 'application/json'
        );

        try {
            //Do request and get response with submit data
            $response = self::doRequest('POST', $serviceurl, $headers, false, true, json_encode($data));

            //request success
            if ($response['status'] == '200' || $response['status'] == '201') {
                $valid = true;
            } else {
                $error = self::parsingErrorFromResponse($response);
            }
        } catch (\Exception $e) {
            $error[] = $e->getMessage();
        }

        return $valid;
    }

    public function getJsonData($getfullinfo = false)
    {
        $data = array(
            'id' => (int)$this->id,
            'fullname' => (string)$this->fullname,
            'avatar' => (string)$this->avatar,
        );

        if ($getfullinfo) {
            $data = array_merge($data, array(
                'email' => (string)$this->email,
                'password' => (string)$this->password,
                'gender' => (int)$this->gender,
                'groupid' => (int)$this->groupid,
                'from_company' => (int)$this->fromcompany,
                'birthday' => (string)$this->birthday,
                'phone' => (string)$this->phone,
                'address' => (string)$this->address,
                'city' => (string)$this->city,
                'country' => (string)$this->country,
                'website' => (string)$this->website,
                'bio' => (string)$this->bio,
                'activatedcode' => (string)$this->activatedcode,
                'datecreated' => (int)$this->datecreated,
                'datemodified' => (int)$this->datemodified,
                'datelastlogin' => (int)$this->datelastlogin,
                'oauthpartner' => (int)$this->oauthPartner,
                'oauthuid' => (string)$this->oauthUid,
            ));
        }

        return $data;
    }

    public static function cacheKey($userid)
    {
        return 'tc_u_' . $userid;
    }

    public function getDataByArrayCache($row)
    {
        $this->id = isset($row['u_id']) ? $row['u_id'] : 0;
        $this->fullname = isset($row['u_fullname']) ? $row['u_fullname'] : '';
        $this->avatar = isset($row['u_avatar']) ? $row['u_avatar'] : '';
        $this->groupid = isset($row['u_groupid']) ? $row['u_groupid'] : '';
        $this->region = isset($row['u_region']) ? $row['u_region'] : '';
        $this->gender = isset($row['u_gender']) ? $row['u_gender'] : '';
        $this->fromcompany = isset($row['u_fromcompany']) ? $row['u_fromcompany'] : '';
        $this->datelastaction = isset($row['u_datelastaction']) ? $row['u_datelastaction'] : 0;
    }

    public function getDataByJson($jsonData)
    {
        $this->id = (int)$jsonData['id'];
        $this->fullname = (string)$jsonData['fullname'];
        $this->avatar = (string)$jsonData['avatar'];

        if (isset($jsonData['email']) && trim($jsonData['email']) != '') {
            $this->email = (string)$jsonData['email'];
            $this->password = (string)$jsonData['password'];
            $this->gender = (int)$jsonData['gender'];
            $this->groupid = (int)$jsonData['group_id'];
            $this->fromcompany = (int)$jsonData['from_company'];
            $this->birthday = (string)$jsonData['birthday'];
            $this->phone = (string)$jsonData['phone'];
            $this->address = (string)$jsonData['address'];
            $this->city = (string)$jsonData['city'];
            $this->country = (string)$jsonData['country'];
            $this->website = (string)$jsonData['website'];
            $this->bio = (string)$jsonData['bio'];
            $this->datecreated = (int)$jsonData['date_created'];
            $this->datemodified = (int)$jsonData['date_modified'];
            $this->datelastlogin = (int)$jsonData['date_last_login'];
        }
    }

    public function thumbImage()
    {
        $pos = strrpos($this->avatar, '.');
        $extPart = substr($this->avatar, $pos + 1);
        $namePart = substr($this->avatar, 0, $pos);
        $filesmall = $namePart . '-small.' . $extPart;

        return $filesmall;
    }

    public function mediumImage()
    {
        $pos = strrpos($this->avatar, '.');
        $extPart = substr($this->avatar, $pos + 1);
        $namePart = substr($this->avatar, 0, $pos);
        $filesmall = $namePart . '-medium.' . $extPart;

        return $filesmall;
    }

    public function getSmallImage($isMedium = false)
    {
        if ($this->avatar == '') {
            $avatarPath = '';
        } else {

            $avatarPath = '';

            if ($isMedium) {
                $avatarPath .= $this->mediumImage();
            } else {
                $avatarPath .= $this->thumbImage();
            }
        }

        return $avatarPath;
    }

    public static function getEmail($userId, &$error)
    {
        $email = '';

        $serviceurl = self::$serviceurl . '/'.$userId.'/email';
        $headers = array(
            'Content-type' => 'application/json'
        );

        try {
            //Do request and get response with submit data
            $response = self::doRequest('GET', $serviceurl, $headers, false, true);

            //request success
            if ($response['status'] == '200') {
                $responseData = $response['data'];
                if ($responseData['id'] == $userId) {
                    $email = $responseData['email'];
                }
            } else {
                $error = self::parsingErrorFromResponse($response);
            }
        } catch (\Exception $e) {
            $error[] = $e->getMessage();
        }

        return $email;
    }
}
