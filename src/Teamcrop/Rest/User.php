<?php

namespace Teamcrop\Rest;

class User extends Base
{
    public static $serviceurl = '/v1/users';

    public $id = 0;
    public $fullname = '';
    public $avatar = '';
    public $groupid = 0;
    public $gender = '-1';
    public $region = 0;

    public $datelastaction = 0;
    public $email = '';
    public $password = '';
    public $birthday = '0000-00-00';
    public $phone = '';
    public $address = '';
    public $city = '';
    public $country = 'VN';
    public $website = '';
    public $bio = '';
    public $activatedcode = '';
    public $datecreated = 0;
    public $datemodified = 0;
    public $datelastlogin = 0;
    public $oauthPartner = 0;
    public $oauthUid = 0;

    public function addData()
    {

    }

    public function updateData()
    {

    }

    public function delete()
    {

    }

    private static function countList($where, $bindParams)
    {

    }

    private static function getList($where, $order, $limit, $bindParams)
    {

    }

    public static function getUsers($formData, $sortby, $sorttype, $limitString = '', $countOnly = false)
    {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }
    }

    public function getJsonData($getfullinfo = false)
    {
        $data = array(
            'id' => (int)$this->id,
            'fullname' => (string)$this->fullname,
            'avatar' => (string)$this->avatar,
        );

        if ($getfullinfo) {
            $data = array_merge($data, array(
                'email' => (string)$this->email,
                'gender' => (int)$this->gender,
                'groupid' => (int)$this->groupid,
                'birthday' => (string)$this->birthday,
                'phone' => (string)$this->phone,
                'address' => (string)$this->address,
                'city' => (string)$this->city,
                'country' => (string)$this->country,
                'website' => (string)$this->website,
                'bio' => (string)$this->bio,
                'activatedcode' => (string)$this->activatedcode,
                'datecreated' => (int)$this->datecreated,
                'datemodified' => (int)$this->datemodified,
                'datelastlogin' => (int)$this->datelastlogin,
                'oauthpartner' => (int)$this->oauthPartner,
                'oauthuid' => (string)$this->oauthUid,
            ));
        }

        return $data;
    }

    public static function cacheKey($userid)
    {
        return 'tc_u_' . $userid;
    }

    public function getDataByArrayCache($row)
    {
        $this->id = isset($row['u_id']) ? $row['u_id'] : 0;
        $this->fullname = isset($row['u_fullname']) ? $row['u_fullname'] : '';
        $this->avatar = isset($row['u_avatar']) ? $row['u_avatar'] : '';
        $this->groupid = isset($row['u_groupid']) ? $row['u_groupid'] : '';
        $this->region = isset($row['u_region']) ? $row['u_region'] : '';
        $this->gender = isset($row['u_gender']) ? $row['u_gender'] : '';
        $this->datelastaction = isset($row['u_datelastaction']) ? $row['u_datelastaction'] : 0;
    }

    public function getDataByJson($jsonData)
    {
        $this->id = (int)$jsonData['id'];
        $this->fullname = (string)$jsonData['fullname'];
        $this->avatar = (string)$jsonData['avatar'];

        if (isset($jsonData['email']) && trim($jsonData['email']) != '') {
            $this->email = (string)$jsonData['email'];
            $this->gender = (int)$jsonData['gender'];
            $this->groupid = (int)$jsonData['group_id'];
            $this->birthday = (string)$jsonData['birthday'];
            $this->phone = (string)$jsonData['phone'];
            $this->address = (string)$jsonData['address'];
            $this->city = (string)$jsonData['city'];
            $this->country = (string)$jsonData['country'];
            $this->website = (string)$jsonData['website'];
            $this->bio = (string)$jsonData['bio'];
            $this->datecreated = (int)$jsonData['date_created'];
            $this->datemodified = (int)$jsonData['date_modified'];
            $this->datelastlogin = (int)$jsonData['date_last_login'];
        }
    }
}
