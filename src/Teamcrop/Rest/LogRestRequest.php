<?php

namespace Teamcrop\Rest;

class LogRestRequest extends Base
{
    public static $serviceurl = '/v1/logrestrequests';

    const SOURCE_DIRECT = 1;
    const SOURCE_AJAX = 3;
    const SOURCE_MOBILE = 5;
    const SOURCE_INTERNAL = 7;

    const SEVERITY_LOW = 1;
    const SEVERITY_MEDIUM = 3;
    const SEVERITY_HIGH = 5;
    const SEVERITY_CRITICAL = 7;

    public $cid = 0;
    public $uid = 0;
    public $id = 0;
    public $service = '';
    public $method = '';
    public $controller = '';
    public $action = '';
    public $uri = '';
    public $source = 0;
    public $parentid = 0;
    public $statuscode = 0;
    public $input = '';
    public $output = '';
    public $exectime = 0;
    public $memory = 0;
    public $chaindepth = 0;
    public $chainidentifier = '';
    public $identifier = '';
    public $ipaddress = '';
    public $ipaddresshost = '';
    public $adminnote = '';
    public $severity = 0;
    public $datecreated = 0;
    public $datemodified = 0;

    public function __construct($id = 0, $loadFromCache = false)
    {
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }


    public function addData(&$error = array())
    {
        $data = array(
            'company_id' => $this->cid,
            'creator_id' => $this->uid,
            'service' => $this->service,
            'controller' => $this->controller,
            'action' => $this->action,
            'uri' => $this->uri,
            'source' => $this->source,
            'status_code' => $this->statuscode,
            'input' => $this->input,
            'output' => $this->output,
            'exec_time' => $this->exectime,
            'memory' => $this->memory,
            'chain_identifier' => $this->chaindepth,
            'ip_address' => $this->ipaddress,
            'ip_address_host' => $this->ipaddresshost,
            'admin_note' => $this->adminnote,
            'severity' => $this->severity,
            'date_created' => $this->datecreated,
        );

        $this->id = self::doAdd($data, '', '', $error);

        return $this->id;
    }

    public function updateData(&$error = array())
    {
        $data = array(
            'creator_id' => $this->uid,
            'company_id' => $this->cid,
            'admin_note' => $this->adminnote,
            'severity' => $this->severity,
        );

        $url = self::$serviceurl . '/' . $this->id;
        return self::doUpdate($data, $url, '', $error);
    }


    public function delete(&$error = array())
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }

    public static function getLogRestRequests($formData, $sortby = '', $sorttype = '', $limitString = '', $countOnly = false)
    {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
        	return self::getItems($formData, $sortby, $sorttype, $limitString);
        }
    }

    public function getJsonData()
    {
        $data = array(
            'company_id' => (int)$this->cid,
            'creator_id' => (int)$this->uid,
            'id' => (int)$this->id,
            'service' => (string)$this->service,
            'method' => (string)$this->method,
            'controller' => (string)$this->controller,
            'action' => (string)$this->action,
            'uri' => (string)$this->uri,
            'source' => (int)$this->source,
            'status_code' => (int)$this->statuscode,
            'input' => (string)$this->input,
            'output' => (string)$this->output,
            'exec_time' => (float)$this->exectime,
            'memory' => (float)$this->memory,
            'chain_depth' => (int)$this->chaindepth,
            'chain_identifier' => (string)$this->chainidentifier,
            'identifier' => (string)$this->identifier,
            'ip_address' => (string)$this->ipaddress,
            'ip_address_host' => (string)$this->ipaddresshost,
            'admin_note' => (string)$this->adminnote,
            'severity' => (int)$this->severity,
            'date_created' => (int)$this->datecreated,
            'date_modified' => (int)$this->datemodified
        );

        return $data;
    }


    public function getDataByJson($row)
    {
        $this->cid = (int)$row['company_id'];
        $this->uid = (int)$row['creator_id'];
        $this->id = (int)$row['id'];
        $this->service = (string)$row['service'];
        $this->controller = (string)$row['controller'];
        $this->action = (string)$row['action'];
        $this->uri = (string)$row['uri'];
        $this->source = (int)$row['source'];
        $this->statuscode = (int)$row['status_code'];
        $this->input = (string)$row['input'];
        $this->output = (string)$row['output'];
        $this->exectime = (float)$row['exec_time'];
        $this->memory = (float)$row['memory'];
        $this->chaindepth = (int)$row['chain_depth'];
        $this->chainidentifier = (string)$row['chain_identifier'];
        $this->identifier = (string)$row['identifier'];
        $this->ipaddress = (string)$row['ip_address'];
        $this->ipaddresshost = (string)$row['ip_address_host'];
        $this->adminnote = (string)$row['admin_note'];
        $this->severity = (int)$row['severity'];
        $this->datecreated = (int)$row['date_created'];
        $this->datemodified = (int)$row['date_modified'];
    }
}
