<?php

namespace Teamcrop\Rest;

class ProductPool extends Base
{
    public static $serviceurl = '/v1/productpools';

    const STATUS_ENABLE = 1;
    const STATUS_DISABLE = 3;

    public $cid = 0;
    public $uid = 0;
    public $pid = 0;
    public $id = 0;
    public $name = '';
    public $introduction = '';
    public $slug = '';
    public $seotitle = '';
    public $seokeyword = '';
    public $seodescription = '';
    public $seocanonical = '';
    public $moredata = '';
    public $thumbnailid = 0;
    public $thumbnail = '';
    public $fileidlist = '';
    public $filelist = array();
    public $skus = array();
    public $status = 0;
    public $datecreated = 0;
    public $datemodified = 0;

    public function __construct($id = 0, $loadFromCache = false, $queryData = array())
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id, $loadFromCache, $queryData);
            } else {
                $this->getData($id, $loadFromCache, $queryData);
            }
        }
    }

    public static function getProductPools($formData, $sortby = '', $sorttype = '', $limitString = '', $countOnly = false)
    {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }

    }

    public function addData(&$error = array())
    {
        return $this->id;
    }

    public function updateData(&$error = array())
    {
        return true;
    }

    public function delete(&$error = array())
    {
        return true;
    }

    public function getDataByJson($jsonData)
    {
        $this->id = (int)$jsonData['pid'];
        $this->cid = (int)$jsonData['cid'];
        $this->uid = (int)$jsonData['uid'];
        $this->pid = (int)$jsonData['pid'];
        $this->name = (string)$jsonData['name'];
        $this->price = (float)$jsonData['price'];
        $this->introduction = (string)$jsonData['introduction'];
        $this->slug = (string)$jsonData['slug'];
        $this->seotitle = (string)$jsonData['seo_title'];
        $this->seokeyword = (string)$jsonData['seo_keyword'];
        $this->seodescription = (string)$jsonData['seo_description'];
        $this->seocanonical = (string)$jsonData['seo_canonical'];
        $this->moredata = (string)$jsonData['more_data'];
        $this->thumbnailid = (int)$jsonData['thumbnail_id'];
        $this->fileidlist = (string)$jsonData['file_id_list'];
        $this->status = (int)$jsonData['status'];
        $this->datecreated = (int)$jsonData['date_created'];
        $this->datemodified = (int)$jsonData['date_modified'];

        if (isset($jsonData['thumbnail'])) {
            $this->thumbnail = $jsonData['thumbnail'];
        }
        if (isset($jsonData['filelist']) && $jsonData['filelist'] != '') {
            $this->filelist = explode(',', $jsonData['filelist']);
        }
        if (isset($jsonData['skus'])) {
            $this->skus = $jsonData['skus'];
        }
    }

    public function getImage()
    {
        if ($this->thumbnail != '') {
            return $this->thumbnail;
        } else {
            // return no image
            return 'https://teamcrop.com/templates/default/images/cms/no_image_upload.png';
        }
    }
}
