<?php

namespace Teamcrop\Rest;

class RbacRole extends Base
{
    public static $serviceurl = '/v1/rbacroles';

    public $cid = 0;
    public $id = 0;
    public $module = '';
    public $name = '';
    public $description = '';
    public $creatorid = 0;
    public $datecreated = 0;
    public $datemodified = 0;

    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData()
    {

    }

    public function updateData()
    {

    }


    public function delete()
    {

    }

    public static function getRbacRoles($formData, $sortby = '', $sorttype = '', $limitString = '')
    {
        return self::getItems($formData, $sortby, $sorttype, $limitString);
    }
}