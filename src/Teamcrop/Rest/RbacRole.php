<?php

namespace Teamcrop\Rest;

use Litpi\Cacher;
use Teamcrop\Utility\RbacSubjectItem;

class RbacRole extends Base
{
    public static $serviceurl = '/v1/rbacroles';

    public $cid = 0;
    public $id = 0;
    public $module = '';
    public $name = '';
    public $description = '';
    public $creatorid = 0;
    public $datecreated = 0;
    public $datemodified = 0;

    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData()
    {

    }

    public function updateData()
    {

    }


    public function delete()
    {

    }

    public static function getRbacRoles($formData, $sortby = '', $sorttype = '', $limitString = '')
    {
        return self::getItems($formData, $sortby, $sorttype, $limitString);
    }

    /**
     * Very important service, will be called by every request (in Middleware RoleExtract)
     * to get current user role list
     *
     * @param $companyId
     * @param $userId
     * @param bool $fromCache
     * @param array
     * @return bool|mixed|null|string
     */
    public static function getDetailRole($companyId, $userId, $fromCache = true, &$error = array())
    {
        global $request, $logger;

        $responseData = null;

        $needToLoadData = true;

        if ($fromCache) {
            $myCacher = new Cacher(self::cacheKeyUserDetail($companyId, $userId));
            $roleString = trim($myCacher->get());

            //check empty
            if ($roleString != null && strlen($roleString) > 0) {
                $responseData = $roleString;
                $needToLoadData = false;
            }
        }

        //in case cache not found or explicit do not want cached data
        if ($needToLoadData || isset($_GET['live'])) {
            $jwt = '';
            if ($request->hasHeader('Authorization') && $request->getHeader('Authorization')[0] != '') {
                $jwt = $request->getHeader('Authorization')[0];
            } else {
                //Fallback the Authorization from POST (__jwtAuthorization)
                $formData = $request->getParsedBody();

                if (isset($formData['__jwtAuthorization']) && $formData['__jwtAuthorization'] != '') {
                    $jwt = $formData['__jwtAuthorization'];
                }
            }

            $serviceurl = self::$serviceurl . '/userdetail?company_id='.$companyId.'&user_id=' . $userId;
            $headers = array(
                'Content-type' => 'application/json',
                'Authorization' => $jwt
            );

            $trustedkey = $request->getHeader('AccessTrustedKey')[0];
            if ($trustedkey != '') {
                $headers['AccessTrustedKey'] = $trustedkey;
            }

            try {
                //Do request and get response with submit data
                $response = self::doRequest('GET', $serviceurl, $headers, false, true);


                //request success
                if ($response['status'] == '200') {
                    $responseData = $response['data'];

//                    $logger->addNotice('[tcdebug] 3. key data from response "' . json_encode($responseData) . '".');

                } else {
                    $error = self::parsingErrorFromResponse($response);
                }
            } catch (\Exception $e) {
                $error[] = $e->getMessage() . ' (Role extract: ' . $serviceurl . ')';
            }
        }

        return $responseData;
    }


    /**
     * Clear role cache
     *
     * @param $companyId
     * @param $userId
     * @return bool|mixed|null|string
     */
    public static function clearCache($companyId, $userId)
    {
        $myCacher = new Cacher(self::cacheKeyUserDetail($companyId, $userId));
        return $myCacher->clear();
    }

    public static function cacheKeyUserDetail($companyId, $userId)
    {
        return 'tc_role_'.$companyId.'_' . $userId;
    }

    /**
     * Extract JWT role id list (in string format) to role detail list
     * @param $data
     * @return array
     */
    public static function extractRole($data)
    {
        $role = array();
        if ($data != '') {
            $subjectInfoList = explode(',', $data);
            $subjectMapping = RbacSubjectItem::subjectMapping();
            foreach ($subjectInfoList as $subjectinfo) {
                //separate subjectid & objectid
                $parts = explode(':', $subjectinfo);
                $subjectid = $parts[0];
                $objectlist = array();
                if (count($parts) == 2) {
                    $objectlist = explode('-', $parts[1]);
                }
                if (isset($subjectMapping[$subjectid])) {
                    $ticket = $subjectMapping[$subjectid][0];

                    if (count($objectlist) > 0) {
                        $role[$ticket] = $objectlist;
                    } else {
                        $role[$ticket] = array();
                    }
                }
            }
        }

        return $role;
    }
}