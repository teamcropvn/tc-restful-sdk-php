<?php

namespace Teamcrop\Rest;

class Product extends Base
{
    public static $serviceurl = '/v1/products';

    const STATUS_ENABLE = 1;
    const STATUS_DISABLE = 2;

    const INVENTORY_TRACKING = 10;
    const INVENTORY_NOTTRACKING = 12;

    const IS_DELETE = 1;


    const SOURCE_INTERNAL = 0;
    const SOURCE_WORDPRESS = 3;
    const SOURCE_MAGENTO = 5;
    const SOURCE_PRESTASHOP = 7;
    const SOURCE_OPENCART = 9;
    const SOURCE_OSCOMMERCE = 11;
    const SOURCE_OTHER = 13;
    const SOURCE_WOOECOMMERCE = 15;

    const SOURCE_BIZWEB = 17;
    const SOURCE_HARAVAN = 19;
    const SOURCE_SHOPIFY = 21;

    const INVENTORY_TYPE_NOSTOCK = 0;
    const INVENTORY_TYPE_NORMAL = 1;
    const INVENTORY_TYPE_SERIAL = 3;

    public $uid = 0;
    public $cid = 0;
    public $vid = 0;
    public $pcid = 0;
    public $spuid = 0;
    public $id = 0;
    public $source = 0;
    public $sourceid = '';
    public $sourceupdatedtime = 0;
    public $title = '';
    public $code = '';
    public $image = '';
    public $description = '';
    public $saleprice = '';
    public $promotionlist = '';
    public $inventory = 0;
    public $taglist = array();
    public $supplierid = 0;
    public $isdeleted = 0;
    public $isdeletedby = 0;
    public $status = 0;
    public $ipaddress = 0;
    public $datecreated = 0;
    public $datemodified = 0;
    public $datedeleted = 0;
    public $price = 0;
    public $rcpid = 0;
    public $poid = 0;
    public $iscombo = 0;
    public $posku = '';
    public $variantList = array();

    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData(&$error = array())
    {
        $data = array(
            'company_id' => $this->cid,
            'creator_id' => $this->uid,
            'source_id' => $this->sourceid,
            'source' => $this->source,
            'category_id' => $this->pcid,
            'vendor_id' => $this->vid,
            'sale_price' => $this->saleprice,
            'unit_id' => $this->spuid,
            "tag_list" => $this->taglist,
            'title' => $this->title,
            'code' => $this->code,
            'status'=> $this->status,
            'description' => $this->description,
            'is_combo' => $this->iscombo,
            'supplier_id' => $this->supplierid,
            'inventory' => $this->inventory
        );

        $this->id = self::doAdd($data, '', '', $error);

        return $this->id;
    }

    public function updateData(&$error = array())
    {
        $data = array(
            'company_id' => $this->cid,
            'creator_id' => $this->uid,
            'source_id' => $this->sourceid,
            'source' => $this->source,
            'category_id' => $this->pcid,
            'vendor_id' => $this->vid,
            'unit_id' => $this->spuid,
            "tag_list" => $this->taglist,
            'title' => $this->title,
            'sale_price' => $this->saleprice,
            'code' => $this->code,
            'description' => $this->description,
            'is_combo' => $this->iscombo,
            'supplier_id' => $this->supplierid,
            'is_delete' => $this->isdeleted,
            'delete_by' => $this->isdeletedby,
            'status' => $this->status
        );

        $url = self::$serviceurl . '/' . $this->id;
        return self::doUpdate($data, $url, '', $error);
    }


    public function delete(&$error = array())
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }

    public static function getProducts($formData, $sortby = '', $sorttype = '', $limitString = '', $countOnly = false)
    {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }

    }

    public static function getProductIds($formData)
    {
        return self::getRawItems($formData, self::$serviceurl . "/ids");

    }


    public static function bigcommerceProductSync($formData, &$error = array())
    {
        $url = "/v1/bigcommercesyncproducts/pull";
        return self::doAdd($formData, $url, array(), $error);

    }

    public function getJsonData()
    {

        $data = array(
            'company_id' => $this->cid,
            'creator_id' => $this->uid,
            'title' => $this->title,
            'name' => (string)$this->title,
            'id' => (int)$this->id,
            'source_id' => (string)$this->sourceid,
            'source' => (int)$this->source,
            'vendor_id' => (int)$this->vid,
            'unit_id' => (int)$this->spuid,
            'sale_price' => (float)$this->saleprice,
            'status' => (int)$this->status,
            'category_id' => (int)$this->pcid,
            'code' => (string)$this->code,
            'slug' => (string)$this->slug,
            'tag_list' =>  $this->taglist,
            'supplier_id' => (int)$this->supplierid,
            'is_combo' => (int)$this->iscombo,
            'is_deleted' => (int)$this->isdeleted,
            'is_deleted_by' => (int)$this->isdeletedby,
            'image' => (string)$this->image,
            'inventory' => (int)$this->inventory,
            'date_created' => (int)$this->datecreated,
            'date_modified' => (int)$this->datemodified
        );

        return $data;
    }

    public function getDataByJson($jsonData)
    {
        $this->uid = (int)$jsonData['creator_id'];
        $this->cid = (int)$jsonData['company_id'];
        $this->vid = (int)$jsonData['vendor_id'];
        $this->pcid = (int)$jsonData['category_id'];
        $this->spuid = (int)$jsonData['unit_id'];
        $this->id = (int)$jsonData['id'];
        $this->source = (int)$jsonData['source'];
        $this->sourceid = (string)$jsonData['source_id'];
        $this->sourceupdatedtime = (int)$jsonData['source_updated_time'];
        $this->title = (string)$jsonData['title'];
        $this->code = (string)$jsonData['code'];
        $this->image = (string)$jsonData['image'];
        $this->description = (string)$jsonData['description'];
        $this->saleprice = (float)$jsonData['sale_price'];
        $this->promotionlist = (string)$jsonData['promotion_list'];
        $this->inventory = (int)$jsonData['inventory'];
        $this->taglist = (string)$jsonData['taglist'];
        $this->supplierid = (int)$jsonData['supplier_id'];
        $this->iscombo = (int)$jsonData['is_combo'];
        $this->isdeleted = (int)$jsonData['is_deleted'];
        $this->isdeletedby = (int)$jsonData['is_deleted_by'];
        $this->status = (int)$jsonData['status'];
        $this->ipaddress = (string)long2ip($jsonData['ip_address']);
        $this->datecreated = (int)$jsonData['date_created'];
        $this->datemodified = (int)$jsonData['date_modified'];
        $this->datedeleted = (int)$jsonData['date_deleted'];
        $this->poid = (int)$jsonData['variant_id'];
        $this->posku = (string)$jsonData['variant_sku'];
        $this->variantList = (array)$jsonData['variantlist'];
    }

    /**
     * Get all product (and options)
     *
     * @param $companyId
     * @param array $error
     * @return array userid ist
     */
    public static function getFullData($companyId, &$error)
    {
        $products = array();

        $serviceurl = self::$serviceurl . '/fulldata?company_id=' . $companyId;

        try {
            //Do request and get response with submit data
            $response = self::doRequest('GET', $serviceurl, array(), false, true);

            //request success
            if ($response['status'] == '200') {
                if (is_array($response['data'])) {
                    $products = $response['data'];
                }

            } else {
                $error = self::parsingErrorFromResponse($response);
            }
        } catch (\Exception $e) {
            $error[] = $e->getMessage();
        }

        return $products;
    }
}
