<?php

namespace Teamcrop\Rest;

class Product extends Base
{
    public static $serviceurl = '/v1/products';

    const STATUS_ENABLE = 1;
    const STATUS_DISABLE = 2;

    const INVENTORY_TRACKING = 10;
    const INVENTORY_NOTTRACKING = 12;

    const IS_DELETE = 1;


    const SOURCE_INTERNAL = 0;
    const SOURCE_WORDPRESS = 3;
    const SOURCE_MAGENTO = 5;
    const SOURCE_PRESTASHOP = 7;
    const SOURCE_OPENCART = 9;
    const SOURCE_OSCOMMERCE = 11;
    const SOURCE_OTHER = 13;
    const SOURCE_WOOECOMMERCE = 15;


    public $uid = 0;
    public $cid = 0;
    public $vid = 0;
    public $pcid = 0;
    public $spuid = 0;
    public $id = 0;
    public $source = 0;
    public $sourceid = '';
    public $sourceupdatedtime = 0;
    public $title = '';
    public $code = '';
    public $image = '';
    public $description = '';
    public $saleprice = '';
    public $promotionlist = '';
    public $inventory = 0;
    public $taglist = array();
    public $supplierid = 0;
    public $isdeleted = 0;
    public $isdeletedby = 0;
    public $status = 0;
    public $ipaddress = 0;
    public $datecreated = 0;
    public $datemodified = 0;
    public $datedeleted = 0;
    public $price = 0;
    public $rcpid = 0;
    public $poid = 0;
    public $posku = '';

    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData()
    {

    }

    public function updateData()
    {

    }


    public function delete()
    {

    }

    public static function getProducts($formData, $sortby = '', $sorttype = '', $limitString = '', $countOnly = false)
    {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }

    }

    public function getDataByJson($jsonData)
    {
        $this->uid = (int)$jsonData['creator_id'];
        $this->cid = (int)$jsonData['company_id'];
        $this->vid = (int)$jsonData['vendor_id'];
        $this->pcid = (int)$jsonData['category_id'];
        $this->spuid = (int)$jsonData['unit_id'];
        $this->id = (int)$jsonData['id'];
        $this->source = (int)$jsonData['source'];
        $this->sourceid = (string)$jsonData['source_id'];
        $this->sourceupdatedtime = (int)$jsonData['source_updated_time'];
        $this->title = (string)$jsonData['title'];
        $this->code = (string)$jsonData['code'];
        $this->image = (string)$jsonData['image'];
        $this->description = (string)$jsonData['description'];
        $this->saleprice = (float)$jsonData['sale_price'];
        $this->promotionlist = (string)$jsonData['promotion_list'];
        $this->inventory = (int)$jsonData['inventory'];
        $this->taglist = $jsonData['taglist'];
        $this->supplierid = $jsonData['supplier_id'];
        $this->isdeleted = (int)$jsonData['is_deleted'];
        $this->isdeletedby = (int)$jsonData['is_deleted_by'];
        $this->status = (int)$jsonData['status'];
        $this->ipaddress = (string)long2ip($jsonData['ip_address']);
        $this->datecreated = (int)$jsonData['date_created'];
        $this->datemodified = (int)$jsonData['date_modified'];
        $this->datedeleted = (int)$jsonData['date_deleted'];
        $this->poid = (int)$jsonData['variant_id'];
        $this->posku = (string)$jsonData['variant_sku'];
    }
}
