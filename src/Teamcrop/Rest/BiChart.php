<?php

namespace Teamcrop\Rest;

class BiChart extends Base
{
    public static $serviceurl = '/v1/bicharts';

    const TYPE_BAR = 1;
    const TYPE_LINE = 3;
    const TYPE_PIE = 5;
    const TYPE_SCATTER = 7;
    const TYPE_TABLE = 9;

    const DATEPOINT_ALLTIME = 1;
    const DATEPOINT_DAILY = 3;
    const DATEPOINT_WEEKLY = 5;
    const DATEPOINT_MONTHLY = 7;
    const DATEPOINT_QUARTERLY = 9;
    const DATEPOINT_YEARLY = 11;

    const STATUS_ENABLE = 1;
    const STATUS_DISABLED = 3;

    public $uid = 0;
    public $cid = 0;
    public $id = 0;
    public $status = 0;

    public function addData(&$error = array())
    {
        return $this->id;
    }

    public function updateData(&$error = array())
    {
        $data = array(
            'creator_id' => $this->uid,
            'company_id' => $this->cid,
            'status' => $this->status
        );

        $url = self::$serviceurl . '/' . $this->id;
        return self::doUpdate($data, $url, '', $error);
    }

    public function delete(&$error = array())
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }
}
