<?php

namespace Teamcrop\Rest;

class CallCenterSetting extends Base
{
    public static $serviceurl = '/v1/callcentersettings';

    const PROVIDER_123CS = 1;
    const PROVIDER_HUMANSMS = 3;

    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 3;

    public $uid = 0;
    public $cid = 0;
    public $eid = 0;
    public $id = 0;
    public $provider = 0;
    public $status = 0;
    public $sourcenumber = "";
    public $wsuri = "";
    public $sipuri = "";
    public $sippassword = "";
    public $displayname = "";
    public $datecreated = 0;
    public $datemodified = 0;
    public $istalking = 0;
    public $isonline = 0;
    public $lastcallduration = 0;

    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData(&$error = array())
    {
        $data = array(
            'creator_id' => $this->uid,
            'company_id' => $this->cid,
            'user_id' => $this->eid,
            'provider' => $this->provider,
            'status' => $this->status,
            'source_number' => $this->sourcenumber,
            'ws_uri' => $this->wsuri,
            'sip_uri' => $this->sipuri,
            'sip_password' => $this->sippassword,
            'display_name' => $this->displayname,
        );

        $this->id = self::doAdd($data, '', '', $error);

        return $this->id;
    }

    public function updateData(&$error = array())
    {
        $data = array(
            'user_id' => $this->eid,
            'status' => $this->status,
            'source_number' => $this->sourcenumber,
            'ws_uri' => $this->wsuri,
            'sip_uri' => $this->sipuri,
            'sip_password' => $this->sippassword,
            'display_name' => $this->displayname,
        );

        $url = self::$serviceurl . '/' . $this->id;
        return self::doUpdate($data, $url, '', $error);
    }

    public function delete(&$error = array())
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }

    public static function getCallCenterSettings(
        $formData,
        $sortby = '',
        $sorttype = '',
        $limitString = '',
        $countOnly = false
    ) {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }

    }

    public function getJsonData()
    {
        $data = array(
            'creator_id' => (int)$this->uid,
            'company_id' => (int)$this->cid,
            'user_id' => (int)$this->eid,
            'id' => (int)$this->id,
            'provider' => (int)$this->provider,
            'status' => (int)$this->status,
            'source_number' => (string)$this->sourcenumber,
            'ws_uri' => (string)$this->wsuri,
            'sip_uri' => (string)$this->sipuri,
            'sip_password' => (string)$this->sippassword,
            'display_name' => (string)$this->displayname,
            'date_created' => (int)$this->datecreated,
            'date_modified' => (int)$this->datemodified,
            'is_talking' => (int)$this->istalking,
            'is_online' => (int)$this->isonline,
            'last_call_duration' => (int)$this->lastcallduration
        );

        return $data;
    }

    public function getDataByJson($jsonData)
    {
        $this->cid = (int)$jsonData['company_id'];
        $this->uid = (int)$jsonData['creator_id'];
        $this->eid = (int)$jsonData['user_id'];
        $this->id = (int)$jsonData['id'];
        $this->provider = (int)$jsonData['provider'];
        $this->status = (int)$jsonData['status'];
        $this->sourcenumber = (string)$jsonData['source_number'];
        $this->wsuri = (string)$jsonData['ws_uri'];
        $this->sipuri = (string)$jsonData['sip_uri'];
        $this->sippassword = (string)$jsonData['sip_password'];
        $this->displayname = (string)$jsonData['display_name'];
        $this->datecreated = (int)$jsonData['date_created'];
        $this->datemodified = (int)$jsonData['date_modified'];
        $this->istalking = (int)$jsonData['is_talking'];
        $this->isonline = (int)$jsonData['is_online'];
        $this->lastcallduration = (int)$jsonData['last_call_duration'];
    }
}
