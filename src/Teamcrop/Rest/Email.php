<?php

namespace Teamcrop\Rest;

class Email extends Base
{
    public static $serviceurl = 'http://api.teamcrop.com/v1/emails';

    public $cid = 0;
    public $uid = 0;
    public $fromEmail = '';
    public $fromName = '';
    public $toEmail = '';
    public $toName = '';
    public $subject = '';
    public $content = '';
    public $usingcustomaccount = false;

    //Custom sender information
    public $account = array();

    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }

        //Init account for documentation
        $this->account = array(
            'usingamazonses' => false,
            'amazonpublickey' => '',
            'amazonsecretkey' => '',
            'smtphost' => '',
            'smtpusername' => '',
            'smtppassword' => '',
            'smtpport' => '',
            'smtpssl' => ''
        );
    }

    public function addData(&$error = array())
    {
        $result = false;

        $data = array(
            'fromemail' => $this->fromEmail,
            'fromname' => $this->fromName,
            'toemail' => $this->toEmail,
            'toname' => $this->toName,
            'subject' => $this->subject,
            'content' => $this->content,
            'usingcustomaccount' => $this->usingcustomaccount,
            'account' => $this->account
        );

        $response = $this->doRequest(
            'POST',
            self::$serviceurl,
            array('Content-type' => 'application/json'),
            true,
            true,
            json_encode($data)
        );
        if ($response['status'] == 201) {
            $result = true;

        } else {
            $error = $response['data'];
        }

        return $result;
    }

    public function updateData()
    {

    }


    public function delete()
    {

    }
}