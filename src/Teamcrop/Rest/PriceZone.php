<?php

namespace Teamcrop\Rest;

class PriceZone extends Base
{
    public static $serviceurl = '/v1/pricezones';

    const STATUS_ENABLE = 1;
    const STATUS_DISABLE = 2;

    const IS_DELETE = 1;

    public $cid = 0;
    public $uid = 0;
    public $id = 0;
    public $name = '';
    public $displayname = '';
    public $status = 0;
    public $displayorder = 0;
    public $isdelete = 0;
    public $isdeleteby = 0;
    public $datecreated = 0;
    public $datemodifed = 0;
    public $datedeleted = 0;

    public $store_list = array();


    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData(&$error = array())
    {
        $data = array(
            'creator_id' => $this->uid,
            'company_id' => $this->cid,
            'name' => $this->name,
            'display_name' => $this->displayname,
            'store_list' => $this->store_list,
            'status' => $this->status
        );

        $this->id = self::doAdd($data, '', '', $error);

        return $this->id;
    }

    public function updateData(&$error = array())
    {
        $data = array(
            'creator_id' => $this->uid,
            'company_id' => $this->cid,
            'name' => $this->name,
            'display_name' => $this->displayname,
            'store_list' => $this->store_list,
            'display_order' => $this->displayorder,
            'status' => $this->status
        );

        $url = self::$serviceurl . '/' . $this->id;
        return self::doUpdate($data, $url, '', $error);
    }

    public function delete(&$error = array())
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }

    public static function getPricezones(
        $formData,
        $sortby = '',
        $sorttype = '',
        $limitString = '',
        $countOnly = false
    ) {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }

    }

    public function getJsonData()
    {
        $data = array(
            'id' => (int)$this->id,
            'company_id' => $this->cid,
            'creator_id' => $this->uid,
            'name' => (string)$this->name,
            'display_name' => (string)$this->displayname,
            'status' => (int)$this->status,
            'dis_playorder' => (int)$this->displayorder,
            'is_delete' => (int)$this->isdelete,
            'is_delete_by' => (int)$this->isdeleteby,
            'date_created' => (int)$this->datecreated,
            'date_modified' => (int)$this->datemodified,
            'date_deleted' => (int)$this->datedeleted,
        );

        return $data;
    }

    public function getDataByJson($jsonData)
    {
        $this->id = (int)$jsonData['id'];
        $this->cid = (int)$jsonData['company_id'];
        $this->uid = (int)$jsonData['creator_id'];
        $this->name = (string)$jsonData['name'];
        $this->displayname = (string)$jsonData['display_name'];
        $this->status = (int)$jsonData['status'];
        $this->displayorder = (int)$jsonData['dis_playorder'];
        $this->isdelete = (int)$jsonData['is_deleted'];
        $this->isdeleteby = (int)$jsonData['is_delete_by'];
        $this->datecreated = (int)$jsonData['date_created'];
        $this->datemodifed = (int)$jsonData['date_modified'];
        $this->datedeleted = (int)$jsonData['date_deleted'];
    }
}
