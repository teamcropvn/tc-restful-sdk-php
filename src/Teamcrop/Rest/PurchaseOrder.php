<?php

namespace Teamcrop\Rest;

class PurchaseOrder extends Base
{
    public static $serviceurl = '/v1/purchaseorders';

    const SOURCE_DOMESTIC = 1;
    const SOURCE_INTERNATIONAL = 3;

    const STATUS_NEW = 1;
    const STATUS_OPEN = 3;
    const STATUS_PENDING_APPROVAL = 5;
    const STATUS_APPROVED = 7;
    const STATUS_COMPLETE = 9;
    const STATUS_PENDING_CANCEL = 11;
    const STATUS_CANCEL = 13;



    const DEVICE_DESKTOP = 1;
    const DEVICE_TABLET = 5;
    const DEVICE_MOBILE = 9;

    const DEVICEOS_ANDROID = 1;
    const DEVICEOS_IOS = 3;
    const DEVICEOS_WINDOWSPHONE = 5;
    const DEVICEOS_JAVA = 7;
    const DEVICEOS_BLACKBERRY = 9;
    const DEVICEOS_OTHER = 11;

    const PAYMENTSTATUS_PENDING = 1;
    const PAYMENTSTATUS_PAID = 3;
    const PAYMENTSTATUS_AUTHORIZED = 5;
    const PAYMENTSTATUS_REFUNDED = 7;
    const PAYMENTSTATUS_PARTIALLYREFUND = 9;
    const PAYMENTSTATUS_VOID = 0;


    const PAYMENT_METHOD_PAYPAL = 37;
    const PAYMENT_METHOD_INTERNETBANKING = 39;
    const PAYMENT_METHOD_INTERNATIONAL = 40;
    const PAYMENT_METHOD_CASH = 41;
    const PAYMENT_METHOD_COD = 42;

    public $cid = 0;
    public $uid = 0;
    public $swid = 0;
    public $sid = 0;
    public $id = 0;
    public $invoiceid = '';
    public $invoiceidsimple = '';
    public $weekday = 0;
    public $dayhour = 0;
    public $device = 0;
    public $deviceos = 0;
    public $deviceosversion = '';
    public $sourceid = '';
    public $pricesell = 0;
    public $priceshipping = 0;
    public $pricehandling = 0;
    public $pricetax = 0;
    public $pricediscount = 0;
    public $pricefinal = 0;
    public $pricedeposit = 0;
    public $pricedebt = 0;
    public $contactemail = '';
    public $contactphone = '';
    public $billinggender = 0;
    public $billingfullname = '';
    public $billingphone = '';
    public $billingaddress = '';
    public $billingcompany = '';
    public $billingsubsubregionid = 0;
    public $billingsubregionid = 0;
    public $billingregionid = 0;
    public $billingcountry = '';
    public $billingzipcode = 0;
    public $shippingfullname = '';
    public $shippingphone = '';
    public $shippingaddress = '';
    public $shippingcompany = '';
    public $shippingsubsubregionid = 0;
    public $shippingsubregionid = 0;
    public $shippingregionid = 0;
    public $shippingcountry = '';
    public $shippinglat = '';
    public $shippinglng = '';
    public $shippingservice = 0;
    public $shippingtrackingcode = '';
    public $relatedorderid = 0;
    public $productreceipt = '';
    public $cashflowid = '';
    public $quantity = 0;
    public $deliverystatus = 0;
    public $note = '';
    public $paymentstatus = 0;
    public $paymentmethod = 0;
    public $randomcode = '';
    public $source = 0;
    public $fileid = 0;
    public $status = 0;
    public $isreturn = 0;
    public $hasreturn = 0;
    public $reviewer1 = 0;
    public $reviewer1status = 0;
    public $reviewer1note = '';
    public $reviewer2 = 0;
    public $reviewer2status = 0;
    public $reviewer2note = '';
    public $issuppliereditable = 0;
    public $isopennedbysupplier = 0;
    public $issenttosupplier = 0;
    public $ipaddress = 0;
    public $datecreated = 0;
    public $datemodified = 0;
    public $datecompleted = 0;
    public $tag = '';
    public function __construct($id = 0, $loadFromCache = false)
    {
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                //$this->copy(self::cacheGet($id));
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData(&$error = array())
    {
        $row['company_id'] = (int)$this->cid;
        $row['creator_id'] = (int)$this->uid;
        $row['ware_house_id'] = (int)$this->swid;
        $row['supplier_id'] = (int)$this->sid;
        $row['id'] = (int)$this->id;
        $row['invoice_id'] = (string)$this->invoiceid;
        $row['weekday'] = (int)$this->weekday;
        $row['dayhour'] = (int)$this->dayhour;
        $row['device'] = (int)$this->device;
        $row['deviceos'] = (int)$this->deviceos;
        $row['deviceosversion'] = (string)$this->deviceosversion;
        $row['sourceid'] = (string)$this->sourceid;
        $row['price_sell'] = (float)$this->pricesell;
        $row['price_shipping'] = (float)$this->priceshipping;
        $row['price_handling'] = (float)$this->pricehandling;
        $row['price_tax'] = (float)$this->pricetax;
        $row['price_discount'] = (float)$this->pricediscount;
        $row['price_final'] = (float)$this->pricefinal;
        $row['price_deposit'] = (float)$this->pricedeposit;
        $row['price_debt'] = (float)$this->pricedebt;
        $row['contact_email'] = (string)$this->contactemail;
        $row['contact_phone'] = (string)$this->contactphone;
        $row['billing_gender'] = (int)$this->billinggender;
        $row['billing_full_name'] = (string)$this->billingfullname;
        $row['billing_phone'] = (string)$this->billingphone;
        $row['billing_address'] = (string)$this->billingaddress;
        $row['billing_company'] = (string)$this->billingcompany;
        $row['billing_sub_sub_region_id'] = (int)$this->billingsubsubregionid;
        $row['billing_sub_region_id'] = (int)$this->billingsubregionid;
        $row['billing_region_id'] = (int)$this->billingregionid;
        $row['billing_country'] = (string)$this->billingcountry;
        $row['billing_zipcode'] = (int)$this->billingzipcode;
        $row['shipping_full_name'] = (string)$this->shippingfullname;
        $row['shipping_phone'] = (string)$this->shippingphone;
        $row['shipping_address'] = (string)$this->shippingaddress;
        $row['shipping_company'] = (string)$this->shippingcompany;
        $row['shipping_sub_sub_region_id'] = (int)$this->shippingsubsubregionid;
        $row['shipping_sub_region_id'] = (int)$this->shippingsubregionid;
        $row['shipping_region_id'] = (int)$this->shippingregionid;
        $row['shipping_country'] = (string)$this->shippingcountry;
        $row['shipping_lat'] = (string)$this->shippinglat;
        $row['shipping_lng'] = (string)$this->shippinglng;
        $row['shipping_service'] = (int)$this->shippingservice;
        $row['shipping_trackingcode'] = (string)$this->shippingtrackingcode;
        $row['related_order_id'] = (int)$this->relatedorderid;
        $row['product_receipt'] = (string)$this->productreceipt;
        $row['cashflow_id'] = (string)$this->cashflowid;
        $row['tag'] = (string)$this->tag;
        $row['quantity'] = (float)$this->quantity;
        $row['delivery_status'] = (int)$this->deliverystatus;
        $row['note'] = (string)$this->note;
        $row['payment_status'] = (int)$this->paymentstatus;
        $row['payment_method'] = (int)$this->paymentmethod;
        $row['random_code'] = (string)$this->randomcode;
        $row['source'] = (int)$this->source;
        $row['file_id'] = (int)$this->fileid;
        $row['status'] = (int)$this->status;
        $row['is_return'] = (int)$this->isreturn;
        $row['has_return'] = (int)$this->hasreturn;
        $row['reviewer1'] = (int)$this->reviewer1;
        $row['reviewer1_status'] = (int)$this->reviewer1status;
        $row['reviewer1_note'] = (string)$this->reviewer1note;
        $row['reviewer2'] = (int)$this->reviewer2;
        $row['reviewer2_status'] = (int)$this->reviewer2status;
        $row['reviewer2_note'] = (string)$this->reviewer2note;
        $row['is_supplier_edit_able'] = (int)$this->issuppliereditable;
        $row['is_openned_by_supplier'] = (int)$this->isopennedbysupplier;
        $row['is_sent_to_supplier'] = (int)$this->issenttosupplier;
        $row['ip_address'] = (string)$this->ipaddress;
        $row['date_created'] = (int)$this->datecreated;
        $row['date_modified'] = (int)$this->datemodified;
        $row['date_completed'] = (int)$this->datecompleted;

        $this->id = self::doAdd($row, '', '', $error);

        return $this->id;
    }

    public function updateData(&$error = array())
    {
        $row['company_id'] = (int)$this->cid;
        $row['creator_id'] = (int)$this->uid;
        $row['ware_house_id'] = (int)$this->swid;
        $row['supplier_id'] = (int)$this->sid;
        $row['id'] = (int)$this->id;
        $row['invoice_id'] = (string)$this->invoiceid;
        $row['weekday'] = (int)$this->weekday;
        $row['dayhour'] = (int)$this->dayhour;
        $row['device'] = (int)$this->device;
        $row['deviceos'] = (int)$this->deviceos;
        $row['deviceosversion'] = (string)$this->deviceosversion;
        $row['sourceid'] = (string)$this->sourceid;
        $row['price_sell'] = (float)$this->pricesell;
        $row['price_shipping'] = (float)$this->priceshipping;
        $row['price_handling'] = (float)$this->pricehandling;
        $row['price_tax'] = (float)$this->pricetax;
        $row['price_discount'] = (float)$this->pricediscount;
        $row['price_final'] = (float)$this->pricefinal;
        $row['price_deposit'] = (float)$this->pricedeposit;
        $row['price_debt'] = (float)$this->pricedebt;
        $row['contact_email'] = (string)$this->contactemail;
        $row['contact_phone'] = (string)$this->contactphone;
        $row['billing_gender'] = (int)$this->billinggender;
        $row['billing_full_name'] = (string)$this->billingfullname;
        $row['billing_phone'] = (string)$this->billingphone;
        $row['billing_address'] = (string)$this->billingaddress;
        $row['billing_company'] = (string)$this->billingcompany;
        $row['billing_sub_sub_region_id'] = (int)$this->billingsubsubregionid;
        $row['billing_sub_region_id'] = (int)$this->billingsubregionid;
        $row['billing_region_id'] = (int)$this->billingregionid;
        $row['billing_country'] = (string)$this->billingcountry;
        $row['billing_zipcode'] = (int)$this->billingzipcode;
        $row['shipping_full_name'] = (string)$this->shippingfullname;
        $row['shipping_phone'] = (string)$this->shippingphone;
        $row['shipping_address'] = (string)$this->shippingaddress;
        $row['shipping_company'] = (string)$this->shippingcompany;
        $row['shipping_sub_sub_region_id'] = (int)$this->shippingsubsubregionid;
        $row['shipping_sub_region_id'] = (int)$this->shippingsubregionid;
        $row['shipping_region_id'] = (int)$this->shippingregionid;
        $row['shipping_country'] = (string)$this->shippingcountry;
        $row['shipping_lat'] = (string)$this->shippinglat;
        $row['shipping_lng'] = (string)$this->shippinglng;
        $row['shipping_service'] = (int)$this->shippingservice;
        $row['shipping_trackingcode'] = (string)$this->shippingtrackingcode;
        $row['related_order_id'] = (int)$this->relatedorderid;
        $row['product_receipt'] = (string)$this->productreceipt;
        $row['cashflow_id'] = (string)$this->cashflowid;
        $row['tag'] = (string)$this->tag;
        $row['quantity'] = (float)$this->quantity;
        $row['delivery_status'] = (int)$this->deliverystatus;
        $row['note'] = (string)$this->note;
        $row['payment_status'] = (int)$this->paymentstatus;
        $row['payment_method'] = (int)$this->paymentmethod;
        $row['random_code'] = (string)$this->randomcode;
        $row['source'] = (int)$this->source;
        $row['file_id'] = (int)$this->fileid;
        $row['status'] = (int)$this->status;
        $row['is_return'] = (int)$this->isreturn;
        $row['has_return'] = (int)$this->hasreturn;
        $row['reviewer1'] = (int)$this->reviewer1;
        $row['reviewer1_status'] = (int)$this->reviewer1status;
        $row['reviewer1_note'] = (string)$this->reviewer1note;
        $row['reviewer2'] = (int)$this->reviewer2;
        $row['reviewer2_status'] = (int)$this->reviewer2status;
        $row['reviewer2_note'] = (string)$this->reviewer2note;
        $row['is_supplier_edit_able'] = (int)$this->issuppliereditable;
        $row['is_openned_by_supplier'] = (int)$this->isopennedbysupplier;
        $row['is_sent_to_supplier'] = (int)$this->issenttosupplier;
        $row['ip_address'] = (string)$this->ipaddress;
        $row['date_created'] = (int)$this->datecreated;
        $row['date_modified'] = (int)$this->datemodified;
        $row['date_completed'] = (int)$this->datecompleted;

        $url = self::$serviceurl . '/' . $this->id;
        return self::doUpdate($row, $url, '', $error);
    }


    public function delete(&$error = array())
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }

    public static function updatecashflow($cashflowid, $orderId, $priceDeposit, $action, &$error = array())
    {
        $arrData["cashflow_id"] = $cashflowid;
        $arrData["action"] = $action;
        $arrData["order_id"] = $orderId;
        $arrData["price_deposit"] = $priceDeposit;
        $url = self::$serviceurl . '/updatecashflow';
        return self::doAdd($arrData, $url, '', $error);
    }

    public function updateOneColumn($column, $order_id, $creator_id, &$error = array())
    {
        $arrData["column"] = $column;
        $arrData["creator_id"] = $creator_id;
        $arrData["order_id"] = $order_id;
        $url = self::$serviceurl . '/updateonecolumn';
        return self::doAdd($arrData, $url, '', $error);
    }

    public static function getOrders($formData, $sortby = '', $sorttype = '', $limitString = '', $countOnly = false)
    {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }
    }



    public static function getStatusList()
    {
        $output = array();

        $output[self::STATUS_NEW] = 'New';
        $output[self::STATUS_OPEN] = 'Open';
        $output[self::STATUS_PENDING_APPROVAL] = 'PendingApproval';
        $output[self::STATUS_APPROVED] = 'Approved';
        $output[self::STATUS_PENDING_CANCEL] = 'PendingCancel';
        $output[self::STATUS_CANCEL] = 'Cancel';
        $output[self::STATUS_COMPLETE] = 'Complete';


        return $output;
    }






    public function getDataByJson($row)
    {
        $this->cid = (int)$row['company_id'];
        $this->uid = (int)$row['creator_id'];
        $this->swid = (int)$row['ware_house_id'];
        $this->sid = (int)$row['supplier_id'];
        $this->id = (int)$row['id'];
        $this->invoiceid = (string)$row['invoice_id'];
        $this->weekday = (int)$row['weekday'];
        $this->dayhour = (int)$row['dayhour'];
        $this->device = (int)$row['device'];
        $this->deviceos = (int)$row['deviceos'];
        $this->deviceosversion = (string)$row['deviceosversion'];
        $this->sourceid = (string)$row['sourceid'];
        $this->pricesell = (float)$row['price_sell'];
        $this->priceshipping = (float)$row['price_shipping'];
        $this->pricehandling = (float)$row['price_handling'];
        $this->pricetax = (float)$row['price_tax'];
        $this->pricediscount = (float)$row['price_discount'];
        $this->pricefinal = (float)$row['price_final'];
        $this->pricedeposit = (float)$row['price_deposit'];
        $this->pricedebt = (float)$row['price_debt'];
        $this->contactemail = (string)$row['contact_email'];
        $this->contactphone = (string)$row['contact_phone'];
        $this->billinggender = (int)$row['billing_gender'];
        $this->billingfullname = (string)$row['billing_full_name'];
        $this->billingphone = (string)$row['billing_phone'];
        $this->billingaddress = (string)$row['billing_address'];
        $this->billingcompany = (string)$row['billing_company'];
        $this->billingsubsubregionid = (int)$row['billing_sub_sub_region_id'];
        $this->billingsubregionid = (int)$row['billing_sub_region_id'];
        $this->billingregionid = (int)$row['billing_region_id'];
        $this->billingcountry = (string)$row['billing_country'];
        $this->billingzipcode = (int)$row['billing_zipcode'];
        $this->shippingfullname = (string)$row['shipping_full_name'];
        $this->shippingphone = (string)$row['shipping_phone'];
        $this->shippingaddress = (string)$row['shipping_address'];
        $this->shippingcompany = (string)$row['shipping_company'];
        $this->shippingsubsubregionid = (int)$row['shipping_sub_sub_region_id'];
        $this->shippingsubregionid = (int)$row['shipping_sub_region_id'];
        $this->shippingregionid = (int)$row['shipping_region_id'];
        $this->shippingcountry = (string)$row['shipping_country'];
        $this->shippinglat = (string)$row['shipping_lat'];
        $this->shippinglng = (string)$row['shipping_lng'];
        $this->shippingservice = (int)$row['shipping_service'];
        $this->shippingtrackingcode = (string)$row['shipping_trackingcode'];
        $this->relatedorderid = (int)$row['related_order_id'];
        $this->productreceipt = (string)$row['product_receipt'];
        $this->cashflowid = (string)$row['cashflow_id'];
        $this->tag = (string)$row['tag'];
        $this->quantity = (float)$row['quantity'];
        $this->deliverystatus = (int)$row['delivery_status'];
        $this->note = (string)$row['note'];
        $this->paymentstatus = (int)$row['payment_status'];
        $this->paymentmethod = (int)$row['payment_method'];
        $this->randomcode = (string)$row['random_code'];
        $this->source = (int)$row['source'];
        $this->fileid = (int)$row['file_id'];
        $this->status = (int)$row['status'];
        $this->isreturn = (int)$row['is_return'];
        $this->hasreturn = (int)$row['has_return'];
        $this->reviewer1 = (int)$row['reviewer1'];
        $this->reviewer1status = (int)$row['reviewer1_status'];
        $this->reviewer1note = (string)$row['reviewer1_note'];
        $this->reviewer2 = (int)$row['reviewer2'];
        $this->reviewer2status = (int)$row['reviewer2_status'];
        $this->reviewer2note = (string)$row['reviewer2_note'];
        $this->issuppliereditable = (int)$row['is_supplier_edit_able'];
        $this->isopennedbysupplier = (int)$row['is_openned_by_supplier'];
        $this->issenttosupplier = (int)$row['is_sent_to_supplier'];
        $this->ipaddress = (string)$row['ip_address'];
        $this->datecreated = (int)$row['date_created'];
        $this->datemodified = (int)$row['date_modified'];
        $this->datecompleted = (int)$row['date_completed'];
    }
}
