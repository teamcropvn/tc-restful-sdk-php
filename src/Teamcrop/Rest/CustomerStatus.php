<?php

namespace Teamcrop\Rest;

class CustomerStatus extends Base
{
    public static $serviceurl = '/v1/customerstatuss';

    const STATUS_ENABLE = 1;
    const STATUS_DISABLE = 0;

    public $uid = 0;
    public $cid = 0;
    public $id = 0;
    public $name = '';
    public $code = '';
    public $color = '';
    public $description = '';
    public $displayorder = 0;
    public $status = 0;
    public $datecreated = 0;
    public $datemodified = 0;

    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData(&$error = array())
    {
        $data = array(
            'creator_id' => $this->uid,
            'company_id' => $this->cid,
            'code' => $this->code,
            'name' => $this->name,
            'color' => $this->color,
            'description' => $this->description
        );

        $this->id = self::doAdd($data, '', '', $error);

        return $this->id;
    }

    public function updateData(&$error = array())
    {
        $data = array(
            'creator_id' => $this->uid,
            'company_id' => $this->cid,
            'code' => $this->code,
            'name' => $this->name,
            'color' => $this->color,
            'description' => $this->description,
            'status' => $this->status
        );

        $url = self::$serviceurl . '/' . $this->id;
        return self::doUpdate($data, $url, '', $error);
    }

    public function delete(&$error = array())
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }

    public static function getCustomerStatus(
        $formData,
        $sortby = '',
        $sorttype = '',
        $limitString = '',
        $countOnly = false
    ) {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }

    }

    public function getJsonData()
    {
        $countCustomer = 0;

        $data = array(
            'creator_id' => (int)$this->uid,
            'company_id' => (int)$this->cid,
            'id' => (int)$this->id,
            'code' => (string)$this->code,
            'name' => (string)$this->name,
            'color' => (string)$this->color,
            'description' => (string)$this->description,
            'display_order' => (int)$this->displayorder,
            'count_customer' => (int)$countCustomer,
            'status' => (int)$this->status,
            'date_created' => (int)$this->datecreated,
            'date_modified' => (int)$this->datemodified
        );

        return $data;
    }

    public function getDataByJson($jsonData)
    {
        $this->id = (int)$jsonData['id'];
        $this->cid = (int)$jsonData['company_id'];
        $this->uid = (int)$jsonData['creator_id'];
        $this->code = (string)$jsonData['code'];
        $this->name = (string)$jsonData['name'];
        $this->color = (string)$jsonData['color'];
        $this->description = (string)$jsonData['description'];
        $this->displayorder = (string)$jsonData['display_order'];
        $this->status = (int)$jsonData['status'];
        $this->datecreated = (int)$jsonData['date_created'];
        $this->datemodifed = (int)$jsonData['date_modified'];
    }
}
