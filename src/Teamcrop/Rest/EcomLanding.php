<?php

namespace Teamcrop\Rest;

class EcomLanding extends Base
{
    public static $serviceurl = '/v1/ecomlandings';


    const STATUS_ENABLE = 1;
    const STATUS_DISABLE = 2;

    public $cid = 0;
    public $uid = 0;
    public $ewid = 0;
    public $id = 0;
    public $name = '';
    public $slug = '';
    public $template = '';
    public $clonefromid = 0;
    public $renderredhtml = '';
    public $builderblockelement = array();
    public $builderblockframe = array();
    public $builderseoindex = array();
    public $iscustomhtmlcssjavascript = 0;
    public $codehtmlhead = '';
    public $codehtmlbody = '';
    public $inheritpageid = 0;
    public $status = 0;
    public $ipaddress = 0;
    public $datecreated = 0;
    public $datemodified = 0;

    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData(&$error = array())
    {
        $data = array(
            "company_id" => $this->cid,
            "creator_id" => $this->uid,
            'website_id' => $this->ewid,
            'name' => $this->name,
            'slug' => $this->slug,
            'status' => $this->status
        );

        $this->id = self::doAdd($data, '', '', $error);

        return $this->id;
    }

    public function updateData(&$error = array())
    {
        $data = array(
            'name' => $this->name,
            'slug' => $this->slug,
            'status' => $this->status
        );

        $url = self::$serviceurl . '/' . $this->id;
        return self::doUpdate($data, $url, '', $error);
    }


    public function delete(&$error = array())
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }

    public static function getEcomLandings($formData, $sortby = '', $sorttype = '', $limitString = '', $countOnly = false)
    {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }

    }


    public function getDataByJson($jsonData)
    {

        $this->id = (int)$jsonData['id'];
        $this->cid = (int)$jsonData['company_id'];
        $this->uid = (int)$jsonData['creator_id'];
        $this->ewid = (int)$jsonData['website_id'];
        $this->name = (string)$jsonData['name'];
        $this->slug = (string)$jsonData['slug'];
        $this->template = (string)$jsonData['template'];
        $this->clonefromid = (int)$jsonData['clone_from_id'];
        $this->renderredhtml = (string)$jsonData['renderredhtml'];
        $this->builderblockelement = $jsonData['builder_block_element'];
        $this->builderblockframe = $jsonData['builder_block_frame'];
        $this->builderseoindex = $jsonData['builder_seo_index'];
        $this->iscustomhtmlcssjavascript = (int)$jsonData['is_custom_html_css_javascript'];
        $this->codehtmlhead = (string)$jsonData['code_html_head'];
        $this->codehtmlbody = (string)$jsonData['code_html_body'];
        $this->inheritpageid = (int)$jsonData['inherit_page_id'];
        $this->status = (int)$jsonData['status'];
        $this->ipaddress = (string)$jsonData['ip_address'];
        $this->datecreated= (int)$jsonData['date_created'];
        $this->datemodified = (int)$jsonData['date_modified'];
    }

    /**
     * Search landing page
     *
     * @param $websiteId
     * @param $slug
     * @return EcomWebsite
     */
    public static function getBySlug($websiteId, $slug)
    {
        $obj = new self();

        if ($slug != '') {
            //Todo:search in cache first
            //..

            $ecomlandings = self::getEcomLandings(array('website_id' => $websiteId, 'slug' => $slug), '', '', 1);
            if (!empty($ecomlandings)) {
                $obj = $ecomlandings[0];
            }
        }

        return $obj;
    }
}
