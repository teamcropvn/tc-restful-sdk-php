<?php

namespace Teamcrop\Rest;

class Cashflow extends Base
{
    public static $serviceurl = '/v1/cashflows';

    const DIRECTION_INCOME = 5;
    const DIRECTION_EXPENSE = 10;

    const PARTNERTYPE_EMPLOYEE = 1;
    const PARTNERTYPE_CUSTOMER = 3;
    const PARTNERTYPE_SUPPLIER = 5;
    const PARTNERTYPE_OTHER = 7;

    const SESSION_DEPOSIT = 1;
    const SESSION_PAYMENT_DEBTS = 3;
    //const SESSION_COMPLETE_PAYMENT = 5;
    const SESSION_PAY_OFF= 7;


    const TYPE_ORDER  = 1;
    const TYPE_RECEIPT_INPUT  = 3;
    const TYPE_PURCHASE_ORDER  = 5;

    const METHOD_CASH = 1;
    const METHOD_BANK = 3;
    const METHOD_GIFTCARD = 5;
    const METHOD_CUSTOMER_POINT = 7;
    const METHOD_DEBIT_CREDIT = 9;
    const METHOD_COD = 11;

    const STATUS_NEW = 0;
    const STATUS_ENABLE = 1;
    const STATUS_DISABLE = 2;

    public $cid = 0;
    public $uid = 0;
    public $sctid = 0;
    public $id = 0;
    public $type = 0;
    public $session = 0;
    public $objectid = 0;
    public $partnertype = 0;
    public $partnerid = 0;
    public $accountid = 0;
    public $identifier = '';
    public $code = '';
    public $direction = 0;
    public $locationid = 0;
    public $storeid = 0;
    public $warehouseid = 0;
    public $title = '';
    public $description = '';
    public $method = 0;
    public $value = 0;
    public $filelist = '';
    public $status = 0;
    public $approvedby = 0;
    public $datecreated = 0;
    public $datemodified = 0;

    public $customerid = 0;
    public $employeeid = 0;
    public $supplierid = 0;

    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData(&$error = array())
    {
        $data = array(
            'creator_id' => $this->uid,
            'company_id' => $this->cid,
            'title' => $this->title,
            'direction' => $this->direction,
            'cashflow_type_id' => $this->sctid,
            'code' => $this->code,
            'type' => $this->type,
            'session' => $this->session,
            'object_id' => $this->objectid,
            'account_id' => $this->accountid,
            'warehouse_id' => $this->warehouseid,
            'store_id' => $this->locationid,
            'partner_type' => $this->partnertype,
            'description' => $this->description,
            'method' => $this->method,
            'value' => $this->value,
            'approved_by' => $this->approvedby,
            'customer_id' => $this->customerid,
            'employee_id' => $this->employeeid,
            'supplier_id' => $this->supplierid,
            'date_created' => $this->datecreated,
            'status' => $this->status

        );

        $this->id = self::doAdd($data, '', '', $error);

        return $this->id;
    }

    public function updateData(&$error = array())
    {
        $data = array(
            'creator_id' => $this->uid,
            'company_id' => $this->cid,
            'title' => $this->title,
            'direction' => $this->direction,
            'cashflow_type_id' => $this->sctid,
            'type' => $this->type,
            'session' => $this->session,
            'object_id' => $this->objectid,
            'account_id' => $this->accountid,
            'code' => $this->code,
            'warehouse_id' => $this->warehouseid,
            'store_id' => $this->locationid,
            'partner_type' => $this->partnertype,
            'description' => $this->description,
            'method' => $this->method,
            'value' => $this->value,
            'approved_by' => $this->approvedby,
            'customer_id' => $this->customerid,
            'employee_id' => $this->employeeid,
            'supplier_id' => $this->supplierid,
            'date_created' => $this->datecreated,
            'status' => $this->status
        );

        $url = self::$serviceurl . '/' . $this->id;
        return self::doUpdate($data, $url, '', $error);
    }

    public function delete(&$error = array())
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }

    public static function getByIds($formData)
    {
        return self::getRawItems($formData, self::$serviceurl . '/ids');
    }

    public static function getCashflows($formData, $sortby = '', $sorttype = '', $limitString = '', $countOnly = false)
    {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }
    }

    public static function getSessionList()
    {
        $output = array();
        $output[self::SESSION_DEPOSIT] = 'session_deposit';
        $output[self::SESSION_PAYMENT_DEBTS] = 'session_payment_debts';
        //$output[self::SESSION_COMPLETE_PAYMENT] = 'session_complete_payment';
        $output[self::SESSION_PAY_OFF] = 'session_pay_off';


        return $output;
    }

    public function getDataByJson($row)
    {
        $this->cid = (int)$row['company_id'];
        $this->uid = (int)$row['creator_id'];
        $this->sctid = (int)$row['cashflow_type_id'];
        $this->id = (int)$row['id'];
        $this->type = (int)$row['type'];
        $this->session = (int)$row['session'];
        $this->objectid = (int)$row['object_id'];
        $this->partnertype = (int)$row['partner_type'];
        $this->partnerid = (int)$row['partner_id'];
        $this->accountid = (int)$row['account_id'];
        $this->identifier = (string)$row['identifier'];
        $this->code = (string)$row['code'];
        $this->direction = (int)$row['direction'];
        $this->locationid = (int)$row['store_id'];
        $this->warehouseid = (int)$row['warehouse_id'];
        $this->title = (string)$row['title'];
        $this->description = (string)$row['description'];
        $this->method = (int)$row['method'];
        $this->value = (float)$row['value'];
        $this->filelist = (string)$row['file_list'];
        $this->status = (int)$row['status'];
        $this->approvedby = (int)$row['approved_by'];
        $this->datecreated = (int)$row['date_created'];
        $this->datemodified = (int)$row['date_modified'];
    }

    public function getJsonData()
    {
        $data = array(
            'company_id' => (int)$this->cid,
            'creator_id' => (int)$this->uid,
            'cashflow_type_id' => (int)$this->sctid,
            'id' => (int)$this->id,
            'type' => (int)$this->type,
            'session' => (int)$this->session,
            'object_id' => (int)$this->objectid,
            'partner_type' => (int)$this->partnertype,
            'partner_id' => (int)$this->partnerid,
            'account_id' => (int)$this->accountid,
            'identifier' => (string)$this->identifier,
            'code' => (string)$this->code,
            'direction' => (int)$this->direction,
            'store_id' => (int)$this->locationid,
            'warehouse_id' => (int)$this->warehouseid,
            'title' => (string)$this->title,
            'description' => (string)$this->description,
            'method' => (int)$this->method,
            'value' => (float)$this->value,
            'file_list' => (int)$this->filelist,
            'status' => (int)$this->status,
            'approved_by' => (int)$this->approvedby,
            'date_created' => (int)$this->datecreated,
            'date_modified' => (int)$this->datemodified
        );

        return $data;
    }
}
