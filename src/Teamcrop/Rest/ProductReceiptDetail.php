<?php

namespace Teamcrop\Rest;

class ProductReceiptDetail extends Base
{
    public static $serviceurl = '/v1/productreceiptdetails';

    public $cid = 0;
    public $uid = 0;
    public $swid = 0;
    public $pid = 0;
    public $poid = 0;
    public $posku = '';
    public $sprid = 0;
    public $id = 0;
    public $type = 0;
    public $direction = 0;
    public $itemcategory = 0;
    public $itemname = '';
    public $itemcode = '';
    public $itemoption = "";
    public $itemquantity = 0;
    public $itemquantitysigned = 0;
    public $itemserialnumber = "";
    public $itemunit = '';
    public $itemvendor = 0;
    public $itemsupplier = 0;
    public $unitprice = 0;
    public $subtotal = 0;
    public $room = 0;
    public $block = 0;
    public $blockrow = 0;
    public $blockbox = 0;
    public $note = '';
    public $status = 0;
    public $isdeleted = 0;
    public $isdeletedby = 0;
    public $datecreated = 0;
    public $datemodified = 0;
    public $datedeleted = 0;


    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData(&$error = array())
    {
        $data = array(
            'creator_id' => $this->uid,
            'company_id' => $this->cid,
            'product_receipt_id' => $this->sprid,
            'product_variant_id' => $this->poid,
            'item_serial_number' => $this->itemserialnumber,
            'product_sku' => $this->posku,
            'product_price' => $this->unitprice,
            'product_quantity' => $this->itemquantity
        );

        $this->id = self::doAdd($data, '', '', $error);

        return $this->id;
    }

    public function updateData(&$error = array())
    {
        $data = array(
            'creator_id' => $this->uid,
            'company_id' => $this->cid,
            'product_receipt_id' => $this->sprid,
            'item_serial_number' => $this->itemserialnumber,
            'product_price' => $this->unitprice,
            'product_quantity' => $this->itemquantity
        );

        $url = self::$serviceurl . '/' . $this->id;
        return self::doUpdate($data, $url, '', $error);
    }

    public function delete(&$error = array())
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }

    public function deleteByProduct($product_id, &$error = array())
    {
        $url = self::$serviceurl . '/product/' . $product_id;
        return self::doDelete($url, '', $error);
    }

    public function deleteBySku($poid, &$error = array())
    {
        $url = self::$serviceurl . '/sku/' . $poid;
        return self::doDelete($url, '', $error);
    }


    public static function getProductReceiptDetails(
        $formData,
        $sortby = '',
        $sorttype = '',
        $limitString = '',
        $countOnly = false
    )
    {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }

    }

    public function getJsonData()
    {
        $data = array(
            'id' => (int)$this->id,
            'company_id' => (int)$this->cid,
            'warehouse_id' => (int)$this->swid,
            'creator_id' => (int)$this->uid,
            'product_sku' => (string)$this->posku,
            'product_variant_id' => (string)$this->poid,
            'product_id' => (int)$this->pid,
            'product_receipt_id' => (int)$this->sprid,
            'type' => (int)$this->type,
            'direction' => (int)$this->direction,
            'item_category' => (int)$this->itemcategory,
            'item_name' => (string)$this->itemname,
            'item_code' => (string)$this->itemcode,
            'item_option' => (string)$this->itemoption,
            'item_quantity' => (float)$this->itemquantity,
            'item_quantity_signed' => (float)$this->itemquantitysigned,
            'item_serial_number' => (string)$this->itemserialnumber,
            'item_unit' => (string)$this->itemunit,
            'item_vendor' => (int)$this->itemvendor,
            'item_supplier' => (int)$this->itemsupplier,
            'unit_price' => (float)$this->unitprice,
            'subtotal' => (float)$this->subtotal,
            'room' => (int)$this->room,
            'block' => (int)$this->block,
            'block_row' => (int)$this->blockrow,
            'block_box' => (int)$this->blockbox,
            'note' => (string)$this->note,
            'status' => (int)$this->status,
            'date_created' => (int)$this->datecreated,
            'date_modified' => (int)$this->datemodified
        );

        return $data;
    }

    public function getDataByJson($row)
    {
        $this->cid = (int)$row['company_id'];
        $this->uid = (int)$row['creator_id'];
        $this->swid = (int)$row['warehouse_id'];
        $this->pid = (int)$row['product_id'];
        $this->poid = (int)$row['product_variant_id'];
        $this->posku = (string)$row['product_sku'];
        $this->sprid = (int)$row['product_receipt_id'];
        $this->id = (int)$row['id'];
        $this->type = (string)$row['type'];
        $this->direction = (string)$row['direction'];
        $this->itemcategory = (string)$row['item_category'];
        $this->itemname = (string)$row['item_name'];
        $this->itemcode = (string)$row['item_code'];
        $this->itemoption = (string)$row['item_option'];
        $this->itemquantity = (float)$row['item_quantity'];
        $this->itemquantitysigned = (float)$row['item_quantity_signed'];
        $this->itemunit = (string)$row['item_unit'];
        $this->itemvendor = (int)$row['item_vendor'];
        $this->itemsupplier = (int)$row['item_supplier'];
        $this->unitprice = (float)$row['unit_price'];
        $this->subtotal = (float)$row['subtotal'];
        $this->room = (int)$row['room'];
        $this->block = (int)$row['block'];
        $this->blockrow = (int)$row['block_row'];
        $this->blockbox = (int)$row['block_box'];
        $this->note = (string)$row['note'];
        $this->status = (int)$row['status'];
        $this->isdeleted = (int)$row['is_deleted'];
        $this->isdeletedby = (int)$row['is_deleted_by'];
        $this->datecreated = (int)$row['date_created'];
        $this->datemodified = (int)$row['date_modified'];
        $this->datedeleted = (int)$row['date_deleted'];
    }
}
