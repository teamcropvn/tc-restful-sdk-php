<?php

namespace Teamcrop\Rest;

class BookingItem extends Base
{
    public static $serviceurl = '/v1/bookingitems';

    const TYPE_HOTEL = 1;
    const  TYPE_ACTIVITY = 3;
    const  TYPE_PEOPLE = 5;
    const  TYPE_EQUIPMENT = 7;

    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 3;

    public $cid = 0;
    public $uid = 0;
    public $bigid = 0;
    public $bipid = 0;
    public $id = 0;
    public $name = '';
    public $code = '';
    public $type = 0;
    public $description = '';
    public $tag = '';
    public $displayorder = 0;
    public $maxconcurrent = 0;
    public $status = 0;
    public $datecreated = 0;
    public $datemodified = 0;

    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData(&$error = array())
    {
        $row = $this->getJsonData();
        $this->id = self::doAdd($row, '', '', $error);

        return $this->id;
    }

    public function updateData(&$error = array())
    {
        $row = $this->getJsonData();
        $url = self::$serviceurl . '/' . $this->id;
        return self::doUpdate($row, $url, '', $error);
    }

    public function delete(&$error = array())
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }

    public static function getBookingItems(
        $formData,
        $sortby = '',
        $sorttype = '',
        $limitString = '',
        $countOnly = false
    ) {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }
    }


    public function getDataByJson($row)
    {
        $this->cid = (int)$row['company_id'];
        $this->uid = (int)$row['creator_id'];
        $this->bigid = (int)$row['group_id'];
        $this->bipid = (int)$row['item_price_id'];
        $this->id = (int)$row['id'];
        $this->name = (string)$row['name'];
        $this->code = (string)$row['code'];
        $this->type = (int)$row['type'];
        $this->description = (string)$row['description'];
        $this->tag = (string)$row['tag'];
        $this->displayorder = (int)$row['display_order'];
        $this->maxconcurrent = (string)$row['max_concurrent'];
        $this->status = (int)$row['status'];
        $this->datecreated = (int)$row['date_created'];
        $this->datemodified = (int)$row['date_modified'];
    }

    public function getJsonData()
    {
        $row['company_id'] = $this->cid;
        $row['creator_id'] = $this->uid;
        $row['group_id'] = $this->bigid;
        $row['item_price_id'] = $this->bipid;
        $row['id'] = $this->id;
        $row['name'] = $this->name;
        $row['code'] = $this->code;
        $row['type'] = $this->type;
        $row['description'] = $this->description;
        $row['tag'] = $this->tag;
        $row['display_order'] = $this->displayorder;
        $row['max_concurrent'] = $this->maxconcurrent;
        $row['status'] = $this->status;
        $row['date_created'] = $this->datecreated;
        $row['date_modified'] = $this->datemodified;


        return $row;
    }


}
