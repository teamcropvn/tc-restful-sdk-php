<?php

namespace Teamcrop\Rest;

class Notify extends Base
{
    public static $serviceurl = '/v1/notifies';

    public $cid = 0;
    public $uid = 0;
    public $uidfrom = 0;
    public $id = 0;
    public $type = '';
    public $objectid = 0;
    public $subobjectid = 0;
    public $metafrom = '';
    public $metasummary = '';
    public $metaimage = '';
    public $metaurl = '';
    public $countpush = 0;
    public $isread = 0;
    public $datecreated = 0;
    public $datemodified = 0;
    public $datepushed = 0;
    public $dateread = 0;

    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData(&$error = array())
    {
        $data = array(
            'creator_id' => $this->uidfrom,
            'owner_id' => $this->uid,
            'company_id' => $this->cid,
            'type' => $this->type,
            'object_id' => $this->objectid,
            'sub_object_id' => $this->subobjectid,
            'meta_from' => $this->metafrom,
            'meta_summary' => $this->metasummary,
            'meta_image' => $this->metaimage,
            'meta_url' => $this->metaurl,
        );

        $this->id = self::doAdd($data, '', '', $error);

        return $this->id;
    }

    public function updateData(&$error = array())
    {
        $data = array(
            'creator_id' => $this->uidfrom,
            'owner_id' => $this->uid,
            'company_id' => $this->cid,
            'meta_from' => $this->metafrom,
            'meta_summary' => $this->metasummary,
            'meta_image' => $this->metaimage,
            'meta_url' => $this->metaurl,
            'count_push' => $this->countpush,
            'is_read' => $this->isread,
            'date_pushed' => $this->datepushed,
            'date_read' => $this->dateread
        );

        $url = self::$serviceurl . '/' . $this->id;
        return self::doUpdate($data, $url, '', $error);
    }


    public function delete(&$error = array())
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }

    public static function getNotifies($formData, $sortby = '', $sorttype = '', $limitString = '', $countOnly = false)
    {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }
    }

    /**
     * @return array
     */
    public function getJsonData()
    {
        $data = array(
            'owner_id' => (int)$this->uid,
            'creator_id' => (int)$this->uidfrom,
            'company_id' => (int)$this->cid,
            'id' => (int)$this->id,
            'type' => (string)$this->type,
            'objectid' => (int)$this->objectid,
            'subobjectid' => (int)$this->subobjectid,
            'meta_from' => (string)$this->metafrom,
            'meta_summary' => (string)$this->metasummary,
            'meta_image' => (string)$this->metaimage,
            'meta_url' => (string)$this->metaurl,
            'count_push' => (int)$this->countpush,
            'is_read' => (int)$this->isread,
            'date_created' => (int)$this->datecreated,
            'date_pushed' => (int)$this->datepushed,
            'date_read' => (int)$this->dateread,
        );

        return $data;
    }

    public static function cacheKey($notifyid)
    {
        return 'tc_notify_' . $notifyid;
    }

    public function getDataByArrayCache($row)
    {

    }

    public function getDataByJson($jsonData)
    {
        $this->cid = (int)$jsonData['company_id'];
        $this->uidfrom = (int)$jsonData['creator_id'];
        $this->uid = (int)$jsonData['owner_id'];
        $this->id = (int)$jsonData['id'];
        $this->type = (string)$jsonData['type'];
        $this->objectid = (int)$jsonData['object_id'];
        $this->subobjectid = (int)$jsonData['sub_object_id'];
        $this->metafrom = (string)$jsonData['meta_from'];
        $this->metasummary = (string)$jsonData['meta_summary'];
        $this->metaimage = (string)$jsonData['meta_image'];
        $this->metaurl = (string)$jsonData['meta_url'];
        $this->countpush = (int)$jsonData['count_push'];
        $this->isread = (int)$jsonData['is_read'];
        $this->datecreated = (int)$jsonData['date_created'];
        $this->datepushed = (int)$jsonData['date_pushed'];
        $this->dateread = (int)$jsonData['date_read'];
    }
}
