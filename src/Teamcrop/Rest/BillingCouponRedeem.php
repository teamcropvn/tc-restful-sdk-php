<?php

namespace Teamcrop\Rest;

class BillingCouponRedeem extends Base
{
    public static $serviceurl = '/v1/billingcouponredeems';


    const APPLYTYPE_VALUE = 1;
    const APPLYTYPE_PERCENT = 3;

    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 3;

    public $cid = 0;
    public $uid = 0;
    public $bcid = 0;
    public $id = 0;
    public $code = '';
    public $applytype = 0;
    public $applyvalue = 0;
    public $transactionid = 0;
    public $note = '';
    public $status = 0;
    public $ipaddress = 0;
    public $datecreated = 0;
    public $datemodified = 0;

    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData(&$error = array())
    {
        $row = $this->getJsonData();
        $this->id = self::doAdd($row, '', '', $error);

        return $this->id;
    }

    public function updateData(&$error = array())
    {
        $row = $this->getJsonData();
        $url = self::$serviceurl . '/' . $this->id;
        return self::doUpdate($row, $url, '', $error);
    }

    public function delete(&$error = array())
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }

    public static function getGroups(
        $formData,
        $sortby = '',
        $sorttype = '',
        $limitString = '',
        $countOnly = false
    ) {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }
    }


    public function getDataByJson($row)
    {
        $this->cid = (int)$row['company_id'];
        $this->uid = (int)$row['creator_id'];
        $this->bcid = (int)$row['billing_coupon_id'];
        $this->id = (int)$row['id'];
        $this->code = (string)$row['code'];
        $this->applytype = (int)$row['apply_type'];
        $this->applyvalue = (float)$row['apply_value'];
        $this->transactionid = (int)$row['transaction_id'];
        $this->note = (string)$row['note'];
        $this->status = (int)$row['status'];
        $this->datecreated = (int)$row['date_created'];
        $this->datemodified = (int)$row['date_modified'];
    }

    public function getJsonData()
    {
        $row['company_id'] = (int)$this->cid;
        $row['creator_id'] = (int)$this->uid;
        $row['billing_coupon_id'] = (int)$this->bcid;
        $row['id'] = (int)$this->id;
        $row['code'] = (string)$this->code;
        $row['apply_type'] = (int)$this->applytype;
        $row['apply_value'] = (float)$this->applyvalue;
        $row['transaction_id'] = (int)$this->transactionid;
        $row['note'] = (string)$this->note;
        $row['status'] = (int)$this->status;
        $row['date_created'] = (int)$this->datecreated;
        $row['date_modified'] = (int)$this->datemodified;

        return $row;
    }


}
