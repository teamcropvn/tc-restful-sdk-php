<?php

namespace Teamcrop\Rest;

class LogServerMetric extends Base
{
    public static $serviceurl = '/v1/logservermetrics';

    public $id = 0;
    public $serverid = 0;
    public $metric = '';
    public $value = 0;
    public $dayhour = 0;
    public $datecreated = 0;

    public function __construct($id = 0, $loadFromCache = false)
    {
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }


    public function addData(&$error = array())
    {
        $data = array(
            'server_id' => $this->serverid,
            'metric' => $this->metric,
            'value' => $this->value,
            'date_created' => $this->datecreated,
        );

        $this->id = self::doAdd($data, '', '', $error);

        return $this->id;
    }

    public function updateData(&$error = array())
    {
        $data = array(
            'server_id' => $this->serverid,
            'metric' => $this->metric,
            'value' => $this->value,
        );

        $url = self::$serviceurl . '/' . $this->id;
        return self::doUpdate($data, $url, '', $error);
    }


    public function delete(&$error = array())
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }

    public static function getLogRestRequests($formData, $sortby = '', $sorttype = '', $limitString = '', $countOnly = false)
    {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
        	return self::getItems($formData, $sortby, $sorttype, $limitString);
        }
    }

    public function getJsonData()
    {
        $data = array(
            'id' => (int)$this->id,
            'server_id' => (int)$this->serverid,
            'metric' => (string)$this->metric,
            'value' => (float)$this->value,
            'dayhour' => (int)$this->dayhour,
            'date_created' => (int)$this->datecreated,
        );

        return $data;
    }


    public function getDataByJson($row)
    {
        $this->id = (int)$row['id'];
        $this->serverid = (string)$row['server_id'];
        $this->metric = (string)$row['metric'];
        $this->value = (string)$row['value'];
        $this->dayhour = (string)$row['dayhour'];
        $this->datecreated = (int)$row['date_created'];
    }
}
