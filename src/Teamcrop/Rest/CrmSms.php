<?php

namespace Teamcrop\Rest;

class CrmSms extends Base
{
    public static $serviceurl = '/v1/crmsmss';

    const STATUS_PENDING = 1;
    const STATUS_SUCCESS = 3;
    const STATUS_ERROR = 5;

    const GW_ESMS = 1;
    const GW_INCOM = 3;

    const SENDTYPE_BRANDNAME = 2;
    const SENDTYPE_RANDOMNUMBER = 3;
    const SENDTYPE_FIXNUMBER = 4;
    const SENDTYPE_FIXNUMBERSHORT = 6;

    const TYPE_NORMAL = 1;
    const TYPE_ORDER = 3;

    public $cid = 0;
    public $uid = 0;
    public $ccid = 0;
    public $objectid = 0;
    public $id = 0;
    public $phone = '';
    public $telco = '';
    public $provider = '';
    public $content = '';
    public $countcharacter = '';
    public $countsms = 0;
    public $pricepersms = 0;
    public $totalprice = 0;
    public $messageid = 0;
    public $sendtype = 0;
    public $trackingdetail = '';
    public $status = 0;
    public $type = 0;
    public $ipaddress = '';
    public $datecreated = 0;
    public $datemodified = 0;

    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData(&$error = array())
    {
        $data = array(
            'creator_id' => $this->uid,
            'company_id' => $this->cid,
            'customer_id' => $this->ccid,
            'object_id' => $this->objectid,
            'phone' => $this->phone,
            'provider' => $this->provider,
            'content' => $this->content,
            'type' => $this->type
        );

        $this->id = self::doAdd($data, '', '', $error);

        return $this->id;
    }

    public function updateData(&$error = array())
    {
        $data = array(
            'creator_id' => $this->uid,
            'company_id' => $this->cid,
            'status' => $this->status
        );

        $url = self::$serviceurl . '/' . $this->id;
        return self::doUpdate($data, $url, '', $error);
    }


    public function delete(&$error = array())
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }

    public static function getCrmSmss($formData, $sortby = '', $sorttype = '', $limitString = '', $countOnly = false)
    {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }
    }

    /**
     * @return array
     */
    public function getJsonData()
    {
        $data = array(
            'creator_id' => (int)$this->uid,
            'company_id' => (int)$this->cid,
            'customer_id' => (int)$this->ccid,
            'object_id' => (int)$this->objectid,
            'id' => (int)$this->id,
            'phone' => (string)$this->phone,
            'telco' => (string)$this->telco,
            'provider' => (string)$this->provider,
            'content' => (string)$this->content,
            'count_character' => (int)$this->countcharacter,
            'count_sms' => (int)$this->countsms,
            'price_per_sms' => (int)$this->pricepersms,
            'total_price' => (int)$this->totalprice,
            'message_id' => (int)$this->messageid,
            'send_type' => (int)$this->sendtype,
            'tracking_detail' => (string)$this->trackingdetail,
            'status' => (int)$this->status,
            'type' => (int)$this->type,
            'ip_address' => (string)$this->ipaddress,
            'date_created' => (int)$this->datecreated,
            'date_modified' => (int)$this->datemodified
        );

        return $data;
    }

    public static function cacheKey($notifyid)
    {
        return 'tc_crm_sms_' . $notifyid;
    }

    public function getDataByArrayCache($row)
    {

    }

    public function getDataByJson($row)
    {
        $this->cid = (int)$row['company_id'];
        $this->uid = (int)$row['creator_id'];
        $this->ccid = (int)$row['customer_id'];
        $this->objectid = (int)$row['object_id'];
        $this->id = (int)$row['id'];
        $this->phone = (string)$row['phone'];
        $this->telco = (string)$row['telco'];
        $this->provider = (string)$row['provider'];
        $this->content = (string)$row['content'];
        $this->countsms = (int)$row['count_sms'];
        $this->pricepersms = (int)$row['price_per_sms'];
        $this->totalprice = (int)$row['total_price'];
        $this->messageid = (int)$row['message_id'];
        $this->sendtype = (int)$row['send_type'];
        $this->trackingdetail = (string)$row['tracking_detail'];
        $this->status = (int)$row['status'];
        $this->type = (int)$row['type'];
        $this->ipaddress = long2ip($row['ip_address']);
        $this->datecreated = (int)$row['date_created'];
        $this->datemodified = (int)$row['date_modified'];
    }
}
