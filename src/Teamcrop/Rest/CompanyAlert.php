<?php

namespace Teamcrop\Rest;

class CompanyAlert extends Base
{
    public static $serviceurl = '/v1/companyalerts';

    const SEVERITY_INFO = 1;
    const SEVERITY_WARNING = 3;
    const SEVERITY_CRITICAL = 5;


    const STATUS_NEW = 1;
    const STATUS_DISABLED = 3;
    const STATUS_DONE = 5;
    const STATUS_DELETED = 7;

    const CREATION_MODE_AUTO = 1;
    const CREATION_MODE_MANUAL = 3;

    public $cid;
    public $uid;
    public $id;
    public $type;
    public $severity;
    public $creationmode;
    public $title;
    public $content;
    public $status;
    public $hideable;
    public $ispinned;
    public $isread;
    public $isnotified;
    public $redirecturl;
    public $datecreated;
    public $datemodified;


    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            $this->getData($id, $loadFromCache);
        }
    }




    public function addData(&$error = array())
    {
        $data = array(
            'creator_id' => (int)$this->uid,
            'company_id' => (int)$this->cid,
            'id' => (int)$this->id,
            'type' => (string)$this->type,
            'severity' => (int)$this->severity,
            'creation_mode' => (int)$this->creationmode,
            'title' => (string)$this->title,
            'content' => (string)$this->content,
            'status' => (int)$this->status,
            'hide_able' => (int)$this->hideable,
            'is_pinned' => (int)$this->ispinned,
            'is_read' => (int)$this->isread,
            'is_notified' => (int)$this->isnotified,
            'redirect_url' => (string)$this->redirecturl
        );

        $this->id = self::doAdd($data, '', '', $error);

        return $this->id;
    }

    public function updateData(&$error = array())
    {
        $data = array(
            'creator_id' => (int)$this->uid,
            'company_id' => (int)$this->cid,
            'id' => (int)$this->id,
            'type' => (string)$this->type,
            'severity' => (int)$this->severity,
            'creation_mode' => (int)$this->creationmode,
            'title' => (string)$this->title,
            'content' => (string)$this->content,
            'status' => (int)$this->status,
            'hide_able' => (int)$this->hideable,
            'is_pinned' => (int)$this->ispinned,
            'is_read' => (int)$this->isread,
            'is_notified' => (int)$this->isnotified,
            'redirect_url' => (string)$this->redirecturl
        );

        $url = self::$serviceurl . '/' . $this->id;
        return self::doUpdate($data, $url, '', $error);
    }

    public function delete(&$error = array())
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }

    public static function getCompanyAlerts(
        $formData,
        $sortby = '',
        $sorttype = '',
        $limitString = '',
        $countOnly = false
    )
    {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }
    }

    public function getDataByJson($row)
    {
        $this->cid = (int)$row['company_id'];
        $this->uid = (int)$row['user_id'];
        $this->id = (int)$row['id'];
        $this->type = (string)$row['type'];
        $this->severity = (int)$row['severity'];
        $this->creationmode = (int)$row['creation_mode'];
        $this->title = (string)$row['title'];
        $this->content = (string)$row['content'];
        $this->status = (int)$row['status'];
        $this->hideable = (int)$row['hide_able'];
        $this->ispinned = (int)$row['is_pinned'];
        $this->isread = (int)$row['is_read'];
        $this->isnotified = (int)$row['is_notified'];
        $this->redirecturl = (string)$row['redirect_url'];
        $this->datecreated = (int)$row['date_created'];
        $this->datemodified = (int)$row['date_modified'];
    }

    public function getJsonData()
    {
        $data = array(
            'user_id' => (int)$this->uid,
            'company_id' => (int)$this->cid,
            'id' => (int)$this->id,
            'type' => (string)$this->type,
            'severity' => (int)$this->severity,
            'creation_mode' => (int)$this->creationmode,
            'title' => (string)$this->title,
            'content' => (string)$this->content,
            'status' => (int)$this->status,
            'hide_able' => (int)$this->hideable,
            'is_pinned' => (int)$this->ispinned,
            'is_read' => (int)$this->isread,
            'is_notified' => (int)$this->isnotified,
            'redirect_url' => (string)$this->redirecturl,
            'date_created' => (int)$this->datecreated,
            'date_modified' => (int)$this->datemodified
        );

        return $data;
    }
}
