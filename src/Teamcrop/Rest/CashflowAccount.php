<?php

namespace Teamcrop\Rest;

class CashflowAccount extends Base
{
    public static $serviceurl = '/v1/cashflowaccounts';

    const TYPE_CASH = 1;
    const TYPE_BANK = 3;
    const TYPE_GIFTCARD = 5;
    const TYPE_CUSTOMER_POINT = 7;
    const TYPE_DEBIT_CREDIT = 9;
    const TYPE_COD = 11;

    const STATUS_ENABLE = 1;
    const STATUS_DISABLE = 3;

    public $uid = 0;
    public $cid = 0;
    public $id = 0;
    public $type = 0;
    public $name = '';
    public $number = '';
    public $status = 0;
    public $note = '';
    public $datecreated = 0;
    public $datemodifed = 0;

    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData(&$error = array())
    {
//        $data = array();
//
//        $this->id = self::doAdd($data, '', '', $error);

        return $this->id;
    }

    public function updateData(&$error = array())
    {
//        $data = array( );
//
//        $url = self::$serviceurl . '/' . $this->id;
//        return self::doUpdate($data, $url, '', $error);
    }

    public function delete(&$error = array())
    {
//        $url = self::$serviceurl . '/' . $this->id;
//        return self::doDelete($url, '', $error);
    }

    public static function getCashflowAccounts(
        $formData,
        $sortby = '',
        $sorttype = '',
        $limitString = '',
        $countOnly = false
    )
    {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }
    }


    public function getDataByJson($row)
    {
        $this->cid = (int)$row['company_id'];
        $this->uid = (int)$row['creator_id'];
        $this->id = (int)$row['id'];
        $this->type = (int)$row['type'];
        $this->name = (string)$row['name'];
        $this->number = (string)$row['number'];
        $this->status = (int)$row['status'];
        $this->note = (string)$row['note'];
        $this->datecreated = (int)$row['date_created'];
        $this->datemodifed = (int)$row['date_modifed'];
    }

    public function getJsonData()
    {
        $data = array(
            'creator_id' => (int)$this->uid,
            'company_id' => (int)$this->cid,
            'id' => (int)$this->id,
            'type' => (int)$this->type,
            'name' => (string)$this->name,
            'number' => (string)$this->number,
            'status' => (int)$this->status,
            'note' => (string)$this->note,
            'date_created' => (int)$this->datecreated,
            'date_modifed' => (int)$this->datemodifed
        );

        return $data;
    }
}
