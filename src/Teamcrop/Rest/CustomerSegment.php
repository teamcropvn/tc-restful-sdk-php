<?php

namespace Teamcrop\Rest;

class CustomerSegment extends Base
{
    public static $serviceurl = '/v1/customersegments';

    public $cid = 0;
    public $uid = 0;
    public $id = 0;
    public $code = '';
    public $name = '';
    public $color = '';
    public $description = '';
    public $countcustomer = 0;
    public $ipaddress = 0;
    public $datecreated = 0;
    public $datemodified = 0;
    public $datelastfetched = 0;

    public $customers = array();
    public $details = array();
    public $deleteitems = array();

    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData(&$error = array())
    {
        $data = array(
            'creator_id' => $this->uid,
            'company_id' => $this->cid,
            'code' => $this->code,
            'name' => $this->name,
            'color' => $this->color,
            'description' => $this->description,
            'details' => $this->details
        );

        $this->id = self::doAdd($data, '', '', $error);

        return $this->id;
    }

    public function updateData(&$error = array())
    {
        $data = array(
            'creator_id' => $this->uid,
            'company_id' => $this->cid,
            'code' => $this->code,
            'name' => $this->name,
            'color' => $this->color,
            'description' => $this->description,
            'details' => $this->details,
            'delete_items' => $this->deleteitems
        );

        $url = self::$serviceurl . '/' . $this->id;
        return self::doUpdate($data, $url, '', $error);
    }

    public function delete(&$error = array())
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }

    public static function getCustomerSegments(
        $formData,
        $sortby = '',
        $sorttype = '',
        $limitString = '',
        $countOnly = false
    ) {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }

    }

    // GET customer segment detail
    public function getCustomerSegmentDetails()
    {
        return CustomerSegmentDetail::getCustomerSegmentDetails(
            array(
                'company_id' => $this->cid,
                'customer_segment_id' => $this->id
            ),
            'id',
            'ASC'
        );
    }

    // get customer list
    public function getCustomers($countOnly = false)
    {
        // Lay danh sach detail
        $customerSegmentDetails = $this->getCustomerSegmentDetails();

        $formData = array();
        /** @var CustomerSegmentDetail $myCustomerSegmentDetail */
        foreach ($customerSegmentDetails as $myCustomerSegmentDetail) {
            $segmentDetail = $myCustomerSegmentDetail->getJsonData(false);
            $segmentDetail['field_name'] = $myCustomerSegmentDetail->getFieldName();

            $formData[$myCustomerSegmentDetail->rule][] = $segmentDetail;
        }

        $customers = Customer::getCustomers(
            array(
                'company_id' => $this->cid,
                'customer_segment' => $formData
            ),
            '',
            '',
            '',
            $countOnly
        );

        return $customers;
    }

    public function getJsonData()
    {
        $data = array(
            'creator_id' => (int)$this->uid,
            'company_id' => (int)$this->cid,
            'id' => (int)$this->id,
            'code' => (string)$this->code,
            'name' => (string)$this->name,
            'color' => (string)$this->color,
            'description' => (string)$this->description,
            'count_customer' => (int)$this->countcustomer,
            'customers' => $this->customers,
            'ip_address' => (string)$this->ipaddress,
            'date_created' => (int)$this->datecreated,
            'date_modified' => (int)$this->datemodified,
            'date_last_fetched' => (int)$this->datelastfetched
        );

        return $data;
    }

    public function getDataByJson($jsonData)
    {
        $this->id = (int)$jsonData['id'];
        $this->cid = (int)$jsonData['company_id'];
        $this->uid = (int)$jsonData['creator_id'];
        $this->code = (string)$jsonData['code'];
        $this->name = (string)$jsonData['name'];
        $this->color = (string)$jsonData['color'];
        $this->description = (string)$jsonData['description'];
        $this->countcustomer = (int)$jsonData['count_customer'];
        $this->ipaddress = (int)$jsonData['ip_address'];
        $this->datecreated = (int)$jsonData['date_created'];
        $this->datemodifed = (int)$jsonData['date_modified'];
        $this->datelastfetched = (int)$jsonData['date_last_fetched'];

        if (isset($jsonData['customers'])) {
            $this->customers = $jsonData['customers'];
        }
    }
}
