<?php

namespace Teamcrop\Rest;

class Company extends Base
{
    public static $serviceurl = '/v1/companies';

    public $uid = 0;
    public $id = 0;
    public $domain = '';
    public $screenname = '';
    public $name = '';
    public $description = '';
    public $colorscheme = '';
    public $email = '';
    public $phone = '';
    public $website = '';
    public $address = '';
    public $isdeleted = 0;
    public $ipaddress = 0;
    public $datecreated = 0;
    public $datemodified = 0;
    public $datedeleted = 0;

    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData()
    {

    }

    public function updateData()
    {

    }


    public function delete()
    {

    }

    public function getDataFromScreenname($screenname)
    {
        $items = self::getCompanies(array('screenname' => $screenname), '', '', 1);
        if (count($items) > 0) {
            $this->copy($items[0]);
        }
    }

    public static function getCompanies($formData, $sortby = '', $sorttype = '', $limitString = '', $countOnly = false)
    {
        return self::getItems($formData, $sortby, $sorttype, $limitString, $countOnly);
    }

    public function getDataByJson($jsonData)
    {
        $this->uid = (int)$jsonData['user_id'];
        $this->id = (int)$jsonData['id'];
        $this->name = (string)$jsonData['name'];
        $this->description = (string)$jsonData['description'];
        $this->domain = (string)$jsonData['domain'];
        $this->screenname = (string)$jsonData['screenname'];
        $this->colorscheme = (string)$jsonData['color_scheme'];
        $this->email = (string)$jsonData['email'];
        $this->phone = (string)$jsonData['phone'];
        $this->website = (string)$jsonData['website'];
        $this->address = (string)$jsonData['address'];
        $this->isdeleted = (int)$jsonData['is_deleted'];
        $this->ipaddress = (string)$jsonData['ip_address'];
        $this->datecreated = (int)$jsonData['date_created'];
        $this->datemodified = (int)$jsonData['date_modified'];
        $this->datedeleted = (int)$jsonData['date_deleted'];
    }
}
