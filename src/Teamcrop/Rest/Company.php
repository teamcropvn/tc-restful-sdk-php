<?php

namespace Teamcrop\Rest;

use Teamcrop\Utility\CompanyModule;

class Company extends Base
{
    public static $serviceurl = '/v1/companies';

    const PAYING_TYPE_MOTHLY_CREDIT = 0;
    const PAYING_TYPE_CONTRACT = 1;

    const PLANOFFICE_FREE = 0;
    const PLANOFFICE_PRO = 1;

    const PLANSTORE_NONE = 0;
    const PLANSTORE_FREE = 1;
    const PLANSTORE_PRO = 3;

    const TRIALSOURCE_REGISTER = 1;
    const TRIALSOURCE_SUGGEST = 3;
    const TRIALSOURCE_ADMIN = 5;

    public $uid = 0;
    public $id = 0;
    public $domain = '';
    public $screenname = '';
    public $apikey = '';
    public $apipass = '';
    public $name = '';
    public $description = '';
    public $colorscheme = '';
    public $email = '';
    public $phone = '';
    public $website = '';
    public $address = '';
    public $region = 0;
    public $moduleenable = array();
    public $modulebeta = array();
    public $maxstore = 1;
    public $setting = array();
    public $planoffice = 0;
    public $planstore = 0;
    public $payingtype = 0;
    public $istrial = 0;
    public $trialsource = 0;
    public $crmemailenable = 0;
    public $crmsmsenable = 0;
    public $crmzaloenable = 0;
    public $crmvoiceenable = 0;
    public $referreruserid = 0;
    public $referrercommissionpercent = 0;
    public $allowtogglefeature = 0;
    public $tccustomerid = 0;
    public $price = '';
    public $isdeleted = 0;
    public $ipaddress = 0;
    public $datecreated = 0;
    public $datemodified = 0;
    public $datedeleted = 0;
    public $datetrialexpired = 0;

    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            $this->getData($id, $loadFromCache);
        }
    }




    public function addData(&$error = array())
    {
        $data = array(
            'user_id' => $this->uid,
            'screenname' => $this->screenname,
            'name' => $this->name,
            'description' => $this->description,
            'color_scheme' => $this->colorscheme,
            'email' => $this->email,
            'phone' => $this->phone,
            'website' => $this->website,
            'address' => $this->address,
            'region_id' => $this->region,
            'module_enable' => $this->moduleenable,
            'module_beta' => $this->modulebeta,
            'max_store' => $this->maxstore,
            'plan_office' => $this->planoffice,
            'plan_store' => $this->planstore,
            'paying_type' => $this->payingtype,
            'is_trial' => $this->istrial,
            'trial_source' => $this->trialsource,
            'referrer_user_id' => $this->referreruserid,
            'referrer_commission_percent' => $this->referrercommissionpercent,
            'allow_toggle_feature' => $this->allowtogglefeature,
            'tc_customer_id' => $this->allowtogglefeature,
            'price' => $this->price,
            'date_trial_expired' => $this->datetrialexpired,
        );

        $this->id = self::doAdd($data, '', '', $error);

        return $this->id;
    }

    public function updateData(&$error = array())
    {
        $data = array(
            'user_id' => $this->uid,
            'screenname' => $this->screenname,
            'name' => $this->name,
            'description' => $this->description,
            'color_scheme' => $this->colorscheme,
            'email' => $this->email,
            'phone' => $this->phone,
            'website' => $this->website,
            'address' => $this->address,
            'region_id' => $this->region,
            'module_enable' => $this->moduleenable,
            'module_beta' => $this->modulebeta,
            'max_store' => $this->maxstore,
            'plan_office' => $this->planoffice,
            'plan_store' => $this->planstore,
            'paying_type' => $this->payingtype,
            'is_trial' => $this->istrial,
            'trial_source' => $this->trialsource,
            'referrer_user_id' => $this->referreruserid,
            'referrer_commission_percent' => $this->referrercommissionpercent,
            'allow_toggle_feature' => $this->allowtogglefeature,
            'tc_customer_id' => $this->allowtogglefeature,
            'price' => $this->price,
            'date_trial_expired' => $this->datetrialexpired,
        );

        $url = self::$serviceurl . '/' . $this->id;
        return self::doUpdate($data, $url, '', $error);
    }

    public function delete(&$error = array())
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }

    public function getDataFromScreenname($screenname)
    {
        $items = self::getCompanies(array('screenname' => $screenname), '', '', 1);
        if (count($items) > 0) {
            $this->copy($items[0]);
        }
    }

    public static function getCompanies($formData, $sortby = '', $sorttype = '', $limitString = '', $countOnly = false)
    {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }
    }

    public function getJsonData($getApiInfo = false)
    {
        $data = array(
            'user_id' => (int)$this->uid,
            'id' => (int)$this->id,
            'name' => (string)$this->name,
            'description' => (string)$this->description,
            'domain' => (string)$this->domain,
            'screenname' => (string)$this->screenname,
            'color_scheme' => (string)$this->colorscheme,
            'email' => (string)$this->email,
            'phone' => (string)$this->phone,
            'website' => (string)$this->website,
            'module_enable' => $this->moduleenable,
            'module_beta' => $this->modulebeta,
            'module_disabled' => $this->moduledisabled,
            'module_group_disabled' => $this->modulegroupdisabled,
            'max_store' => (int)$this->maxstore,
            'plan_office' => (int)$this->planoffice,
            'plan_store' => (int)$this->planstore,
            'paying_type' => (int)$this->payingtype,
            'is_trial' => (int)$this->istrial,
            'trial_source' => (int)$this->trialsource,
            'crm_email_enable' => (int)$this->crmemailenable,
            'crm_email_available' => (int)$this->crmemailavailable,
            'crm_sms_enable' => (int)$this->crmsmsenable,
            'crm_zalo_enable' => (int)$this->crmzaloenable,
            'crm_voice_enable' => (int)$this->crmvoiceenable,
            'referrer_user_id' => (int)$this->referreruserid,
            'referrer_commission_percent' => (float)$this->referrercommissionpercent,
            'allow_toggle_feature' => (int)$this->allowtogglefeature,
            'tc_customer_id' => (int)$this->tccustomerid,
            'price' => (string)$this->price,
            'address' => (string)$this->address,
            'is_deleted' => (int)$this->isdeleted,
            'ip_address' => (string)$this->ipaddress,
            'date_created' => (int)$this->datecreated,
            'date_modified' => (int)$this->datemodified,
            'date_deleted' => (int)$this->datedeleted,
            'date_trial_expired' => (int)$this->datetrialexpired,
        );

        if ($getApiInfo) {
            $data['apikey'] = (string)$this->apikey;
            $data['apipass'] = (string)$this->apipass;

            $data['crm_email_setting'] = $this->crmemailsetting;
            $data['crm_sms_setting'] = $this->crmsmssetting;
            $data['crm_zalo_setting'] = $this->crmzalosetting;
            $data['crm_voice_setting'] = $this->crmvoicesetting;
        }

        return $data;
    }

    public function getDataByJson($jsonData)
    {
        $this->uid = (int)$jsonData['user_id'];
        $this->id = (int)$jsonData['id'];
        $this->name = (string)$jsonData['name'];
        $this->description = (string)$jsonData['description'];
        $this->domain = (string)$jsonData['domain'];
        $this->screenname = (string)$jsonData['screenname'];
        $this->apikey = (string)$jsonData['apikey'];
        $this->apipass = (string)$jsonData['apipass'];
        $this->colorscheme = (string)$jsonData['color_scheme'];
        $this->email = (string)$jsonData['email'];
        $this->phone = (string)$jsonData['phone'];
        $this->website = (string)$jsonData['website'];
        $this->address = (string)$jsonData['address'];
        $this->moduleenable = $jsonData['module_enable'];
        $this->modulebeta = $jsonData['module_beta'];
        $this->moduledisabled = $jsonData['module_disabled'];
        $this->modulegroupdisabled = $jsonData['module_group_disabled'];
        $this->maxstore = (int)$jsonData['max_store'];
        $this->planoffice = (int)$jsonData['plan_office'];
        $this->planstore = (int)$jsonData['plan_store'];
        $this->payingtype = (int)$jsonData['paying_type'];
        $this->istrial = (int)$jsonData['is_trial'];
        $this->trialsource = (int)$jsonData['trial_source'];
        $this->crmemailenable = (int)$jsonData['crm_email_enable'];
        $this->crmemailsetting = $jsonData['crm_email_setting'];
        $this->crmemailavailable = (int)$jsonData['crm_email_available'];
        $this->crmsmsenable = (int)$jsonData['crm_sms_enable'];
        $this->crmsmssetting = $jsonData['crm_sms_setting'];
        $this->crmzaloenable = (int)$jsonData['crm_zalo_enable'];
        $this->crmzalosetting = $jsonData['crm_zalo_setting'];
        $this->crmvoiceenable = (int)$jsonData['crm_voice_enable'];
        $this->crmvoicesetting = $jsonData['crm_voice_setting'];
        $this->referreruserid = (int)$jsonData['referrer_user_id'];
        $this->referrercommissionpercent = (float)$jsonData['referrer_commission_percent'];
        $this->allowtogglefeature = (int)$jsonData['allow_toggle_feature'];
        $this->tccustomerid = (int)$jsonData['tc_customer_id'];
        $this->price = (string)$jsonData['price'];
        $this->isdeleted = (int)$jsonData['is_deleted'];
        $this->ipaddress = (string)$jsonData['ip_address'];
        $this->datecreated = (int)$jsonData['date_created'];
        $this->datemodified = (int)$jsonData['date_modified'];
        $this->datedeleted = (int)$jsonData['date_deleted'];
        $this->datetrialexpired = (int)$jsonData['date_trial_expired'];
    }

    public static function cacheKey($id)
    {
        return 'tc_company_' . (int)$id;
    }

    /**
     * After fetch data from database, call this method to populate value to properties
     * @param  array $row Data fetched from database
     * @return void      No return value
     */
    public function getDataByArrayCache($row)
    {
        $this->uid = (int)$row['u_id'];
        $this->id = (int)$row['c_id'];
        $this->domain = (string)$row['c_domain'];
        $this->screenname = (string)$row['c_screenname'];
        $this->apikey = (string)$row['c_apikey'];
        $this->apipass = (string)$row['c_apipass'];
        $this->name = (string)$row['c_name'];
        $this->description = (string)$row['c_description'];
        $this->colorscheme = (string)$row['c_colorscheme'];
        $this->email = (string)$row['c_email'];
        $this->phone = (string)$row['c_phone'];
        $this->website = (string)$row['c_website'];
        $this->address = (string)$row['c_address'];
        $this->region = (int)$row['c_region'];
        $this->moduleenable = $this->decodeModuleList($row['c_moduleenable']);
        $this->modulebeta = $this->decodeModuleList($row['c_modulebeta']);
        $this->moduledisabled = $this->decodeModuleList($row['c_moduledisabled']);
        $this->modulegroupdisabled = $this->decodeModuleList($row['c_modulegroupdisabled']);
        $this->maxstore = (int)$row['c_maxstore'];
        $this->setting = json_decode($row['c_setting'], true);
        $this->planoffice = (int)$row['c_planoffice'];
        $this->planstore = (int)$row['c_planstore'];
        $this->payingtype = (int)$row['c_payingtype'];
        $this->istrial = (int)$row['c_istrial'];
        $this->trialsource = (int)$row['c_trialsource'];
        $this->crmemailenable = (int)$row['c_crmemailenable'];
        $this->crmemailsetting = json_decode($row['c_crmemailsetting'], true);
        $this->crmemailavailable = (int)$row['c_crmemailavailable'];
        $this->crmsmsenable = (int)$row['c_crmsmsenable'];
        $this->crmsmssetting = json_decode($row['c_crmsmssetting'], true);
        $this->crmzaloenable = (int)$row['c_crmzaloenable'];
        $this->crmzalosetting = json_decode($row['c_crmzalosetting'], true);
        $this->crmvoiceenable = (int)$row['c_crmvoiceenable'];
        $this->crmvoicesetting = json_decode($row['c_crmvoicesetting'], true);
        $this->referreruserid = (int)$row['c_referreruserid'];
        $this->referrercommissionpercent = (float)$row['c_referrercommissionpercent'];
        $this->allowtogglefeature = (int)$row['c_allowtogglefeature'];
        $this->tccustomerid = (int)$row['c_tccustomerid'];
        $this->price = (string)$row['c_price'];
        $this->isdeleted = (int)$row['c_isdeleted'];
        $this->ipaddress = (string)long2ip($row['c_ipaddress']);
        $this->datecreated = (int)$row['c_datecreated'];
        $this->datemodified = (int)$row['c_datemodified'];
        $this->datedeleted = (int)$row['c_datedeleted'];
        $this->datetrialexpired = (int)$row['c_datetrialexpired'];

    }

    /**
     * Convert module from string to array
     *
     * @param string $moduleString
     * @return array
     */
    private function decodeModuleList($moduleString)
    {
        $modulesTmp = explode(',', $moduleString);
        $modules = array();
        foreach ($modulesTmp as $module) {
            $modules[] = (int)trim($module);
        }
        return $modules;
    }

    /**
     * Get all companies with store plan
     *
     * @param $error
     * @return null
     */
    public static function getCompanyWithStore(&$error)
    {
        $responseData = null;

        $serviceurl = self::$serviceurl . '/store';
        try {
            //Do request and get response with submit data
            $response = self::doRequest('GET', $serviceurl);

            //request success
            if ($response['status'] == '200') {
                $responseData = $response['data'];
            } else {
                $error = self::parsingErrorFromResponse($response);
            }
        } catch (\Exception $e) {
            $error[] = $e->getMessage();
        }

        return $responseData;
    }

    /**
     * Get all companies with store plan
     *
     * @param $error
     * @return null
     */
    public static function getAvailableIds(&$error)
    {
        $responseData = null;

        $serviceurl = self::$serviceurl . '/availableids';
        try {
            //Do request and get response with submit data
            $response = self::doRequest('GET', $serviceurl);

            //request success
            if ($response['status'] == '200') {
                $responseData = $response['data']['items'];
            } else {
                $error = self::parsingErrorFromResponse($response);
            }
        } catch (\Exception $e) {
            $error[] = $e->getMessage();
        }

        return $responseData;
    }

    /**
     * Check if planstore name is correct with input planstore from registry
     * @param $name, must be 'none', 'free', 'pro'
     * @param null $planstoreId
     * @return bool
     */
    public static function checkPlanstore($name, $planstoreId = null)
    {
        global $registry;

        $pass = false;

        $name = strtolower($name);

        //get default planstore from registry
        if ($planstoreId === null) {
            $planstoreId = $registry->get('planstore');
        }

        if (($name == 'none' && $planstoreId == self::PLANSTORE_NONE)
            || ($name == 'free' || $planstoreId == self::PLANSTORE_FREE)
            || ($name == 'pro' || $planstoreId == self::PLANSTORE_PRO)
        ) {
            $pass = true;
        }

        return $pass;
    }

    /**
     * Check if this company is still paying our services (not trial and not expire)
     *
     * @param $companyId
     * @return bool
     */
    public static function isPaying($companyId)
    {
        $pass = true;
        $myObj = new self($companyId, true);
        if ($myObj->istrial == 1 && $myObj->datetrialexpired < time()) {
            $pass = false;
        }

        return $pass;
    }

    /**
     * Check if this company enable a specific module
     *
     * @param int $companyId
     * @param string $module
     * @return bool
     */
    public static function hasModule($companyId, $module)
    {
        //convert moduletext to moduleid
        $moduleId = CompanyModule::textToId($module);

        $myCompany = new self($companyId, true);

        if ($moduleId > 0 && $myCompany->id > 0
            && is_array($myCompany->moduleenable) && in_array($moduleId, $myCompany->moduleenable)) {
            $pass = true;
        } else {
            $pass = false;
        }

        return $pass;
    }
}
