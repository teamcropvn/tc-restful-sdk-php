<?php

namespace Teamcrop\Rest;

class BillingTransaction extends Base
{
    public static $serviceurl = '/v1/billingtransactions';

    const  METHOD_MANUAL = 1;
    const METHOD_PAYMENTGATEWAY = 3;

    const TYPE_RECHARGE = 1;
    const TYPE_FEE_DAILY = 3;
    const TYPE_FEE_MONTHLY = 5;
    const TYPE_FEE_SUPPORT = 7;
    const TYPE_GIFT_POINT = 9;
    const TYPE_TRANFER = 11;
    const TYPE_RECEIVE = 13;
    const TYPE_REFUND = 15;
    const TYPE_PROMOTION = 17;
    const TYPE_OTHER = 19;

    const STATUS_ENABLE = 1;
    const STATUS_DISABLE = 3;

    public $cid = 0;
    public $uid = 0;
    public $id = 0;
    public $method = 0;
    public $type = 0;
    public $amount = '';
    public $credit = '';
    public $transactionid = '';
    public $transactiondetail = '';
    public $status = 0;
    public $note = '';
    public $adminnote = '';
    public $ipaddress = 0;
    public $useragent = '';
    public $datecreated = 0;
    public $datemodified = 0;

    public $billinginvoiceid = 0;
    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData(&$error = array())
    {
        $row['company_id'] = (int)$this->cid;
        $row['creator_id'] = (int)$this->uid;
        $row['method'] = (int)$this->method;
        $row['type'] = (int)$this->type;
        $row['amount'] = (string)$this->amount;
        $row['credit'] = (string)$this->credit;
        $row['transaction_id'] = (string)$this->transactionid;
        $row['transaction_detail'] = (string)$this->transactiondetail;
        $row['status'] = (int)$this->status;
        $row['note'] = (string)$this->note;
        $row['admin_note'] = (string)$this->adminnote;
        $row['ip_address'] = (string) $this->ipaddress;
        $row['user_agent'] = (string)$this->useragent;

        $this->id = self::doAdd($row, '', '', $error);

        return $this->id;
    }

    public function updateData(&$error = array())
    {
        $row['company_id'] = (int)$this->cid;
        $row['creator_id'] = (int)$this->uid;
        $row['id'] = (int)$this->id;
        $row['method'] = (int)$this->method;
        $row['type'] = (int)$this->type;
        $row['amount'] = (string)$this->amount;
        $row['credit'] = (string)$this->credit;
        $row['transaction_id'] = (string)$this->transactionid;
        $row['billing_invoice_id'] = (string)$this->billinginvoiceid;
        $row['transaction_detail'] = (string)$this->transactiondetail;
        $row['status'] = (int)$this->status;
        $row['note'] = (string)$this->note;
        $row['admin_note'] = (string)$this->adminnote;
        $row['ip_address'] = (string) $this->ipaddress;
        $row['user_agent'] = (string)$this->useragent;

        $url = self::$serviceurl . '/' . $this->id;
        return self::doUpdate($row, $url, '', $error);
    }

    public function delete(&$error = array())
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }

    public static function getGroups(
        $formData,
        $sortby = '',
        $sorttype = '',
        $limitString = '',
        $countOnly = false
    ) {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }
    }


    public function getDataByJson($row)
    {
        $this->cid = (int)$row['company_id'];
        $this->uid = (int)$row['creator_id'];
        $this->id = (int)$row['id'];
        $this->method = (int)$row['method'];
        $this->type = (int)$row['type'];
        $this->amount = (string)$row['amount'];
        $this->credit = (string)$row['credit'];
        $this->billinginvoiceid = (string)$row['billing_invoice_id'];
        $this->transactionid = (string)$row['transaction_id'];
        $this->transactiondetail = (string)$row['transaction_detail'];
        $this->status = (int)$row['status'];
        $this->note = (string)$row['note'];
        $this->adminnote = (string)$row['admin_note'];
        $this->ipaddress = (string) $row['ip_address'];
        $this->useragent = (string)$row['user_agent'];
        $this->datecreated = (int)$row['date_created'];
        $this->datemodified = (int)$row['date_modified'];
    }

    public function getJsonData()
    {
        $row['company_id'] = (int)$this->cid;
        $row['creator_id'] = (int)$this->uid;
        $row['id'] = (int)$this->id;
        $row['method'] = (int)$this->method;
        $row['type'] = (int)$this->type;
        $row['credit'] = (int)$this->credit;
        $row['amount'] = (string)$this->amount;
        $row['transaction_id'] = (string)$this->transactionid;
        $row['transaction_detail'] = (string)$this->transactiondetail;
        $row['status'] = (int)$this->status;
        $row['note'] = (string)$this->note;
        $row['admin_note'] = (string)$this->adminnote;
        $row['ip_address'] = (string) $this->ipaddress;
        $row['user_agent'] = (string)$this->useragent;
        $row['date_created'] = (int)$this->datecreated;
        $row['date_modified'] = (int)$this->datemodified;

        return $row;
    }



}
