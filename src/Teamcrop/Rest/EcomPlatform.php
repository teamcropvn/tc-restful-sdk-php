<?php

namespace Teamcrop\Rest;

class EcomPlatform extends Base
{
    public static $serviceurl = '/v1/ecomplatforms';

    const TYPE_HARAVAN = 1;
    const TYPE_BIZWEB = 3;
    const TYPE_SHOPIFY = 5;
    const TYPE_WOOCOMMERCE = 7;
    const TYPE_FACEBOOK = 9;
    const TYPE_TEAMCROP_LIVECHAT = 11;
    //onbehalf usage via Teamcrop App on zalo
    //via accesstoken with oauth process
    const TYPE_ZALO = 13;

    //normal OA api key access
    const TYPE_ZALO_PAGE = 15;
    const TYPE_CUSTOM = 100;

    const STATUS_ENABLE = 1;
    const STATUS_DISABLED = 3;

    public $cid = 0;
    public $uid = 0;
    public $id = 0;
    public $title = '';
    public $url = '';
    public $type = 0;
    public $apikey = '';
    public $apisecret = '';
    public $accesstoken = '';
    public $store = 0;
    public $orderorigin = 0;
    public $settings = array();
    public $status = 0;
    public $datecreated = 0;
    public $datemodified = 0;
    public $datelastsynced = 0;
    public $datelasthooked = 0;

    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData(&$error = array())
    {
        $data = array(
            'creator_id' => $this->uid,
            'company_id' => $this->cid,
            'id' => $this->id,
            'title' => $this->title,
            'url' => $this->url,
            'type' => $this->type,
            'api_key' => $this->apikey,
            'api_secret' => $this->apisecret,
            'access_token' => $this->accesstoken,
            'store' => $this->store,
            'order_origin' => $this->orderorigin,
            'settings' => $this->settings,
            'status' => $this->status

        );

        $this->id = self::doAdd($data, '', '', $error);

        return $this->id;
    }

    public function updateData(&$error = array())
    {
        $data = array(
            'creator_id' => $this->uid,
            'company_id' => $this->cid,
            'id' => $this->id,
            'title' => $this->title,
            'url' => $this->url,
            'type' => $this->type,
            'api_key' => $this->apikey,
            'api_secret' => $this->apisecret,
            'access_token' => $this->accesstoken,
            'store' => $this->store,
            'order_origin' => $this->orderorigin,
            'settings' => $this->settings,
            'status' => $this->status

        );

        $url = self::$serviceurl . '/' . $this->id;
        return self::doUpdate($data, $url, '', $error);
    }

    public function delete(&$error = array())
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }

    public static function getEcomPlatforms(
        $formData,
        $sortby = '',
        $sorttype = '',
        $limitString = '',
        $countOnly = false
    )
    {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }
    }

    public function getDataByJson($row)
    {
        $this->cid = (int)$row['company_id'];
        $this->uid = (int)$row['creator_id'];
        $this->id = (int)$row['id'];
        $this->title = (string)$row['title'];
        $this->url = (string)$row['url'];
        $this->type = (int)$row['type'];
        $this->apikey = (string)$row['api_key'];
        $this->apisecret = (string)$row['api_secret'];
        $this->accesstoken = (string)$row['access_token'];
        $this->store = (int)$row['store'];
        $this->orderorigin = (int)$row['order_origin'];
        $this->settings = $row['settings'];
        $this->status = (int)$row['status'];
        $this->datecreated = (int)$row['date_created'];
        $this->datemodified = (int)$row['date_modified'];
        $this->datelastsynced = (int)$row['date_last_synced'];
        $this->datelasthooked = (int)$row['date_last_hooked'];
    }

    public function getJsonData()
    {
        $data = array(
            'creator_id' => (int)$this->uid,
            'company_id' => (int)$this->cid,
            'id' => (int)$this->id,
            'title' => (string)$this->title,
            "url" => (string)$this->url,
            "type" => (int)$this->type,
            "api_key" => (string)$this->apikey,
            "api_secret" => (string)$this->apisecret,
            "access_token" => (string)$this->accesstoken,
            "store" => (int)$this->store,
            "order_origin" => (int)$this->orderorigin,
            "settings" => (array)$this->settings,
            "status" => (int)$this->status,
            "date_created" => (int)$this->datecreated,
            "date_modified" => (int)$this->datemodified,
            "date_last_synced" => (int)$this->datelastsynced,
            "date_last_hooked" => (int)$this->datelasthooked,
        );

        return $data;
    }

    public static function getTypeList()
    {
        $output = array();

        $output[self::TYPE_HARAVAN] = 'Haravan';
        $output[self::TYPE_BIZWEB] = 'Bizweb';
        $output[self::TYPE_SHOPIFY] = 'Shopify';
        $output[self::TYPE_WOOCOMMERCE] = 'Woocommerce';
        $output[self::TYPE_FACEBOOK] = 'Facebook';
        $output[self::TYPE_TEAMCROP_LIVECHAT] = 'Teamcrop Live Chat';
        $output[self::TYPE_CUSTOM] = 'Custom';

        return $output;
    }


    public static function getStatusList()
    {
        $output = array();

        $output[self::STATUS_ENABLE] = 'Enable';
        $output[self::STATUS_DISABLED] = 'Disabled';

        return $output;
    }
}
