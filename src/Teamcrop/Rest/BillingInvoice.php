<?php

namespace Teamcrop\Rest;

class BillingInvoice extends Base
{
    public static $serviceurl = '/v1/billinginvoices';

    const TYPE_ENABLE = 1;
    const TYPE_DISABLE = 3;

    const PAID_ENABLE = 1;
    const PAID_DISABLE = 3;

    const STATUS_ENABLE = 1;
    const STATUS_DISABLE = 3;

    public $cid = 0;
    public $uid = 0;
    public $btid = 0;
    public $id = 0;
    public $pricesell = '';
    public $priceexternal = '';
    public $priceexternalexplain = '';
    public $pricediscount = '';
    public $pricetax = '';
    public $pricefinal = '';
    public $discounttype = 0;
    public $discountvalueperstore = '';
    public $commissionpercent = '';
    public $commissionamount = '';
    public $ym = 0;
    public $paidstatus = 0;
    public $status = 0;
    public $ipaddress = 0;
    public $fileidlist = '';
    public $datecreated = 0;
    public $datemodified = 0;
    public $dateexpired = 0;

    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData(&$error = array())
    {
        $row = $this->getJsonData();
        $this->id = self::doAdd($row, '', '', $error);

        return $this->id;
    }

    public function updateData(&$error = array())
    {
        $row = $this->getJsonData();
        $url = self::$serviceurl . '/' . $this->id;
        return self::doUpdate($row, $url, '', $error);
    }

    public function delete(&$error = array())
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }

    public static function getGroups(
        $formData,
        $sortby = '',
        $sorttype = '',
        $limitString = '',
        $countOnly = false
    ) {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }
    }


    public function getDataByJson($row)
    {
        $this->cid = (int)$row['company_id'];
        $this->uid = (int)$row['creator_id'];
        $this->btid = (int)$row['billing_transaction_id'];
        $this->id = (int)$row['id'];
        $this->pricesell = (string)$row['price_sell'];
        $this->priceexternal = (string)$row['price_external'];
        $this->priceexternalexplain = (string)$row['price_external_explain'];
        $this->pricediscount = (string)$row['price_discount'];
        $this->pricetax = (string)$row['price_tax'];
        $this->pricefinal = (string)$row['price_final'];
        $this->discounttype = (int)$row['discount_type'];
        $this->discountvalueperstore = (string)$row['discount_valueper_store'];
        $this->commissionpercent = (string)$row['commission_percent'];
        $this->commissionamount = (string)$row['commission_amount'];
        $this->ym = (int)$row['ym'];
        $this->paidstatus = (int)$row['paid_status'];
        $this->status = (int)$row['status'];
        $this->ipaddress = (string)$row['ip_address'];
        $this->fileidlist = (string)$row['fileid_list'];
        $this->datecreated = (int)$row['date_created'];
        $this->datemodified = (int)$row['date_modified'];
        $this->dateexpired = (int)$row['date_expired'];
    }

    public function getJsonData()
    {
        $row['company_id'] = (int)$this->cid;
        $row['creator_id'] = (int)$this->uid;
        $row['billing_transaction_id'] = (int)$this->btid;
        $row['id'] = (int)$this->id;
        $row['price_sell'] = (string)$this->pricesell;
        $row['price_external'] = (string)$this->priceexternal;
        $row['price_external_explain'] = (string)$this->priceexternalexplain;
        $row['price_discount'] = (string)$this->pricediscount;
        $row['price_tax'] = (string)$this->pricetax;
        $row['price_final'] = (string)$this->pricefinal;
        $row['discount_type'] = (int)$this->discounttype;
        $row['discount_valueper_store'] = (string)$this->discountvalueperstore;
        $row['commission_percent'] = (string)$this->commissionpercent;
        $row['commission_amount'] = (string)$this->commissionamount;
        $row['ym'] = (int)$this->ym;
        $row['paid_status'] = (int)$this->paidstatus;
        $row['status'] = (int)$this->status;
        $row['ip_address'] = (string)$this->ipaddress;
        $row['fileid_list'] = (string)$this->fileidlist;
        $row['date_created'] = (int)$this->datecreated;
        $row['date_modified'] = (int)$this->datemodified;
        $row['date_expired'] = (int)$this->dateexpired;

        return $row;
    }


}
