<?php

namespace Teamcrop\Rest;

class LogAudit extends Base
{
    public static $serviceurl = '/v1/logaudits';

    const METHOD_MANUAL = 1;
    const METHOD_API = 3;

    const SEVERITY_LOW = 1;
    const SEVERITY_MEDIUM = 3;
    const SEVERITY_HIGH = 5;
    const SEVERITY_CRITICAL = 7;

    public $cid = 0;
    public $uid = 0;
    public $id = 0;
    public $method = 0;
    public $severity = 0;
    public $controller = '';
    public $action = '';
    public $objectid = 0;
    public $tag = '';
    public $moredata = '';
    public $needresponse = 0;
    public $ipaddress = 0;
    public $datecreated = 0;
    public $datemodified = 0;

    public function __construct($id = 0, $loadFromCache = false)
    {
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }


    public function addData(&$error = array())
    {
        $data = array(
            'creator_id' => $this->uid,
            'company_id' => $this->cid,
            'method' => $this->method,
            'severity' => $this->severity,
            'controller' => $this->controller,
            'action' => $this->action,
            'object_id' => $this->objectid,
            'tag' => $this->tag,
            'ip_address' => Helper::getIpAddress(),
            'date_created' => time()
        );

        $this->id = self::doAdd($data, '', '', $error);

        return $this->id;
    }

    public function updateData(&$error = array())
    {
        $data = array(
            'tag' => $this->tag,
        );

        $url = self::$serviceurl . '/' . $this->id;
        return self::doUpdate($data, $url, '', $error);
    }


    public function delete(&$error = array())
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }

    public static function getLogAudits($formData, $sortby = '', $sorttype = '', $limitString = '', $countOnly = false)
    {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
        	return self::getItems($formData, $sortby, $sorttype, $limitString);
        }
    }

    public function getJsonData()
    {
        $data = array(
            'company_id' => (int)$this->cid,
            'creator_id' => (int)$this->uid,
            'id' => (int)$this->id,
            'method' => (int)$this->method,
            'severity' => (int)$this->severity,
            'controller' => (string)$this->controller,
            'action' => (string)$this->action,
            'object_id' => (int)$this->objectid,
            'tag' => (string)$this->tag,
            'more_data' => $this->moredata,
            'need_response' => (int)$this->needresponse,
            'ip_address' => (string)$this->ipaddress,
            'date_created' => (int)$this->datecreated,
            'date_modified' => (int)$this->datemodified,
        );

        return $data;
    }


    public function getDataByJson($row)
    {
        $this->cid = (int)$row['company_id'];
        $this->uid = (int)$row['creator_id'];
        $this->id = (int)$row['id'];
        $this->method = (int)$row['method'];
        $this->severity = (int)$row['severity'];
        $this->controller = (string)$row['controller'];
        $this->action = (string)$row['action'];
        $this->objectid = (int)$row['object_id'];
        $this->tag = (string)$row['tag'];
        $this->moredata = $row['more_data'];
        $this->needresponse = (int)$row['need_response'];
        $this->ipaddress = (string)$row['ip_address'];
        $this->datecreated = (int)$row['date_created'];
        $this->datemodified = (int)$row['date_modified'];
    }

    public static function getChangeProperties(
        $myOldObject,
        $myNewObject,
        $excludeProperties = array()
    ) {
        $changeProperties = array();

        if (empty($excludeProperties)) {
            $excludeProperties = array(
                'cid',
                'id',
                'datecreated',
                'datemodified'
            );
        }

        foreach ($myNewObject as $property => $value) {
            if (!in_array($property, $excludeProperties)) {
                if (is_null($myOldObject)
                    || !property_exists($myOldObject, $property)
                    || $value != $myOldObject->{$property}) {

                    $oldValue = (string)$myOldObject->{$property};

                    //do not track change on empty value
                    if (!empty($oldValue) || !empty($value)) {
                        $changeProperties[$property] = array(
                            $oldValue, // old value
                            $value // new value
                        );
                    }
                }
            }
        }

        return $changeProperties;
    }
}
