<?php

namespace Teamcrop\Rest;

class ShopifyOrderSync extends Base
{
    public static $serviceurl = '/v1/shopifyordersyncs';

    public function addData()
    {

    }

    public function updateData()
    {

    }

    public function delete()
    {

    }
    public static function initTopic($formData, &$error = array())
    {
        $url = "/v1/shopifyorderwebhooks/inittopic";
        return self::doAdd($formData, $url, array(), $error);

    }

    public static function shopifyOrderSync($formData, &$error = array())
    {
        $url = "/v1/shopifysyncorders/pull";
        return self::doAdd($formData, $url, array(), $error);

    }
    public static function shopifyOrderHook($formData, &$error = array())
    {
        $url = "/v1/shopifysyncorders/hook";
        return self::doAdd($formData, $url, array(), $error);

    }
    /**
     * Get all order from bizweb
     *
     * @param int $companyid
     * @param int $userid
     * @param string $store
     * @param string $accesstoken
     * @param $error
     * @return null
     */
    public static function pull($companyid, $userid, $store, $accesstoken, &$error)
    {
        $responseData = null;

        $data = array(
            'company_id' => $companyid,
            'creator_id' => $userid,
            'store' => $store,
            'code' => $accesstoken
        );

        $serviceurl = self::$serviceurl . '/pull';
        $headers = array(
            'Content-type' => 'application/json',
        );

        try {
            //Do request and get response with submit data
            $response = Base::doRequest('POST', $serviceurl, $headers, false, true, json_encode($data));

            //request success
            if ($response['status'] == '200') {
                $responseData = $response['data'];
            } else {
                $error = Base::parsingErrorFromResponse($response);
            }
        } catch (\Exception $e) {
            $error[] = $e->getMessage();
        }

        return $responseData;
    }
}
