<?php

namespace Teamcrop\Rest;

class ProductPrice extends Base
{
    public static $serviceurl = '/v1/productprices';

    const DIRECTION_INPUT = 5;
    const DIRECTION_OUTPUT = 10;

    const IS_DELETE = 1;

    public $cid = 0;
    public $uid = 0;
    public $pid = 0;
    public $poid = 0;
    public $posku = '';
    public $id = 0;
    public $direction = 0;
    public $outputtype = 0;
    public $pricezone = 0;
    public $warehouse = 0;
    public $price = 0;
    public $note = '';
    public $isdeleted = 0;
    public $isdeletedby = 0;
    public $datecreated = 0;
    public $datemodified = 0;
    public $datedeleted = 0;

    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData(&$error = array())
    {
        $data = array(
            "company_id" => $this->cid,
            "creator_id" => $this->uid,
            "product_id" => $this->pid,
            "variant_id" => $this->poid,
            "sku" => $this->posku,
            "direction" => $this->direction,
            "out_put_type" => $this->outputtype,
            "price_zone" => $this->pricezone,
            "price" => $this->price
        );

        $this->id = self::doAdd($data, '', '', $error);

        return $this->id;
    }

    public function updateData(&$error = array())
    {
        $data = array(
            "company_id" => $this->cid,
            "creator_id" => $this->uid,
            "product_id" => $this->pid,
            "variant_id" => $this->poid,
            "sku" => $this->posku,
            "direction" => $this->direction,
            "out_put_type" => $this->outputtype,
            "price_zone" => $this->pricezone,
            "price" => $this->price
        );

        $url = self::$serviceurl . '/' . $this->id;
        return self::doUpdate($data, $url, '', $error);
    }


    public function delete(&$error = array())
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }

    public static function getPrices($formData, $sortby = '', $sorttype = '', $limitString = '', $countOnly = false)
    {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }
    }

    public function getDataByJson($jsonData)
    {
        $this->cid = (int)$jsonData['company_id'];
        $this->uid = (int)$jsonData['creator_id'];
        $this->pid = (int)$jsonData['product_id'];
        $this->poid = (int)$jsonData['variant_id'];
        $this->posku = (string)$jsonData['sku'];
        $this->id = (int)$jsonData['id'];
        $this->direction = (int)$jsonData['direction'];
        $this->outputtype = (int)$jsonData['out_put_type'];
        $this->pricezone = (int)$jsonData['price_zone'];
        $this->warehouse = (int)$jsonData['ware_house'];
        $this->price = (float)$jsonData['price'];
        $this->note = (string)$jsonData['note'];
        $this->isdeleted = (int)$jsonData['is_deleted'];
        $this->isdeletedby = (int)$jsonData['is_deleted_by'];
        $this->datecreated = (int)$jsonData['date_created'];
        $this->datemodified = (int)$jsonData['date_modified'];
        $this->datedeleted = (int)$jsonData['date_deleted'];


    }
}
