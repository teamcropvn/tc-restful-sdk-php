<?php

namespace Teamcrop\Rest;

class LogActivity extends Base
{
    public static $serviceurl = '/v1/logactivities';

    const SOURCE_MANUAL = 1;
    const SOURCE_AUTOMATIC = 3;

    const PRIORITY_LOW = 1;
    const PRIORITY_MEDIUM = 3;
    const PRIORITY_HIGH = 5;
    const PRIORITY_CRITICAL = 7;

    const STATUS_ENABLE = 1;
    const STATUS_DISABLED = 3;

    public $cid = 0;
    public $uid = 0;
    public $id = 0;
    public $section = '';
    public $action = '';
    public $objectid = 0;
    public $objectsummary = '';
    public $identifier = '';
    public $countcomment = 0;
    public $changelog = array();
    public $note = '';
    public $followerlist = array();
    public $tags = '';
    public $source = 0;
    public $priority = 0;
    public $status = 0;
    public $ipaddress = '';
    public $datecreated = 0;
    public $datemodified = 0;

    public function __construct($id = 0, $loadFromCache = false)
    {
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }


    public function addData(&$error = array())
    {
        $data = array(
            'company_id' => $this->cid,
            'creator_id' => $this->uid,
            'section' => $this->section,
            'action' => $this->action,
            'object_id' => $this->objectid,
            'object_summary' => $this->objectsummary,
            'identifier' => $this->identifier,
            'count_comment' => $this->countcomment,
            'change_log' => $this->changelog,
            'note' => $this->note,
            'follow_list' => $this->followerlist,
            'tags' => $this->tags,
            'source' => $this->source,
            'priority' => $this->priority,
            'status' => $this->status,
            'ip_address' => $this->ipaddress,
        );

        $this->id = self::doAdd($data, '', '', $error);

        return $this->id;
    }

    public function updateData(&$error = array())
    {
        $data = array(
            'creator_id' => $this->uid,
            'company_id' => $this->cid,
            'tags' => $this->tags,
            'note' => $this->note,
        );

        $url = self::$serviceurl . '/' . $this->id;
        return self::doUpdate($data, $url, '', $error);
    }


    public function delete(&$error = array())
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }

    public static function getLogActivities($formData, $sortby = '', $sorttype = '', $limitString = '', $countOnly = false)
    {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
        	return self::getItems($formData, $sortby, $sorttype, $limitString);
        }
    }

    public function getJsonData()
    {
        $data = array(
            'company_id' => (int)$this->cid,
            'creator_id' => (int)$this->uid,
            'id' => (int)$this->id,
            'section' => (string)$this->section,
            'action' => (string)$this->action,
            'object_id' => (int)$this->objectid,
            'object_summary' => (string)$this->objectsummary,
            'identifier' => (string)$this->identifier,
            'count_comment' => (int)$this->countcomment,
            'change_log' => (array)$this->changelog,
            'note' => (string)$this->note,
            'follower_list' => (array)$this->followerlist,
            'tags' => (string)$this->tags,
            'source' => (int)$this->source,
            'priority' => (int)$this->priority,
            'status' => (int)$this->status,
            'ip_address' => (string)$this->ipaddress,
            'date_created' => (int)$this->datecreated,
            'date_modified' => (int)$this->datemodified
        );

        return $data;
    }


    public function getDataByJson($row)
    {
        $this->cid = (int)$row['company_id'];
        $this->uid = (int)$row['creator_id'];
        $this->id = (int)$row['id'];
        $this->section = (string)$row['section'];
        $this->action = (string)$row['action'];
        $this->objectid = (int)$row['object_id'];
        $this->objectsummary = (string)$row['object_summary'];
        $this->identifier = (string)$row['identifier'];
        $this->countcomment = (int)$row['count_comment'];
        $this->changelog = (array)$row['change_log'];
        $this->note = (string)$row['note'];
        $this->followerlist = (array)$row['follow_list'];
        $this->tags = (string)$row['tags'];
        $this->source = (int)$row['source'];
        $this->priority = (int)$row['priority'];
        $this->status = (int)$row['status'];
        $this->ipaddress = (string)$row['ip_address'];
        $this->datecreated = (int)$row['date_created'];
        $this->datemodified = (int)$row['date_modified'];
    }
}
