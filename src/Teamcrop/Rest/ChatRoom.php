<?php

namespace Teamcrop\Rest;

class ChatRoom extends Base
{
    public static $serviceurl = '/v1/chatrooms';

    const TYPE_INTERNAL_PRIVATE = 3;
    const TYPE_INTERNAL_DIRECT = 5;
    const TYPE_BOT = 7;
    const TYPE_FACEBOOK_PAGE = 9;
    const TYPE_FACEBOOK_COMMENT = 10;
    const TYPE_TEAMCROP_LIVECHAT = 11;
    const TYPE_ZALO = 13;


    const SOURCE_MANUAL = 1;
    const SOURCE_AUTO = 3;

    const CURRENCY_VND = 1;

    const STATUS_ENABLE = 1;
    const STATUS_DISABLED = 3;

    public $uid = 0;
    public $cid = 0;
    public $id = 0;
    public $name = '';
    public $customerid = 0;
    public $description = '';
    public $summary = '';
    public $type = 0;
    public $source = 0;
    public $sourceid = '';
    public $identifier = '';
    public $directidentifier = '';
    public $setting = array();
    public $status = 0;
    public $ipaddress = 0;
    public $datecreated = 0;
    public $datemodified = 0;
    public $datelastreplied = 0;

    //store info from member table such as lastviewid, latestchatdataid, countunread
    public $stats = array();
    public $invitedlist = array();

    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData(&$error = array())
    {
        $data = array(
            'creator_id' => $this->uid,
            'company_id' => $this->cid,
            'name' => $this->name,
            'customer_id' => $this->customerid,
            'description' => $this->description,
            'type' => $this->type,
            'source' => $this->source,
            'source_id' => $this->sourceid,
            'title' => $this->title,
            'summary' => $this->summary,
            'setting' => $this->setting,
            'status' => $this->status,
            'invited_userid_list' => $this->invitedlist,
            'message' => $this->message
        );

        $this->id = self::doAdd($data, '', '', $error);

        return $this->id;
    }

    public function updateData(&$error = array())
    {
        $data = array(
            'creator_id' => $this->uid,
            'company_id' => $this->cid,
            'name' => $this->name,
            'customer_id' => $this->customerid,
            'description' => $this->description,
            'setting' => $this->setting,
            'status' => $this->status,
            'date_modified' => $this->datemodified,
        );

        $url = self::$serviceurl . '/' . $this->id;
        return self::doUpdate($data, $url, '', $error);
    }


    public function delete(&$error = array())
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }

    public static function getChatRooms($formData, $sortby = '', $sorttype = '', $limitString = '', $countOnly = false)
    {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }
    }

    /**
     * @return array
     */
    public function getJsonData()
    {
        $data = array(
            'creator_id' => (int)$this->uid,
            'company_id' => (int)$this->cid,
            'id' => (int)$this->id,
            'name' => (string)$this->name,
            'customer_id' => (int)$this->customerid,
            'description' => (string)$this->description,
            'summary' => (string)$this->summary,
            'type' => (int)$this->type,
            'source' => (int)$this->source,
            'source_id' => (string)$this->sourceid,
            'identifier' => (string)$this->identifier,
            'direct_identifier' => (string)$this->directidentifier,
            'setting' => $this->setting,
            'status' => (int)$this->status,
            'ip_address' => (string)$this->ipaddress,
            'date_created' => (int)$this->datecreated,
            'date_modified' => (int)$this->datemodified,
            'date_last_replied' => (int)$this->datelastreplied,
            'stats' => array(
                'last_view_id' => $this->stats['lastviewid'],
                'latest_chat_dat_id' => $this->stats['latestchatdataid'],
                'count_unread' => $this->stats['countunread'],
            )
        );

        return $data;
    }

    public static function cacheKey($chatroomid)
    {
        return 'tc_chat_room_' . $chatroomid;
    }

    public function getDataByArrayCache($row)
    {

    }

    public function getDataByJson($jsonData)
    {
        $this->cid = (int)$jsonData['company_id'];
        $this->uid = (int)$jsonData['creator_id'];
        $this->id = (int)$jsonData['id'];
        $this->name = (string)$jsonData['name'];
        $this->customerid = (int)$jsonData['customer_id'];
        $this->description = (string)$jsonData['description'];
        $this->summary = (string)$jsonData['summary'];
        $this->type = (int)$jsonData['type'];
        $this->source = (int)$jsonData['source'];
        $this->sourceid = (string)$jsonData['source_id'];
        $this->identifier = (string)$jsonData['identifier'];
        $this->directidentifier = (string)$jsonData['direct_identifier'];
        $this->setting = $jsonData['setting'];
        $this->status = (int)$jsonData['status'];
        $this->ipaddress = (string)$jsonData['ip_address'];
        $this->datecreated = (int)$jsonData['date_created'];
        $this->datemodified = (int)$jsonData['date_modified'];
        $this->datelastreplied = (int)$jsonData['date_last_replied'];
        $this->stats = $jsonData['stats'];
    }
}
