<?php

namespace Teamcrop\Rest;

class BillingUsage extends Base
{
    public static $serviceurl = '/v1/billingusages';
    const  STATUS_ENABLE = 1;
    const STATUS_DISABLE = 3;

    public $cid = 0;
    public $uid = 0;
    public $id = 0;
    public $group = 0;
    public $module = 0;
    public $status = 0;
    public $durationinsecond = 0;
    public $ipaddress = 0;
    public $datecreated = 0;
    public $datemodified = 0;

    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData(&$error = array())
    {
        $row['company_id'] = (int)$this->cid;
        $row['creator_id'] = (int)$this->uid;
        $row['group'] = (int)$this->group;
        $row['module'] = (int)$this->module;
        $row['status'] = (int)$this->status;
        $row['duration_in_second'] = (int)$this->durationinsecond;

        $this->id = self::doAdd($row, '', '', $error);

        return $this->id;
    }

    public function updateData(&$error = array())
    {
        $row['company_id'] = (int)$this->cid;
        $row['creator_id'] = (int)$this->uid;
        $row['id'] = (int)$this->id;
        $row['group'] = (int)$this->group;
        $row['module'] = (int)$this->module;
        $row['status'] = (int)$this->status;
        $row['duration_in_second'] = (int)$this->durationinsecond;


        $url = self::$serviceurl . '/' . $this->id;
        return self::doUpdate($row, $url, '', $error);
    }

    public function delete(&$error = array())
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }

    public static function getGroups(
        $formData,
        $sortby = '',
        $sorttype = '',
        $limitString = '',
        $countOnly = false
    ) {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }
    }


    public function getDataByJson($row)
    {
        $this->cid = (int)$row['company_id'];
        $this->uid = (int)$row['creator_id'];
        $this->id = (int)$row['id'];
        $this->group = (int)$row['group'];
        $this->module = (int)$row['module'];
        $this->status = (int)$row['status'];
        $this->durationinsecond = (int)$row['duration_in_second'];
        $this->ipaddress = (string)$row['ip_address'];
        $this->datecreated = (int)$row['date_created'];
        $this->datemodified = (int)$row['date_modified'];
    }

    public function getJsonData()
    {
        $row['company_id'] = (int)$this->cid;
        $row['creator_id'] = (int)$this->uid;
        $row['id'] = (int)$this->id;
        $row['group'] = (int)$this->group;
        $row['module'] = (int)$this->module;
        $row['status'] = (int)$this->status;
        $row['duration_in_second'] = (int)$this->durationinsecond;
        $row['ip_address'] = (string)$this->ipaddress;
        $row['date_created'] = (int)$this->datecreated;
        $row['date_modified'] = (int)$this->datemodified;

        return $row;
    }


}
