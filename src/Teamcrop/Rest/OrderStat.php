<?php

namespace Teamcrop\Rest;

class OrderStat extends Base
{
    public static $serviceurl = '/v1/orderstats';



    public function __construct($id = 0, $loadFromCache = false)
    {
        parent::__construct();


    }

    public function addData()
    {

    }

    public function updateData()
    {

    }


    public function delete()
    {

    }

    public static function getSumOrder($formData)
    {
        return self::getRawItems($formData, self::$serviceurl. "/sum");
    }

    public static function getCountOrder($formData)
    {
        return self::getRawItems($formData, self::$serviceurl. "/count");
    }

    public static function getWeekChart($formData)
    {
        return self::getRawItems($formData, self::$serviceurl. "/weekchart");
    }

    public static function getSingleChart($formData)
    {
        return self::getRawItems($formData, self::$serviceurl. "/singlechart");
    }
}
