<?php

namespace Teamcrop\Rest;

class Customer extends Base
{
    public static $serviceurl = '/v1/customers';

    const SOURCE_INTERNAL = 0;
    const SOURCE_WORDPRESS = 3;
    const SOURCE_MAGENTO = 5;
    const SOURCE_PRESTASHOP = 7;
    const SOURCE_OPENCART = 9;
    const SOURCE_OSCOMMERCE = 11;
    const SOURCE_OTHER = 13;
    const SOURCE_WOOECOMMERCE = 15;

    public $uid = 0;
    public $cid = 0;
    public $cctid = 0;
    public $id = 0;
    public $code = "";
    public $cardid = 0;
    public $origin = 0;
    public $originnote = "";
    public $source = 0;
    public $sourceid = '';
    public $sourceupdatedtime = 0;
    public $fullname = "";
    public $gender = 0;
    public $birthday = "";
    public $email = "";
    public $phone = "";
    public $address = "";
    public $city = "";
    public $zipcode = 0;
    public $region = 0;
    public $subregion = 0;
    public $country = "";
    public $company = "";
    public $note = "";
    public $countorder = 0;
    public $countsku = 0;
    public $countcontact = 0;
    public $point = 0;
    public $money = 0;
    public $status = 0;
    public $image = '';
    public $datecreated = 0;
    public $datemodified = 0;
    public $datefirstordered = 0;
    public $datelastordered = 0;

    //dynamic sync magento
    public $postcode = "";
    public $street = "";
    public $telephone = "";
    //dynamic sync prestashop
    public $firstname = "";
    public $lastname = "";
    public $address1 = "";
    public $address2 = "";

    public $employees = array();
    public $changenote = '';

    public function __construct($id = 0, $loadFromCache = false)
    {
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData(&$error = array(), $moreData = array())
    {
        $data = array(
            'creator_id' => $this->uid,
            'company_id' => $this->cid,
            'customer_id' => $this->id,
            'customer_type_id' => $this->cctid,
            'fullname' => $this->fullname,
            'birthday' => $this->birthday,
            'origin' => $this->customer_origin_id,
            'origin_note' => $this->originnote,
            'address' => $this->address,
            'company' => $this->company,
            'code' => $this->code,
            'region_id' => $this->region,
            'sub_region_id' => $this->subregion,
            'country' => $this->country,
            'email' => $this->email,
            'phone' => $this->phone,
            'gender' => $this->gender,
            'note' => $this->note,
            'status' => $this->customer_status_id,
            'employees' => $this->employees
        );

        $data = array_merge($data, $moreData);
        $this->id = self::doAdd($data, '', '', $error);

        return $this->id;
    }

    public function updateData(&$error = array())
    {
        $data = array(
            'creator_id' => $this->uid,
            'company_id' => $this->cid,
            'customer_type_id' => $this->cctid,
            'fullname' => $this->fullname,
            'birthday' => $this->birthday,
            'origin' => $this->customer_origin_id,
            'origin_note' => $this->originnote,
            'address' => $this->address,
            'company' => $this->company,
            'code' => $this->code,
            'region_id' => $this->region,
            'sub_region_id' => $this->subregion,
            'country' => $this->country,
            'email' => $this->email,
            'phone' => $this->phone,
            'gender' => $this->gender,
            'note' => $this->note,
            'status' => $this->customer_status_id,
            'employees' => $this->employees,
            'change_note' => $this->changenote
        );

        $url = self::$serviceurl . '/' . $this->id;
        return self::doUpdate($data, $url, '', $error);
    }


    public function delete(&$error = array())
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }

    public static function getCustomers($formData, $sortby = '', $sorttype = '', $limitString = '', $countOnly = false)
    {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
        	return self::getItems($formData, $sortby, $sorttype, $limitString);
        }
    }

    public function getJsonData()
    {
        $data = array(
            'creator_id' => (int)$this->uid,
            'company_id' => (int)$this->cid,
            'customer_type_id' => (int)$this->cctid,
            'customer_status_id' => (int)$this->status,
            'customer_origin_id' => (int)$this->origin,
            'id' => (int)$this->id,
            'code' => (string)$this->code,
            'card_id' => (int)$this->cardid,
            'fullname' => (string)$this->fullname,
            'gender' => (int)$this->gender,
            'birthday' => (int)$this->birthday,
            'email' => (string)$this->email,
            'phone' => (string)$this->phone,
            'address' => (string)$this->address,
            'city' => (string)$this->city,
            'zipcode' => (int)$this->zipcode,
            'region_id' => (int)$this->region,
            'sub_region_id' => (int)$this->subregion,
            'country' => (string)$this->country,
            'company' => (string)$this->company,
            'note' => (string)$this->note,
            'count_order' => (int)$this->countorder,
            'count_sku' => (int)$this->countsku,
            'count_contact' => (int)$this->countcontact,
            'point' => (int)$this->point,
            'money' => (float)$this->money,
            'image' => (string)$this->image,
            'origin_note' => (string)$this->originnote,
            'source' => (int)$this->source,
            'source_id' => (string)$this->sourceid,
            'employees' => $this->employees,
            'date_created' => (int)$this->datecreated,
            'date_modified' => (int)$this->datemodified,
            'date_first_ordered' => (int)$this->datefirstordered,
            'date_last_ordered' => (int)$this->datelastordered
        );

        return $data;
    }

    public function getDataByJson($row)
    {
        $this->uid = (int)$row['creator_id'];
        $this->cid = (int)$row['company_id'];
        $this->cctid = (int)$row['customer_type_id'];
        $this->id = (int)$row['id'];
        $this->code = (string)$row['code'];
        $this->cardid = (int)$row['card_id'];
        $this->origin = (int)$row['customer_origin_id'];
        $this->originnote = (string)$row['origin_note'];
        $this->source = (int)$row['source'];
        $this->sourceid = (string)$row['source_'];
        $this->fullname = (string)$row['fullname'];
        $this->gender = (int)$row['gender'];
        $this->birthday = (int)$row['birthday'];
        $this->email = (string)$row['email'];
        $this->phone = (string)$row['phone'];
        $this->address = (string)$row['address'];
        $this->city = (string)$row['city'];
        $this->zipcode = (int)$row['zipcode'];
        $this->region = (int)$row['region_id'];
        $this->subregion = (int)$row['sub_region_id'];
        $this->country = (string)$row['country'];
        $this->company = (string)$row['company'];
        $this->note = (string)$row['note'];
        $this->countorder = (int)$row['count_order'];
        $this->countsku = (int)$row['count_sku'];
        $this->countcontact = (int)$row['count_contact'];
        $this->point = (int)$row['point'];
        $this->money = (float)$row['money'];
        $this->status = (int)$row['customer_status_id'];
        $this->image = (string)$row['image'];
        $this->employees = $row['employees'];
        $this->datecreated = (int)$row['date_created'];
        $this->datemodified = (int)$row['date_modified'];
        $this->datefirstordered = (int)$row['date_first_ordered'];
        $this->datelastordered = (int)$row['date_last_ordered'];
    }
}
