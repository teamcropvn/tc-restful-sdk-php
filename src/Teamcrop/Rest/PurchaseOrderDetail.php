<?php

namespace Teamcrop\Rest;

class PurchaseOrderDetail extends Base
{
    public static $serviceurl = '/v1/purchaseorderdetails';

    public $cid = 0;
    public $uid = 0;
    public $pid = 0;
    public $poid = 0;
    public $posku = '';
    public $spid = 0;
    public $id = 0;
    public $itemname = '';
    public $itemcategoryid = 0;
    public $itemserialnumber = 0;
    public $itemunit = '';
    public $quantity = 0;
    public $itemunitpriceoriginal = 0;
    public $itemunitprice = 0;
    public $promotion = '';
    public $tax = 0;
    public $pricehandling = 0;
    public $subtotal = '';
    public $itemoption = '';
    public $displayorder = 0;
    public $originid = '';
    public $status = 0;
    public $datecreated = 0;
    public $datemodified = 0;
    public function __construct($id = 0, $loadFromCache = false)
    {
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                //$this->copy(self::cacheGet($id));
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData(&$error = array())
    {
        $row['company_id'] = (int)$this->cid;
        $row['creator_id'] = (int)$this->uid;
        $row['product_id'] = (int)$this->pid;
        $row['variant_id'] = (int)$this->poid;
        $row['sku'] = (string)$this->posku;
        $row['purchase_order_id'] = (int)$this->spid;
        $row['id'] = (int)$this->id;
        $row['item_name'] = (string)$this->itemname;
        $row['item_categoryid'] = (int)$this->itemcategoryid;
        $row['item_serial_number'] = (string)$this->itemserialnumber;
        $row['item_unit'] = (string)$this->itemunit;
        $row['quantity'] = $this->quantity;
        $row['item_unit_price_original'] = (float)$this->itemunitpriceoriginal;
        $row['item_unit_price'] = (float)$this->itemunitprice;
        $row['promotion'] = (string)$this->promotion;
        $row['tax'] = (int)$this->tax;
        $row['price_handling'] = (float)$this->pricehandling;
        $row['sub_total'] = (string)$this->subtotal;
        $row['item_option'] = (string)$this->itemoption;
        $row['display_order'] = (int)$this->displayorder;
        $row['origin_id'] = (string)$this->originid;
        $row['status'] = (int)$this->status;
        $row['date_created'] = (int)$this->datecreated;
        $row['date_modified'] = (int)$this->datemodified;
        $row['date_expired'] = (int)$this->dateexpired;

        $this->id = self::doAdd($row, '', '', $error);

        return $this->id;
    }

    public function updateData(&$error = array())
    {
        $data = array(

        );

        $url = self::$serviceurl . '/' . $this->id;
        return self::doUpdate($data, $url, '', $error);
    }


    public function delete(&$error = array())
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }
    public static function getWeight($formData, $id)
    {
        $url = self::$serviceurl . "/weight/" . $id;
        return self::getRawItems($formData, $url);
    }

    public static function getDetail($formData, $sortby = '', $sorttype = '', $limitString = '', $countOnly = false)
    {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }
    }
    public function getDataByJson($row)
    {
        $this->cid = (int)$row['company_id'];
        $this->pid = (int)$row['product_id'];
        $this->poid = (int)$row['variant_id'];
        $this->posku = (string)$row['sku'];
        $this->spid = (int)$row['purchase_order_id'];
        $this->id = (int)$row['id'];
        $this->itemname = (string)$row['item_name'];
        $this->itemcategoryid = (int)$row['item_categoryid'];
        $this->itemserialnumber = (string)$row['item_serial_number'];
        $this->itemunit = (string)$row['item_unit'];
        $this->quantity = $row['quantity'];
        $this->itemunitpriceoriginal = (float)$row['item_unit_price_original'];
        $this->itemunitprice = (float)$row['item_unit_price'];
        $this->promotion = (string)$row['promotion'];
        $this->tax = (int)$row['tax'];
        $this->pricehandling = (float)$row['price_handling'];
        $this->subtotal = (string)$row['sub_total'];
        $this->itemoption = (string)$row['item_option'];
        $this->displayorder = (int)$row['display_order'];
        $this->originid = (string)$row['origin_id'];
        $this->status = (int)$row['status'];
        $this->datecreated = (int)$row['date_created'];
        $this->datemodified = (int)$row['date_modified'];
        $this->dateexpired = (int)$row['date_expired'];
    }
}
