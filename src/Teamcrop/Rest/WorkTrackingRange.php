<?php

namespace Teamcrop\Rest;

class WorkTrackingRange extends Base
{
    public static $serviceurl = '/v1/worktrackingranges';


    const STATUS_ENABLE = 1;
    const STATUS_DISABLED = 3;

    public $cid = 0;
    public $uid = 0;
    public $coid = 0;
    public $id = 0;
    public $name = '';
    public $color = '';
    public $displayorder = 0;
    public $timestart = '';
    public $timeend = '';
    public $ratio = 0;
    public $ignoreduration = "";
    public $status = 0;
    public $datecreated = 0;
    public $datemodified = 0;

    public function __construct($id = 0, $loadFromCache = false)
    {
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                //$this->copy(self::cacheGet($id));
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData(&$error = array())
    {
        $data = array(
            'company_id' => $this->cid,
            'creator_id' => $this->uid,
            'office_id' => $this->coid,
            'name' => $this->name,
            'color' => $this->color,
            'display_order' => $this->displayorder,
            'time_start' => $this->timestart,
            'time_end' => $this->timeend,
            'status' => $this->status,
            'ratio' => $this->ratio
        );

        $this->id = self::doAdd($data, '', '', $error);

        return $this->id;
    }

    public function updateData(&$error = array())
    {
        $data = array(
            'company_id' => $this->cid,
            'creator_id' => $this->uid,
            'office_id' => $this->coid,
            'name' => $this->name,
            'color' => $this->color,
            'display_order' => $this->displayorder,
            'time_start' => $this->timestart,
            'time_end' => $this->timeend,
            'status' => $this->status,
            'ratio' => $this->ratio
        );

        $url = self::$serviceurl . '/' . $this->id;
        return self::doUpdate($data, $url, '', $error);
    }


    public function delete(&$error = array())
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }

    public static function getWorkTrackingRanges(
        $formData,
        $sortby = '',
        $sorttype = '',
        $limitString = '',
        $countOnly = false
    )
    {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
        	return self::getItems($formData, $sortby, $sorttype, $limitString);
        }
    }

    public function getJsonData()
    {
        $data = array(
            'company_id' => (int)$this->cid,
            'creator_id' => (int)$this->uid,
            'office_id' => (int)$this->coid,
            'id' => (int)$this->id,
            'name' => (string)$this->name,
            'color' => (string)$this->color,
            'display_order' => (int)$this->displayorder,
            'time_start' => (string)$this->timestart,
            'time_end' => (string)$this->timeend,
            'status' => (int)$this->status,
            'ratio' => (float)$this->ratio,
            'ignore_duration' => (string)$this->ignoreduration,
            'date_created' => (int)$this->datecreated,
            'date_modified' => (int)$this->datemodified
        );

        return $data;
    }

    public function getDataByJson($row)
    {
        $this->cid = (int)$row['company_id'];
        $this->uid = (int)$row['creator_id'];
        $this->coid = (int)$row['office_id'];
        $this->id = (int)$row['id'];
        $this->name = (string)$row['name'];
        $this->color = (string)$row['color'];
        $this->displayorder = (int)$row['display_order'];
        $this->timestart = (string)$row['time_start'];
        $this->timeend = (string)$row['time_end'];
        $this->status = (float)$row['status'];
        $this->ratio = (string)$row['ratio'];
        $this->ignoreduration = (int)$row['ignore_duration'];
        $this->datecreated = (int)$row['date_created'];
        $this->datemodified = (int)$row['date_modified'];
    }


}
