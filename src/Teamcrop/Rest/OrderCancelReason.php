<?php

namespace Teamcrop\Rest;

class OrderCancelReason extends Base
{
    public static $serviceurl = '/v1/ordercancelreasons';

    const STATUS_ENABLE = 1;
    const STATUS_DISABLE = 3;

    public $cid = 0;
    public $uid = 0;
    public $id = 0;
    public $name = '';
    public $note = '';
    public $status = 0;
    public $displayorder = 0;
    public $datecreated = 0;
    public $datemodified = 0;

    public function __construct($id = 0, $loadFromCache = false)
    {
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                //$this->copy(self::cacheGet($id));
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData()
    {

    }

    public function updateData()
    {

    }


    public function delete()
    {

    }

    public static function getOrders($formData, $sortby = '', $sorttype = '', $limitString = '', $countOnly = false)
    {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }

    }

    public function getDataByJson($jsonData)
    {
        $this->cid = (int)$jsonData['comompany_id'];
        $this->uid = (int)$jsonData['creator_id'];
        $this->id = (int)$jsonData['id'];
        $this->name = (string)$jsonData['name'];
        $this->note = (string)$jsonData['note'];
        $this->status = (int)$jsonData['status'];
        $this->displayorder = (int)$jsonData['display_order'];
        $this->datecreated = (int)$jsonData['date_created'];
        $this->datemodified = (int)$jsonData['date_modified'];

    }
}
