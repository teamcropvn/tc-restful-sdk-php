<?php

namespace Teamcrop\Rest;

class OrderCancelReason extends Base
{
    public static $serviceurl = '/v1/ordercancelreasons';

    const STATUS_ENABLE = 1;
    const STATUS_DISABLE = 3;

    public $cid = 0;
    public $uid = 0;
    public $id = 0;
    public $name = '';
    public $note = '';
    public $status = 0;
    public $displayorder = 0;
    public $datecreated = 0;
    public $datemodified = 0;

    public function __construct($id = 0, $loadFromCache = false)
    {
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                //$this->copy(self::cacheGet($id));
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData()
    {
        $data = array(
            "company_id" => $this->cid,
            "creator_id" => $this->uid,
            "name" => $this->name,
            "status" => $this->status,
            "note" => $this->note,
            "display_order" => $this->displayorder,
        );

        $this->id = self::doAdd($data, '', '', $error);

        return $this->id;
    }

    public function updateData()
    {
        $data = array(
            "company_id" => $this->cid,
            "creator_id" => $this->uid,
            "name" => $this->name,
            "status" => $this->status,
            "note" => $this->note,
            "display_order" => $this->displayorder,
        );
        $url = self::$serviceurl . '/' . $this->id;
        return self::doUpdate($data, $url, '', $error);
    }


    public function delete()
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }

    public static function getReason($formData, $sortby = '', $sorttype = '', $limitString = '', $countOnly = false)
    {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }

    }

    public function getDataByJson($jsonData)
    {
        $this->cid = (int)$jsonData['comompany_id'];
        $this->uid = (int)$jsonData['creator_id'];
        $this->id = (int)$jsonData['id'];
        $this->name = (string)$jsonData['name'];
        $this->note = (string)$jsonData['note'];
        $this->status = (int)$jsonData['status'];
        $this->displayorder = (int)$jsonData['display_order'];
        $this->datecreated = (int)$jsonData['date_created'];
        $this->datemodified = (int)$jsonData['date_modified'];

    }
}
