<?php

namespace Teamcrop\Rest;

class CashflowType extends Base
{
    public static $serviceurl = '/v1/cashflowtypes';

    const DIRECTION_INCOME = 5;
    const DIRECTION_EXPENSE = 10;

    const STATUS_ENABLE = 1;
    const STATUS_DISABLE = 2;

    const IS_DELETE = 1;

    public $cid = 0;
    public $uid = 0;
    public $id = 0;
    public $direction = 0;
    public $name = '';
    public $description = '';
    public $icon = '';
    public $displayorder = 0;
    public $status = 0;
    public $parentid = 0;
    public $isdeleted = 0;
    public $isdeletedby = 0;
    public $datecreated = 0;
    public $datemodified = 0;

    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData(&$error = array())
    {
//        $data = array();
//
//        $this->id = self::doAdd($data, '', '', $error);

        return $this->id;
    }

    public function updateData(&$error = array())
    {
//        $data = array( );
//
//        $url = self::$serviceurl . '/' . $this->id;
//        return self::doUpdate($data, $url, '', $error);
    }

    public function delete(&$error = array())
    {
//        $url = self::$serviceurl . '/' . $this->id;
//        return self::doDelete($url, '', $error);
    }

    public static function getCashflowTypes(
        $formData,
        $sortby = '',
        $sorttype = '',
        $limitString = '',
        $countOnly = false
    )
    {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }
    }


    public function getDataByJson($row)
    {
        $this->cid = (int)$row['company_id'];
        $this->uid = (int)$row['creator_id'];
        $this->id = (int)$row['id'];
        $this->direction = (int)$row['direction'];
        $this->name = (string)$row['name'];
        $this->description = (string)$row['description'];
        $this->icon = (string)$row['icon'];
        $this->displayorder = (int)$row['display_order'];
        $this->status = (int)$row['status'];
        $this->parentid = (int)$row['parent_id'];
        $this->isdeleted = (int)$row['is_deleted'];
        $this->isdeletedby = (int)$row['is_deleted_by'];
        $this->datecreated = (int)$row['date_created'];
        $this->datemodified = (int)$row['date_modified'];
    }

    public function getJsonData()
    {
        $data = array(
            'creator_id' => (int)$this->uid,
            'company_id' => (int)$this->cid,
            'id' => (int)$this->id,
            'direction' => (int)$this->direction,
            'name' => (string)$this->name,
            'description' => (string)$this->description,
            'icon' => (string)$this->icon,
            'display_order' => (int)$this->displayorder,
            'status' => (int)$this->status,
            'parent_id' => (int)$this->parentid,
            'is_deleted' => (int)$this->isdeleted,
            'is_deleted_by' => (int)$this->isdeletedby,
            'date_created' => (int)$this->datecreated,
            'date_modified' => (int)$this->datemodified
        );

        return $data;
    }
}
