<?php

namespace Teamcrop\Rest;

class ProductUnit extends Base
{
    public static $serviceurl = '/v1/productunits';

    const STATUS_ENABLE = 1;
    const STATUS_DISABLE = 2;

    const IS_DELETE = 1;

    public $cid = 0;
    public $uid = 0;
    public $id = 0;
    public $name = '';
    public $image = '';
    public $resourceserver = 0;
    public $description = '';
    public $status = 0;
    public $isdeleted = 0;
    public $isdeletedby = 0;
    public $datecreated = 0;
    public $datemodified = 0;
    public $datedeleted = 0;

    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData()
    {

    }

    public function updateData()
    {

    }


    public function delete()
    {

    }

    public static function getProductUnits(
        $formData,
        $sortby = '',
        $sorttype = '',
        $limitString = '',
        $countOnly = false
    ) {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }
    }

    public function getDataByJson($jsonData)
    {
        $this->cid  = (int)$jsonData['company_id'];
        $this->uid  = (int)$jsonData['creator_id'];
        $this->id  = (int)$jsonData['id'];
        $this->name  = (string)$jsonData['name'];
        $this->description  = (string)$jsonData['description'];
        $this->status  = (int)$jsonData['status'];
        $this->isdeleted  = (int)$jsonData['is_deleted'];
        $this->isdeletedby  = (int)$jsonData['is_deletedby'];
        $this->datedeleted = (int)$jsonData['date_deleted'];
        $this->datecreated = (int)$jsonData['date_created'];

    }
}
