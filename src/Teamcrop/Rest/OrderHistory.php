<?php

namespace Teamcrop\Rest;

class OrderHistory extends Base
{
    public static $serviceurl = '/v1/orderhistorys';

    /*
    * moi tao : placeby
    * edit : status , status payment , method , address shipping , address billing , note
    * delete : item = detailid
    * from to : status , status payment , method
    * */

    const ACTION_RECEIVED = 5;
    const ACTION_EDIT_SHIPPINGADDRESS = 10;
    const ACTION_EDIT_BILLINGADDRESS = 15;
    const ACTION_EDIT_NOTE = 20;
    const ACTION_EDIT_STATUS = 25;
    const ACTION_EDIT_STATUSPAYMENT = 30;
    const ACTION_EDIT_STATUSMETHOD = 35;
    const ACTION_DELETE = 40;
    const ACTION_ADD = 45;
    const ACTION_EDIT_DETAIL = 50;
    const ACTION_EDIT_PRICETAX = 55;
    const ACTION_EDIT_PRICEDISCOUNT = 60;
    const ACTION_EDIT_PRICESHIPPING = 65;


    const ACTION_CREATED_PAYMENTMETHOD = 70;
    const ACTION_EDIT_PAYMENTMETHOD = 71;
    const ACTION_DELETE_PAYMENTMETHOD = 72;
    const ACTION_CREATED_SHIPPINGMETHOD = 75;
    const ACTION_EDIT_SHIPPINGMETHOD = 76;
    const ACTION_DELETE_SHIPPINGMETHOD = 77;
    const ACTION_CHANGE_DATECREATED = 79;



    const ACTION_RECEIPT_CREATED = 81;
    const ACTION_RECEIPT_REMOVE = 83;
    const ACTION_RECEIPT_CHANGESTATUS = 85;

    const ACTION_CASHFLOW_CREATED = 87;
    const ACTION_CASHFLOW_REMOVE = 89;
    const ACTION_CASHFLOW_CHANGESTATUS = 91;

    const ACTION_TAG_CREATED = 93;
    const ACTION_TAG_CHANGE = 95;

    const ACTION_TEAMORDER_ADD = 97;
    const ACTION_TEAMORDER_DELETE = 99;

    const SHOW_ENABLE = 1;
    const SHOW_DISABLE = 0;

    public $id = 0;
    public $soid = 0;
    public $uid = 0;
    public $fromstatus = 0;
    public $totatus = 0;
    public $action = 0;
    public $show = 1;
    public $datecreated = 0;
    public $datemodified = 0;
    public $item = '';
    public $ipaddress = 0;

    public function __construct($id = 0, $loadFromCache = false)
    {
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                //$this->copy(self::cacheGet($id));
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }
    /*
     *  $formData["ffromstatus"] : chuyen tu trang thai
        $formData["ftostatus"] : sang trang thai
        $formData["fuid"] : user cap nhat
        $formData["foid"] : ma don hang
        $formData["faction"] : hanh dong (constant)
        $formData["fitem"] : item vua tao , vua cap nhat (tuy vao hanh dong)
     * */
    public function addData(&$error = array())
    {
        $data = array(
            'order_id' => $this->soid,
            'creator_id' => $this->uid,
            'company_id' => 0,
            'from_status' => $this->fromstatus,
            'to_status' => $this->totatus,
            'action' => $this->action,
            'show' => $this->show,
            'item' => $this->item,
        );

        $this->id = self::doAdd($data, '', '', $error);

        return $this->id;
    }


    public function updateData()
    {

    }


    public function delete()
    {

    }

    public static function getHistorys($formData, $sortby = '', $sorttype = '', $limitString = '', $countOnly = false)
    {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }
    }
    public function getDataByJson($jsonData)
    {
        $this->soid = (int)$jsonData['order_id'];
        $this->id = (int)$jsonData['id'];
        $this->uid = (int)$jsonData['creator_id'];
        $this->fromstatus = (int)$jsonData['from_status'];
        $this->totatus = (int)$jsonData['to_status'];
        $this->action = (int)$jsonData['action'];
        $this->show = (int)$jsonData['show'];
        $this->item = (int)$jsonData['item'];
        $this->datecreated = (int)$jsonData['date_created'];
        $this->datemodified = (int)$jsonData['date_modified'];
        $this->ipaddress = (string)$jsonData['ip_address'];

    }
}
