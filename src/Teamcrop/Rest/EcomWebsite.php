<?php

namespace Teamcrop\Rest;

class EcomWebsite extends Base
{
    public static $serviceurl = '/v1/ecomwebsites';

    const PRODUCT_LIST_TYPE_ALL = 1;
    const PRODUCT_LIST_TYPE_NONE = 3;
    const PRODUCT_LIST_TYPE_COLLECTION = 5;

    const PLAN_STARTER = 1;
    const PLAN_BUSINESS = 3;
    const PLAN_ENTERPRISE = 5;

    const STATUS_ENABLE = 1;
    const STATUS_DISABLE = 2;

    public $cid = 0;
    public $uid = 0;
    public $id = 0;
    public $name = '';
    public $summary = '';
    public $subdomain = '';
    public $domain = 0;
    public $piwiksiteid = 0;
    public $settings = array();
    public $eshopsettings = array();
    public $filterattributes = array();
    public $filterpriceranges = array();
    public $productids = '';
    public $variantids = '';
    public $defaultLanguage = '';
    public $productlisttype = 0;
    public $plan = 0;
    public $status = 0;
    public $ipaddress = 0;
    public $datecreated = 0;
    public $datemodified = 0;
    public $datelastsynced = 0;

    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData(&$error = array())
    {
        $data = array(
            "company_id" => $this->cid,
            "creator_id" => $this->uid,
            'name' => $this->name,
            'summary' => $this->summary,
            'subdomain' => $this->subdomain,
            'domain' => $this->domain,
            'status' => $this->status
        );

        $this->id = self::doAdd($data, '', '', $error);

        return $this->id;
    }

    public function updateData(&$error = array())
    {
        $data = array(
            'company_id' => $this->cid,
            'creator_id' => $this->uid,
            'name' => $this->name,
            'summary' => $this->summary,
            'product_ids' => $this->productids,
            'variant_ids' => $this->variantids,
            'subdomain' => $this->subdomain,
            'domain' => $this->domain,
            'status' => $this->status
        );

        $url = self::$serviceurl . '/' . $this->id;
        return self::doUpdate($data, $url, '', $error);
    }


    public function delete(&$error = array())
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }

    public static function getEcomWebsites($formData, $sortby = '', $sorttype = '', $limitString = '', $countOnly = false)
    {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }

    }


    public function getDataByJson($jsonData)
    {
        $this->id = (int)$jsonData['id'];
        $this->cid = (int)$jsonData['company_id'];
        $this->uid = (int)$jsonData['creator_id'];
        $this->name = (string)$jsonData['name'];
        $this->summary = (string)$jsonData['summary'];
        $this->subdomain = (string)$jsonData['subdomain'];
        $this->domain = (string)$jsonData['domain'];
        $this->piwiksiteid = (int)$jsonData['piwik_site_id'];
        $this->settings = $jsonData['settings'];
        $this->eshopsettings = $jsonData['eshop_settings'];
        $this->filterattributes = $jsonData['filter_attributes'];
        $this->filterpriceranges = $jsonData['filter_price_ranges'];
        $this->productids = (string)$jsonData['product_ids'];
        $this->variantids = (string)$jsonData['variant_ids'];
        $this->defaultLanguage = (string)$jsonData['default_language'];
        $this->productlisttype = (int)$jsonData['product_list_type'];
        $this->plan = (int)$jsonData['plan'];
        $this->status = (int)$jsonData['status'];
        $this->ipaddress = (string)$jsonData['ip_address'];
        $this->datecreated= (int)$jsonData['date_created'];
        $this->datemodified = (int)$jsonData['date_modified'];
        $this->datelastsynced = (int)$jsonData['date_last_synced'];
    }

    /**
     * Search website by domain
     *
     * @param $domain
     * @return EcomWebsite
     */
    public static function getByDomain($domain)
    {
        $obj = new self();

        if ($domain != '') {
            //Todo:search in cache first
            //..

            $websites = self::getEcomWebsites(array('domain' => $domain), '', '', 1);
            if (!empty($websites)) {
                $obj = $websites[0];
            }
        }

        return $obj;
    }

    /**
     * Search website by subdomain
     *
     * @param $subdomain
     * @return EcomWebsite
     */
    public static function getBySubDomain($subdomain)
    {
        $obj = new self();

        if ($subdomain != '') {
            //Todo:search in cache first
            //..

            $websites = self::getEcomWebsites(array('subdomain' => $subdomain), '', '', 1);
            if (!empty($websites)) {
                $obj = $websites[0];
            }
        }

        return $obj;
    }
}
