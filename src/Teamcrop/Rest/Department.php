<?php

namespace Teamcrop\Rest;

class Department extends Base
{
    public static $serviceurl = '/v1/departments';

    const STATUS_ENABLE = 1;
    const STATUS_DISABLED = 3;

    public $uid = 0;
    public $cid = 0;
    public $id = 0;
    public $name = '';
    public $description = '';
    public $parentid = 0;
    public $parentpath = '';
    public $adminlist = '';
    public $countemployee = 0;
    public $status = 0;
    public $isofficial = 0;
    public $isdeleted = 0;
    public $displayorder = 0;
    public $datecreated = 0;
    public $datemodified = 0;
    public $datedeleted = 0;
    public $level = 0;
    public $parent = 0;
    public $title = '';

    public function __construct($id = 0, $loadFromCache = false, $asyncName = '')
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id, $loadFromCache);
            } else {
                $this->getData($id, $loadFromCache);
            }
        }
    }

    public function addData()
    {

    }

    public function updateData()
    {

    }


    public function delete()
    {

    }

    /**
     * @param $formData
     * @param string $sortby
     * @param string $sorttype
     * @param string $limitString
     * @param bool|false $countOnly
     * @return array
     */
    public static function getDepartments(
        $formData,
        $sortby = '',
        $sorttype = '',
        $limitString = '',
        $countOnly = false)
    {
        if ($countOnly) {
            return self::countItems($formData, $limitString);
        } else {
            return self::getItems($formData, $sortby, $sorttype, $limitString);
        }
    }

    public function getJsonData($getfullinfo = false)
    {
        $data = array(
            'id' => (int)$this->id,
            'name' => (string)$this->name,
            'description' => (string)$this->description,
        );

        if ($getfullinfo) {
            $data = array_merge($data, array(
                'creator_id' => (int)$this->uid,
                'company_id' => (int)$this->cid,
                'parent_id' => (string)$this->parentid,
                'parent_path' => (string)$this->parentpath,
                'admin_list' => (string)$this->adminlist,
                'count_employee' => (string)$this->countemployee,
                'status' => (string)$this->status,
                'is_official' => (string)$this->isofficial,
                'is_deleted' => (int)$this->datecreated,
                'display_order' => (int)$this->displayorder,
                'level' => (int)$this->level,
                'parent' => (int)$this->parent,
                'title' => (string)$this->title,
            ));
        }

        return $data;
    }

    public static function cacheKey($departmentid)
    {
        return 'tc_department_' . $departmentid;
    }

    public function getDataByArrayCache($row)
    {
        $this->id = isset($row['cd_id']) ? (string)$row['cd_id'] : "";
        $this->creator_id = isset($row['u_id']) ? (int)$row['u_id'] : 0;
        $this->company_id = isset($row['c_id']) ? (int)$row['c_id'] : 0;
        $this->name = isset($row['cd_name']) ? (string)$row['cd_name'] : "";
        $this->description = isset($row['cd_description']) ? (string)$row['cd_description'] : "";
        $this->parent_id = isset($row['cd_parentid']) ? (int)$row['cd_parentid'] : 0;
        $this->parentpath = isset($row['cd_parentpath']) ? (string)$row['cd_parentpath'] : "";
        $this->adminlist = isset($row['cd_adminlist']) ? (string)$row['cd_adminlist'] : "";
        $this->countemployee = isset($row['cd_countemployee']) ? (int)$row['cd_countemployee'] : 0;
        $this->status = isset($row['cd_status']) ? (string)$row['cd_status'] : 0;
        $this->isofficial  = isset($row['cd_isofficial']) ? (int)$row['cd_isofficial'] : 0;
        $this->isdeleted = isset($row['cd_isdeleted']) ? (int)$row['cd_isdeleted'] : 0;
        $this->display_order = isset($row['cd_displayorder']) ? (int)$row['cd_displayorder'] : 0;
        $this->level = isset($row['cd_level']) ? (int)$row['cd_level'] : 0;
        $this->parent = isset($row['cd_parent']) ? (int)$row['cd_parent'] : 0;
        $this->title = isset($row['cd_title']) ? (string)$row['cd_title'] : "";
        $this->datecreated = isset($row['cd_datecreated']) ? (int)$row['cd_datecreated'] : 0;
        $this->datemodified = isset($row['cd_datemodified']) ? (int)$row['cd_datemodified'] : 0;
        $this->datedeleted = isset($row['cd_datedeleted']) ? (int)$row['cd_datedeleted'] : 0;
    }
}
