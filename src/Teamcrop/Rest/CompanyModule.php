<?php

namespace Teamcrop\Rest;

class CompanyModule extends Base
{
    public static $serviceurl = '/v1/companymodules';


    public function __construct($id = 0, $loadFromCache = false)
    {
        //Init parent info
        parent::__construct();
    }
    public function updateData(&$error = array())
    {
    }

    public function delete(&$error = array())
    {
    }
    public function addData(&$error = array())
    {
    }

    public static function togglePriceModule($company_id, $group, $status, &$error = array())
    {
        $data = array(
            'company_id' => (int)$company_id,
            'group' => (string)$group,
            'status' => (string)$status,
        );

        return self::doAdd($data, self::$serviceurl . "/tooglepricegroup", '', $error);
    }


}
