<?php

namespace Teamcrop\Rest;

class Office extends Base
{
    public static $serviceurl = '/v1/offices';

    const STATUS_ENABLE = 1;
    const STATUS_DISABLED = 3;

    public $cid = 0;
    public $uid = 0;
    public $id = 0;
    public $name = '';
    public $filepath = '';
    public $description = '';
    public $address = '';
    public $region = 0;
    public $country = '';
    public $adminlist = '';
    public $workdaystart = '';
    public $workdayend = '';
    public $workdayallowip = '';
    public $workdayallowlatlng = '';
    public $status = 0;
    public $displayorder = 0;
    public $ipaddress = 0;
    public $datecreated = 0;
    public $datemodified = 0;

    public function __construct($id = 0, $loadFromCache = false, $asyncName = '')
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id, $loadFromCache);
            } else {
                $this->getData($id, $loadFromCache);
            }
        }
    }

    public function addData(&$error = array())
    {
        $data = array(
            'creator_id' => $this->uid,
            'company_id' => $this->cid,
            'name' => $this->name,
            'file_path' => $this->filepath,
            'description' => $this->description,
            'address' => $this->address,
            'region_id' => $this->region,
            'country' => $this->country,
            'admin_list' => $this->adminlist,
            'work_day_start' => $this->workdaystart,
            'work_day_end' => $this->workdayend,
            'work_day_allow_lat_lng' => $this->workdayallowlatlng
        );

        $this->id = self::doAdd($data, '', '', $error);

        return $this->id;
    }

    public function updateData(&$error = array())
    {
        $data = array(
            'creator_id' => $this->uid,
            'company_id' => $this->cid,
            'name' => $this->name,
            'file_path' => $this->filepath,
            'description' => $this->description,
            'address' => $this->address,
            'region_id' => $this->region,
            'country' => $this->country,
            'admin_list' => $this->adminlist,
            'work_day_start' => $this->workdaystart,
            'work_day_end' => $this->workdayend,
            'work_day_allow_lat_lng' => $this->workdayallowlatlng
        );

        $url = self::$serviceurl . '/' . $this->id;
        return self::doUpdate($data, $url, '', $error);
    }

    public function delete(&$error = array())
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }

    public static function getOffices(
        $formData,
        $sortby = '',
        $sorttype = '',
        $limitString = '',
        $countOnly = false,
        &$requestSuccess = null
    ) {
		if ($countOnly) {
            return self::countItems($formData, $limitString, '', $requestSuccess);
        }
		else {
        	return self::getItems($formData, $sortby, $sorttype, $limitString, '', $requestSuccess);
		}
    }

    public function checkValidIp($ipaddress)
    {

    }

    public function isEmployed($cid, $uid)
    {

    }

    public function getJsonData($getfullinfo = false)
    {
        $data = array(
            'id' => (int)$this->id,
            'name' => (string)$this->name,
            'description' => (string)$this->description,
            'address' => (string)$this->address
        );

        if ($getfullinfo) {
            $data = array_merge($data, array(
                "creator_id" => (int)$this->uid,
                "company_id" => (int)$this->cid,
                "file_path" => (string)$this->filepath,
                "region_id" => (int)$this->region,
                "country" => (string)$this->country,
                "admin_list" => (string)$this->adminlist,
                "work_day_start" => (string)$this->workdaystart,
                "work_day_end" => (string)$this->workdayend,
                "work_day_allow_ip" => (string)$this->workdayallowip,
                "work_day_allow_lat_lng" => (string)$this->workdayallowlatlng,
                "status" => (int)$this->status,
                "ip_address" => (string)$this->ipaddress,
                "date_created" => (int)$this->datecreated,
                "date_modified" => (int)$this->datemodified
            ));
        }

        return $data;
    }

    public function getDataByJson($jsonData)
    {
        $this->cid = (int)$jsonData['company_id'];
        $this->uid = (int)$jsonData['creator_id'];
        $this->id = (int)$jsonData['id'];
        $this->name = (string)$jsonData['name'];
        $this->filepath = (string)$jsonData['file_path'];
        $this->description = (string)$jsonData['description'];
        $this->address = (string)$jsonData['address'];
        $this->region = (int)$jsonData['region_id'];
        $this->country = (string)$jsonData['country'];
        $this->adminlist = (string)$jsonData['admin_list'];
        $this->workdaystart = (string)$jsonData['work_day_start'];
        $this->workdayend = (string)$jsonData['work_day_end'];
        $this->workdayallowip = (string)$jsonData['work_day_allow_ip'];
        $this->workdayallowlatlng = (string)$jsonData['work_day_allow_lat_lng'];
        $this->status = (int)$jsonData['status'];
        $this->displayorder = (int)$jsonData['display_order'];
        $this->ipaddress = (string)long2ip($jsonData['ip_address']);
        $this->datecreated = (int)$jsonData['date_created'];
        $this->datemodified = (int)$jsonData['date_modified'];
    }

    public static function cacheKey($departmentid)
    {
        return 'tc_office_' . $departmentid;
    }
}
