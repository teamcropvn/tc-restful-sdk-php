<?php

namespace Teamcrop\Rest;

class Office extends Base
{
    public static $serviceurl = '/v1/offices';

    const STATUS_ENABLE = 1;
    const STATUS_DISABLED = 3;

    public $cid = 0;
    public $uid = 0;
    public $id = 0;
    public $name = '';
    public $filepath = '';
    public $description = '';
    public $address = '';
    public $region = 0;
    public $country = '';
    public $adminlist = '';
    public $workdaystart = '';
    public $workdayend = '';
    public $workdayallowip = '';
    public $workdayallowlatlng = '';
    public $status = 0;
    public $displayorder = 0;
    public $ipaddress = 0;
    public $datecreated = 0;
    public $datemodified = 0;

    public function __construct($id = 0, $loadFromCache = false, $asyncName = '')
    {
        //Init parent info
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                $this->getData($id, $loadFromCache);
            } else {
                $this->getData($id, $loadFromCache);
            }
        }
    }

    public function addData()
    {

    }

    public function updateData()
    {

    }


    public function delete()
    {

    }

    public static function getOffices($formData, $sortby = '', $sorttype = '', $limitString = '', $countOnly = false)
    {
		if($countOnly) {
            return self::countItems($formData, $limitString);
        }
		else {
        	return self::getItems($formData, $sortby, $sorttype, $limitString);
		}
    }

    public function checkValidIp($ipaddress)
    {

    }

    public function isEmployed($cid, $uid)
    {

    }

    public function getJsonData($getfullinfo = false)
    {
        $data = array(
            'id' => (int)$this->id,
            'name' => (string)$this->name,
            'description' => (string)$this->description,
            'address' => (string)$this->address
        );

        if ($getfullinfo) {
            $data = array_merge($data, array(
                "creator_id" => (int)$this->uid,
                "company_id" => (int)$this->cid,
                "file_path" => (string)$this->filepath,
                "region_id" => (int)$this->region,
                "country" => (string)$this->country,
                "admin_list" => (string)$this->adminlist,
                "work_day_start" => (string)$this->workdaystart,
                "work_day_end" => (string)$this->workdayend,
                "work_day_allow_ip" => (string)$this->workdayallowip,
                "work_day_allow_lat_lng" => (string)$this->workdayallowlatlng,
                "status" => (int)$this->status,
                "ip_address" => (string)$this->ipaddress,
                "date_created" => (int)$this->datecreated,
                "date_modified" => (int)$this->datemodified
            ));
        }

        return $data;
    }

    public static function cacheKey($departmentid)
    {
        return 'tc_department_' . $departmentid;
    }

    public function getDataByArrayCache($row)
    {
        $this->cid = isset($row['c_id']) ? (int)$row['c_id'] : 0;
        $this->uid = isset($row['u_id']) ? (int)$row['u_id'] : 0;
        $this->id = isset($row['co_id']) ? (int)$row['co_id'] : 0;
        $this->name = isset($row['co_name']) ? (string)$row['co_name'] : "";
        $this->filepath = isset($row['co_filepath']) ? (string)$row['co_filepath'] : "";
        $this->description = isset($row['co_description']) ? (int)$row['co_description'] : "";
        $this->address = isset($row['co_address']) ? (string)$row['co_address'] : "";
        $this->region = isset($row['co_region']) ? (int)$row['co_region'] : 0;
        $this->country = isset($row['co_country']) ? (string)$row['co_country'] : "";
        $this->adminlist = isset($row['co_adminlist']) ? (string)$row['co_adminlist'] : "";
        $this->workdaystart = isset($row['co_workdaystart']) ? (string)$row['co_workdaystart'] : "";
        $this->workdayend = isset($row['co_workdayend']) ? (string)$row['co_workdayend'] : "";
        $this->workdayallowip = isset($row['co_workdayallowip']) ? (string)$row['co_workdayallowip'] : "";
        $this->workdayallowlatlng = isset($row['co_workdayallowlatlng']) ? (string)$row['co_workdayallowlatlng'] : "";
        $this->status = isset($row['co_status']) ? (int)$row['co_status'] : 0;
        $this->displayorder = isset($row['co_displayorder']) ? (int)$row['co_displayorder'] : 0;
        $this->ipaddress = isset($row['co_ipaddress']) ? (string)$row['co_ipaddress'] : "";
        $this->datecreated = isset($row['co_datecreated']) ? (int)$row['co_datecreated'] : 0;
        $this->datemodified = isset($row['co_datemodified']) ? (int)$row['co_datemodified'] : 0;
    }
}
