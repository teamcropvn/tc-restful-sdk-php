<?php

namespace Teamcrop\Rest;

class SubscriptionSchedule extends Base
{
    public static $serviceurl = '/v1/subscriptionschedules';


    public $cid = 0;
    public $uid = 0;
    public $ccid = 0;
    public $ssid = 0;
    public $sspid = 0;
    public $id = 0;
    public $totalorder = 0;
    public $cartstore = 0;
    public $carttag = '';
    public $cartorigin = 0;
    public $cartshippingfullname = "";
    public $cartshippingregionid = 0;
    public $cartshippingsubregionid = 0;
    public $quantitytype = 0;
    public $ruletype = 0;
    public $frequency = "";
    public $hour = 0;
    public $dayofweek = 0;
    public $dateofmonth = 0;
    public $datestart = 0;
    public $dateend = 0;
    public $datecreateorder = 0;
    public $datecreated = 0;
    public $datemodified = 0;

    public function __construct($id = 0, $loadFromCache = false)
    {
        parent::__construct();

        if ($id > 0) {
            if ($loadFromCache) {
                //$this->copy(self::cacheGet($id));
                $this->getData($id);
            } else {
                $this->getData($id);
            }
        }
    }

    public function addData(&$error = array())
    {
        $row['company_id']  = (int)$this->cid;
        $row['creator_id']  = (int)$this->uid;
        $row['customer_id']  = (int)$this->ccid;
        $row['subscription_id']  = (int)$this->ssid;
        $row['subscription_plan_id']  = (int)$this->sspid;
        $row['total_order']  = (int)$this->totalorder;
        $row['cart_store']  = (int)$this->cartstore;
        $row['cart_tag']  = (string)$this->carttag;
        $row['cart_origin']  = (int)$this->cartorigin;
        $row['cart_shipping_fullname']  = (string)$this->cartshippingfullname;
        $row['cart_shipping_region_id']  = (int)$this->cartshippingregionid;
        $row['cart_shipping_sub_region_id']  = (int)$this->cartshippingsubregionid;
        $row['quantity_type']  = (int)$this->quantitytype;
        $row['rule_type']  = (int)$this->ruletype;
        $row['frequency']  = (string)$this->frequency;
        $row['hour']  = (int)$this->hour;
        $row['day_of_week']  = (int)$this->dayofweek;
        $row['date_of_month']  = (int)$this->dateofmonth;
        $row['date_start']  = (int)$this->datestart;
        $row['date_end']  = (int)$this->dateend;
        $row['date_create_order']  = (int)$this->datecreateorder;
        $row['date_created']  = (int)$this->datecreated;
        $row['date_modified']  = (int)$this->datemodified;

        $this->id = self::doAdd($row, '', '', $error);

        return $this->id;
    }

    public function updateData(&$error = array())
    {
        $row['company_id']  = (int)$this->cid;
        $row['creator_id']  = (int)$this->uid;
        $row['customer_id']  = (int)$this->ccid;
        $row['subscription_id']  = (int)$this->ssid;
        $row['subscription_plan_id']  = (int)$this->sspid;
        $row['total_order']  = (int)$this->totalorder;
        $row['cart_store']  = (int)$this->cartstore;
        $row['cart_tag']  = (string)$this->carttag;
        $row['cart_origin']  = (int)$this->cartorigin;
        $row['cart_shipping_fullname']  = (string)$this->cartshippingfullname;
        $row['cart_shipping_region_id']  = (int)$this->cartshippingregionid;
        $row['cart_shipping_sub_region_id']  = (int)$this->cartshippingsubregionid;
        $row['quantity_type']  = (int)$this->quantitytype;
        $row['rule_type']  = (int)$this->ruletype;
        $row['frequency']  = (string)$this->frequency;
        $row['hour']  = (int)$this->hour;
        $row['day_of_week']  = (int)$this->dayofweek;
        $row['date_of_month']  = (int)$this->dateofmonth;
        $row['date_start']  = (int)$this->datestart;
        $row['date_end']  = (int)$this->dateend;
        $row['date_create_order']  = (int)$this->datecreateorder;
        $row['date_created']  = (int)$this->datecreated;
        $row['date_modified']  = (int)$this->datemodified;

        $url = self::$serviceurl . '/' . $this->id;
        return self::doUpdate($row, $url, '', $error);
    }

    public static function updateschedule($ssid)
    {
        $row["subscription_id"] = $ssid;
        $url = self::$serviceurl . '/updateschedule';
        return self::doAdd($row, $url, '', $error);
    }

    public function delete(&$error = array())
    {
        $url = self::$serviceurl . '/' . $this->id;
        return self::doDelete($url, '', $error);
    }


    public function getJsonData()
    {
        $row['company_id']  = (int)$this->cid;
        $row['creator_id']  = (int)$this->uid;
        $row['customer_id']  = (int)$this->ccid;
        $row['subscription_id']  = (int)$this->ssid;
        $row['subscription_plan_id']  = (int)$this->sspid;
        $row['id']  = (int)$this->id;
        $row['total_order']  = (int)$this->totalorder;
        $row['cart_store']  = (int)$this->cartstore;
        $row['cart_tag']  = (string)$this->carttag;
        $row['cart_origin']  = (int)$this->cartorigin;
        $row['cart_shipping_fullname']  = (string)$this->cartshippingfullname;
        $row['cart_shipping_region_id']  = (int)$this->cartshippingregionid;
        $row['cart_shipping_sub_region_id']  = (int)$this->cartshippingsubregionid;
        $row['quantity_type']  = (int)$this->quantitytype;
        $row['rule_type']  = (int)$this->ruletype;
        $row['frequency']  = (string)$this->frequency;
        $row['hour']  = (int)$this->hour;
        $row['day_of_week']  = (int)$this->dayofweek;
        $row['date_of_month']  = (int)$this->dateofmonth;
        $row['date_start']  = (int)$this->datestart;
        $row['date_end']  = (int)$this->dateend;
        $row['date_create_order']  = (int)$this->datecreateorder;
        $row['date_created']  = (int)$this->datecreated;
        $row['date_modified']  = (int)$this->datemodified;

        return $row;
    }

    public function getDataByJson($row)
    {
        $this->cid   = (int)$row['company_id'];
        $this->uid   = (int)$row['creator_id'];
        $this->ccid   = (int)$row['customer_id'];
        $this->ssid   = (int)$row['subscription_id'];
        $this->sspid   = (int)$row['subscription_plan_id'];
        $this->id   = (int)$row['id'];
        $this->totalorder   = (int)$row['total_order'];
        $this->cartstore   = (int)$row['cart_store'];
        $this->carttag   = (string)$row['cart_tag'];
        $this->cartorigin   = (int)$row['cart_origin'];
        $this->cartshippingfullname   = (string)$row['cart_shipping_fullname'];
        $this->cartshippingregionid   = (int)$row['cart_shipping_region_id'];
        $this->cartshippingsubregionid   = (int)$row['cart_shipping_sub_region_id'];
        $this->quantitytype   = (int)$row['quantity_type'];
        $this->ruletype   = (int)$row['rule_type'];
        $this->frequency   = (string)$row['frequency'];
        $this->hour   = (int)$row['hour'];
        $this->dayofweek   = (int)$row['day_of_week'];
        $this->dateofmonth   = (int)$row['date_of_month'];
        $this->datestart   = (int)$row['date_start'];
        $this->dateend   = (int)$row['date_end'];
        $this->datecreateorder   = (int)$row['date_create_order'];
        $this->datecreated   = (int)$row['date_created'];
        $this->datemodified   = (int)$row['date_modified'];
    }


}
