<?php

namespace Teamcrop\Utility;

/**
 * CompanyModule Class
 *
 * File contains the class used for CompanyModule Model
 *
 * @category Litpi
 * @package Model
 * @author Vo Duy Tuan <tuanmaster2012@gmail.com>
 * @copyright Copyright (c) 2013 - Litpi Framework (http://www.litpi.com)
 */
class CompanyModule
{
    const MODULE_GROUP_TEAMWORK = 1;
    const MODULE_GROUP_HRM = 3;
    const MODULE_GROUP_SALE = 5;
    const MODULE_GROUP_CRM = 7;
    const MODULE_GROUP_INVENTORY = 9;
    const MODULE_GROUP_ACCOUNTING = 11;
    const MODULE_GROUP_ECOMMERCE = 13;

    const PRICING_GROUP_EMPLOYEE = 101;
    const PRICING_GROUP_CHECKIN = 103;
    const PRICING_GROUP_EDUCATION = 105;
    const PRICING_GROUP_EMAIL = 109;

    const PRICING_GROUP_ORDER = 201;
    const PRICING_GROUP_INVENTORY = 203;
    const PRICING_GROUP_ACCOUNTING = 205;
//    const PRICING_GROUP_DMS = 207;
    const PRICING_GROUP_PRODUCT_ADVANCED = 209;
    const PRICING_GROUP_ESHOP = 213;
    const PRICING_GROUP_CHAT_CENTER = 215;
//    const PRICING_GROUP_CONTACT_CENTER = 217;
    const PRICING_GROUP_SERIAL_NUMBER = 219;
    const PRICING_GROUP_WARRANTY = 221;

    const PRICING_MULTIPLY_STORE = 1;
    const PRICING_MULTIPLY_OFFICE = 3;
    const PRICING_MULTIPLY_WAREHOUSE = 5;
    const PRICING_MULTIPLY_SHIPPER = 7;

    public static function moduleMapping()
    {
        //item in format
        //array(group-name-code, item-name-code, type(0-normal, 1-beta, 2-vip), pricinggroup)
        $mapping = array(
            11 => array('teamwork', 'mail', 0, self::PRICING_GROUP_EMAIL),
            12 => array('teamwork', 'chat', 1, self::PRICING_GROUP_CHAT_CENTER),
            13 => array('teamwork', 'project', 0, self::PRICING_GROUP_EMAIL),
            14 => array('teamwork', 'file', 0, self::PRICING_GROUP_EMAIL),
            15 => array('teamwork', 'blog', 0, self::PRICING_GROUP_EMAIL),


            21 => array('hrm', 'salary', 0, self::PRICING_GROUP_EMPLOYEE),
            22 => array('hrm', 'checkin', 0, self::PRICING_GROUP_CHECKIN),
            23 => array('hrm', 'employeepoint', 0, self::PRICING_GROUP_EMPLOYEE),
            24 => array('hrm', 'storechecklist', 0, self::PRICING_GROUP_EMPLOYEE),
            25 => array('hrm', 'leave', 0, self::PRICING_GROUP_EMPLOYEE),
            26 => array('hrm', 'workscheduling', 0, self::PRICING_GROUP_CHECKIN),
            27 => array('hrm', 'elearning', 1, self::PRICING_GROUP_EDUCATION),

            31 => array('sale', 'order', 0, self::PRICING_GROUP_ORDER),
            32 => array('sale', 'pos', 0, self::PRICING_GROUP_ORDER),
            33 => array('sale', 'combo', 0, self::PRICING_GROUP_PRODUCT_ADVANCED),
            34 => array('sale', 'promotion', 0, self::PRICING_GROUP_ORDER),
            35 => array('sale', 'couponvoucher', 0, self::PRICING_GROUP_ORDER),
//            36 => array('sale', 'shipper', 0, self::PRICING_GROUP_DMS),
            37 => array('sale', 'subscription', 0, self::PRICING_GROUP_PRODUCT_ADVANCED),
            38 => array('sale', 'pipeline', 1, self::PRICING_GROUP_PRODUCT_ADVANCED),

            41 => array('crm', 'customer', 0, self::PRICING_GROUP_ORDER),
            42 => array('crm', 'newsletter', 0, self::PRICING_GROUP_ORDER),
            43 => array('crm', 'ticket', 0, self::PRICING_GROUP_ORDER),
            44 => array('crm', 'sms', 0, self::PRICING_GROUP_ORDER),
            45 => array('crm', 'livechat', 2, self::PRICING_GROUP_CHAT_CENTER),
            46 => array('crm', 'facebookinbox', 1, self::PRICING_GROUP_CHAT_CENTER),
            47 => array('crm', 'facebookcomment', 1, self::PRICING_GROUP_CHAT_CENTER),
            48 => array('crm', 'zaloshop', 1, self::PRICING_GROUP_CHAT_CENTER),

            49 => array('crm', 'customerpoint', 0, self::PRICING_GROUP_PRODUCT_ADVANCED),
            50 => array('crm', 'callcenter', 1, self::PRICING_GROUP_ORDER),

            51 => array('inventory', 'inventorytrack', 0, self::PRICING_GROUP_INVENTORY),
            52 => array('inventory', 'inventorycheck', 0, self::PRICING_GROUP_INVENTORY),
            53 => array('inventory', 'inventorytransfer', 0, self::PRICING_GROUP_INVENTORY),
            54 => array('inventory', 'inventoryalert', 0, self::PRICING_GROUP_INVENTORY),
            55 => array('inventory', 'purchaseorder', 0, self::PRICING_GROUP_INVENTORY),
            56 => array('inventory', 'productcost', 0, self::PRICING_GROUP_PRODUCT_ADVANCED),
            57 => array('inventory', 'serialnumber', 0, self::PRICING_GROUP_SERIAL_NUMBER),
            58 => array('inventory', 'warranty', 0, self::PRICING_GROUP_WARRANTY),

            61 => array('accounting', 'cashflow', 0, self::PRICING_GROUP_ACCOUNTING),

            72 => array('integration', 'ecomplatform', 0, self::PRICING_GROUP_ESHOP),
            73 => array('integration', 'shippingservice', 0, self::PRICING_GROUP_ORDER),

            81 => array('plugin', 'menulink', 0, self::PRICING_GROUP_ORDER),



//            91 => array('ecommerce', 'ecommerce', 2, self::PRICING_GROUP_ESHOP),
//            92 => array('ecommerce', 'landingpage', 2, self::PRICING_GROUP_ESHOP),


        );

        return $mapping;
    }

    public static function idbyPriceGroup($price_group)
    {
        $arr = array();
        $mapping = self::moduleMapping();
        foreach ($mapping as $id => $moduleInfo) {
            if ($moduleInfo[3] == $price_group) {
                $arr[] = $id;
            }
        }

        return $arr;
    }

    public static function textToId($moduleName)
    {
        $moduleId = 0;

        $mapping = self::moduleMapping();
        foreach ($mapping as $id => $moduleInfo) {
            if ($moduleInfo[1] == $moduleName) {
                $moduleId = $id;
                break;
            }
        }

        return $moduleId;
    }

    public static function grouptextToId($moduleGroupName)
    {
        $groupId = 0;

        switch ($moduleGroupName) {
            case 'teamwork':
                $groupId = self::MODULE_GROUP_TEAMWORK;
                break;
            case 'sale':
                $groupId = self::MODULE_GROUP_SALE;
                break;
            case 'crm':
                $groupId = self::MODULE_GROUP_CRM;
                break;
            case 'inventory':
                $groupId = self::MODULE_GROUP_INVENTORY;
                break;
            case 'hrm':
                $groupId = self::MODULE_GROUP_HRM;
                break;
            case 'accounting':
                $groupId = self::MODULE_GROUP_ACCOUNTING;
                break;
            case 'ecommerce':
                $groupId = self::MODULE_GROUP_ECOMMERCE;
                break;
        }

        return $groupId;
    }

    /**
     * Return the multiplier of pricing group
     *
     * @param $pricinggroup
     * @return int
     */
    public static function getMultiplyByPricingGroup($pricinggroup)
    {
        switch ($pricinggroup) {
            case self::PRICING_GROUP_EMPLOYEE:
            case self::PRICING_GROUP_CHAT_CENTER:
            case self::PRICING_GROUP_EDUCATION:
            case self::PRICING_GROUP_EMAIL:
                $multiplier = self::PRICING_MULTIPLY_OFFICE;
                break;
            case self::PRICING_GROUP_INVENTORY:
            case self::PRICING_GROUP_SERIAL_NUMBER:
                $multiplier = self::PRICING_MULTIPLY_WAREHOUSE;
                break;
            default:
                $multiplier = self::PRICING_MULTIPLY_STORE;
        }

        return $multiplier;
    }
}
