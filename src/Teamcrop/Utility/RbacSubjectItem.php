<?php

namespace Teamcrop\Utility;


/**
 * RbacSubjectItem Class
 *
 * File contains the class used for RbacSubject Model
 *
 * @category Litpi
 * @package Model
 * @author Vo Duy Tuan <tuanmaster2012@gmail.com>
 * @copyright Copyright (c) 2013 - Litpi Framework (http://www.litpi.com)
 */
class RbacSubjectItem
{
    const OBJECTTYPE_STORE = 1;
    const OBJECTTYPE_WAREHOUSE = 3;
    const OBJECTTYPE_PRODUCTCATEGORY = 5;
    const OBJECTTYPE_PAGECATEGORY = 9;

    public static function subjectMapping()
    {
        $mapping = array(

            //New Roles 2.0
            231 => array('erp.company.setting', '', 0),
            233 => array('erp.product.manage', '', self::OBJECTTYPE_PRODUCTCATEGORY),
            235 => array('erp.promotion.view', '', 0),
            236 => array('erp.promotion.manage', '', 0),
            237 => array('erp.order.view', '', self::OBJECTTYPE_STORE),
            238 => array('erp.order.add', '', self::OBJECTTYPE_STORE),
            240 => array('erp.order.editstatus', '', self::OBJECTTYPE_STORE),
            244 => array('erp.order.editinfo', '', self::OBJECTTYPE_STORE),
            245 => array('erp.order.return', '', self::OBJECTTYPE_STORE),
            269 => array('erp.order.cancelbeforeshipping', '', self::OBJECTTYPE_STORE),
            272 => array('erp.order.cancelfromshipping', '', self::OBJECTTYPE_STORE),
            273 => array('erp.order.complete', '', self::OBJECTTYPE_STORE),
            280 => array('erp.order.pos', '', self::OBJECTTYPE_STORE),
            246 => array('erp.customer.view', '', 0),
            247 => array('erp.customer.manage', '', 0),
            282 => array('erp.customer.privateviewall', '', 0),
            283 => array('erp.customer.privatetoggle', '', 0),
            248 => array('erp.cashflow.view', '', self::OBJECTTYPE_STORE),
            249 => array('erp.cashflow.add', '', self::OBJECTTYPE_STORE),
            250 => array('erp.cashflow.editstatus', '', self::OBJECTTYPE_STORE),
            251 => array('erp.cashflow.editinfo', '', self::OBJECTTYPE_STORE),
            270 => array('erp.cashflow.cancel', '', self::OBJECTTYPE_STORE),
            304 => array('erp.cashflow.summary', '', 0),
            252 => array('erp.report.view', '', self::OBJECTTYPE_STORE),
            253 => array('erp.report.explorer', '', 0),
            254 => array('erp.inventory.view', '', self::OBJECTTYPE_WAREHOUSE),
            255 => array('erp.inventory.add', '', self::OBJECTTYPE_WAREHOUSE),
            256 => array('erp.inventory.editstatus', '', self::OBJECTTYPE_WAREHOUSE),
            258 => array('erp.inventory.editinfo', '', self::OBJECTTYPE_WAREHOUSE),
            259 => array('erp.inventory.transfer', '', 0),
            260 => array('erp.inventory.check', '', self::OBJECTTYPE_WAREHOUSE),
            261 => array('erp.inventory.alert', '', self::OBJECTTYPE_WAREHOUSE),
            271 => array('erp.inventory.cancel', '', self::OBJECTTYPE_WAREHOUSE),
            302 => array('erp.inventory.report', '', 0),
            303 => array('erp.inventory.number', '', self::OBJECTTYPE_WAREHOUSE),
            262 => array('erp.employee.manage', '', 0),
            307 => array('erp.employee.add', '', 0),
            308 => array('erp.employee.detail', '', 0),
            263 => array('erp.employee.manageworktime', '', 0),
            264 => array('erp.employee.managepoint', '', 0),
            265 => array('erp.employee.managebonus', '', 0),
            266 => array('erp.project.manage', '', 0),
            268 => array('erp.file.manage', '', 0),
            274 => array('erp.purchaseorder.view', '', 0),
            275 => array('erp.purchaseorder.add', '', 0),
            276 => array('erp.purchaseorder.edit', '', 0),
            277 => array('erp.purchaseorder.approve', '', 0),
            278 => array('erp.purchaseorder.complete', '', 0),
            279 => array('erp.purchaseorder.cancel', '', 0),
            281 => array('erp.ticket.manage', '', 0),
            284 => array('erp.productcost.view', '', 0),
            285 => array('erp.productcost.edit', '', 0),
            286 => array('erp.telegram.edit', '', 0),
            287 => array('erp.workschedule.review', '', 0),
            306 => array('erp.workschedule.change', '', 0),
            288 => array('erp.subscription.view', '', 0),
            289 => array('erp.subscription.manage', '', 0),
            290 => array('erp.shipper.view', '', 0),
            291 => array('erp.shipper.manage', '', 0),
            295 => array('erp.shipper.managejob', '', 0),
            292 => array('erp.ecommerce.listing', '', 0),
            293 => array('erp.ecommerce.manage', '', 0),
            294 => array('erp.ecommerce.landingpage', '', 0),
            296 => array('erp.customerpoint.view', '', 0),
            297 => array('erp.customerpoint.manage', '', 0),
            298 => array('erp.customerpoint.managerule', '', 0),
            299 => array('erp.callcenter.manage', '', 0),
            300 => array('erp.callcenter.operator', '', 0),
            301 => array('erp.elearning.manage', '', 0),
            305 => array('erp.emailmarketing.manage', '', 0),
            309 => array('erp.ecomwebsite.view', '', 0),
            310 => array('erp.ecomwebsite.add', '', 0),
            311 => array('erp.ecomwebsite.edit', '', 0),
            312 => array('erp.landingpage.view', '', 0),
            313 => array('erp.landingpage.add', '', 0),
            314 => array('erp.landingpage.edit', '', 0),
            315 => array('erp.logactivity.view', '', 0),
            316 => array('erp.logactivity.edit', '', 0),
            317 => array('erp.pipeline.setting', '', 0),
            318 => array('erp.pipeline.deallisting', '', 0),
            319 => array('erp.pipeline.dealmanage', '', 0),
            320 => array('erp.blog.managepost', '', 0),
            321 => array('erp.blog.managecomment', '', 0),
        );

        return $mapping;
    }
}
