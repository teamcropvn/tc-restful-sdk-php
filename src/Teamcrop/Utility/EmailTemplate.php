<?php

namespace Teamcrop\Utility;

class EmailTemplate
{
    public static function getHeader()
    {
        $output = '
            <table width="100%" border="0" cellspacing="0" cellpadding="0" bgcolor="#22baa0" style="border:40px solid #22baa0;border-left-width:0; border-right-width:0;">
                <tr>
                    <td align="center"><img src="https://teamcrop.com/templates/default/images/mail/app/teamcrop-logo.png"/></td>
                </tr>
                <tr><td>&nbsp;</td></tr>
                <tr>
                    <td><table width="600" border="0" cellspacing="0" cellpadding="0" bgcolor="#FFFFFF" align="center">
                            <tr>
                            </tr>
                            <tr>
                                <td align="center">&nbsp;</td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td><table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family: Helvetica, Arial, sans-serif; color:#68696a; font-size:13px;">
                                        <tr>
                                            <td width="10%">&nbsp;</td>
                                            <td width="80%" align="left" valign="top">
        ';

        return $output;
    }

    public static function getFooter()
    {
        $output = '
            </td>
            <td width="10%">&nbsp;</td>
            </tr>

            </table></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td><img src="https://teamcrop.com/templates/default/images/mail/bottombar.jpg" width="598" height="2" style="display:block" border="0" alt=""/></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td align="center"><font style="font-family: Helvetica, Arial, sans-serif; color:#010203; font-size:10px;"><a href= "https://teamcrop.com" style="color:#010203; text-decoration:none"><strong>TeamCrop.com - Free Project Management &amp; Team Collaboration.</strong></a></font></td>

                        </tr>
                    </table></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>

            <tr>
                <td>&nbsp;</td>
            </tr>
            </table></td>
            </tr>
            </table>

        ';

        return $output;
    }

    public static function render($content, $header = null, $footer = null)
    {
        if (is_null($header)) {
            $header = self::getHeader();
        }

        if (is_null($footer)) {
            $footer = self::getFooter();
        }

        return $header . $content . $footer;
    }
}
